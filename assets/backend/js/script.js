/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function(){
    
    $(".file_select").each(function(){
            initFileSelect($(this));
            $(this).change(function(){
                initFileSelect($(this));
            });
        });
        
    
    $('[data-toggle="tooltip"]').tooltip()
    
    
    Datepicker.initDatepicker();
        
    $(document).on('click',".confirm_submit",function(){
		q=$(this).attr("alt");
		if(q==""){
			//alert("empty");
			return true;
		}
		else
		{
			if(confirm(q)){return true;}
			else {return false;}
		}

    });    
});

function setFileFromUpload(name,url){
    obj=$("#"+name);
    obj.val(url);
    initFileSelect(obj);
}


function initFileSelect(obj){

    obj.parent().find(".file-select-btns").remove();
    
    btn1=$("<a/>",{'class':'btn btn-xs btn-info','text':'Nahrať súbor'});
    btn2=$("<a/>",{'class':'btn btn-xs btn-danger','text':'Vymazať'});
    
    btn2.click(function(){
        $(this).parent().parent().find(".file_select").val("");
        initFileSelect($(this).parent().parent().find(".file_select"));
    });
    
    
    
    
    btn1.click(function(){
        obj=$(this).parent().parent().find(".file_select");
       
        crop="";
        if(obj.attr("data-crop"))
            crop="/crop/"+obj.attr("data-crop");
        type="image";
        if(obj.attr("data-type"))
            type=obj.attr("data-type");
       
        url=baseUrl+'/upload/'+type+'/name/'+obj.attr("id")+crop;
       
        if(obj.attr("data-filename"))
            url+='/filename/'+obj.attr("data-filename");
       
       
       w=window.open(url,"fileSelect","width=620,height=600,titlebar=no,status=no,location=no,top=20,left=100"); 
       w.focus();
    });
    
    
    obj.hide();
    data=$("<div/>",{'class':'file-select-btns'});
    data.append(btn1);
    
    if(obj.val()!=""){
        data.append(" ");
        data.append(btn2);
        img=$("<img/>",{src:obj.val()+"?"+Math.random(),'style':'max-width:400px'});
        data.append($("<p/>",{html:img,'style':'padding-top:10px;'}));
        
        
    }
    
    
    obj.parent().append(data);
}



function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    var path = "path=/";
    
    document.cookie = cname + "=" + cvalue + "; " + expires + "; " + path;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}