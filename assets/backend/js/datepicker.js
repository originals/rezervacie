var Datepicker = function () {

    return {
        
        //Datepickers
        initDatepicker: function () {

                $.datepicker.regional['sk'] = {
                        clearText: 'Zmazať', clearStatus: '',
                        closeText: 'Zavrieť', closeStatus: '',
                        prevText: '<i class="fa fa-angle-left"></i>',
                        nextText: '<i class="fa fa-angle-right"></i>',
                        prevBigText: '&#x3c;&#x3c;', prevBigStatus: '',
                        nextBigText: '&#x3e;&#x3e;', nextBigStatus: '',
                        currentText: 'Dnes', currentStatus: '',
                        monthNames: ['Január','Február','Marec','Apríl','Máj','Jún',
                        'Júl','August','September','Október','November','December'],
                        monthNamesShort: ['Jan','Feb','Mar','Apr','Máj','Jún',
                        'Júl','Aug','Sep','Okt','Nov','Dec'],
                        monthStatus: '', yearStatus: '',
                        weekHeader: 'Ty', weekStatus: '',
                        dayNames: ['Nedeľa','Pondelok','Utorok','Streda','Štvrtok','Piatok','Sobota'],
                        dayNamesShort: ['Ned','Pon','Uto','Str','Štv','Pia','Sob'],
                        dayNamesMin: ['Ne','Po','Ut','St','Št','Pia','So'],
                        dayStatus: 'DD', dateStatus: 'D, M d',
                        dateFormat: 'dd.mm.yy', firstDay: 1,
                        initStatus: '', isRTL: false,
                        changeMonth:true,
                        changeYear:true,
                        showButtonPanel: true,mandatory: true,
                        yearRange: '2000:2020'
                        //showAnim: 'slideDown'
								};
                $.datepicker.setDefaults($.datepicker.regional['sk']);


                // Regular datepicker
	        $('#date').datepicker({
	            dateFormat: 'yy-mm-dd'
	        });
	        
                // Date range
	        $('input#born').datepicker({
	            dateFormat: 'd.m.yy',
	            onSelect: function( selectedDate )
	            {
	                
	            }
	        });
                
	        // Date range
	        $('#start').datepicker({
	            dateFormat: 'dd.mm.yy',
	            onSelect: function( selectedDate )
	            {
	                $('#end').datepicker('option', 'minDate', selectedDate);
	            }
	        });
	        $('#end').datepicker({
	            dateFormat: 'dd.mm.yy',
	            onSelect: function( selectedDate )
	            {
	                $('#start').datepicker('option', 'maxDate', selectedDate);
	            }
	        });
	        
	        // Inline datepicker
	        $('#inline').datepicker({
	            dateFormat: 'dd.mm.yy',
	            prevText: '<i class="fa fa-angle-left"></i>',
	            nextText: '<i class="fa fa-angle-right"></i>'
	        });
	        
	        // Inline date range
	        $('#inline-start').datepicker({
	            dateFormat: 'dd.mm.yy',
	            prevText: '<i class="fa fa-angle-left"></i>',
	            nextText: '<i class="fa fa-angle-right"></i>',
	            onSelect: function( selectedDate )
	            {
	                $('#inline-finish').datepicker('option', 'minDate', selectedDate);
	            }
	        });
	        $('#inline-finish').datepicker({
	            dateFormat: 'dd.mm.yy',
	            prevText: '<i class="fa fa-angle-left"></i>',
	            nextText: '<i class="fa fa-angle-right"></i>',
	            onSelect: function( selectedDate )
	            {
	                $('#inline-start').datepicker('option', 'maxDate', selectedDate);
	            }
	        });
                
                
                
                $(".pickDate").datepicker({
                                showButtonPanel: true,
                                onClose: function( selectedDate ) {
                                    id=$(this).attr("id");
                                    if(id.match(/\-from/i))
                                        $( "#"+id.replace("-from","-to") ).datepicker( "option", "minDate", selectedDate );
                                    if(id.match(/\-to/i))
                                        $( "#"+id.replace("-to","-from") ).datepicker( "option", "maxDate", selectedDate );
                                },
                                beforeShow: function( input ) {
                                    
                                    var $inp=$(input);
                                    if($inp.attr("id").match(/\-from/i)){
                                        
                                        $inp2=$("#"+$inp.attr("id").replace("-from","-to"));
                                        
                                        setTimeout(function() {
                                            var buttonPane = $( input )
                                                .datepicker( "widget" )
                                                .find( ".ui-datepicker-buttonpane" );
                                            buttonPane.html("");

                                            btnClass="ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all";

                                            
                                            $( "<select>", {
                                                html: "<option>Obdobie</option><option value=today>Dnes</option><option value=yesterday>Včera</option><option value=week>Tento týždeň</option><option value=lastweek>Minulý týždeň</option><option value=month>Tento mesiac</option><option value=lastmonth>Minulý mesiac</option><option value=year>Tento rok</option>",
                                                class: btnClass,
                                                change: function() {
                                                    var date=new Date();
                                                    
                                                    if($(this).val()=="today"){
                                                        var firstDay = date;
                                                        var lastDay = date;
                                                    }
                                                    if($(this).val()=="yesterday"){
                                                        var firstDay = new Date(new Date().setDate(new Date().getDate()-1));
                                                        var lastDay = firstDay;
                                                    }
                                                    if($(this).val()=="week"){
                                                        var dates=startAndEndOfWeek();
                                                        var firstDay = dates[0];
                                                        var lastDay  = dates[1];
                                                    }
                                                    if($(this).val()=="lastweek"){
                                                        var dates=startAndEndOfWeek(new Date(new Date().setDate(new Date().getDate()-7)));
                                                        var firstDay = dates[0];
                                                        var lastDay  = dates[1];
                                                    }
                                                    
                                                    if($(this).val()=="month"){
                                                        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
                                                        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                                                    }
                                                    if($(this).val()=="lastmonth"){
                                                        var firstDay = new Date(date.getFullYear(), date.getMonth()-1, 1);
                                                        var lastDay = new Date(date.getFullYear(), date.getMonth(), 0);
                                                    }
                                                    if($(this).val()=="year"){
                                                        var firstDay = new Date(date.getFullYear(), 0, 1);
                                                        var lastDay = new Date(date.getFullYear()+1,0,0);
                                                    }
                                                    
                                                    $inp.datepicker("option","maxDate",null);
                                                    $inp2.datepicker("option","minDate",null);
                                                    $inp.datepicker("setDate",lastDay);
                                                    $inp2.val($inp.val());
                                                    $inp.datepicker("setDate",firstDay);
                                                    $inp.datepicker("hide");
                                                }
                                            }).appendTo( buttonPane );
                                            
                                            
                                            
                                        }, 1 );
                                    }
                                }
                        });
                
                
                $( ".dateselect-year" ).each(function(){
                        var dateSelector=$(this);

                        /*range=(dateSelector.children("option[value!='']:first").val())
                                 +":"
                                 +(dateSelector.children("option:last").val());
                         */
                           range=(dateSelector.children("option[value!='']:first").val())
                                 +":"
                                 +(dateSelector.children("option[value!='']:last").val());
                         
                        nid=$(this).attr("id").replace("-year","-datepicker");
                        input=$('<input/>',{id : nid,css:{display:"none"}});
                        input.insertAfter($(this));
                        input.datepicker({
                                showOn: "button",
                                yearRange: range,
                                /*buttonImage: baseUrl+"/assets/img/ic_calendar.gif",
                                buttonImageOnly: true,*/
                                beforeShow: function(input, inst) {
                                        instance = $( this ).data( "datepicker" );
                                        input=instance.input;
                                        var curDate=input.parent().children(".dateselect-day").val()
                                        +"."+input.parent().children(".dateselect-month").val()
                                        +"."+input.parent().children(".dateselect-year").val();
                                        input.val(curDate);
                                },
                                onSelect: function(dateText, inst) {
                                        instance = $( this ).data( "datepicker" );
                                        input=instance.input;
                                        input.parent().children(".dateselect-year").val(inst.selectedYear);
                                        input.parent().children(".dateselect-month").val(inst.selectedMonth+1);
                                        input.parent().children(".dateselect-day").val(inst.selectedDay);
                                }
                        });
                        
                        input.next("button").addClass("btn btn-primary fa fa-calendar").html("");

                });

                
                
        }

    };
}();