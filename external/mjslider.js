(function( $ ) {
	$.fn.mjslider = function( options ) 
	{
		// pole farieb, z ktorych sa bude nahodne vyberat 
		// a vyskovy rozdiel v elementoch
		var defaults = {
                            speed: 1000,
                            timeout: 2000
                },
                settings = $.extend({}, defaults, options);
		
		// tutu srandu vykoname na kazdy jeden element kolekcie [ each ]
		// na konci vratime celu kolekciu, aby dalej fungoval chaining  [ return ]
		return this.each(function()
		{
			
                        // rozbijeme element
			var slider = $(this);
                        
                        var length=(slider.children().length);
                        
                        if(length>1){
                            setTimeout(function(){ nextSlide(slider) },settings.timeout);
                        }
                        //slider.css('border',"5px solid red");
                        
                        
                        
		});
                
                function nextSlide(slider){
                    var active=slider.children(":visible");
                    var next=active.next();
                    
                    if(next.length==0)next=slider.children(":eq(0)");
                   
                    //return;
                    slider.css('position','relative')
                          .css('width',slider.width()+'px')
                          .css('height',slider.height()+'px');
                    
                    
                    active.css('position','absolute')
                          .css('top','0').css('left','0').css('z-index','100')
                          .css('width',next.width()+'px')
                          .css('height',next.height()+'px');
                    
                    
                    next.css('position','absolute')
                          .css('top','0').css('left','0').css('z-index','99')
                          .css('width',next.width()+'px')
                          .css('height',next.height()+'px');
                    
                    next.show();
                    
                    active.fadeOut(settings.speed,function(){
                        setTimeout(function(){ nextSlide(slider) },settings.timeout);
                        $(this)
                          .css('position','static')
                          .css('width','auto')
                          .css('height','auto');
                        next.css('position','static')
                          .css('width','auto')
                          .css('height','auto');
                        slider.css('width','auto')
                          .css('height','auto');
                    });
                    
                    
                    
                        
                    
                }
                
	}
})( jQuery );