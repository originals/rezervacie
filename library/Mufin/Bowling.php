<? 
class Mufin_Bowling
{
  private $rolls = array();
   
  public function roll($pins) {
    $this->rolls[] = $pins;
  }
   
  public function score() {
    $score = 0;
    $rollsMax = count($this->rolls);
    $firstInFrame = 0;
   
    for ($frame = 0; $frame < 10; $frame++) {
      if ($this->isStrike($firstInFrame)) {
        $score += 10 + $this->nextTwoBallsForStrike($firstInFrame);
        $firstInFrame++;
      } else if ($this->isSpare($firstInFrame)) {
          $score += 10 + $this->nextBallForSpare($firstInFrame);
          $firstInFrame += 2;
      } else {
        $score += $this->twoBallsInFrame($firstInFrame);
        $firstInFrame += 2;
      }
    }
   
    return $score;
  }
   
  public function nextTwoBallsForStrike($firstInFrame) {
    return $this->rolls[$firstInFrame + 1] + $this->rolls[$firstInFrame + 2];
  }
 
  public function nextBallForSpare($firstInFrame) {
    return $this->rolls[$firstInFrame + 2];
  }
   
  public function twoBallsInFrame($firstInFrame) {
    return $this->rolls[$firstInFrame] + $this->rolls[$firstInFrame + 1];
  }
 
   
  public function isStrike($firstInFrame) {
    return $this->rolls[$firstInFrame] == 10;
  }
 
  public function isSpare($firstInFrame) {
    return $this->rolls[$firstInFrame] + $this->rolls[$firstInFrame+1] == 10;
  }
 
  public function rollSpare() {
    $this->roll(5);
    $this->roll(5);
  }
  
  public function convertToBinary($string){
  	$array=explode("|",$string);
	foreach($array as $k=>$v){
		$arr[$v]="1";
	}
	for ($i = 1; $i <= 10; $i++) {
		$binarr[$i]="0";
		if(isset($arr[$i])){
			$binarr[$i]=$arr[$i];
		}
	}	
	$bin=implode("",$binarr);
	return $bin;
  }
  
  public function convertFromBinary($string){
       $lenght=strlen($string);       
       if($lenght<10){
           $d=10-$lenght;
       }else{
           $d=0;
       }
        $pins=array();
	for ($i=0; $i < 10; $i++) {
		if($i<$d){
                    $pins[$i]="0";
                }else{                    
                    $pins[$i]=$string[$i-$d];
                }		
	}	
	return $pins;
      
  }
}
 
 
