<?php

function url(array $urlOptions = array(), $name = null, $reset = false, $encode = true){
    return Matj_Get::url($urlOptions, $name, $reset, $encode);
}

function url_host($forceWWW=false){
        $s=Matj_Get::getRequest();
        $url="http://";
        if($s->getServer("HTTP_HTTPS") && empty($forceWWW))
                $url="https://";

        $url.=strip_tags(htmlspecialchars($s->getServer("HTTP_HOST")));
        return $url;//".
}

function base_url($server=false){
    return Matj_Get::getBaseUrl($server);
}

function translate($str,$params=array()){
    $translator=Matj_Get::getTranslator();
    
    if($translator)
        return $translator->_($str,$params);
    else
        return $str;
}

function msg(){
    return Matj_Get::getFlashMessenger();
}


function trim_all( $str , $what = NULL , $with = ' ' )
{
    if( $what === NULL )
    {
        //  Character      Decimal      Use
        //  "\0"            0           Null Character
        //  "\t"            9           Tab
        //  "\n"           10           New line
        //  "\x0B"         11           Vertical Tab
        //  "\r"           13           New Line in Mac
        //  " "            32           Space
       
        $what   = "\\x00-\\x20";    //all white-spaces and control chars
    }
   
    return trim( preg_replace( "/[".$what."]+/" , $with , $str ) , $what );
}

/**
 * return Matj_View_Head
 */
function head(){
    return Matj_View_Head::getInstance();
}