<?php
/**
 * 
 * options:
 * url
 * columnParams
 * urlParams
 * reset
 * resetSort
 * route
 * 
 * Enter description here ...
 * @author Matus
 *
 */
class Matj_SmartTable2_Plugin_Filter extends Matj_SmartTable2_Plugin_Abstract {
	
	protected $_element;
	protected $_elementType;
	protected $_filters = array('StripTags','StringTrim');
	protected $_defaultSearchType = "%LIKE%";
	
	
	function getFiltersForm(){
		return $this->column->getTable()->getFiltersForm();
	}
	
	function init(){
		if(empty($this->_elementType)){
			throw new Exception("Must set element type");
		}
		$e=$this->getElement($this->_elementType);
		$this->getFiltersForm()->addElement($e);
		$this->_element=$e;
	}
	
	
	function getElement($type,$name=null,$attribs=array()){
		$form=$this->getFiltersForm();
		if(!$name){
			if($this->getOption("name")){
				$name=$this->getOption("name");
			}
			else{
				$name=$this->column->getAttrib("Column");
			}
		}
		
		
		if(!key_exists("decorators",$attribs))$attribs['decorators']=array('ViewHelper',array('HtmlTag',array('tag'=>'span','class'=>'filter'.$this->getPluginName().' filter_column_'.$name)));
		if(!key_exists("filters",$attribs))$attribs['filters']=$this->_filters;
		
		
		return $form->createElement($type,$name,$attribs);
	}
	
	
	
	
	function joinWhere($where){
		$value=$this->_element->getValue();
		if($value!=""){
			if(($col=$this->getOption("column"))==null){
				$col=$this->_element->getName();
			}
			
			if(($searchType=$this->getOption("searchType"))==null){
				$searchType=$this->_defaultSearchType;
			}
			
			switch ($searchType){
				case "ignore":
					$w='';
				break;
				case "LIKE":
					$w=$col.' LIKE "'.$value.'"';
				break;
				case "%LIKE%":
					$w=$col.' LIKE "%'.$value.'%"';
				break;
				case "LIKE%":
					$w=$col.' LIKE "'.$value.'%"';
				break;
				case "%LIKE":
					$w=$col.' LIKE "%'.$value.'"';
				break;
				case ">":
					$w=$col.' > "'.$value.'"';
				break;
				case "<":
					$w=$col.' < "'.$value.'"';
				break;
				case ">=":
					$w=$col.' >= "'.$value.'"';
				break;
				case "<=":
					$w=$col.' <= "'.$value.'"';
				break;
				case "<>":
					$w=$col.' <> "'.$value.'"';
				break;
				case "!=":
					$w=$col.' != "'.$value.'"';
				break;
				case "=":
					$w=$col.' = "'.$value.'"';
				break;
				case "parse":
					if(!$this->getOption("searchParse")){
						throw new Matj_SmartTable2_Exception('SearchType Parse must have option searchParse');
					}
					$w=str_replace('{value}',$value,$this->getOption("searchParse"));
				break;
				default:
						throw new Matj_SmartTable2_Exception('Unknown SearchType in Filter Plugin');
				break;
			}
			$where=$this->_joinWheres($where, $w);
		}
		return $where;
	}
	
	
	
	
	/**
	 * spoji dve where klauzule pomocou operatora v $join
	 *
	 * @param string $where1
	 * @param string $where2
	 * @param string $join
	 * @return string
	 */
	protected function _joinWheres($where1, $where2, $join = 'AND'){
		if(!empty($where1) && !empty($where2)){
			return $where1." ".$join." ".$where2;
		}else{
			return $where1.$where2;
		}
	}
	
	
}