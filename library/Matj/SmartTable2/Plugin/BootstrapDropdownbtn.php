<?php

require_once 'Zend/View/Helper/Abstract.php';

class Matj_SmartTable2_Plugin_BootstrapDropdownbtn extends Matj_SmartTable2_Plugin_Abstract {
	
	function renderCell ($data,$row=array()) {
		
		$links=array();
		
		foreach($this->getOptions() as $o){
			$p=new Matj_SmartTable2_Plugin_Link($o,$this->column);
			if($p->checkCondition($row)){
				$p->setOption('returnArray','true');
				$a=$p->renderCell($data,$row);	
				if(!empty($a)){
					$a[1]["text"]=$a[0];
					$links[]=$a[1];
				}
			}
		}
		
		
		if(empty($links)){
			return null;
		}
		
		return Matj_Get::getView()->bootstrapDropdownbtn($links);
	}
	
	

}
