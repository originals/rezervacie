<?php
/**
 * 
 * options:
 * url
 * columnParams
 * urlParams
 * reset
 * resetSort
 * route
 * 
 * Enter description here ...
 * @author Matus
 *
 */
class Matj_SmartTable2_Plugin_FilterSubmitReset extends Matj_SmartTable2_Plugin_Filter {
	
	protected $_elementType = "submit";
	
	  
	
	function renderFilter ($content, $row) {
		return $this->_element;
	}
	
	function getElement($type,$name=null,$attribs=array()){
		$attribs["ignore"]=true;

                $click='$(this).closest(\'form\').find(\'.filter_row input[type=text],.filter_row textarea,.filter_row select\').val(\'\');';
                $click.='if($.fn.multiselect !=\'undefined\' ){ $(this).closest(\'form\').find(\'.filter_row .multiselect\').closest(\'th\').find(\'select\').multiselect(\'select\',{});  };';
                
                
		if(($trlabel=$this->getOption("translatedLabel"))!=null)
			$label=Matj_Get::getTranslator()->_($trlabel);
		
		if(empty($label) and ($label=$this->getOption("label"))==null)
			$label=Matj_Get::getTranslator()->_('core.filter'); 
		
                $attribs["decorators"]=array('ViewHelper',array('parse',array('text'=>'<div class="btn-group filter_submit_reset"><button type="submit" class="btn btn-info btn-xs">'.$label.'</button><button type="button" onclick="'.$click.'" class="btn btn-danger btn-xs">x</button></div>')));
                $attribs["class"]="btn btn-primary";
                
		
                
                $e=parent::getElement($type,$name,$attribs);
		
                
                $e->setLabel($label);
                
                
                
                
			
			
		
		
		return $e;
	}
	
	
	function joinWhere($where){
		return $where;
	}
}