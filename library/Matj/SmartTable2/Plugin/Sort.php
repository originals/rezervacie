<?php

class Matj_SmartTable2_Plugin_Sort extends Matj_SmartTable2_Plugin_Abstract {
	
	function renderCell ($content, $row, $column=null) {
		return $content;
	}
	
	function renderHeader ($content, $row, $column=null) {
		$attr = array ();
		
                if(!$this->column->getTable()->getOption('sortable'))
                    return $content;
                
                
		$attr["href"] = "#";
		$url = array ();
		if (($s = $this->getOption("sort")) !== null) {
			$url["sort"]=$column->getColumn();
		}
		
		foreach ($this->getOptions() as $k => $d) {
			$attr[strtolower($k)] = (string) $d;
		}
		
		
		if($this->column->getTable()->getSortColumn()==$column->getColumn()){
			if($this->column->getTable()->isSortAsc()){
				//$content.='<span class="arrowAsc"></span>';
				//$attr["class"].="sortSelected sortAsc";
				$url[$this->column->getTable()->getOption("sortKey")].="|DESC";
				$column->addAttrib('class','sortable forwardSort','th');
			}
			else{
				//$content.='<span class="arrowDesc"></span>';
				//$attr["class"].="sortSelected sortDesc";
				$column->addAttrib('class','sortable reverseSort','th');
			}
		
			$column->addAttrib('class','alt','td');
		}
		else{
			$column->addAttrib('class','sortable','th');
		}
		
		if (!empty($url)) {
			$attr["href"]=Matj_Get::url($url);
		}
		
		
		
		$html=Matj_Html::a($content,$attr);
		
		if($this->column->getTable()->getOption("ajax")){
			$this->column->getTable()->addJavascript('sortPlugin','
				$(document).ready(function(){
					$("table .first_row .sortable a").click(function(){
							return delphi.smarttable.runUrl($(this).attr("href"),"'.$this->column->getTable()->getId().'");
					});
				});
			');
		}
		
		
		return $html;
	}

	
	function getSql(){
		return $this->getOption("sort");
	}
	
}