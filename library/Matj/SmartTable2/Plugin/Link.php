<?php
/**
 *
 * options:
 * url
 * columnParams
 * urlParams
 * reset
 * resetSort
 * route
 *
 * Enter description here ...
 * @author Matus
 *
 */
class Matj_SmartTable2_Plugin_Link extends Matj_SmartTable2_Plugin_Abstract {

	/**
	 * @see Matj_SmartTable2_Plugin_Abstract::renderCell()
	 */
	function renderCell ($content, $row) {

		$attr = array ();
		$attr["href"] = "#";
		if (is_array($this->getOption("url"))) {

			$urlParams=$this->getOption("url");

			if(is_array($this->getOption("columnParams"))){
				foreach($this->getOption("columnParams") as $p=>$column){
					$urlParams[$p]=isset($row[$column]) ? $row[$column] : null;
				}
			}

			if($this->getOption("resetSort")){
				$urlParams[$this->column->getTable()->getOption("sortKey")]=null;
			}


			$attr["href"] = Matj_Get::url($urlParams, $this->getOption("route"),
				(bool) $this->getOption("reset"));
		}
		else {
			if ($this->getOption("url") !== null){
				$attr["href"] = $this->getOption("url");
				$attr["href"] = Matj_Util::parseParams($attr["href"],$row);
			}
		}



		// title
		if ($this->getOption("title") !== null){
				$attr["title"] = $this->getOption("title");
				$attr["title"]=Matj_Util::parseParams($attr["title"],$row);
		}


		foreach ($this->getOptions() as $k => $d) {
			$attr[strtolower($k)] = (string) $d;
		}


		if($this->getOption("text")!=""){
			$content=str_replace(array('{content}','%content%'),$content,$this->getOption("text"));
		}
		if($this->getOption("translatedText")!=""){
			$content=str_replace(array('{content}','%content%'),$content,Matj_Get::getTranslator()->_($this->getOption("translatedText")));
		}
		if ($this->getOption("target") !== null)
				$attr["target"] = $this->getOption("target");

				//Zend_Debug::dump($attr);


		if($this->getOption("returnArray")!=""){
			return array($content,$attr);
		}

		$html=Matj_Html::a($content,$attr);
		return $html;
	}


}