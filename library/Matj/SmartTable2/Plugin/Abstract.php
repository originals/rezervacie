<?php

abstract class Matj_SmartTable2_Plugin_Abstract {
	
	/**
	 * vlastnosti
	 */
	private $_options;
	
	/**
	 * @var Matj_SmartTable2_Column
	 */
	public $column;
	/**
	 * vrati translate objekt
	 * @return Zend_Translate
	 */
	public function getTranslator(){
		return Matj_Get::getTranslator();
	}
	
	
	/**
	 * vracia view premennu
	 */
	public function getView(){	
	    return Matj_Get::getView();
	}
	
	
	function __construct(array $options,$column){
		$this->column=$column;
		$this->_options=$options;
		$this->init();
	}
	
	function init(){ }
	
	
	function getOption($key){
		if(key_exists($key, $this->_options))
			return $this->_options[$key];
			
		return null;
	}
	
	
	function getOptions(){
		return $this->_options;
	}
	
	function setOption($name,$value){
		$this->_options[$name]=$value;
		return $this;
	}
	
	
	
	/**
	 * 
	 * Vyrendruje bunku
	 * @param string $content
	 * @param string $row
	 * @param Matj_SmartTable2_Column $column
	 */
	function renderCell ($content, $row) {
		return $content;
	}
	
	function renderHeader($content, $row) {
		return $content;
	}
	
	function renderFilter($content, $row) {
		return $content;
	}
	
	function checkCondition($row=array()){
		if($this->getOption('condition')){
			$cond='$return = '.$this->getOption('condition').' ? true : false;';
			eval($cond);
			return $return;
		}
		return true;
	}
	
	function getPluginName(){
		$c=explode("_",get_class($this));
		return $c[count($c)-1];
	}
	
	
	function joinWhere($where){
		return $where;
	}
	
}