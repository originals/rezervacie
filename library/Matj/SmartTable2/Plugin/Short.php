<?php

class Matj_SmartTable2_Plugin_Short extends Matj_SmartTable2_Plugin_Abstract {
	
	/**
	 * @see Matj_SmartTable2_Plugin_Abstract::renderCell()
	 */
	function renderCell ($content, $row) {
		
		return Matj_String::shorten($content,
						($this->getOption("length") > 0 ? $this->getOption("length") : 50 ),
						($this->getOption("type") !="" ? $this->getOption("type") : "span"));
	}


}