<?php
/**
 * 
 * options:
 * url
 * columnParams
 * urlParams
 * reset
 * resetSort
 * route
 * 
 * Enter description here ...
 * @author Matus
 *
 */
class Matj_SmartTable2_Plugin_Parse extends Matj_SmartTable2_Plugin_Abstract {
	
	/**
	 * @see Matj_SmartTable2_Plugin_Abstract::renderCell()
	 */
	function renderCell ($content, $row) {
		
		$row["_value"]=$content;
		
		$html="";
		if ($this->getOption("text")!==null) {
			$html=$this->getOption("text");
		}
		foreach($row as $k=>$d){
			$html=str_replace(array("%$k%","{".$k."}"),$d,$html);
		}
		//Zend_Debug::dump($row);exit;
		return $html;
	}


}