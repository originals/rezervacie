<?php
/**
 * 
 * options:
 * url
 * columnParams
 * urlParams
 * reset
 * resetSort
 * route
 * 
 * Enter description here ...
 * @author Matus
 *
 */
class Matj_SmartTable2_Plugin_Formtext extends Matj_SmartTable2_Plugin_Abstract {
	
	/**
	 * @see Matj_SmartTable2_Plugin_Abstract::renderCell()
	 */
	function renderCell ($content, $row) {
		
		$attr = array ();
		
		
		
		
		$name=$this->getOption("name")."[".$row[$this->getOption("idColumn")]."]";
		
		$attribs=$this->getOptions();
		unset($attribs["name"]);
		unset($attribs["idColumn"]);
		unset($attribs["value"]);
		
		
		
		$element=$this->getView()->formText($name,$row[$this->getOption("value")],$attribs);
		
		
		$html=$element;
		return $html;
	}


}