<?php

class Matj_SmartTable2_Plugin_Translate extends Matj_SmartTable2_Plugin_Abstract {
	
	/**
	 * @see Matj_SmartTable2_Plugin_Abstract::renderCell()
	 */
	function renderCell ($content, $row) {
		
		$ttext=$this->getOption("prefix").$content.$this->getOption("suffix");
		
		
		return Matj_Get::getTranslator()->_($ttext);
	}


}