<?php
/**
 * 
 * options:
 * url
 * columnParams
 * urlParams
 * reset
 * resetSort
 * route
 * 
 * Enter description here ...
 * @author Matus
 *
 */
class Matj_SmartTable2_Plugin_FilterSubmit extends Matj_SmartTable2_Plugin_Filter {
	
	protected $_elementType = "submit";
	
	  
	
	function renderFilter ($content, $row) {
		return $this->_element;
	}
	
	function getElement($type,$name=null,$attribs=array()){
		$attribs["ignore"]=true;
		$e=parent::getElement($type,$name,$attribs);
		
		if(($trlabel=$this->getOption("translatedLabel"))!=null)
			$label=Matj_Get::getTranslator()->_($trlabel);
		
		if(empty($label) and ($label=$this->getOption("label"))==null)
			$label=translate('filter.'.$e->getName());
		
			
			
		$e->setLabel($label);
		
		return $e;
	}
	
	
	function joinWhere($where){
		return $where;
	}
}