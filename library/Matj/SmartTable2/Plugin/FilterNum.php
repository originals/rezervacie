<?php
/**
 * 
 * options:
 * url
 * columnParams
 * urlParams
 * reset
 * resetSort
 * route
 * 
 * Enter description here ...
 * @author Matus
 *
 */
class Matj_SmartTable2_Plugin_FilterNum extends Matj_SmartTable2_Plugin_Filter {
	
	  
	function init(){
		
		if(!isset($name)){
			if($this->getOption("name")){
				$name=$this->getOption("name");
			}
			else{
				$name=$this->column->getAttrib("Column");
			}
		}
		
		
		
		
		$dec1=array('ViewHelper'
					,array('HtmlTag',array('tag'=>'span','class'=>'selectWrapper'))
					,array('Parse',array('text'=>'<span class="filter'.$this->getPluginName().' filter_column_'.$name.'">{content}'))
			);
		
		
		$dec2=$dec1;
		$dec2[1][1]["class"]='inputWrapper';
		$dec2[2][1]["text"]='{content}</span>';
		//$dec2[2][1]["closeOnly"]=true;
		//Zend_Debug::dump($dec2);exit;
		
		
		$e=$this->getElement("text",$name,array('filters'=>array('StringTrim','StripTags',array('DecimalComma',true)),'decorators'=>$dec2));
		$this->getFiltersForm()->addElement($e);
		
		
		
		$e2 = $this->getElement("select",$name."_searchType",array('filters'=>array('StringTrim'),'decorators'=>$dec1));
		$this->getFiltersForm()->addElement($e2);
		$e2->addMultiOptions(array ( 
			'=' => '=', 
			'<' => '<', 
			'>' => '>', 
			'<=' => '<=', 
			'>=' => '>=', 
			'<>' => '<>',
			'!=' => '!=', 
		));
		
		
		$this->_element=$e;
		$this->_element2=$e2;
	}
	
	function joinWhere($where){
		if($value=$this->_element2->getValue()){
			$this->setOption("searchType", $value);
		}
		return parent::joinWhere($where);
	}
	
	
	function renderFilter ($content, $row) {
		return $this->_element2.$this->_element;
	}

}