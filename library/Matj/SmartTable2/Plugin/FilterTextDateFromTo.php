<?php
/**
 * 
 * options:
 * url
 * columnParams
 * urlParams
 * reset
 * resetSort
 * route
 * 
 * Enter description here ...
 * @author Matus
 * 
 */
class Matj_SmartTable2_Plugin_FilterTextDateFromTo extends Matj_SmartTable2_Plugin_Filter {
	
	protected $_elementType = "dateTextFromTo";
	
	function getElement($type,$name=null,$attribs=array()){
		$attribs["class"]="pickDate";
                $attribs["emptyOptions"]="true";
                
                if($this->getOption('years'))
                    $attribs['years']=$this->getOption('years');
                
                return parent::getElement($type,$name,$attribs);
	}
	
	function renderFilter ($content, $row) {
		
                
            
            
            
                return $this->_element;
	}
	
	
	function joinWhere($where){
		$value=$this->_element->getValue();
		if(empty($value))return $where;
		
                
                if(($col=$this->getOption("column"))==null){
				$col=$this->_element->getName();
		}

                //return '';
                if(!empty($value["from"]) && $value["from"]>0 && !empty($value["to"]) && $value["to"]>0){
                    
                    if(!preg_match('/:/',$value["from"]))$value["from"].=" 00:00:00";
                    if(!preg_match('/:/',$value["to"]))$value["to"].=" 23:59:59";
                    
                    
                    $w=$col.' BETWEEN "'.$value["from"].'" AND  "'.$value["to"].'"';
                }
                elseif(!empty($value["from"]) && $value["from"]>0){
                    if(!preg_match('/:/',$value["from"]))$value["from"].=" 00:00:00";
                    $w=$col.' >= "'.$value["from"].'"';
                }
                elseif(!empty($value["to"]) && $value["to"]>0){
                    if(!preg_match('/:/',$value["to"]))$value["to"].=" 23:59:59";
                    $w=$col.' <= "'.$value["to"].'"';
                }
                
                if(!empty($w))
                    $where=$this->_joinWheres($where, $w);
		
                
               // echo $w;
                
                
                return $where;
	}
	
	
}