<?php

class Matj_SmartTable2_Plugin_Timeago extends Matj_SmartTable2_Plugin_Abstract {
	
	/**
	 * @see Matj_SmartTable2_Plugin_Abstract::renderCell()
	 */
	function renderCell ($content, $row) {
		
		return Matj_Util::timeago($content,$this->getOption("timepointer"),$this->getOption("measureby"),(bool)$this->getOption("ago"));
	}


}