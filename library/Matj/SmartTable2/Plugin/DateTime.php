<?php
/**
 * 
 * options:
 * url
 * columnParams
 * urlParams
 * reset
 * resetSort
 * route
 * 
 * Enter description here ...
 * @author Matus
 *
 */
class Matj_SmartTable2_Plugin_DateTime extends Matj_SmartTable2_Plugin_Abstract {
	
	/**
	 * @see Matj_SmartTable2_Plugin_Abstract::renderCell()
	 */
	function renderCell ($content, $row) {
		
		if(null===($format=$this->getOption("format"))){
			$format="d.m.Y H:i";
		}
		
		$value=Matj_Format::formatDate($content,$format);
		
		return $value;
	}


}