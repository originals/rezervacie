<?php

class Matj_SmartTable2_Plugin_Price extends Matj_SmartTable2_Plugin_Abstract {
	
	/**
	 * @see Matj_SmartTable2_Plugin_Abstract::renderCell()
	 */
	function renderCell ($content, $row) {
		
		return Matj_Format::toPrice($content,
						$this->getOptions());
	}


}