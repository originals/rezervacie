<?php

class Matj_SmartTable2_Plugin_Checkbox extends Matj_SmartTable2_Plugin_Abstract {
	
    
        function renderFilter ($content, $row) {
		$name=$this->getOption("name");
                if(!$name)
                    $name=$this->column->getAttrib("Column");
                
                $column=$this->getOption("column");
                if(!$column)
                    $column=$this->column->getAttrib("Column");
                
                
                
                $js='$("input[name^='.$name.']").each(function(){ if(!checked)$(this).removeAttr("checked"); else $(this).attr("checked","checked");  });';
                
                return '<div class="pluginCheckbox">'.$this->getView()->formCheckbox($name."[selectall]",1,array('onchange'=>$js)).'</div>'.$content;
	}
    
	/**
	 * @see Matj_SmartTable2_Plugin_Abstract::renderCell()
	 */
	function renderCell ($content, $row) {
		
		
                $id=$this->getOption("id");
		$value=$this->getOption('value') ? $row[$this->getOption('value')] : 1;
                
                
                $name=$this->getOption("name");
                if(!$name)
                    $name=$this->column->getAttrib("Column");
                
                $column=$this->getOption("column");
                if(!$column)
                    $column=$this->column->getAttrib("Column");
                
               
                
                $attribs=array();
                
                if($this->getOption("attribs")){
                    $attribs=$this->getOption("attribs");
                }
                
                if($this->getOption("class")){
                    $attribs["class"]=$this->getOption("class");
                }
                
                
		return $this->getView()->formCheckbox($name."[".$row[$column]."]",$value,$attribs).$content;
		
              
	}


}