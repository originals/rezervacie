<?php
/**
 * 
 * options:
 * url
 * columnParams
 * urlParams
 * reset
 * resetSort
 * route
 * 
 * Enter description here ...
 * @author Matus
 *
 */
class Matj_SmartTable2_Plugin_FilterMulti extends Matj_SmartTable2_Plugin_Filter {
	
	
	function addMultiOptions($e){
		$o=$this->getOption("options");
		if(is_array($o)){
			foreach($o as $k=>$d){
				$e->addMultiOption($k,$d);
			}
		}
	}
	
	
	function getElement($type,$name=null,$attribs=array()){
		$e=parent::getElement($type,$name,$attribs);
		$this->addMultiOptions($e);
		return $e;
	}
	
}