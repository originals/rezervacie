<?php

class Matj_SmartTable2_Plugin_Icon extends Matj_SmartTable2_Plugin_Abstract {
	
	/**
	 * @see Matj_SmartTable2_Plugin_Abstract::renderCell()
	 */
	function renderCell ($content, $row) {
		
		$prefix=$this->getOption("prefix");
		
		$key=$prefix.$content;
		
		return $this->getView()->icon($key);
	}


}