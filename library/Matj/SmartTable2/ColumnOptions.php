<?php

class Matj_SmartTable2_ColumnOptions extends Matj_SmartTable2_Abstract {
	
    
    function getSettings(){
        $text=$this->table->getSessionTable()->smartTableColumnOptions;
        $data= Zend_Json::decode($text);
        
        if(empty($data)){
            if($d=$this->table->getOption('columnOptionsDefault')){
                foreach($d as $col){
                    $data[$col]["active"]=1;
                }
            }
        }
        
        
        return $data;
    }
    
    function render(){
        
        
        $columns=$this->table->getColumns();
        
        
        
        $settings=$this->getSettings();
        $settingsJson=$this->table->getSessionTable()->smartTableColumnOptions;
        
        
        
        
        
        
        $rows[0]=array('text'=>'<i class=icon-cog></i>');
        foreach($columns as $k=>$d){
            $header=$d->getHeader();
            
            if(empty($settings) || !empty($settings[$k]["active"])){
                $a='<i class=icon-ok></i> ';
            }
            else{
                $a='<i class=icon-remove></i> ';
                $this->table->removeColumn($k);
            }
            
            
            $rows[$k]=array('text'=>$a.($header ? $header : "-".$k."-")
                           ,'href'=>'#'.$k);
        }
        
        
        $html.=$this->getView()->formTextarea('smartTableColumnOptions',$settingsJson,array('class'=>'hide'))
             . $this->getView()->bootstrapDropdownbtn($rows,array('split'=>false,'first'=>false,'class'=>'columnOptions'));
        return $html;
        
    }
    
    
}