<?php

class Matj_SmartTable2_Paginator extends Matj_SmartTable2_Abstract {
		
	
	protected $_adapter;
	
	function getAdapter(){
		if($this->_adapter===null){	
		    Zend_Loader::loadClass("Matj_Paginator_Numbers");
    	    $paginator=new Matj_Paginator_Numbers();
    		$this->_adapter=$paginator;    
		} 
		return $this->_adapter;   
	}
	
	
	function setAdapter(Matj_Paginator_Abstract $adapter){
		$this->_adapter=$adapter;
	}
	
	function __toString(){
		try{
			return $this->render();	
		}
		catch(Exception $e){
			echo $e;exit;
		}
	}
	
	
	protected $_attribs;
	
	protected $_table;
	
	function getTable(){
		return $this->_table;
	}
	
	
	function __construct ($attribs, $table) {
		
		$this->_table = $table;
		if (! empty($attribs["plugins"])) {
			$this->addPlugins($attribs["plugins"]);
			unset($attribs["plugins"]);
		}
		
		if (! empty($attribs["sort"])) {
			if(is_array($attribs["sort"]))
				$sortOpt=$attribs['sort'];
			else
				$sortOpt=array('sort'=>$attribs['sort']);
			$this->addPlugin('Sort',$sortOpt);
		}
		

		$this->setAttribs($attribs);
	}
	
	
	
	function addAttrib ($name,$value,$element=null){
		$name=ucfirst($name);
		if($element==null){
			if(empty($this->_attribs[$name])){
				$this->_attribs[$name]=$value;
			}
			else{
				$this->_attribs[$name].=" ".$value;
			}
		}
		else{
			if(empty($this->_attribs[$element][$name])){
				$this->_attribs[$element][$name]=$value;
			}
			else{
				$this->_attribs[$element][$name].=" ".$value;
			}
		}
		return $this;
	}
	

	function setAttribs ($attribs) {
		foreach ($attribs as $k => $d) {
			$this->setAttrib($k, $d);
		}
		return $this;
	}
	
	
	
	
	
	function render(){
		if($this->getTable()->getOption("ajax")){
			$this->getTable()->addJavascript('paginatorPlugin','
				$(document).ready(function(){
					$("#'.$this->_table->getId().' .paginator a").click(function(){
							return delphi.smarttable.runUrl($(this).attr("href"),"'.$this->getTable()->getId().'");
					});
				});
			');
		}
		
		return $this->getAdapter()->render();
	}
	
	
	
	function __call($method,$args){
		return call_user_func_array(array($this->getAdapter(), $method), $args);
	}
	
}