<?php

abstract class Matj_SmartTable2_Abstract {
         
    
    
        function _construct(){
            $this->init();
        }
        
        
        function init(){}
        
	/**
	 * vrati translate objekt
	 * @return Zend_Translate
	 */
	public function getTranslator(){
		return Matj_Get::getTranslator();
	}
	
	
	/**
	 * vracia view premennu
	 */
	public function getView(){	
	    return Matj_Get::getView();
	}
	
        
        protected $table;
        
        public function setTable(Matj_SmartTable2 $table){
            $this->table=$table;
            return true;
        }
	
}