<?php

class Matj_SmartTable2_Column extends Matj_SmartTable2_Abstract {
	
	protected $_plugins = array ();
	

	protected $_renderAttribs = array ( 
		'Class', 
		'Style', 
		'Alt', 
		'Id' 
	);
	

	protected $_attribs;
	
	protected $_table;
	
	function getTable(){
		return $this->_table;
	}
	

	function __construct ($attribs, $table) {
		
		$this->_table = $table;
		$this->setAttribs($attribs);
		
		if (! empty($attribs["plugins"])) {
			$this->addPlugins($attribs["plugins"]);
			unset($attribs["plugins"]);
		}
		
		if (! empty($attribs["sort"])) {
			if(is_array($attribs["sort"]))
				$sortOpt=$attribs['sort'];
			else
				$sortOpt=array('sort'=>$attribs['sort']);
			$this->addPlugin('Sort',$sortOpt);
		}
		

		
	}
	

	function isSortable () {
		return (bool) $this->getAttrib("sort");
	}
	

	function setAttrib ($name, $value,$element=null) {
		$this->_attribs[ucfirst($name)] = $value;
		return $this;
	}
	
	function getAttrib ($name) {
		if(isset($this->_attribs[ucfirst($name)]))
                    return $this->_attribs[ucfirst($name)];
        }
	
	
	function getAttribs($element=null){
		$attr=array();
		foreach($this->_attribs as $k=>$d){
			$attr[$k]=$d;
		}
		
		
		
		if($element && !empty($this->_attribs[$element])){
			foreach($this->_attribs[$element] as $k=>$d){
				if(isset($attr[$k])){
					$attr[$k].=" ".$d;
				}		
				else{
					$attr[$k]=$d;
				}
			}
		}
		return $attr;
	}
	
	
	function addAttrib ($name,$value,$element=null){
		$name=ucfirst($name);
		if($element==null){
			if(empty($this->_attribs[$name])){
				$this->_attribs[$name]=$value;
			}
			else{
				$this->_attribs[$name].=" ".$value;
			}
		}
		else{
			if(empty($this->_attribs[$element][$name])){
				$this->_attribs[$element][$name]=$value;
			}
			else{
				$this->_attribs[$element][$name].=" ".$value;
			}
		}
		return $this;
	}
	

	function setAttribs ($attribs) {
		foreach ($attribs as $k => $d) {
			$this->setAttrib($k, $d);
		}
		return $this;
	}
	

	function addPlugins (array $plugins) {
		foreach ($plugins as $k => $d) {
			if (is_array($d)) {
				$this->addPlugin($k, $d);
			}
			else {
				$this->addPlugin($d);
			}
		}
		return $this;
	}
	
	function addPlugin ($name, $options = array()) {
		$class = preg_replace("/_Column$/", "", get_class($this)) . "_Plugin_" . ucfirst($name);
		try {
			Zend_Loader::loadClass($class);
			$plugin = new $class($options, $this);
			$this->_plugins[$name] = $plugin;
		}
		catch (Exception $e) {
			//Zend_Debug::dump($e);exit;
			//$e->setMessage(sprintf("Plugin \"%s\" has error : ".$e->getMessage(), $name));
			throw $e;
			//throw new Matj_SmartTable2_Exception();
			//throw new Matj_SmartTable2_Exception(sprintf("Plugin \"%s\" not exists in SmartTable", $name));
		}
		return $this;
	}
	
	
	function removePlugin($name){
		if(key_exists($name,$this->_plugins)){
			unset($this->_plugins[$name]);
		}
		return $this;
	}
	

	function __call ($method, $params) {
		if (preg_match('/^get/', $method)) {
			$col = ucfirst(preg_replace('/^get/', '', $method));
			return $this->getAttrib($col);
		}
		if (preg_match('/^set/', $method)) {
			$col = ucfirst(preg_replace('/^set/', '', $method));
			return $this->setAttrib($col, $params[0]);
		}
		throw new Matj_SmartTable2_Exception("Method $method not exists in model " . get_class($this));
	}
	
	/**
	 * 
	 * vykresli jednu bunku <td></td>
	 * @param array $row jeden riadok z tabulky
	 */
	function renderCell ($row) {
		$html = '<td';
		
		foreach ($this->getAttribs("td") as $k => $d) {
			if (in_array($k, $this->_renderAttribs)) {
				$html .= ' ' . strtolower($k) . ' = "' . $d . '"';
			}
		}
		$html .= '>';
		
                $html.=$this->getRenderContent($row);
                
		$html .= '</td>';
		return $html;
	}
	
        
        function getRenderContent($row){
            $content="";
            if(isset($row[$this->getColumn()]))
                    $content = $row[$this->getColumn()];

            if (! empty($this->_plugins) and empty($row["_disablePlugins"])) {
                    foreach ($this->_plugins as $plugin) {
                            if($plugin->checkCondition($row))
                                    $content = $plugin->renderCell($content, $row, $this);
                    }
            }
            return $content;
        }
        
        

	function renderHeader ($row=array()) {
		
		
		//najskor content
		$content = $this->getHeader();
		if (! empty($this->_plugins)) {
			foreach ($this->_plugins as $plugin) {
                            $content = $plugin->renderHeader($content, $row, $this);
			}
		}
		
		
		// th tag
		$attr=array();
		if ($c = $this->getClass()) {
			$attr["class"] = $c;
		}
		
		if ((bool) ($w = $this->getWidth())) {
			$this->addAttrib("style", 'width:'.(is_numeric($w) ? $w.'px' : $w).';');
		}
		
		$attr=$this->getAttribs("th");
		$html = '<th';
		foreach ($attr as $a => $v) {
			if (in_array($a, $this->_renderAttribs))
				$html .= ' ' . $a . '="' . $v . '"';
		}
		
		$html .= '>';
		$html .= $content;
		$html .= '</th>';
		
		return $html;
	}
	
	
	function renderFilter ($row=array()) {
		
		//najskor content
		$content = "";//$this->getHeader();
		if (! empty($this->_plugins)) {
			foreach ($this->_plugins as $plugin) {
				$content = $plugin->renderFilter($content, $row, $this);
			}
		}
		
		
		// th tag
		$attr=array();
                if ($c = $this->getClass()) {
                    $attr["class"] = $c;
		}
		
		/*if ((bool) ($w = $this->getWidth())) {
			$this->addAttrib("style", 'width:'.(is_numeric($w) ? $w.'px' : $w));
		}*/
		
		$html = '<th';
		
		$attr=$this->getAttribs("th");
		foreach ($attr as $a => $v) {
			if (in_array($a, $this->_renderAttribs))
				$html .= ' ' . $a . '="' . $v . '"';
		}
		
		$html .= '>';
		$html .= $content;
		$html .= '</th>';
		
		return $html;
	}
	
	function joinWhere($where){
		if (! empty($this->_plugins)) {
			foreach ($this->_plugins as $plugin) {
				$where = $plugin->joinWhere($where);
			}
		}
		return $where;
	}
	
	
	
	function getSortSql(){
		if($this->_plugins["Sort"]){
			return $this->_plugins["Sort"]->getSql();
		}
		return null;
	}

	
	function getPlugin($name){
		if(!empty($this->_plugins[strtolower($name)])){
			return $this->_plugins[strtolower($name)];
		}
		return null;
	}
	
}