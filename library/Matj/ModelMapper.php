<?php
/** 
 * @author Delphi
 * @package Delphi
 * 
 */
class Matj_ModelMapper
{
	/**
	 * 
	 * @return Zend_Db_Adapter_Mysqli
	 */
	function getAdapter(){
    	return Matj_Get::getDbAdapter();
    }
    
   	function getFoundRows(){
		return $this->getAdapter()->fetchOne('select FOUND_ROWS()');	
	}
	
	
	function addOptionsToSelect (Zend_Db_Select $select, $options) {
		if (! empty($options["where"]))
			$select->where($options["where"]);
		
		
		if (! empty($options["order"]))
			$select->order($options["order"]);
		
		if (! empty($options["group"]))
			$select->group($options["group"]);
		
                if (! empty($options["limit"]))
			$select->limit($options["limit"],(!empty($options["offset"]) ? ($options["offset"]+0) : 0 ));
		
		return $select;
	}
	 
}
