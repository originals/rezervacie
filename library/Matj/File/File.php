<?php
/**
 * Creates an empty pdf document using TCPDF
 * @package DPdf
 * @author Lukas Zemcak
 */
class Matj_File_File {

	protected $url;
	
	protected $_fileResource;
	
	protected $_fileProperties=array();
	
	protected $options;
	
	public function __construct($url, $options = array()){
		$this->options = $options;
		$this->setUrl($url);
    	
	} 	
	
	
	function setUrl($url){
	   if(!file_exists($url) and !is_dir($url)){
	       if(!empty($this->options["create"])){
    	        $this->create($url);   
    	    }
    	    else{
	            throw new Exception($url.' not exists');
    	    }
	   }
	   if(is_dir($url)){
    	    throw new Exception($url.' is not a file, but it is a directory');
	   }
	   $this->url=$url;
	   return $this;
	}
	

	
	function getName(){
	   $f=explode(DIRECTORY_SEPARATOR,$this->getUrl(true));
	   return $f[count($f)-1];
	}
	
	function getExtension(){
	   $f=explode(".",$this->getName());
	   return $f[count($f)-1];
	}
	
	
	function getFileSize($format=false){
	    if(!isset($this->_fileProperties["size"])){
	        $this->_fileProperties["size"]=filesize($this->getUrl(true));
	    }
	    $size=$this->_fileProperties["size"];
	    
	    if($format)$size=Matj_Cms_Util::formatBytes($size);
	    
	    return $size;
	}
	
	function getModifiedTime($format = null){
	    if(!isset($this->_fileProperties["mtime"])){
	        $this->_fileProperties["mtime"]=filemtime($this->getUrl(true));
	    }
	    if($format){
	        return date($format,$this->_fileProperties["mtime"]);
	    }
	    return $this->_fileProperties["mtime"];
	}
	
	
	function getObjectType(){
	    return 'file';
	}
	
	
	function getUrl($realpath = false){
	    if($realpath)return realpath($this->url);
	    return $this->url;
	}
	
        function getPublicUrl(){
            return str_replace(PUBLIC_PATH,"",$this->getUrl(true));
        }
        
	function getSize($format=false){
	   $size=filesize($this->getUrl());
	   if($format){
	       return Matj_Cms_Util::formatBytes($size,2);
	   }
	   return $size;
	}
    
	protected $_content;
	/**
	 * 
	 * @return string
	 */
	function getContent($reset = false){
		if(!$this->_content or $reset){
			$this->_content=file_get_contents($this->getUrl());
		}
		return $this->_content;
	}
	
	/**
	 * 
	 */
	function setContent($content){
		$this->_content=$content;
		return $this;
	}
	
	
	
	function save(){
        $this->open("w");
        $this->write($this->getContent());
        $this->close();
	}
	
	
	function open($type){
	    $this->_fileResource=fopen($this->getUrl(),$type);
	    return $this;
	}
	
	function close(){
	    if($this->_fileResource){
	        fclose($this->_fileResource);
	    }
	    $this->_fileResource=null;
	    return $this;
	}
	
	
	function write($content){
	    if($this->_fileResource){
	        fwrite($this->_fileResource,$content,strlen($content));
	        return $this;
	    }
	    throw new Exception("file is not open for write");
	}
	
	
	function create($url){
	    $f=fopen($url,"w");
	    fclose($f);
	    return $url;
	}

    function getEditDate($formatted = false, $format = 'd.m.Y H:i:s'){
        $date=filemtime($this->getUrl());
        if($formatted){
            $date=date($format,$date);
        }
        return $date;
    }


    function getMimeType(){
        if(!isset($this->_fileProperties["mime"])){
    	    Zend_Loader::loadClass("Matj_File_Mime");
            $mime=Matj_File_Mime::getMimeType($this->getUrl(true));
            $this->_fileProperties["mime"]=$mime;
	    }
	    return $this->_fileProperties["mime"];
    }
    
    function getPreview($w=100,$h=100,$t='shrink',$o='none'){
    	$mime=$this->getMimeType();
    	$av=array('image/jpeg','image/gif','image/png');
    	if(in_array($mime, $av)){
    	    Zend_Loader::loadClass("Matj_Cms_Image");
            $image=new Matj_Cms_Image();
            $url=$this->getUrl(true);
            
            $dname=str_replace("library".DIRECTORY_SEPARATOR."Matj_File","",realpath(dirname(__FILE__)));
            $url=str_replace($dname,"",$url);
            return $image->image($url,$this->getName(),array('width'=>$w,'height'=>$h,'open'=>$o,'type'=>$t));
    	}
    }
	
    function getImageSize(){
        if(!isset($this->_fileProperties["imagesize"])){
    	    $mime=$this->getMimeType();
        	$av=array('image/jpeg','image/gif','image/png');
        	if(in_array($mime, $av)){
        	    $imageSize=getimagesize($this->getUrl(true));
    	        $this->_fileProperties["imagesize"]=$imageSize[0]." x ".$imageSize[1];
        	}
        	else{
        	    $this->_fileProperties["imagesize"]='';
        	}
	    }
	    return $this->_fileProperties["imagesize"];
    }
    
    
    function delete(){
        return @unlink($this->getUrl(true));
    }
    
    function rename($newFile){
        return @rename($this->getUrl(true),$newFile);
    }
	
	
	
	function getAsImage(){
		return new Matj_Image($this->getUrl(true), new Matj_Image_Driver_Gd2 );
	}
	
	function getAsImageTransform(){
		$image=$this->getAsImage();
		return new Matj_Image_Transform($image);
	}
	
	
	function saveResized($destUrl,$type,$size0,$size1=0,$size2=0,$size3=0){
		//$image = $this->getAsImage($destUrl);
		$transform=$this->getAsImageTransform();
		switch($type){
			case "height":
				$transform->fitToHeight($size0);	
			break;		
			case "width":
				$transform->fitToWidth($size0);
			break;
			case "fit":
				$transform->fitIn($size0,$size1);
			break;
			case "crop":
				$transform->crop($size0,$size1,$size2,$size3);
			break;
			default:
				$transform->resize($size0,$size1);			
			break;
		}
		$transform->save($destUrl);
		return new self($destUrl);
	}
				
	
	function __toString(){
		return $this->getUrl(true);
	}
	
	
	
}

?>
