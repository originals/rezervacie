<?php

/**
 * Creates an empty pdf document using TCPDF
 * @package DPdf
 * @author Lukas Zemcak
 */
class DFolder_Util{
	
	static function getFile($filename){
				
	}
	
	static function createFolder($url){
		
		
		$return=array();
		$url = str_replace('\\','/',$url);
		$pi = strpos($url,':');
		$prefix = substr($url,0,$pi);
		$url = substr($url,$pi+1);
				
		if(substr($url,0,1)=="/"){
			$prefix.='/';
			$url=substr($url,1);
		}		
		
		$d=explode("/",$url);
		$createFolder='';
		foreach($d as $dir){
			if(!empty($dir)){
				if(!empty($createFolder)){
					$createFolder.="/";
					$createFolder.=$dir;	
					if(!is_dir($createFolder)){
						if(!mkdir($createFolder,0777)){	
							echo $createFolder." can´t create<br>";
						}
					}	
				}else{				
					$createFolder.="/";
					$createFolder.=$dir;
				}
			}	
			$return[]=$dir;
		}
		return $prefix.implode("/",$return);		
	}
	
	/**
	 * @param 
	 * @return DFolder_Folder
	 */
	public static function getFolder($url ){
	 	self::createFolder($url);
	 	return new DFolder_Folder($url);
	 }
	
}

?>
