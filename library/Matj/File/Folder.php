<?php

require_once 'File.php';

/**
 * Creates an empty pdf document using TCPDF
 * @package DPdf
 * @author Lukas Zemcak
 */
class Matj_File_Folder {

	protected $url;

	protected $options;

	protected $_fileProperties=array();


	/**
	 * @param unknown_type $url
	 * @param unknown_type $options
	 */
	public function __construct($url, $options = array()){
		$this->setUrl($url);
    	$this->options = $options;
    }


	public function getFileSize($format=false){
	    if(!isset($this->_fileProperties["size"])){
	        $this->_fileProperties["size"]=filesize($this->getUrl(true));
	    }
	    $size=$this->_fileProperties["size"];

	    if($format)$size=Matj_Cms_Util::formatBytes($size);

	    return $size;
	}

	function getModifiedTime($format = null){
	    if(!isset($this->_fileProperties["mtime"])){
	        $this->_fileProperties["mtime"]=filemtime($this->getUrl(true));
	    }
	    if($format){
	        return date($format,$this->_fileProperties["mtime"]);
	    }
	    return $this->_fileProperties["mtime"];
	}


	public function getMimeType(){
    	return 'folder';
	}

	public function getImageSize(){
    	return '';
	}

	function getPreview($w=100,$h=100,$t='shrink',$o='none'){
   	    Zend_Loader::loadClass("Matj_Cms_Image");
        $image=new Matj_Cms_Image();
        return $image->image($this->_urlPreview,$this->getName(),array('width'=>$w,'height'=>$h,'open'=>$o,'type'=>$t));
	}

	public function setUrl($url){
	   if(!is_dir($url)){
    	    throw new Exception($url.' is not directory');
	   }
	   //odstrani lomitko z konca url
	   $url=preg_replace('/(\/|\\\)$/','',$url);
	   $this->url=$url;
	   return $this;
	}



	public function getFilesAsObjects(){
	    $data=array();
	    foreach($this->getFiles() as $f){
	        $f=$this->getUrl(true).DIRECTORY_SEPARATOR.$f;
	        if(is_dir($f)){
	            $data[]=new self($f);
	        }
	        else{
	            $data[]=new Matj_File_File($f);
	        }
	    }
	    return $data;
	}


	public function getFilesWithSuffixAsUrls( $suffix, $full = false){
		$result = array();
		$files = $this->getFiles();
		foreach ($files as &$f) {
			if(strncmp(strrev($f),strrev($suffix), strlen($suffix)) == 0){
				if($full){
					$result[] = $this->url.'/'.$f;
				}else{
					$result[] = $f;
				}
			}
		}
		return $result;
	}


	/**
	 *
	 * @param $suffix
	 * @return DFolder_File
	 */
	public function getFilesWithSuffixAsObjects($suffix){
	    $files=$this->getFilesWithSuffixAsUrls($suffix,true);
	    if(!empty($files)){
	       foreach($files as $d){
	           $return[]=new Matj_File_File($d);
	       }
	       return $return;
	    }
	    return null;
	}

	/**
	 *
	 * @param $suffix
	 * @return DFolder_File
	 */
	public function getFilesWithSuffixAsArray($suffix){
	    $files=$this->getFilesWithSuffixAsObjects($suffix);
	    if(!empty($files)){
	       foreach($files as $d){
	           $return[$d->getName()]=$d->getUrl();
	       }
	       return $return;
	    }
	    return null;
	}


	public function getUrl($realpath=false){
	    if($realpath)return realpath($this->url);
	    return $this->url;
	}


	/**
	 * DFolder_File
	 * @param $url
	 * @param $createIfNotExists
	 * @return unknown_type
	 */
	function getFile($url,$createIfNotExists = false){
	    $file=$this->getUrl().'/'.$url;
	    $file=new Matj_File_File($file,array('create'=>$createIfNotExists));
	    return $file;
	}

	/**
	 * DFolder_Folder
	 * @param $url
	 * @param $createIfNotExists
	 * @return unknown_type
	 */
	function getFolder($url,$createIfNotExists = false,$mode=0777){
	    $file=$this->getUrl().'/'.$url;
	    
		if(!file_exists($file) && $createIfNotExists){
			$this->createSubfolder($url,$mode);
		}
		$file=new self($file);
	    return $file;
	}


	function getName(){
	   if(isset($this->_customName))return $this->_customName;
	   $f=explode(DIRECTORY_SEPARATOR,$this->getUrl(true));
	   return $f[count($f)-1];
	}

	protected $_customName;

	function setCustomName($name){
	    $this->_customName=$name;
	}

	function getObjectType(){
	    return 'folder';
	}

	protected $_urlPreview='cms/images/filemanager/icon_folder_big.png';
	function setUrlPreview($url){
	    $this->_urlPreview=$url;
    }

    protected $_error;
    function getError(){
        return $this->_error;
    }

    function createSubfolder($folderName,$mode=0777){
        $url=$this->getUrl(true).DIRECTORY_SEPARATOR.$folderName;
        if(is_dir($url)){
            $this->_error="dirExists";
            return false;
        }
        if(@mkdir($url,$mode)){
            $this->_error=null;
            return true;
        }
        else{
            $this->_error='notCreated';
            return false;
        }
    }

    function delete(){
        return @rmdir($this->getUrl(true));
    }


    function getFilesRecursive(){
        $files=$this->getFilesAsObjects();
        $return=array();
        foreach($files as $k=>$d){
            if($d->getMimeType()=="folder"){
                $f=$d->getFilesRecursive();
                $return=array_merge($return,$f);
            }
            else{
                $return[]=$d;
            }
        }
        return $return;
    }

    public function getFiles(){
		$results = array();
	    $handler = opendir($this->url);
    	while ($file = readdir($handler)) {
	        if ($file != '.' && $file != '..')
    	        $results[] = $file;
    	}
    	return $results;
	}


    function fileExists($name){
        $files=$this->getFiles();
        return in_array($name,$files);
    }

	
	function __toString(){
		return $this->getUrl(true);
	}
	
        
        
        
        /**
	 *
	 * @param $suffix
	 * @return DFolder_File
	 */
	public function findFiles($string){
	    $files=$this->getFiles();
            $return=array();
	    if(!empty($files)){
               
               foreach($files as $d){
                   
                   if(strpos($d,$string)!==false)
                           
        	           $return[]=new Matj_File_File($this->getUrl().DIRECTORY_SEPARATOR.$d);
	       }
	       
	    }
            return $return;
	}

        
        
}
