<?php

/**
 * SmartTable je trieda pre jednoduche definovanie a vykreslovanie tabuliek,
 * vychadza z Matj_Core_SmartTable ale upravuje jej funkcionalitu o ajax
 * @package Core
 * @author Matus Jancik
 * @since 31.3.2011
 */

require_once 'Zend/Form/Element/Hidden.php';

class Matj_SmartTable2 extends Matj_SmartTable2_Abstract {

	protected $_id;

	protected $_columns;

	protected $_data;

	protected $_dataCount;

	protected $_javascript;

	protected $_paginator;

	protected $_html;


	protected $_options = array (
		'form' => true,
		'header' => true,
		'sortable' => true,
		'autoHeaders' => false,
		'popupAlwaysVisible' => true,
		'actionUrl' => '',

		'sortKey' => 'sort',
		'sortDefault' => null,
		'sortDefaultDesc' => false,
		'ajax' => false,

		'paginator' => false,
		'paginatorClass' => "Matj_SmartTable2_Paginator",
		'paginatorPosition' => 'bottom',

		'translatePrefix' => '',

		'pageKey' => 'page',
		'perpageKey' => 'perpage',
                'perpageDefault' => '50',

		'filter' => false,
                'columnOptions'=>false,
		'columnOptionsDefault'=>array(),
		'class'=>''
	);


	function __get ($name) {
		throw new Exception(sprintf("variable \"%s\" not exists in SmartTable", $name));
	}

	function __set ($name, $value) {
		throw new Exception(sprintf("variable \"%s\" not exists in SmartTable", $name));
	}


	function setOption ($name, $value) {
		if (key_exists($name, $this->_options)) {
			$this->_options[$name] = $value;
			return $this;
		}
		throw new Exception(sprintf("option \"%s\" not exists in SmartTable", $name));
	}

	function getOption ($name) {
		if (key_exists($name, $this->_options)) {
			return $this->_options[$name];
		}
		throw new Exception(sprintf("option \"%s\" not exists in SmartTable", $name));
	}


	function setOptions (array $options) {
		foreach ($options as $k => $d) {
			$this->setOption($k, $d);
		}
		return $this;
	}


	function getSessionTable(){
		return new Zend_Session_Namespace("smartTable_".$this->getId());
	}

	/**
	 * nezauijimavy konstruktor
	 *
	 * @param Zend_View $view
	 * @param Zend_Translate $translator
	 */
	public function __construct ($id = null, array $options = array()) {
		if (! $id)
			$id = get_class($this);
		$this->_id = $id;
		$this->init();
                
                $this->readGetOptions();
        }


	/**
	 * @return Zend_Controller_Request_Http
	 */
	function getRequest () {
		return Matj_Get::getFrontController()->getRequest();
	}



	/**
	 * sets translator
	 * @param unknown_type $translator
	 */
	public function setTranslator ($translator) {
		$this->_translator = $translator;
		return $this;
	}

	/**
	 * inicializuje tabulku
	 */
	public function init () {}


	function getFormId () {
		return "form_" . $this->_id;
	}



	/**
	 * nastavi data
	 *
	 * @param array $data
	 * @return Matj_JQuery_Table
	 */
	public function setData (array $data) {
		if($this->getPaginator());
		
                foreach($data as $k=>$d){
                    if($d instanceof Matj_Model)
                        $data[$k]=$d->getData();
                    if($d instanceof Matj_Mongo_Model)
                        $data[$k]=$d->getTableData();
                    if($d instanceof Matj_Dibi_Model)
                        $data[$k]=$d->getTableData();
                }
                
                
                $this->_data = $data;
		return $this;
	}

	/**
	 * vrati data
	 * @return array
	 */
	public function getData () {
		return $this->_data;
	}

	/**
	 * prida column
	 * @param $column
	 * @return Matj_SmartTable2
	 */
	public function addColumn ($column) {

		if (empty($column["column"])) {
			throw new Matj_SmartTable2_Exception('column must have a Column parameter');
		}
		$id = (string) $column["column"];

		if ($this->getOption('autoHeaders')) {
			if (! key_exists('header', $column) && ! key_exists('Header', $column)) {
				$column["header"] = $this->translate($id);
			}
		}


		$this->_columns[$id] = new Matj_SmartTable2_Column($column, $this);
		return $this;
	}


	/**
	 * Odstrani column
	 * @param $id
	 * @return Matj_SmartTable2
	 */
	public function removeColumn ($id) {
		unset($this->_columns[$id]);
		return $this;
	}


	/**
	 * vrati referenciu na pole columnu
	 * @param string $id
	 * @return Matj_SmartTable2_Column
	 */
	public function getColumn ($id) {
            if(isset($this->_columns[$id]))
		
               return $this->_columns[$id];
	}
        
        public function getColumns () {
            return $this->_columns;
        }

	/**
	 * nastavi triedu tabulky
	 * @param string $clazz
	 * @return Matj_JQuery_SmartTable
	 */
	public function setClass ($clazz) {
		$this->_clazz = $clazz;
		return $this;
	}


	/**
	 * vrati id
	 * @return unknown
	 */
	public function getId () {
		return $this->_id;
	}

	/**
	 * nastavi action url, na ktore sa odosiela formular pri ajaxovom update tabulky
	 * @param unknown_type $url
	 * @return unknown
	 */
	public function setActionUrl ($url) {
		$this->_actionurl = $url;
		return $this;
	}

	/**
	 * vrati action
	 *
	 * @return unknown
	 */
	public function getActionUrl () {
		return $this->_actionurl;
	}

	/**
	 * Nastavi premennu _sort a _sort2
	 *
	 */
	public function setSortedBy ($sort, $ascDesc = "asc") {
		$this->setSessionData("sort1", $sort);
		$this->setSessionData("sort2", $ascDesc);
		return $this;
	}


	/**
	 * vrati order by klauzulu
	 *
	 * @return unknown
	 */
	public function getOrderBy () {
		$request = Matj_Get::getRequest();
		$col = $this->getSortColumn();
		if ($column = $this->getColumn($col)) {
			return $column->getSortSql() . ' ' . ($this->isSortAsc() ? "ASC" : "DESC");
		}

	}

	/**
	 * vrati order by klauzulu v poli
	 *
	 * @return unknown
	 */
	public function getOrderByArray () {
		$sort1 = $this->getSessionData('sort1');
		if (! empty($sort1)) {
			return array (
				"sort1" => $this->_columns[$this->getSessionData('sort1')],
				"sort2" => $this->getSessionData('sort2')
			);
		}
		else
			return array ();
	}

	/**
	 * nastavi pocet r
	 *
	 * @param unknown_type $count
	 */
	public function setDataCount ($count) {
		if($p=$this->getPaginator())
			$this->getPaginator()->setCount($count);
		return false;
	}

	public function getCount () {
		return $this->getPaginator()->getCount();
	}

	/**
	 * vrati offset potrebny pre citanie z db
	 *
	 * @return unknown
	 */
	public function getOffset () {
		if($p=$this->getPaginator()){
			return $p->getOffset();
		}
		return null;
	}
	/**
	 * vrati offset potrebny pre citanie z db
	 *
	 * @return unknown
	 */
	public function getLimit () {
		if($p=$this->getPaginator()){
			return $p->getLimit();
		}
		return null;
	}

	/**
	 * vykresli stranku s datami
	 *
	 * @return unknown
	 */
	public function __toString () {
		try {
			return $this->render();
		}
		catch (Exception $e) {
			Zend_Debug::dump($e);
			exit();
		}
	}

        
        protected $_columnOptions = null;
        
        function getColumnOptions(){
            if($this->_columnOptions===null){
                if($this->getOption('columnOptions')){
                    $this->_columnOptions = new Matj_SmartTable2_ColumnOptions();
                    $this->_columnOptions->setTable($this);
                }
                else 
                    $this->_columnOptions=false;
            }
            return $this->_columnOptions;
        }
        
        
        function getRenderData(){
            $return=array();
            $data = $this->getData();
            if (is_array($data)) {
                    foreach ($data as $k=>$row) {
                            if (is_array($row)) {
                                    
                                
                                    foreach ($this->_columns as $key => $column) {
                                            $return[$k][$key]= $column->getRenderContent($row);
                                    }
                                
                            }
                    }
            }
            return $return;
        }
        
        
        
	public function render () {
		
                $result='<div id="'.$this->getId().'"><!-- start of SmartTable -->';


			if ((bool) $this->getOption("form")) {
				$result .= '<form ' . 'id="' . $this->getFormId() . '" ' . 'name="' . $this->getFormId() . '" ' .
					 'class="smartTable" ' . 'action="' . $this->getOption("actionUrl") . '" ' . 'method="post"' . '>';
			
                                
                                
                                $tableOptions='';
                                if($columnOptions=$this->getColumnOptions()){
                                    $tableOptions.=$columnOptions->render();
                                }
                                
                                if(!empty($tableOptions)){
                                    $this->_html["append"][]=('<div class="smartTableOptions"><div class="btn-toolbar">'
                                            .$tableOptions
                                            .'<a href="#" class="btn btn-primary smartTableOptionsSubmit"><i class="icon-forward icon-white"></i></a>'
                                            .'</div></div>');
                                }
                                
                                
                                
                        }

			//potom paginator
			if ($this->getOption("paginator") && $this->getOption("paginatorPosition")=="top") {
				$result .= $this->renderPaginator();
			}

			if(!empty($this->_html['prepend'])){
				$result.=implode("",$this->_html["prepend"]);
			}
                        
                        
                        $result .= '<table';
			if($this->getOption("class")!=""){
				$result.= ' class="'.$this->getOption("class").'"';
			}
				$result .= '>';



			if($this->getOption("header")){
				$thead=true;
				$result.= '<thead>';

				$result .= '<tr class="first_row ' . /* (isset($this->_filters) ? "nobottomborder" : "") . */ '">';

				//hlavicky:
				foreach ($this->_columns as $key => $column) {
					$result .= $column->renderHeader();
				}
				$result .= '</tr>';
			}




			if($this->getOption("filter")){
				if(!isset($thead)){
					$result.= '<thead>';
					$thead=true;
				}

				//hlavicky:
				$resultFilter="";
				foreach ($this->_columns as $key => $column) {
					$resultFilter .= $column->renderFilter();
				}

				$result .= '<tr class="filter_row">';
					$result .=$resultFilter;
				$result .= '</tr>';
			}



			if(isset($thead))$result .= '</thead>';
			//a tabulka
			$rowCount = 0;

			$data = $this->getData();
			if (is_array($data)) {
				foreach ($data as $row) {
					if (is_array($row)) {
						if (isset($row['class'])) {
							$rowClass = " " . $row['class'];
						}
						else {
							$rowClass = "";
						}
						$result .= '<tr class="row' . ($rowCount % 2) . $rowClass . '">';
						foreach ($this->_columns as $key => $column) {
							$result .= $column->renderCell($row);
						}
						$result .= '</tr>';
						$rowCount ++;
					}
				}
			}
			$result .= '</table>';

                        $result.=$this->getPaginator();
				
                        if(!empty($this->_html['append'])){
				$result.=implode("",$this->_html["append"]);
			}

			if ($this->getFormId() and $this->getOption('form')) {
				$result.='<input type="hidden" name="_filter" value="'.$this->getFormId().'" />';
				$result .= '<div class="loading"></div></form>';
			}


			

			$result.=$this->renderJavascript();

			$result.='</div><!-- end of SmartTable -->';
			return $result;
		}

		/**
		 * prekladac
		 *
		 * @see Matj_Translate::_()
		 */
		function translate ($text, $params = array()) {
			return $this->getTranslator()->_($this->getOption('translatePrefix') . $text, $params);
		}


		/** stlpec pre triedenie */
		protected $_sortColumn;
		/** sql pre triedenie */
		protected $_sortSql;
		/** zostupne ci vzostupne */
		protected $_sortAsc;
		/**
		 * vrati stlpec podla ktoreho sa triedi
		 */
		function getSortColumn () {
			if ($this->_sortColumn === null) {
				$c = $this->getRequest()->getParam($this->getOption("sortKey"), false);
				if($c){
					$o = true;
					if (preg_match("/\|/", $c)) {
						$sc = explode("|", $c);
						$c = $sc[0];
						$o = $sc[1] == "DESC" ? false : true;
					}
				}
				else{
					if($this->getOption("ajax")){
						$c=$this->getSessionTable()->sort;
						$o=(bool)$this->getSessionTable()->sortAsc;
					}
				}
				if ($sc = $this->getColumn($c)) {
					$this->_sortColumn = $sc->getColumn();
					$this->_sortSql = $sc->getSortSql();
					$this->_sortAsc = $o;

					if($this->getOption("ajax")){
						$this->getSessionTable()->sort=$c;
						$this->getSessionTable()->sortAsc=$o;
					}
				}
				else {
					$this->_sortColumn = false;
					$this->_sortSql = null;
					$this->_sortAsc = true;
				}
			}

			if(!$this->_sortColumn && ($c=$this->getOption('sortDefault'))!=null){
					$sc = $this->getColumn($c);
					$this->_sortColumn = $sc->getColumn();
					$this->_sortSql = $sc->getSortSql();
					$this->_sortAsc = $this->getOption('sortDefaultDesc') ? false : true;
			}

			return $this->_sortColumn;
		}
		/**
		 * Je vzostupne triedenie?
		 * @return bool
		 */
		function isSortAsc () {
			return (bool) $this->_sortAsc;
		}



		function addJavascript($key,$value){
			$this->_javascript[(string)$key]=$value;
			return $this;
		}

		function getJavascript($key){
			return $this->_javascript[(string)$key];
		}

		function renderJavascript(){
			if(empty($this->_javascript))return null;

			if(Matj_Get::getRequest()->isXmlHttpRequest()){
				return '<script type="text/javascript">
					'.implode("\n",$this->_javascript).'
						</script>';
			}
			else{
				$js=implode("\n",$this->_javascript);
				Matj_Get::getView()->headScript()->appendScript($js);
			}
			return '';
		}

		/**
		 *
		 * @return Matj_Paginator_Numbers
		 */
		function getPaginator(){
			if($this->getOption("paginator") && $this->_paginator===null){
				$paginatorClass=$this->getOption("paginatorClass");
				$this->_paginator=new $paginatorClass(array(),$this);
				$this->_paginator->setPerPage($this->getRequest()->getParam($this->getOption("perpageKey"), $this->getOption("perpageDefault")));
				$this->_paginator->setPage($this->getRequest()->getParam($this->getOption("pageKey"), 1));
			}
			return $this->_paginator;
		}


		function addHtml($content,$position=null,$key=null){
			$position = strtolower($position) == "prepend" ? "prepend" : "append";
			if($key){
				$this->_html[$position][$key]=$content;
			}
			else{
				$this->_html[$position][]=$content;
			}
			return $this;
		}


		function getModelOptions(){
			$options['order']=$this->getOrderBy();
			$options['limit']=$this->getLimit();
			$options['offset']=$this->getOffset();
			$options['where']=$this->getWhere();
			return $options;
		}






		protected $_filtersForm;


		function getFiltersForm(){
			if($this->_filtersForm===null){
				$this->_filtersForm=new Matj_Form();
			}
			return $this->_filtersForm;
		}

		function getWhere(){
			$where=null;
			if($this->_filtersForm){
				$form=$this->getFiltersForm();
				$set=0;
				if($this->getRequest()->isPost()){
					$formData=$this->getRequest()->getPost();
					if($formData["_filter"]==$this->getFormId()){
						$form->isValid($formData);
						$this->getSessionTable()->filtersForm=$form->getValues();
						$set=1;
					}
				}
				if(!$set){
					$formData=$this->getSessionTable()->filtersForm;
					if(!empty($formData))
						$form->isValid($formData);
				}



				foreach($this->_columns as $column){
					$where=$column->joinWhere($where);
				}
			}
			return $where;
		}


                
                
                
                
                
                protected $_readGetOptions=array('smartTableColumnOptions'=>'json');
                
                //premenna udrziava informaciu o zmenach
                public $changes=array();
                
                function readGetOptions(){
                    
                    $st=$this->getRequest()->getParam('_smartTable');
                    if(!empty($st[$this->_id])){
                        foreach($st[$this->_id] as $k=>$d){
                            if(key_exists($k, $this->_readGetOptions)){
                                
                                
                                // ak je JSON rozkoduje a zakoduje ho
                                if($this->_readGetOptions[$k]=="json"){
                                    $a=Zend_Json::decode(stripslashes($d));
                                    if(is_array($a))
                                        $d=Zend_Json::encode($a);
                                    else
                                        $d=null;
                                    $this->getSessionTable()->{$k}=$d;
                                    $this->changes[$k]=true;
                                }
                            }
                        }
                    }
                    
                }
                
                
                
                function resetFilters(){
                    $this->getFiltersForm()->reset();
                    $this->getSessionTable()->filtersForm=$this->getFiltersForm()->getValues();
                    
                            
                }
}
