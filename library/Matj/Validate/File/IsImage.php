<?php
require_once 'Zend/Validate/File/IsImage.php';

class Matj_Validate_File_IsImage extends Zend_Validate_File_IsImage
{
    
    public function __construct($options){
        foreach($this->_messageTemplates as $k=>$v){
            $key='validator.'.$k;
            //echo $key." ".$v."<br>";
            $this->_messageTemplates[$k]=Matj_Get::getTranslator()->_($key);
        }
        return parent::__construct($options);
    }
    
}
