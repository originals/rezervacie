<?php

require_once 'Zend/Validate/File/Extension.php';

class Matj_Validate_File_Extension extends Zend_Validate_File_Extension
{
    public function __construct($options){
        foreach($this->_messageTemplates as $k=>$v){
            $key='validator.'.$k;
            //echo $key." ".$v."<br>";
            $this->_messageTemplates[$k]=Matj_Get::getTranslator()->_($key);
        }
        return parent::__construct($options);
    }
   
}
