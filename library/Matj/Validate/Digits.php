<?php
/**
 * @category   Delphi
 * @package    Matj_Validate
 * @version    $Id: EmailAddress.php
 * @author 	matus.jancik@gmail.com
 */

/**
 * @see Zend_Validate_Abstract
 */
require_once 'Zend/Validate/Digits.php';

class Matj_Validate_Digits extends Zend_Validate_Digits
{

	const NOT_EMAIL = 'notEmail';

    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_EMAIL => "'%value%' is not valid email address"
    );
	
    
    public function getMessages() {
		$this->_messages = array($this->getTranslator()->_("validator.notDigits"));
    	return $this->_messages;
    }	
    
    
}
