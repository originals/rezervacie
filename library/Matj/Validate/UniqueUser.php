<?php
/**
 * @see Zend_Validate_Abstract
 */
require_once 'Zend/Validate/Abstract.php';

/**
 * @category   Zend
 * @package    Zend_Validate
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Matj_Validate_UniqueUser extends Zend_Validate_Abstract
{
    const INPUT_ERROR = 'inputError';
	const NOT_FIELDS = 'notFields';
    const USER_EXIST = 'userExist';
    
    protected $_users=array();
    
    
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::INPUT_ERROR => "Do validátora musí vstupovať model Užívateľ, alebo pole.",
        self::NOT_FIELDS => "Vstup musí obsahovať meno, priezvisko a dátum narodenia.",
        self::USER_EXIST => "Podobný používateľ už existuje. <a href='/pomoc'>Čo to znamená?</a>",
    );
	
    
    public function isValid($value)
    {
        $this->_setValue( $value);
		
       	if($value instanceof Core_Model_User){
       		$value=$value->getData();
       	}
       	if(!is_array($value)){
       		$this->_error(self::INPUT_ERROR);
            return false;		
       	}
        
       	
       	$dateValidator=new Matj_Validate_BornDate();
       	
       	if(empty($value["user_name"]) || empty($value["user_surname"]) || !$dateValidator->isValid($value["user_born"])){
       		$this->_error(self::NOT_FIELDS);
            return false;
       	}
       	
       	
       	$n=substr(Matj_String::removeAccents($value["user_name"]),0,3);
       	$sn=substr(Matj_String::removeAccents($value["user_surname"]),0,3);
       	$d=$value["user_born"];
       	
       	$select=Matj_Get::getDbAdapter()->select();
       	$select->from('users');
       	$select->where('user_name collate utf8_general_ci LIKE "'.$n.'%"');
       	$select->where('user_surname collate utf8_general_ci LIKE "'.$sn.'%"');
       	$select->where('user_born = "'.$d.'"');
		
       	//echo $select;exit;
       	
       	$data=Matj_Get::getDbAdapter()->fetchAll($select);
       	if(empty($data))
       		return true;
		
       	else{
	       	$this->_users=$data;	
	       	$this->_error(self::USER_EXIST);
	        return false;
       	}
    }
    
    function getUsers(){
    	return $this->_users;
    }
}
