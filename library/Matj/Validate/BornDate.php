<?php
/**
 * @see Zend_Validate_Abstract
 */
require_once 'Zend/Validate/Abstract.php';

/**
 * @category   Zend
 * @package    Zend_Validate
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Matj_Validate_BornDate extends Zend_Validate_Abstract
{
    const NOT_BORNDATE = 'notBornDate';

    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_BORNDATE => "'%value%' is not valid date of born"
    );
	
    
    public function isValid($value)
    {
        $this->_setValue((string) $value);
		
       
        
        $v=explode("-",$this->_value);
        
        if(count($v)!=3)return false;
        
	if (!checkdate($v[1], $v[2], $v[0])) {
            $this->_error(self::NOT_BORNDATE);
            return false;
        }

        return true;
    }
    
    public function getMessages() {
		$this->_messages = array($this->getTranslator()->_("validator.notBornDate"));
    	return $this->_messages;
    }	
    
}
