<?php
/**
 * @category   Delphi
 * @package    Matj_Validate
 * @version    $Id: EmailAddress.php
 * @author 	matus.jancik@gmail.com
 */

/**
 * @see Zend_Validate_Abstract
 */
require_once 'Zend/Validate/EmailAddress.php';

class Matj_Validate_EmailAddress extends Zend_Validate_EmailAddress
{

	const NOT_EMAIL = 'notEmail';

    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_EMAIL => "'%value%' is not valid email address"
    );
	
    
    public function getMessages() {
		$this->_messages = array($this->getTranslator()->_("validator.notEmail"));
    	return $this->_messages;
    }	
    
    
}
