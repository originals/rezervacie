<?php


class Matj_Validate_NotEmpty extends Zend_Validate_NotEmpty
{

	
    public function __construct($options = null)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        } else if (!is_array($options)) {
            $options = func_get_args();
            $temp    = array();
            if (!empty($options)) {
                $temp['type'] = array_shift($options);
            }

            $options = $temp;
        }

        if (is_array($options) && array_key_exists('type', $options)) {
            $this->setType($options['type']);
        }
        
        
        $this->_messageTemplates = array(
        	self::IS_EMPTY => $this->getTranslator()->_("validator.notEmpty"),//"Value is required and can't be empty",
        	self::INVALID  => $this->getTranslator()->_("validator.notEmpty.format"),
   		);
    }
    
    
    
    

}