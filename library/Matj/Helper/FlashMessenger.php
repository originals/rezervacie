<?php
/**
 * CMS
 */
 
class Matj_Helper_FlashMessenger
{
    /**
     * $_messages - Messages from previous request
     *
     * @var array
     */
    static protected $_messages = array();

    /**
     * $_session - Zend_Session storage object
     *
     * @var Zend_Session 
     */
    static protected $_session = null;

    /**
     * $_messageAdded - Wether a message has been previously added
     *
     * @var boolean
     */
    static protected $_messageAdded = false;

    /**
     * $_namespace - Instance namespace, default is 'default'
     *
     * @var string
     */
    protected $_namespace = 'messages';

    /**
     * __construct() - Instance constructor, needed to get iterators, etc
     *
     * @param  string $namespace
     * @return void
     */
    public function __construct()
    {
        
        
    	if (!self::$_session instanceof Matj_Session ) {
            self::$_session = new Matj_Session($this->getName());
         	if(is_array(self::$_session->get($this->_namespace))){
         		foreach (self::$_session->get($this->_namespace) as $key => $messages) {
	                self::$_messages[$this->_namespace][$key] = $messages;
	            }
            }

        }
    }

    /**
     * setNamespace() - change the namespace messages are added to, useful for
     * per action controller messaging between requests
     *
     * @param  string $namespace
     * @return Zend_Controller_Action_Helper_FlashMessenger Provides a fluent interface
     */
    public function setNamespace($namespace = 'defaultMsg')
    {
        $this->_namespace = $namespace;
        return $this;
    }

    public function getName()
    {
       return  $this->_namespace;
    }

    /**
     * resetNamespace() - reset the namespace to the default
     *
     * @return Zend_Controller_Action_Helper_FlashMessenger Provides a fluent interface
     */
    public function resetNamespace()
    {
        $this->setNamespace();
        return $this;
    }

    /**
     * addMessage() - Add a message to flash message
     *
     * @param  string $message
     * @param  string $class
     * @return Zend_Controller_Action_Helper_FlashMessenger Provides a fluent interface
     */
    public function addMessage($message,$class="")
    {
        
    	if (!is_array(self::$_session->get("messages"))) {
            self::$_session->set("messages",array());
        }
	    $newArray = self::$_session->get("messages");
		$newArray[] = array("msg"=>$message,"class"=>$class);
        self::$_messages[$this->_namespace] = $newArray;
        self::$_session->set("messages",$newArray);
			
		return $this;
    }

	public function __toString(){		
		$result="";
	    if($this->hasMessages()){
	    	$type="div";
        	if($type=="ul"){
			    $result = "<ul class=\"".$this->_namespace."\">";
				foreach ($this->getMessages() as $k => $value){
					$result.='<li class="'.$value["class"].'">'.$value["msg"].'</li>';
				}		
				$result.="</ul>";
        	}		
			else{
				
				
				$result = "<div class=\"flash ".$this->_namespace."\">";
				foreach ($this->getMessages() as $k => $value){
					switch($value["class"]){
						case "success": $class="notice"; break;
						default: $class=$value["class"]; break;
					}
					
					$result.='<div class="message '.$class.' alert alert-'.$value["class"].'"><p>'.$value["msg"].'</p></div>';
				}		
				$result.="</div>";
			}
	    }
		$this->clearMessages();	
		return $result;
		
	}


	public function printMessages($clearMessages=true){		
		$string=$this->__toString();
		if($clearMessages){
			$this->clearMessages();	
		}
		return $string;
	}
    /**
     * hasMessages() - Wether a specific namespace has messages
     *
     * @return boolean
     */
    public function hasMessages()
    {
    	return isset(self::$_messages[$this->_namespace]);
    }

    /**
     * getMessages() - Get messages from a specific namespace
     *
     * @return array
     */
    public function getMessages()
    {
        if ($this->hasMessages()) {
            return self::$_messages[$this->_namespace];
        }
		return array();
    }

    /**
     * Clear all messages from the previous request & current namespace
     *
     * @return boolean True if messages were cleared, false if none existed
     */
    public function clearMessages()
    {
        if ($this->hasMessages()) {
            unset(self::$_messages[$this->_namespace]);
			self::$_session->set("messages",array());
            return true;
        }

        return false;
    }

    /**
     * hasCurrentMessages() - check to see if messages have been added to current
     * namespace within this request
     *
     * @return boolean
     */
    public function hasCurrentMessages()
    {
        return isset(self::$_session->{$this->_namespace});
    }

    /**
     * getCurrentMessages() - get messages that have been added to the current
     * namespace within this request
     *
     * @return array
     */
    public function getCurrentMessages()
    {
        if ($this->hasCurrentMessages()) {
            return self::$_session->{$this->_namespace};
        }

        return array();
    }

    /**
     * clear messages from the current request & current namespace
     *
     * @return boolean
     */
    public function clearCurrentMessages()
    {
        if ($this->hasCurrentMessages()) {
            unset(self::$_session->{$this->_namespace});
            return true;
        }
        
        return false;
    }
    
    /**
     * getIterator() - complete the IteratorAggregate interface, for iterating
     *
     * @return ArrayObject
     */
    public function getIterator()
    {
        if ($this->hasMessages()) {
            return new ArrayObject($this->getMessages());
        }

        return new ArrayObject();
    }

    /**
     * count() - Complete the countable interface
     *
     * @return int
     */
    public function count()
    {
        if ($this->hasMessages()) {
            return count($this->getMessages());
        }

        return 0;
    }

    /**
     * Strategy pattern: proxy to addMessage()
     * 
     * @param  string $message 
     * @return void
     */
    public function direct($message,$class="")
    {
        return $this->addMessage($message,$class);
    }
}
