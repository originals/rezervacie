<?php

/**
 * Trieda pre posielanie mailov
 * @package Core
 * @author Matus Jancik
 * @since 2011
 */

require_once 'Email/class.phpmailer.php';

class Matj_Email extends PHPMailer_5_1{
		
	protected $_config;
	
	function __construct($exceptions = false){
		parent::__construct($exceptions);
	}

	
	function setConfig($config){
		$this->_config=$config;
	}
	function getConfig(){
		if($this->_config===null){
			$this->_config=(Matj_Get::getBootstrap()->getOption("email") );
		}
		return $this->_config;
	}
	
    function sendEmail($data){
		try { 
	        if(!is_array($data["to"])){
				if(preg_match("/\,/",$data["to"])){
				    $to=array();
				    foreach(explode(",",$data["to"]) as $m){
				        $to[]=$m;
				    }   
				}
	            else{
    				$to=array($data["to"]);
	            }
			}
			else{
			    $to=$data["to"];
			}
			
			
			$mail=$this;
			
			$mail->Subject=$data["subject"];
            $mail->IsHTML(true);
            $mail->CharSet="utf-8";
			$this->exceptions =true;
            
			$config=$this->getConfig();
//Zend_Debug::dump($config);exit;
			
			if(!empty($config) && !empty($config["smtp"]["enable"])){
                $mail->IsSMTP();
                $mail->SMTPAuth = !empty($config["smtp"]["auth"]);
                $mail->Host = $config["smtp"]["host"];
                $mail->Username = $config["smtp"]["username"];
                $mail->Password = $config["smtp"]["password"];
                $mail->Port = !empty($config["smtp"]["port"]) ? $config["smtp"]["port"] : 25;
                $mail->Secure = !empty($config["smtp"]["SMTPSecure"]) ? $config["smtp"]["SMTPSecure"] : "";
            }      
            
        
            $mail->From = $data["from"];
            $mail->FromName = $data["from_name"];
            $mail->Body=$data["body"];
    		
            if(!empty($data["altBody"]))
    			$mail->altBody=$data["altBody"];
    				
    			foreach($to as $val){
    	        	$mail->ClearAddresses();
                    $mail->ClearBCCs();
                    $mail->addAddress($val);
                
    		        if(!empty($data["file"])){
    		            if(is_array($data["file"])){
    		                foreach($data["file"] as $k=>$d){
                                    if(empty($d["file"]) || empty($d["name"]))
                                        throw new Exception('Spravne file[]=array(file,name)');
                                    
    		                    if(file_exists($d["file"])){
    		                        if(!$mail->addAttachment($d["file"],$k["name"])){
                                            die("file add error");
                                        }
    		                        
    		                    }
    		                }
    		            }
    		        }
                        
                        if(!empty($data["string_attachment"])){
    		            if(is_array($data["string_attachment"])){
    		                foreach($data["string_attachment"] as $k=>$d){
                                    
                                    $mail->addStringAttachment($d["content"],$d["name"]);

    		                }
    		            }
    		        }
                        
    		        if(!$mail->Send()){
    		            $error=1;
						throw new Zend_Exception($mail->ErrorInfo);
    		        }
                
    			}
    	    } catch (Exception $e) { 
    	       	$this->setError($e->getMessage());
    	       	return false;
    		} 
    	    return true;
        }
        
        
        
	
	
	
}
