<?php

class Matj_Sms {
	
    const API_URL = "http://s.originals.sk/facebook/api/sms";
    
    static public $config=array('apikey'=>0,'sandbox'=>false);
    
    
    static public $error,$errorCode,$status;
    
    static function setConfig($config){
        self::$config=$config;
    }
    
    
    static function send($number,$message){
        self::$error=null;
        self::$errorCode=null;
        self::$status=null;
        
        $url=self::API_URL."/send?apikey=".self::$config["apikey"]."&number=".$number."&message=".  urlencode($message);
        if(self::$config["sandbox"])
            $url.="&sandbox=1";
        
           
        $json=file_get_contents($url);
        $data=  json_decode($json);
        
        if($data->code==0){
            self::$status=$data->status;
            return true;
        }
        else{
            self::$error=$data->error;
            self::$errorCode=$data->code;
            return false;
        }
    }
    
    static function getInfo($attribs=array()){
        $url=self::API_URL."/info?apikey=".self::$config["apikey"];
        
        if(!empty($attribs["tables"]))
            $url.='&tables=1';
        
        
        $json=file_get_contents($url);
        return (array)json_decode($json);
    }
    
    
    function getErrorCode(){
        return self::$errorCode;
    }
    
    function getError(){
        return self::$error;
    }
    
    
}



