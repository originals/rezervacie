<?php

/**
 * Zend Image
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 *
 * @category   Zend
 * @package    Matj_Image
 * @author     Stanislav Seletskiy <s.seletskiy@gmail.com>
 * @author     Leonid Shagabutdinov <leonid@shagabutdinov.com>
 * @copyright  Copyright (c) 2010
 * @license    http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @version    $Id: Exception.php 45 2010-02-24 16:51:47Z s.seletskiy $
 */

/**
 * @see Matj_Image_Exception
 */
require_once 'Matj/Image/Exception.php';

/**
 * @category   Zend
 * @package    Matj_Image
 * @copyright  Copyright (c) 2010
 * @license    http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
class Matj_Image_Transform_Exception extends Matj_Image_Exception
{
    
}
