<?php

class Matj_Image_Driver_Gd2 extends Matj_Image_Driver_Abstract {
    
    protected $_quality = 90;
    
    
    function setQuality($q){
        $this->_quality=$q;
    }
    
    /**
     * Load image from $fileName
     *
     * @throws Matj_Image_Driver_Exception
     * @param string $fileName Path to image
     */
    public function load($fileName) {
        parent::load($fileName);

        $this->_imageLoaded = false;

        $info = getimagesize($fileName);
        switch ($this->_type) {
            case 'jpg':
                $this->_image = imageCreateFromJpeg($fileName);
                if ($this->_image !== false) {
                    $this->_imageLoaded = true;
                }
                break;
            case 'png':
                $this->_image = imageCreateFromPng($fileName);
                if ($this->_image !== false) {
                    $this->_imageLoaded = true;
                }
                break;
            case 'gif':
                $this->_image = imageCreateFromGif($fileName);
                if ($this->_image !== false) {
                    $this->_imageLoaded = true;
                }
                break;
        }
    }

    /**
     * Get image size
     *
     * @throws Matj_Image_Driver_Exception
     * @return array Format: array( width, height )
     */
    public function getSize() {
        if ($this->_image === null) {
            throw new Matj_Image_Driver_Exception(
                    'Trying to get size of non-loaded image'
            );
        }

        return array(
            imagesx($this->_image),
            imagesy($this->_image)
        );
    }

    /**
     * Get image contents
     *
     * @return string
     */
    public function getBinary() {
        if ($this->_image === null) {
            throw new Matj_Image_Driver_Exception(
                    'Trying to get size of non-loaded image'
            );
        }

        ob_start();
        imagejpeg($this->_image,null,$this->_quality);
        return ob_get_clean();
    }

    /**
     *
     * @throws Matj_Image_Driver_Exception
     * @param string $file File name to save image
     * @param string $type File type to save
     */
    public function save($file, $type = 'auto') {
        if (!$this->_image) {
            throw new Matj_Image_Driver_Exception(
                    'Trying to save non-loaded image'
            );
        }

        if ($type == 'auto') {
            $type = $this->getType();
        }

        switch ($type) {
            case 'jpg': case 'jpeg':
                imagejpeg($this->_image, $file,$this->_quality);
                break;
            case 'png':
                imagepng($this->_image, $file);
                break;
            case 'gif':
                imagegif($this->_image, $file);
                break;
            default:
                throw new Matj_Image_Driver_Exception(
                        'Undefined image type: "' . $type . '"'
                );
                break;
        }
    }

    /**
     * Resize image into specified width and height
     *
     * @throws Matj_Image_Driver_Exception
     * @param int $width Width to resize
     * @param int $height Height to resize
     * @return bool
     */
    public function resize($width, $height) {
        parent::resize($width, $height);

        $imageSize = $this->getSize();
        $resizedImage = imagecreatetruecolor($width, $height);
        $resizedImage = $this->createTransparent($resizedImage);

        $successfull = imagecopyresampled(
                $resizedImage, $this->_image, 0, 0, 0, 0, $width, $height, $imageSize[0], $imageSize[1]
        );

        unset($this->_image);
        $this->_image = $resizedImage;






        return $successfull;
    }

    function createTransparent($result) {

        switch ($this->getType()) {
            case 'png':
                imagealphablending($result, false);
                $colorTransparent = imagecolorallocatealpha($result, 0, 0, 0, 127);
                imagefill($result, 0, 0, $colorTransparent);
                imagesavealpha($result, true);
                break;
            case 'gif':
                $trnprt_color = imagecolorsforindex($result, $trnprt_indx);
                $trnprt_indx = imagecolorallocate($result, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
                imagefill($result, 0, 0, $trnprt_indx);
                imagecolortransparent($result, $trnprt_indx);
                break;
        }

        return $result;
    }

    /**
     * Crop image into specified frame
     *
     * @throws Matj_Image_Driver_Exception
     * @param int $left Left point to crop from
     * @param int $top Top point to crop from
     * @param int $width Width to crop
     * @param int $height Height to crop
     * @return bool
     */
    public function crop($left, $top, $width, $height) {
        parent::crop($left, $top, $width, $height);

        $imageSize = $this->getSize();
        $croppedImage = imagecreatetruecolor($width, $height);
        $this->createTransparent($croppedImage);

        $successfull = imagecopyresized(
                $croppedImage, $this->_image, 0, 0, $left, $top, $width, $height, $left + $width, $top + $height
        );

        unset($this->_image);
        $this->_image = $croppedImage;

        return $successfull;
    }

    
    function insertImage(Matj_Image_Transform $image, $left = 0, $top = 0,$destw=0,$desth=0) {
        //bool imagecopyresampled ( resource $dst_image , resource $src_image , int $dst_x , int $dst_y , int $src_x , int $src_y , int $dst_w , int $dst_h , int $src_w , int $src_h )
        
        $size=$image->getSize();
        $srcw=$destw=$size[0];
        $srch=$desth=$size[1];
        
        if(empty($destw))$destw=$srcw;
        if(empty($desth))$desth=$srch;
        
        $dst=$this->_image;
        $r=imagecopyresampled($dst,$image->getResource(),$left,$top,0,0,$destw,$desth,$srcw,$srch);
        
        if($r)
            $this->_image=$dst;
        return $this;
    }
    
    
    
    function insertText($text,$size=20,$x=10,$y=30,$color=0x000000,$font="arial",$angle=0){
        //imagettftext($im, $size, $angle, $left, $right, $color, $font, $text);
        $c=(Matj_Get::getBootstrap()->getOption('includePaths'));
        $img=$this->_image;
        if(!strpos(".",$font))$font=$c["library"].DIRECTORY_SEPARATOR."fonts/".$font.".ttf";
       
        imagettftext($img, $size, $angle, $x, $y, $color, $font, $text);
        
        $this->_image=$img;
        return $this;
        
    }
    
    
    
    function getColor($r,$g,$b){
        return imagecolorallocate($this->_image, $r, $g, $b);
    }
    
    /**
    * Funkcia na vlozenie a vycentrovanie textu do bloku
    * 
    * 
    * @param int $size Velkost pisma
    * @param int $angle
    * @param int $left
    * @param int $top
    * @param int $color Farba ziskana z imagecolorallocate
    * @param string $font cesta k fontu ttf
    * @param string $text
    * @param int $max_width maximalna sirka bloku
    * @param string $align left|justify zarovnanie textu
    * @param int $minspacing
    * @param int $linespacing
    * @return boolean
    */
   function imageTextToBlock($size, $angle, $left, $top, $color, $font, $text, $max_width, $align = "left", $minspacing = 3, $linespacing = 1.5) {
       $debug=false;
       
       $image=$this->getResource();
       $wordwidth = array();
       $linewidth = array();
       $linewordcount = array();
       $largest_line_height = 0;
       $lineno = 0;
       $text=str_replace("\n"," \n",$text);
       $words = explode(" ", $text);
       $wln = 0;
       $linewidth[$lineno] = 0;
       $linewordcount[$lineno] = 0;
       foreach ($words as $word) {
           
           if($debug)echo $word."<br>";
           
           $lnsHelp=explode("\n",$word);
           
           if(count($lnsHelp)>1){
                echo "break line<br>";
                $word=str_replace(array("\n","\r"),"",trim($word));
                
                $dimensions = imagettfbbox($size, $angle, $font, $word);
                $line_width = $dimensions[2] - $dimensions[0];
                $line_height = $dimensions[1] - $dimensions[7];
                
                if ($line_height > $largest_line_height)
                    $largest_line_height = $line_height;
                
                $lineno++;
                $linewidth[$lineno] = 0;
                $linewordcount[$lineno] = 0;
                $wln = 0;
                
           }
           else{
                $dimensions = imagettfbbox($size, $angle, $font, $word);
                $line_width = $dimensions[2] - $dimensions[0];
                $line_height = $dimensions[1] - $dimensions[7];
                if ($line_height > $largest_line_height)
                    $largest_line_height = $line_height;
                
                if (($linewidth[$lineno] + $line_width + $minspacing) > $max_width) {
                                   if($debug) echo "break size<br>";

                    $lineno++;
                    $linewidth[$lineno] = 0;
                    $linewordcount[$lineno] = 0;
                    $wln = 0;
                }
           }
           
           if($debug)echo "<br>size:".$line_width." ".$line_height."<hr>";
           
           
           $linewidth[$lineno]+=$line_width + $minspacing;
           $wordwidth[$lineno][$wln] = $line_width;
           $wordtext[$lineno][$wln] = $word;
           $linewordcount[$lineno]++;
           $wln++;
       }
       
      if($debug){
            Zend_Debug::dump($linewidth);
            Zend_Debug::dump($wordwidth);
            Zend_Debug::dump($wordtext);exit;
      }
      
       for ($ln = 0; $ln <= $lineno; $ln++) {
           $slack = $max_width - $linewidth[$ln];
           if (isset($_GET["justify"])==true && ($linewordcount[$ln] > 1) && ($ln != $lineno))
               $spacing = ($slack / ($linewordcount[$ln] - 1));
           else
               $spacing = $minspacing;

           $x = 0;
           for ($w = 0; $w < $linewordcount[$ln]; $w++) {
               imagettftext($image, $size, $angle, $left + intval($x), $top + $largest_line_height + ($largest_line_height * $ln * $linespacing), $color, $font, $wordtext[$ln][$w]);
               $x+=$wordwidth[$ln][$w] + $spacing + $minspacing;
           }
       }
       $this->_image=$image;
       return $this;
   }

    
    
    
    
    
    
    
}
