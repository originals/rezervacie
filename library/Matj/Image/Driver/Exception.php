<?php

/**
 * @see Matj_Image_Exception
 */
require_once 'Matj/Image/Exception.php';

/**
 * Exception for Matj_Image_Driver*.
 *
 * @category    Zend
 * @package     Matj_Image
 * @subpackage  Matj_Image_Driver
 * @author      Stanislav Seletskiy <s.seletskiy@office.ngs.ru>
 * @author      Leonid A Shagabutdinov <leonid@shagabutdinov.com>
 */
class Matj_Image_Driver_Exception extends Matj_Image_Exception
{
}
