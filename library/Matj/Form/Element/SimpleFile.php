<?php
require_once 'Zend/Form/Element/Xhtml.php';

class Matj_Form_Element_SimpleFile extends Zend_Form_Element_Text
{
     /**
     * Use formButton view helper by default
     * @var string
     */
    public $helper = 'formFile';
    
}
