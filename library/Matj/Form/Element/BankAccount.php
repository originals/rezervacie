<?php
require_once 'Zend/Form/Element.php';

class Matj_Form_Element_BankAccount extends Zend_Form_Element
{
    /**
     * Default form view helper to use for rendering
     * @var string
     */
    public $helper = 'formBankAccount';
    
    
     function getValue(){
    	$value=parent::getValue();
    	return $value;
    }
    
    function setValue($value){
    	if(is_array($value)){
    		$value=sprintf("%06d-%010d/%04d",$value["prefix"],$value["account"],$value["bank"]);
    	}
    	
    	
    	
    	parent::setValue($value);
    }
    
    function isValid($value){

    	$this->setValue($value);
        $valid=true;
        if(!$this->isRequired())return true;
        
        
        if(empty($value))$valid=false;

        foreach($value as $k=>$d){
            if(empty($d))$valid=false;    
        }
        
        if(!$valid){
            $this->addError($this->getTranslator()->_("validator.banknumber"));
            return false;
        }
        return true;
    }
   
    
    
}
