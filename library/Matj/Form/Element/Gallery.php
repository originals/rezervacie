<?php
class Matj_Form_Element_Gallery extends Zend_Form_Element_Textarea
{
  public $helper = "formGallery";
  public $options;
  
  public function __construct($image_name, $attributes) {
    
    parent::__construct($image_name, $attributes);
  }
  
  function setValue($value){
      $this->setIsArray(true);
      
      $value=stripslashes($value);
      
      if(is_array($value)){
          $value=json_encode($value);
      }
      
      
      
      
      return parent::setValue($value);
  }
  
  
  
  /**
     * Retrieve filtered element value
     *
     * @return mixed
     */
    public function getValue()
    {
        
        $this->setIsArray(true);
        $valueFiltered = $this->_value;

        $valueFiltered=  json_decode(stripslashes($valueFiltered),true);
        
        
        if ($this->isArray() && is_array($valueFiltered)) {
            array_walk_recursive($valueFiltered, array($this, '_filterValue'));
        } else {
            $this->_filterValue($valueFiltered, $valueFiltered);
        }

        if($this->getAttrib('json') && is_array($valueFiltered))
            $valueFiltered=json_encode($valueFiltered);
        
        return $valueFiltered;
    }
  
}