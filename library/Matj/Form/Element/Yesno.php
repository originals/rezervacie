<?php
require_once 'Zend/Form/Element/Radio.php';


class Matj_Form_Element_Yesno extends Zend_Form_Element_Radio {
	
	public function __construct ($spec, $options = null) {
		if (empty($options["multiOptions"])) {
			$options["multiOptions"] = array ( 
				'0' => $this->getTranslator()->_("core.no"), 
				 1 => $this->getTranslator()->_("core.yes") 
			);
		}
		return parent::__construct($spec,$options);	
	}
	
	function setValue($value){
		return parent::setValue($value);
	}

}
