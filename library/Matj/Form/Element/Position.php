<?php
require_once 'Zend/Form/Element.php';

class Matj_Form_Element_Position extends Zend_Form_Element
{
    /**
     * Default form view helper to use for rendering
     * @var string
     */
    public $helper = 'position';
    
    
    function getValue(){
    	$value=parent::getValue();
    	if(is_array($value)){
    	    $data=array($value["lat"]+0,$value["lng"]+0);
			if(!empty($value["zoom"]))
				$data[]=$value["zoom"]+0;
			$value=implode(",",$data);
    	}
    	return $value;
    }
    
    function setValue($value){
    	if(is_array($value)){
    	    $data=array($value["lat"]+0,$value["lng"]+0);
			if(!empty($value["zoom"]))
				$data[]=$value["zoom"]+0;
			$value=implode(",",$data);
    	}
        parent::setValue($value);
    }
    
    function isValid($value){
        $this->setValue($value);
        $valid=true;
        if(!$this->isRequired())return true;
        
        
        if(empty($value))$valid=false;

        foreach($value as $k=>$d){
            if(empty($d))$valid=false;    
        }
        
        if(!$valid){
            $this->addError($this->getTranslator()->_("validator.position"));
            return false;
        }
        return true;
    }
   
    
    
}
