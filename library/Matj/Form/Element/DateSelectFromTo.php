<?php
require_once 'Zend/Form/Element.php';

class Matj_Form_Element_DateSelectFromTo extends Zend_Form_Element_Multi
{
    /**
     * Default form view helper to use for rendering
     * @var string
     */
    public $helper = 'formDateSelectFromTo';
    
    
    /**
     * Retrieve filtered element value
     *
     * @return mixed
     */
    public function getValue()
    {
        
        $this->setIsArray(true);
        $valueFiltered = $this->_value;

        if ($this->isArray() && is_array($valueFiltered)) {
            array_walk_recursive($valueFiltered, array($this, '_filterValue'));
        } else {
            $this->_filterValue($valueFiltered, $valueFiltered);
        }

        return $valueFiltered;
    }
    
    function setValue($value){
        
        
        //Zend_Debug::dump($value);exit;
        
        if(is_array($value)){
            foreach($value as $k=>$v){
               	if(is_array($v)){
                        if($this->getAttrib("time")){
                                $value[$k]=sprintf("%04d-%02d-%02d %02d:%02d:%02d",$v["year"],$v["month"],$v["day"],$v["hour"],$v["minutes"],0);
                        }
                        else{
                                $value[$k]=sprintf("%04d-%02d-%02d",$v["year"],$v["month"],$v["day"]);
                        }
                }
                else{
                    $value[$k]=$v;
                }
            }
        }
        
        
        parent::setValue($value);
    }
    
    function isValid($value){
        
        
        
        $this->setValue($value);
        
        
        //$value=$this->getValue();
        
        $valid=true;
	
        

        if(is_array($value)){
            foreach($value as $key=>$val){
                $t=$val["day"].$val["month"].$val["year"];
                
                if(!$this->isRequired() && empty($t)){
                    //true
                }
                else{
                    
                    if(is_array($val)){
                        foreach($val as $k=>$d){
                                if(empty($d) && in_array($k,array('day','month','year'))){
                                      $valid=false;    
                                }
                        }

                                    //Zend_Debug::dump($value);exit;


                        if($valid && !checkdate($val["month"], $val["day"], $val["year"]))
                        {
                               $valid=false;
                        }
                    }
                    else{
                        if(!empty($val) && $val < '1901-01-01')
                            $valid=false;
                        
                        
                    }
                    
                    
                }
                
            }
        }
        
        
        if(!$valid){
            $this->addError($this->getTranslator()->_("validator.date"));
            return false;
        }
        return true;
    }
   
    
    
}
