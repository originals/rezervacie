<?php
require_once 'Zend/Form/Element.php';

class Matj_Form_Element_DateSelect extends Zend_Form_Element
{
    /**
     * Default form view helper to use for rendering
     * @var string
     */
    public $helper = 'formDateSelect';
    
    
     function getValue(){
    	$value=parent::getValue();
    	return $value;
    }
    
    function setValue($value){
    	if(is_array($value)){
    		if($this->getAttrib("time")){
    			$value=sprintf("%04d-%02d-%02d %02d:%02d:%02d",$value["year"],$value["month"],$value["day"],$value["hour"],$value["minutes"],0);
    		}
    		else{
	    		$value=sprintf("%04d-%02d-%02d",$value["year"],$value["month"],$value["day"]);
    		}
    	}
    	parent::setValue($value);
    }
    
    function isValid($value){
        $this->setValue($value);
        $valid=true;
		$t=$value["day"].$value["month"].$value["year"];
        if(!$this->isRequired() && empty($t))return true;
        
        
        if(empty($value))$valid=false;

        foreach($value as $k=>$d){
            if(empty($d) && in_array($k,array('day','month','year'))){
				$valid=false;    
				//echo "Y";exit;
				
			}
        }
        
		//Zend_Debug::dump($value);exit;
		
		
        if($valid && !checkdate($value["month"], $value["day"], $value["year"]))
        {
		//		echo "X";
				$valid=false;
		}
        
        
        if(!$valid){
            $this->addError($this->getTranslator()->_("validator.date"));
            return false;
        }
        return true;
    }
   
    
    
}
