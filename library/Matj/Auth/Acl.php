<?php

class Matj_Auth_Acl {
    
    /**
     *
     * @var Matj_Auth_Session
     */
    protected $auth;
    
    
    
    function __construct($auth) {
        $this->auth=$auth;
    }
    
    
    function write(Matj_Model_Abstract $model,$user=null){
        if($user===null)
            $user=$this->auth->getUser();
        
        return $model->access($user,'write');
        
        
    }
    

}