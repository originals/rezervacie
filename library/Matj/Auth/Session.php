<?php

class Matj_Auth_Session {
    
    protected $_config;
    protected $_session;
    protected $_user;
    protected $_acl;
    protected $_errors;
    /**
     * 
     * @param string $key
     * @return array
     */
    function getConfig($key = null){
        
        if($this->_config==null)
            $this->_config=Matj_Get::getBootstrap()->getOption('auth');
        
        if($key!==null)
            return key_exists ($key, $this->_config) ? $this->_config[$key] : null;
        
        return $this->_config;
    }
    
    function getSession(){
        if($this->_session==null)
            $this->_session=new Zend_Session_Namespace($this->getConfig("sessionName"));
        return $this->_session;
    }
    
    
    function __construct() {
        $ac=Matj_Get::getBootstrap()->getOption('auth');
        
        

    }
    
    function isAuthenticated(){
        return $this->getUser() && !$this->getUser()->isEmpty();
    }
        
    
    function getUser(){
        if($this->_user==null){
            
            if(!$this->getSession()->user){
                if($this->getLoginCookie()!==null){
                    
                    
                    
                    $model=new Model_LogLogin();
                    $model->fetchOne($this->getLoginCookie());
                    
                    if(!$model->isEmpty() && $model->user_id>0){
                        $user=array('id'=>$model->user_id);
                        //prihlasi 
                    }
                    else{
                        return false;
                    }
                }
                else{
                    return false;
                }
            }
            else{
                $user=$this->getSession()->user;
            }
            
            
            
            
            $class=$this->getConfig('userClass');
            $this->_user=new $class();
            $this->_user->getAuthUser($user);
        }
        return $this->_user;
    }
    
    function getLoginUrl($return=null){
        
        
        try{
            $loginUrl=url(array(),'login',true);
        } catch (Exception $ex) {
            $loginUrl=$this->getConfig('loginUrl');
        }
        
        
        
        
        if(empty($loginUrl))
            $loginUrl='/sk/login';
        
        if($return)$loginUrl.="?return=".urlencode($return);
        
        
        
        return $loginUrl;
    }
        
    function setReturnUrl($url){
        $this->getSession()->returnUrl=urldecode($url);
    }
    
    function getReturnUrl(){
        
        $returnUrl=$this->getSession()->returnUrl;
        
        if(empty($returnUrl))
            $returnUrl=$this->getConfig('returnUrl');
        if(empty($returnUrl))
            $returnUrl='/';
        
        return $returnUrl;
    }
        
    
    
    function forceLogin(Model_User $user){
        $this->getSession()->user=$user->getFormData();
        
    }
    
    function authenticate($data){
        $class=$this->getConfig('userClass');
        /** 
         * var $model Model_User 
         */
        
        $model=new $class();
        $this->_errors=array();
        
        if($userData=$model->authenticate($data)){
            
            if($data["pernament"]){
                try{
                    $model=new Model_LogLogin();
                    $model->user_id=$userData["id"];
                    $model->token=md5(uniqid(mt_rand(), true));
                    $model->save();
                    
                    $domain=$this->getConfig("cookieDomain")?: $_SERVER["HTTP_HOST"];
                    
                    setcookie("pernamentlogin", $model->user_id.":".$model->token, strtotime("+1 month"),"/",$domain);
                } catch (Exception $ex) {

                }
            }
            
            $this->logAccess($userData);
            
            
            $this->getSession()->user=$userData;
            return $model;
        }
        
        
        $this->_errors[]="Bad username or password";
        
        return false;
    }
    
    
    function getLoginCookie(){
        $ctoken=@$_COOKIE["pernamentlogin"];   
        
        if(empty($ctoken))
            return null;   
      
        list($user_id, $token) = explode(":", $ctoken, 2); 
      
        if(($user_id+0)>0)
            return array("user_id"=>$user_id,"token"=>$token);
        return false;
    }
    
    
    function logout(){
        $this->getSession()->user=null;
        $this->_user=null;
        
        try{
            if($this->getLoginCookie()!==null){
            
                $model=new Model_LogLogin();
                $model->fetchOne($this->getLoginCookie());
                setcookie("pernamentlogin", null,-1,"/");
                unset($_COOKIE["pernamentlogin"]);
//Zend_Debug::dump($model->getData());exit;
                
                if(!$model->isEmpty())
                    $model->delete();
            }
        } catch (Exception $ex) {

        }
        
    }
    
    function getErrors(){
        return $this->_errors;
    }
    
    function logAccess($user){
        try{
            $log=new Model_LogAccess;
            $log->user_id=$user["id"];
            $log->ip=$_SERVER["REMOTE_ADDR"]."";
            $log->browser=$_SERVER["HTTP_USER_AGENT"]."";
            $log->save();
        }
        catch(Exception $e){
            
        }
    }

    public function acl() {
        if($this->_acl===null)
            $this->_acl=new Matj_Auth_Acl($this);
        return $this->_acl;
    }

}