<?
require_once 'Dom/simple_html_dom.php';

class Matj_Dom{
        
    /**
     * 
     * @param type $str
     * @param type $lowercase
     * @param type $forceTagsClosed
     * @param type $target_charset
     * @param type $stripRN
     * @param type $defaultBRText
     * @return simple_html_dom
     */
	static function getDomFromString($str, $lowercase=true, $forceTagsClosed=true, $target_charset = DEFAULT_TARGET_CHARSET, $stripRN=true, $defaultBRText=DEFAULT_BR_TEXT){
		return str_get_html($str, $lowercase, $forceTagsClosed, $target_charset, $stripRN, $defaultBRText);
	}


}