<?php

/** Zend_Locale */
require_once 'Zend/Locale.php';

/** Zend_Translate_Adapter */
require_once 'Zend/Translate/Adapter.php';

class Matj_Translate_Adapter_Mongo extends Zend_Translate_Adapter
{
	/** 
	 * The table colums
	 *
	 * @var Array
	 */
	protected $tableColumns = array(
		'messageId'	=> 'messageId',
		'locale'	=> 'locale',
		'message'	=> 'message'
	);

    /**
     * Generates the adapter
     *
     * @param  string|array|Zend_Db_Table $data Translation database table name and table columns
     * 										for this adapter
     * @param  string|Zend_Locale $locale  (optional) Locale/Language to set, identical with Locale
     *                                     identifiers see Zend_Locale for more information
     * @param  array $options (optional) Options for the adaptor
     * @throws Zend_Translate_Exception
     * @return void
     */
	public function __construct($data, $locale = null, array $options = array())
	{
		 if (!is_array($data)) {
			$newData['dbTable'] = $data;
			$data = $newData;
			unset($newData);
		}

		if (isset($data['tableColumns'])) {
			$this->setTableColumns((array) $data['tableColumns']);
			unset($data['tableColumns']);
		}
                //Zend_Debug::dump($data);exit;
                parent::__construct($data['content'], $locale, $options);
	}

	/**
	 * Set the table columns
	 *
	 * @param array $columns
	 */
	public function setTableColumns(array $columns)
	{
		foreach ($columns as $column => $value) {
			if (array_key_exists($column, $this->tableColumns))
				$this->tableColumns[$column] = $value;
		}
	}

    /**
     * Load translation data
     *
     * @param  mixed   $dbTable   The database table model
     * @param  string  $locale    Locale to add data for, identical with locale identifier,
     * @param  array   $option    OPTIONAL Options to use
     * @throws Zend_Translate_Exception
     * @return void
     */
    protected function _loadTranslationData($model, $locale, array $options = array())
    {
        $options = array_merge($this->_options, $options);

        
        if (is_string($model)) {
            
            
        	require_once 'Zend/Loader.php';
        	//Zend_Loader::loadClass($model);
			$model = new $model;
        }

        if (! $model instanceof Matj_Mongo_Model) {
        	require_once 'Zend/Translate/Exception.php';
        	throw new Zend_Translate_Exception(
        		"Translation data must be instance of 'Matj_Mongo_Model'"
        	);
        }

		if ($options['clear'] || !isset($this->_translate[$locale])) {
			$this->_translate[$locale] = array();
		}

		$colMessageId = $this->tableColumns['messageId'];
		$colLocale = $this->tableColumns['locale'];
		$colMessage = $this->tableColumns['message'];
                
                
                $translates=$model->getCollection()->find(array($colLocale => $locale))
                                                   ->sort(array($colMessageId => 1));
                
                

		foreach ($translates as $translate) {
                    	$messageId = $translate[$colMessageId];
                        $this->_translate[$translate[$colLocale]][$messageId] = $translate[$colMessage];
		}
                
                
                if(empty($this->_translate[$locale]))
                    $this->_translate[$locale]["none"]="none";
                
                //Zend_Debug::dump($this->_translate);exit;
                
    }

    /**
     * returns the adapters name
     *
     * @return string
     */
    public function toString()
    {
        return 'Db';
    }

    
    protected $_zfdebugCache=array();
    
    
    public function _($messageId, $locale = null)
    {
        $text=$this->translate($messageId, $locale);
    
        
        $key=$messageId;
        /** pre ZFDebug */
        if(Matj_Get::getFrontController()->hasPlugin("ZFDebug_Controller_Plugin_Debug")){
            $debug = Matj_Get::getFrontController()->getPlugin('ZFDebug_Controller_Plugin_Debug');

            $plugin=$debug->getPlugin('translate');
            if(!$plugin){
                $plugin = new ZFDebug_Controller_Plugin_Debug_Plugin_Text();
                $plugin->setTab('Translate')
                       ->setPanel('<h4>Used messages in Matj_Translate</h4>
                                   <form method="post" action="'.Matj_Get::getView()->baseUrl().'/core/tools/locale" target="_blank">
                                   <p>Locale: <input name=lang value="'.$this->getLocale().'"></p>
                                   <hr>
                                   <button>Save</button>
                                   <div>New: key <input name="newKey[]" value="" /> message <input name="newValue[]" value="" /></div>
                                   <div>New: key <input name="newKey[]" value="" /> message <input name="newValue[]" value="" /></div>
                                   <div>New: key <input name="newKey[]" value="" /> message <input name="newValue[]" value="" /></div>
                                   
                                   </form>');
                $plugin->setIdentifier("translate");
                $debug->registerPlugin($plugin);

            }

            if(!key_exists($key, $this->_zfdebugCache)){
                $t=$text;
                $html=$plugin->getPanel();
                $nhtml='<div><div style="width:200px;float:left">'.$key.'</div><input name="locale['.$key.']" style="padding:2px; font-size:10px; width:400px;" value="'.$t.'" /></div><hr>';
                $plugin->setPanel(str_replace('<hr>',$nhtml,$html));
                $this->_zfdebugCache[$key]=$text;
            }
        }


        
        return $text;
    }

    
    
    
    
}