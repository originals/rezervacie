<?php

class Matj_Html {
    
    static function isValidAttribute($a){
		$valid=false;
		if(in_array(strtolower($a), 
				array ( 
					'href', 
					'class', 
					'style', 
					'id', 
					'target', 
					'rel', 
					'onclick', 
					'alt', 
					'title' 
				)))
			$valid=true;
		
		if(preg_match('/data-[a-zA-Z]+/',$a)){
			return true;
		}
			
		return $valid;	
	}
	
	

    static function a($content,$attributes=array()){
    	
    	if(is_array($content)){
			$attributes=$content;
			$content=$attributes["text"];
			unset($attributes["text"]);
		}
    	
    	$html = '<a ';
		
		foreach ($attributes as $a => $v) {
			if (!self::isValidAttribute($a)) {
				continue;		
			}
			$html .= '' . strtolower($a) . '="' . (string)$v . '"';
		}
		
		$html .= '>' . $content . '</a>';
		
		return $html;
    	
    }
    
    
}