<?php

/**
 * @author Delphi
 * @package Delphi
 *
 */
class Matj_Model extends Matj_ModelMapper{

	protected $tableName;

	protected $colPrefix;

	protected $data;

	private $table;

	protected $primary;

	protected $dbupdatelog = false;

	function __construct ($id = null) {
		if (is_numeric($id)) {
			$this->fetchEntry($id);
		}
		elseif (is_array($id)) {
			$this->setData($id);
		}
	}



	/**
	 *
	 * @return Matj_DB_Table_Abstract
	 */
	function getDbTable () {
		if ($this->table == null) {
			if ($this->tableName == null) {
				throw new Matj_Model_Exception('Table not set');
			}
			$params = array (
				'name' => $this->tableName
			);
			if ($this->primary) {
				$params["primary"] = $this->primary;
			}

			if ($this->dbupdatelog) {
				$params["dbupdatelog"] = 1;
			}

			$this->table = Matj_Get::getDbTable($params);
		}
		return $this->table;
	}

	function fetchEntry ($id = null, $where = null, $value = null) {
		$select = $this->getDbTable()->select();
		if ($id !== null) {
			$select->where($this->getDbTable()->getPrimary() . " = ? ", ($id + 0));
		}
		else {
			if (is_array($where)) {
				foreach ($where as $k => $d) {
					$select->where($k, $d);
				}
			}
			else {
				$select->where($where, $value);
			}
		}

		$data = $this->getDbTable()->fetchRow($select);
		if (empty($data)) {
			$this->data = null;
			return null;
		}
		else {
			$data=$data->toArray();
			$this->data = $data;
		}
		return $this;
	}

	function fetchEntries ($options=array()) {
		$select = $this->getDbTable()->select();
		//Zend_Debug::dump($options);exit;
		$select=Matj_Util::addSqlParams($select,$options);
		//echo $select;exit;
		$data = $this->getDbTable()->getAdapter()->fetchAll($select);
		$return=array();
		foreach($data as $k=>$d){
			$class=get_class($this);
			$return[$k]=new $class($d);

		}
		return $return;
	}


	public function save () {
		$data = $this->data;
		$id = $this->getData($this->getDbTable()->getPrimary());
		if (is_numeric($id) && $id>0 ) {
			$this->getDbTable()->updateData($data);
			return $this;
		}
		else {
			$data[$this->colPrefix."date_add"]=date("Y-m-d H:i:s");
			$id = $this->getDbTable()->insertData($data);
			return $this->setData($this->getDbTable()->getPrimary(), $id);
		}
	}


	function update($data){
		if(!empty($this->data[$this->getDbTable()->getPrimary()])){
			$where=$this->getAdapter()->quoteInto("`".$this->getDbTable()->getPrimary()."` = ?",$this->data[$this->getDbTable()->getPrimary()]);
			return $this->getDbTable()->update($data,$where);
		}
		else{
			throw new Exception("Can´t update User");
		}
	}


	function delete () {
		$data = $this->data;
		if (($id = $this->getData($this->getDbTable()->getPrimary()))) {
			$return=$this->getDbTable()->deleteDataById($id);


			return $return;
		}
		return false;
	}

	function isColumn ($name) {
		return $this->getDbTable()->isColumn($name);
	}

	function getData ($name = null) {
		if ($name === null) {
			return $this->data;
		}
		if(is_array($this->data) && key_exists($name,$this->data))
                    return $this->data[$name];
                return null;
	}

	function setData ($name, $value = null) {
		if (is_array($name)) {
			foreach ($name as $k => $d) {
				$this->data[$k] = $d;
			}
		}
		else {
			if(is_object($name))throw new Exception("Cannot set object in setdata name");
			$this->data[$name] = $value;
		}
		return $this;
	}


	function __call ($method, $params) {
		if (preg_match('/^get/', $method)) {
			if ($this->data == null)
				return null;
			$col = strtolower(preg_replace('/^get/', '', $method));
			if ($this->isColumn($this->colPrefix . $col)) {
				return $this->getData($this->colPrefix . $col);
			}
			elseif ($this->isColumn($col)) {
				return $this->getData($col);
			}
			else {
				throw new Matj_Model_Exception("Column $col not exists in table " . $this->tableName);
			}
		}
		if (preg_match('/^set/', $method)) {
			if (empty($params)) {
				throw new Matj_Model_Exception("No value for $method");
			}
			$col = strtolower(preg_replace('/^set/', '', $method));
			if ($this->isColumn($this->colPrefix . $col)) {
				return $this->setData($this->colPrefix . $col, $params[0]);
			}
			elseif ($this->isColumn($col)) {
				return $this->setData($col, $params[0]);
			}
			else {
				throw new Matj_Model_Exception("Column $col not exists in table " . $this->tableName);
			}
		}
		throw new Matj_Model_Exception("Method $method not exists in model " . get_class($this));
	}


	function isEmpty () {
		return empty($this->data);
	}


	function getFormData () {
		$data = $this->getData();
		if (empty($data))
			return array ();
		return $data;
	}

	function setDataFromForm ($data) {
            if($data instanceof Matj_Form)$data=$data->getValues();
                
		$this->setData($data);
		return $this;
	}


	function smartTableOptions ($options) {
		if (! empty($options["smartTable"])) {
			$options=Matj_Util::mergeArrays($options, $options["smartTable"]->getModelOptions());
		}
		return $options;
	}


	function fetchAll ($select, $options = array()) {
		$data = $this->getAdapter()->fetchAll($select);
		if (empty($data)) {
			if (! empty($options["smartTable"]))
				$options["smartTable"]->getPaginator()->setCount(0);

			return array ();
		}

		if (! empty($options["smartTable"])) {
			$cnt = $this->getAdapter()->fetchOne(
				$this->getAdapter()->select()->from(null, new Zend_Db_Expr('FOUND_ROWS()')));
			$options["smartTable"]->getPaginator()->setCount($cnt);

		}
		return $data;
	}

        
        
        function getAllStatuses(){
             foreach(Matj_Get::getDbAdapter()->fetchAll('SELECT * FROM orders_status') as $s)
                    $statuses[$s["order_status_id"]]=new Eshop_Model_OrderStatus($s);
             return $statuses;
        }
        
}
