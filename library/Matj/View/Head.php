<?php
/**
 * <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
  
  <!-- Bootcards CSS files for desktop, iOS and Android -->
  <!-- You'll only need to load one of these (depending on the device you're using) in production -->
  <link href="//cdnjs.cloudflare.com/ajax/libs/bootcards/1.1.1/css/bootcards-ios.min.css" rel="stylesheet">
  <!-- <link href="//cdnjs.cloudflare.com/ajax/libs/bootcards/1.1.1/css/bootcards-desktop.min.css" rel="stylesheet">-->
  <!--<link href="//cdnjs.cloudflare.com/ajax/libs/bootcards/1.1.1/css/bootcards-android.min.css" rel="stylesheet">-->

  <link href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" />
 */



class Matj_View_Head{
    
    
    /**
    * @var Matj_View_Head
    */
    private static $instance = null;
    
    private static $baseUrl = null;
    
    /**
     * @var Matj_View_Seo
     */
    protected $seo = null;
    /**
     *
     * @var Matj_View_Helper_OpenGraph
     */
    private $openGraph=null;
    
    protected $title=null;
    protected $link=array();
    protected $script=array();
    protected $meta=array();
    static $eol = "\n\r";
    

    function stylesheet($file){
        
    }
    
    
    function __construct(){
        return $this;
    }
    
    /**
     * 
     * @return Matj_View_Head
     */
    public static function getInstance () {
        
        if (self::$instance == NULL) {
            $head=new self();
            self::$instance = $head;
        }
        
        
        
        return self::$instance;
    }
    
    
    function setDefaults(){
        $this->meta["charset"]=array('charset'=>'utf-8');
        $this->meta["X-UA-Compatible"]=array('http-equiv'=>'X-UA-Compatible', 'content'=>'IE=edge');
        $this->meta["viewport"]=array('name'=>'viewport', 'content'=>'width=device-width, initial-scale=1');
        //$this->meta["google"]=array('name'=>'google','value'=>'notranslate');
        
        
        return $this;
    }

    function addMeta($params,$key=null){
        if($key)
            $this->meta[$key]=$params;
        else
            $this->meta[]=$params;
        return $this;
    }
    
    function addMetaName($name,$value,$priority=null){
        return $this->addMeta(array('name'=>$name,'content'=>$value,'priority'=>$priority),$name);
    }
    
    
    
    
    function addMetaHttpEquiv($name,$value){
        return $this->addMeta(array('http-equiv'=>$name,'content'=>$value,'priority'=>$priority),$name);
    }
    
    
    
    
    public function setBaseUrl($url) {
        self::$baseUrl=$url;
        
        return $this;
    }

    
    
    public function appendStylesheet($file,$key=null,$appendMtime=true){
        return $this->addStylesheet($file, $key, $appendMtime);
    }
    
    public function prependStylesheet($file,$key=null,$appendMtime=true){
        $link=$this->_createStylesheet($file,$appendMtime);
        $arr = array_reverse($this->link, true); 
        if($key)
            $arr[$key] = $link; 
        else
            $arr[]=$link;
        $this->link = array_reverse($arr, true); 
        return $this;
    }
    
    
    public function addStylesheet($file,$key=null,$appendMtime=true){
        $link=$this->_createStylesheet($file,$appendMtime);
        
        if($key){
            $this->link[$key]=$link;
        }
        else{
            $this->link[]=$link;
        }
        
        return $this;
    }
    
    public function appendJavascript($html,$key=null){
        return $this->addJavascript($html, $key);
    }
    
    public function appendScript($html){
        return $this->addJavascript($html, null);
    }
    
    public function prependScript($html,$key=null){
        return $this->addJavascript($html, null,"prepend");
    }
    
    
    
    public function prependJavascript($html,$key=null){
        $link=array('_html'=>$html,'type'=>'text/javascript');
        $arr = array_reverse($this->script, true); 
        if($key)
            $arr[$key] = $link; 
        else
            $arr[]=$link;
        $this->script = array_reverse($arr, true); 
        return $this;
    }
    
    
    
    
    public function addJavascript($html,$key=null,$placement="append"){
        $link=array('_html'=>$html,'type'=>'text/javascript');
        
        if($placement=="prepend")
            $arr = array_reverse($this->script, true); 
        else
            $arr=$this->script;
        
        if($key){
            $arr[$key]=$link;
        }
        else{
            $arr[]=$link;
        }
        
        
        if($placement=="prepend")
            $this->script = array_reverse($arr, true); 
        else
            $this->script = $arr;
        
        return $this;
    }
    
    
    
    public function appendFile($file){
        return $this->appendScriptFile($file);
    }
    
    public function prependFile($file){
        return $this->prependScriptFile($file);
    }
    
    public function appendScriptFile($file,$key=null,$appendMtime=true){
        return $this->addScriptFile($file, $key, $appendMtime);
    }
    
    public function prependScriptFile($file,$key=null,$appendMtime=true){
        $link=$this->_createScriptFile($file,$appendMtime);
        $arr = array_reverse($this->script, true); 
        if($key)
            $arr[$key] = $link; 
        else
            $arr[]=$link;
        $this->script = array_reverse($arr, true); 
        return $this;
    }
    
    
    
    
    
    public function addScriptFile($file,$key=null,$appendMtime=true){
        $link=$this->_createScriptFile($file,$appendMtime);
        
        
        
        if($key){
            $this->script[$key]=$link;
        }
        else{
            $this->script[]=$link;
        }
        
        return $this;
    }

    
    
    public function isRemoteFile($file) {
        return preg_match('/\/\//',$file);
    }

    public function appendFileMTime($url) {
        
        
        $file=ltrim($url,'/');
        if(file_exists($file)){
            $url.="?".filemtime($file);
            
        }
        else{
            throw new Exception('File not found - '.$url);
        }
        return $url;
    }
    
    
    
    
    function renderHead(){
        
        $html='';
        
        if(is_array($this->meta) && count($this->meta)){
            
            uasort($this->meta,function($a,$b){
                if($a["priority"]=$b["priority"])
                        return 0;
                if($a["priority"]>$b["priority"])
                    return 1;
                return -1;
                
            });
            
            foreach($this->meta as $key=>$data){
                unset($data["priority"]);
                $html.=$this->_renderTag('meta',$data).self::$eol;
            }
        }
        
        if(!empty($this->title))
            $html.=$this->_renderTag('title',array('_html'=>$this->title)).self::$eol;
        
        if(!empty($this->openGraph))
            $html.=$this->openGraph.self::$eol;
        
        return $html;
        
    }
    
    function renderLink($appendMtime=true){
        
        $html='';
        foreach($this->link as $key=>$data){
            
            //Zend_Debug::dump($data);
            
            if(!empty($data["href"])){
                $link=$data["href"];
                    if(!$this->isRemoteFile($link)){
                        if(preg_match('/^\//',$link)){
                            $link=$link;
                        }
                        else{
                            $url=self::$baseUrl.$link;
                            if($appendMtime){
                                $url=$this->appendFileMTime($url);
                            }
                            $link=$url;
                        }
                    }
                    $data["href"]=$link;
            }
            
            
            $html.=$this->_renderTag('link',$data).self::$eol;
        }
        return $html;
        
    }
    
    function renderScript($appendMtime=true){
        
        $html='';
        foreach($this->script as $key=>$data){
            if(!empty($data["src"])){
                $link=$data["src"];
                    if(!$this->isRemoteFile($link)){
                        if(preg_match('/^\//',$link)){
                            $link=$link;
                        }
                        else{
                            $url=self::$baseUrl.$link;
                            if($appendMtime){
                                $url=$this->appendFileMTime($url);
                            }
                            $link=$url;
                        }
                    }
                    $data["src"]=$link;
            }

            $html.=$this->_renderTag('script',$data).self::$eol;
        }
        return $html;
        
    }
    
    
    function render(){
        return $this->renderHead()
               .$this->renderLink()
               .$this->renderScript();
    }

    public function _renderTag($tag, $attribs=array()) {
        
        $html='<'.$tag.'';
        
        if(!empty($attribs)){
            if(!is_array($attribs)){
                Zend_Debug::dump($attribs);exit;
            }
                
            foreach($attribs as $k=>$d){
                if(substr($k,0,1)!="_")
                    $html.=' '.$k.'="'.$d.'"';
            }
        }
        $html.='>';
        
        if(!empty($attribs["_html"]))
            $html.=$attribs["_html"];
        
        
        if(!empty($attribs["_pair"]) || !empty($attribs["_html"]))
            $html.='</'.$tag.'>';
        
        
        return $html;
    }

    public function _createStylesheet($file, $appendMtime) {
        $link=array('href'=>$file,'rel'=>'stylesheet');
        return $link;
    }
    
    public function _createScriptFile($file, $appendMtime) {
        $link=array('src'=>$file,'type'=>'text/javascript','_pair'=>true);
        return $link;
    }

    
    public function setTitle($text){
        $this->title=$text;
    }
    
    
    /**
     * 
     * @return Matj_View_Seo
     */
    function seo(){
        if($this->seo===null)
            $this->seo=new Matj_View_Seo();
        return $this->seo;
    }
    
    
    /**
     * 
     * @return Matj_View_Helper_OpenGraph
     */
    function openGraph(){
        if($this->openGraph===null)
            $this->openGraph=new Matj_View_Helper_OpenGraph();
        return $this->openGraph;
    }

    public function setSeo($title=null, $description=null, $url=null, $image=null) {
        
        if($title){
            head()->seo()->setTitle($title);
            head()->openGraph()->setTitle($title);
        }
        
        if($description){
            head()->seo()->setDescription($description);
            head()->openGraph()->setDescription($description);
        }
        
        if($image)
            head()->openGraph()->setImage($image);
        
        if($url)
            head()->openGraph()->setUrl($url);
        
    }

}