<?php


require_once 'Zend/View/Helper/FormElement.php';
require_once 'Zend/View/Helper/FormSelect.php';

class Matj_View_Helper_FormGallery extends Zend_View_Helper_FormElement {

    public function formGallery($name, $value = null, $attribs = null)
    {
    	$name=str_replace('[]','',$name);
        if(is_array($value))
            $value=json_encode($value);
       
        
    	$helper=new Zend_View_Helper_FormTextarea();
    	$helper->setView($this->view);
    	$text=$helper->formTextarea($name,$value,$attribs);
    	
        
        
        
    	$xhtml='<div class="form-gallery" id="'.$name.'-gallery">
                    <p class=menu>
                        <a href="#" class="btn btn-add btn-success btn-small"><i class="icon-white icon-plus"></i> Pridať fotografie</a> 
                        <a href="#" class="btn btn-danger btn-clear btn-small"><i class="icon-trash icon-white"></i> Zmazať všetky</a>
                    </p>
                    <div class="items">
                        
                    </div>
                    <div class=hide>'.$text.'</div>
                </div>';
    	
        
        

    	$xhtml.='
            

            
<script type="text/javascript">
    
$(document).ready(function(){
    $("#'.$name.'-gallery .btn-add").click(function(){
        fname="setPickFile'.$name.'";
        var fileManagerUrl=baseUrl+"/core/filemanager?callback=javascript&function="+fname;
        win=window.open(fileManagerUrl,
            "fileManagerUrl", "status=0, toolbar=0, location=0, menubar=0, " +
            "directories=0, resizable=1, scrollbars=0, width=1000, height=680"
        );
        win.focus();
        return false;
    }); 
    
    $("#'.$name.'-gallery .btn-clear").click(function(){
        $("#'.$name.'").val("");
                    buildGalleryList'.$name.'();

        return false;
    });
    $("#'.$name.'").change(function(){
        buildGalleryList'.$name.'();
    });
    $(document).on("keyup","#'.$name.'-gallery .items input",function(){
        buildGalleryValue'.$name.'();
    });
    buildGalleryList'.$name.'();


});
    

    function getGalleryFiles'.$name.'() {
        data=$("#'.$name.'").val();
        files=$.parseJSON(data);
        if(!files)
            files=[];
        return files;
    }
    function buildGalleryValue'.$name.'(){
        var $itemsHtml=$("#'.$name.'-gallery .items");
        var $items=[];    
        $itemsHtml.find(".well").each(function(){
            if($(".url",this).val()!="" && $(".url",this).val()!=undefined){
                $items.push({url:$(".url",this).val(),name:$(".name",this).val()});
            }
        });
        
        $("#'.$name.'").val(JSON.stringify($items));

    }
        

    function buildGalleryList'.$name.'(){
        var $itemsHtml=$("#'.$name.'-gallery .items");
        $itemsHtml.html("");
        
        items=getGalleryFiles'.$name.'();
        $.each(items,function(k,f){
            th=baseUrl+"/image/shrink/100x100/"+f.url;
            if(f.name==undefined)f.name="";

            row="<div class=\'well well-small row-fluid\'><div class=span12><img src=\'"+th+"\' style=\'float:left; margin-right:20px;\'  />"
            row+="<p><input class=name value=\'"+f.name+"\' /></p>";
            row+="<p><input class=url value=\'"+f.url+"\' /></p>";
            row+="<p><a href=\"#\" class=\"btn btn-danger btn-remove-row btn-small\"><i class=\"icon-trash icon-white\"></i> Zmazať</a>";
            row+=" <a href=\"#\" class=\"btn btn-move-row btn-info btn-small\"><i class=\"icon-move icon-white\"></i> Presuň</a></p>";
            
            row+="</div></div>";
            $itemsHtml.append(row);
        });
        
        $("#'.$name.'-gallery .btn-remove-row").click(function(){
            $(this).closest(".well").remove();
            buildGalleryValue'.$name.'();
            return false;
        });

        $("#'.$name.'-gallery .items").sortable({
            handle:".btn-move-row",
            change:function(){
                buildGalleryValue'.$name.'();
            }
        });
        
    }


    
    var rebuildTo;
    
    function setPickFile'.$name.'(url) {
                
        files=getGalleryFiles'.$name.'();
        files.push({url:url});
        
        $("#'.$name.'").val(JSON.stringify(files));
            
        rebuildTo=setTimeout(function(){
            buildGalleryList'.$name.'();
        },100);

    }
     

</script>
';
        
        
        
    	
        return $xhtml;
    }
}
