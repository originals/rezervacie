<?php

require_once 'Zend/View/Helper/Abstract.php';

class Matj_View_Helper_Facebook extends Zend_View_Helper_Abstract {


	protected $_facebook;

	function facebook () {
		return $this;
	}


	function initHtml () {
		return '
			<div id="fb-root"></div>
			<script type="text/javascript">

      						window.fbAsyncInit = function() {
					        FB.init({
					          appId   : "'.$this->getConfig('appId').'",
					          status  : true, // check login status
					          cookie  : true, // enable cookies to allow the server to access the session
					          xfbml   : true // parse XFBML
					        });

					        // whenever the user logs in, we refresh the page
					        FB.Event.subscribe("auth.login", function() {
					        //	alert("subscribe auth.login");
							// 	location.href="'.$this->getMyFacebookUrl('login').'"
					        });
							FB.Event.subscribe("auth.sessionChange", function(response) {
							// do something with response
							//	alert("sessionchange");
							});
							FB.Event.unsubscribe("auth.sessionChange", function(response){
								alert("unsuscribe auth.sessionChange");
							});
							FB.Event.unsubscribe("auth.login", function(response){
								alert("unsubscribe auth.login");
							});


					      };

					      (function() {
					        var e = document.createElement("script");
					        e.src = document.location.protocol + "//connect.facebook.net/' . $this->getLang() . '/all.js";
					        e.async = true;
					        document.getElementById("fb-root").appendChild(e);
					      }());

      		</script>';

		/*//session : <?php echo json_encode($session); ?>, // don't refetch the session when PHP already has it

      						FB.init({
							            appId:"' . $this->getConfig('appId') . '", cookie:true,
							            status:true, xfbml:true
							        });
		    */

	}


	function initFacebook ($params=array()) {
		if ($this->_facebook === null or !empty($params)) {
			if(empty($params)){
				$params=array (
					'appId' => $this->getConfig("appId"),
					'secret' => $this->getConfig("appSecret"),
					'cookie' => true
				);
			}

			$this->_facebook = new Matj_Facebook_Facebook($params);
		}
		return $this;
	}

	/**
	 *
	 * @return Facebook
	 */
	function getFacebook(){
		if ($this->_facebook === null) {
			throw new Exception("Facebook not loaded");
		}
		return $this->_facebook;
	}

	function setFacebook(Facebook $facebook){
		$this->_facebook=$facebook;
		return $this;
	}


	function getLang () {
		return 'sk_SK';
	}

	function getConfig ($name = null) {
		$c = Matj_Get::getBootstrap()->getOption("facebook");
		if ($name) {
			return $c[$name];
		}
		return $c;
	}




	function loginButton(){
		$this->initFacebook();

		$s=$this->getFacebook()->getSession();
		if(empty($s)){
			return '<fb:login-button perms="'.$this->getConfig("loginPerms").'"></fb:login-button>';
		}
		else{
			$me=$this->getFacebook()->api("/me");
			$html.='<div class="fb_logged">
				<img src="http://graph.facebook.com/'.$me["id"].'/picture" />
				<p class="fb_logged_name">Ste prihlásený ako: <strong>'.$me["name"].'</strong></p>
				<p class="fb_logged_menu">
					<a href="'.$this->getMyFacebookUrl('login').'" class="continue">Pokračovať</a>
					<a href="'.$this->getFacebook()->getLogoutUrl(array('next'=>$this->getMyFacebookUrl('logoutFacebook',true))).'" class="logout">Odhlásiť</a>
				</p>
			</div>';

			return $html;
		}

	}



	function getMyFacebookUrl($step=null,$fullUrl=false){

		$url=Matj_Get::getView()->url(array('module'=>'frontend','controller'=>'user','action'=>'facebook','step'=>$step));
		if($fullUrl){
			$url="http://".Matj_Get::getRequest()->getServer("HTTP_HOST").$url;
		}
		return $url;

	}



}
