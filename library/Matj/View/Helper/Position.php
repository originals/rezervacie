<?php
/**
 * Dposition
 *
 */


/**
 * Abstract class for extension
 */
require_once 'Zend/View/Helper/FormElement.php';


/**
 * Helper to generate a "text" element
 *
 * @category   Zend
 * @package    Zend_View
 * @subpackage Helper
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Matj_View_Helper_Position extends Zend_View_Helper_FormElement
{

	public $defaultLat=49;
	public $defaultLng=20;
	public $defaultZoom=18;
	
	
	
    public function position($name, $value = null, $attribs = null)
    {
        $v=explode(",",$value."");
        $a=$attribs;
        
        $map=$this->getJavascript($name,$value,$a,!key_exists('appendToHead',$attribs) ? 1 : $attribs["appendToHead"]);
        
		
		if(!empty($attribs["height"]))
			$height=$attribs["height"]."px";
		else
			$height="300px;";
		
				
		unset($attribs["markerIcon"]);
        unset($attribs["markerDraggable"]);
        unset($attribs["mapZoom"]);
        
		
		
        $a["id"]=$name."-lat";
        $xhtml='<div class="dposition" id="'.$name.'-dposition"><div id="'.$name.'Map"  style="width: 100%; height: '.$height.'"></div>';
        
		
		
		if(!empty($attribs["search"])){
			$xhtml.='<div class="dposition-search">
						<label>'.$this->view->translate('dposition.search.label').'</label>
						'.$this->view->formText($name.'[search]','').'
						'.$this->view->formButton($name.'[searchbtn]',$this->view->translate('dposition.search.button')).'
					 </div>';
		}
		
		
		
		if(empty($attribs["disableForms"])){
			$xhtml.='<div class="dposition-form row-fluid">
                            <div class="span4"><label>'.$this->view->translate('dposition.lat').'</label>'
					.$this->view->formText($name.'[lat]',empty($v[0]) ? $this->defaultLat : $v[0],array('class'=>'lat'));
			$xhtml.='</div><div class="span4">';
                        $xhtml.='<label>'.$this->view->translate('dposition.lng').'</label>'
					.$this->view->formText($name.'[lng]',empty($v[1]) ? $this->defaultLng : $v[1],array('class'=>'lng'));
			$xhtml.='</div><div class="span4">';+
                        $xhtml.='<label>'.$this->view->translate('dposition.zoom').'</label>'
					.$this->view->formText($name.'[zoom]',empty($v[2]) ? $this->defaultZoom : $v[2],array('class'=>'zoom')).'</div>';
                        $xhtml.='</div>';
                        
                }
		
		$xhtml.='</div>';
		
		
        $xhtml.=$map;
        return $xhtml;
    }
    
    
    
    function getJavascript($name,$value,$attribs,$appendHead=true){
        $id=$name.'Map';
        
        $v=explode(',',$value);
        $lat=empty($v[0]) ? $this->defaultLat : $v[0];
        $lng=empty($v[0]) ? $this->defaultLng : $v[1];
        $zoom=empty($v[0]) ? $this->defaultLng : $v[1];;
        if(isset($l[2]) && $l[2]>0)$zoom=$l[2]+0;
        elseif(isset($attribs["mapZoom"]) && $attribs["mapZoom"]>0)$zoom=$attribs["mapZoom"]+0;
        
        
        
        if(!isset($attribs["markerDraggable"])){ $attribs["markerDraggable"]=true; }
        if(empty($attribs["markerIcon"])){ $attribs["markerIcon"]="http://gmaps-samples.googlecode.com/svn/trunk/markers/blue/blank.png"; }
       
        
	if(!empty($attribs["overlay"])){
		
		$overlay='var imageBounds = new google.maps.LatLngBounds(
      new google.maps.LatLng('.$attribs["overlay"]["topCorner"].'),
      new google.maps.LatLng('.$attribs["overlay"]["bottomCorner"].'));
	var oldmap = new google.maps.GroundOverlay(
      "'.$attribs["overlay"]["img"].'",
      imageBounds);
  		oldmap.setMap(map);	';
	}	
		
		
		
    $js='
        var map;
        var geocoder;
        var marker;
            
        
		function loadFromForm(){
			if(typeof $("#'.$name.'-lat").val() != "undefined"){
				lat=parseFloat($("#'.$name.'-lat").val());
				lng=parseFloat($("#'.$name.'-lng").val());
				zoom=parseFloat($("#'.$name.'-zoom").val());
				var point=new google.maps.LatLng(lat, lng);
				map.setCenter(point);
				map.setZoom(zoom);
				marker.setPosition(point);
			}
		}
		
		
		    
        function initialize() {
        	lat = '.$lat.';
        	lng = '.$lng.';
        	
        	geocoder = new google.maps.Geocoder();
            
        	var latLng = new google.maps.LatLng(lat, lng);
        
        	map = new google.maps.Map(document.getElementById("'.$id.'"), {
        		center : latLng,
        		zoom : '.$zoom.',
        		mapTypeId : google.maps.MapTypeId.ROADMAP
        	});
			
			'.@$overlay.'
			
        	google.maps.event.addListenerOnce(map, "tilesloaded", mapTitlesLoaded);
        	
        	//google.maps.event.addListenerOnce(map, "click", closeInfoWindows);
        }
        google.maps.event.addDomListener(window, "load", initialize);
        
		
		
        function setMarkerLatLngToForm(obj){
			      $("#'.$name.'-lat").val(obj.getPosition().lat());
                  $("#'.$name.'-lng").val(obj.getPosition().lng());
    	}
            
        
        function mapTitlesLoaded(){
        	marker = new google.maps.Marker({
                position: map.getCenter(),
                icon: "'.$attribs["markerIcon"].'",
                animation: google.maps.Animation.DROP,
                map: map
            });
			
			$("#'.$name.'-dposition input").change(loadFromForm);
			loadFromForm();
			
			google.maps.event.addListener(map, "zoom_changed", function() {
				$("#'.$name.'-zoom").val(map.getZoom());
			});
			
         ';   

    
    if($attribs["markerDraggable"]){
        $js.='
                marker.setDraggable(true);       
            	google.maps.event.addListener(marker, "dragend", function() {
            	    setMarkerLatLngToForm(marker);
            	});
        ';    	
    }
    
    if(!empty($attribs["geolocation"]["auto"])){
        $js.='
          		//console.log("Geolocation API Start");
            	if (navigator.geolocation) {
            	    navigator.geolocation.getCurrentPosition(function(position){ 
    						var point=new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        	map.setCenter(point);
                        	map.setZoom(14);
                         	marker.setPosition(point);
                         	//console.log("Geolocation API Success: "+position.coords.latitude+","+position.coords.longitude);
            	    		
    				},function(){  
					//	 console.log("ERR: Geolocation API Error - declined");
    				});
            	} else {
            	   // console.log("ERR: Geolocation API Error - navigator");
            	}
    	 ';
    }

    
    $js.='
    
    		loadGeocoder();
        }

 	function loadGeocoder(){

';


$js.='
    	$("#'.$name.'-searchbtn").click(function(){
			
			
			var address = $("#'.$name.'-search").val();
			
			console.log(address);
			
    		geocoder.geocode({"address": address}, function(results, status) {
		      if (status == google.maps.GeocoderStatus.OK) {
		        if (results[0]) {
		          var latlng = results[0].geometry.location; //new google.maps.LatLng(results[0].geometry.location.Ba, results[0].geometry.location.Da);
		          marker.setPosition(latlng);
		          
		          
		          if(results[0].geometry.bounds!=undefined){
    		          //console.log(results[0]);
					  //console.log(results[0].getBounds());
					  //return;
					  
					  //var b1 = new google.maps.LatLng(results[0].geometry.bounds.W.d, results[0].geometry.bounds.P.d);
    		          //var b2 = new google.maps.LatLng(results[0].geometry.bounds.W.b, results[0].geometry.bounds.P.b);
    		          
    		          b=results[0].geometry.bounds;//new google.maps.LatLngBounds(b1,b2);
    				  map.fitBounds(b);
					  
				  }
				  else{
    				  map.setCenter(latlng);
    		          map.setZoom(12);
				  }
				  
				  setMarkerLatLngToForm(marker);
		        }
		      } else {
		          if(typeof console != "undefined")console.log("Geocoder failed due to: " + status);
		      }
    		 });
    		return false; 
    	});
    ';
    


if(!empty($attribs["geolocation"]["jquerySelector"])){
    if(!is_array($attribs["geolocation"]["jquerySelector"])){
        $attribs["geolocation"]["jquerySelector"]=array($attribs["geolocation"]["jquerySelector"]);
    }
    foreach($attribs["geolocation"]["jquerySelector"] as $k=>$d){
        $vals[]='$("'.$d.'").val()';
        $evSel[]=$d;
    }
    
	
	$js.='
    	$("'.implode(",",$evSel).'").change(function(){
			var address = '.implode('+","+',$vals).';
			
			//console.log(address);
			
    		geocoder.geocode({"address": address}, function(results, status) {
		      if (status == google.maps.GeocoderStatus.OK) {
		        if (results[0]) {
		          var latlng = results[0].geometry.location; //new google.maps.LatLng(results[0].geometry.location.Ba, results[0].geometry.location.Da);
		          marker.setPosition(latlng);
		          
		          
		          if(results[0].geometry.bounds!=undefined){
    		          //console.log(results[0]);
					  //console.log(results[0].getBounds());
					  //return;
					  
					  //var b1 = new google.maps.LatLng(results[0].geometry.bounds.W.d, results[0].geometry.bounds.P.d);
    		          //var b2 = new google.maps.LatLng(results[0].geometry.bounds.W.b, results[0].geometry.bounds.P.b);
    		          
    		          b=results[0].geometry.bounds;//new google.maps.LatLngBounds(b1,b2);
    				  map.fitBounds(b);
				  }
				  else{
    				  map.setCenter(latlng);
    		          map.setZoom(12);
		          }
		        }
		      } else {
		          //console.log("Geocoder failed due to: " + status);
		      }
    		 });
    		return false; 
    	});
    	
    ';
    
}
    
    
 
$js.='    
    }       
';   

        if($appendHead){
			$this->bodyAttr='';
			$this->view->headScript()->appendFile('http://maps.google.com/maps/api/js?sensor=false');
			$this->view->headScript()->appendScript($js);
        }
        else{
                return '<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>'
                            .'<script type="text/javascript">'.$js.'</script>';
        }
		
    }
    
}
