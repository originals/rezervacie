<?php


require_once 'Zend/View/Helper/FormElement.php';
require_once 'Zend/View/Helper/FormSelect.php';

class Matj_View_Helper_FormPickFile extends Zend_View_Helper_FormElement {

    public function formPickFile($name, $value = null, $attribs = null)
    {
    	
    	
       
    	$helper=new Zend_View_Helper_FormText();
    	$helper->setView($this->view);
    	$text=$helper->formText($name,$value,$attribs);
    	
        if($value!=""){
            $placeholder='<img src="'.$value.'" />';
        }
        
    	$xhtml='<div class="pickfile row" id="'.$name.'-pickfile">
                            <div class="col-sm-8">'.$text.' </div>
                            <div class="col-sm-4"><a href="#" class="btn btn-info btn-sm btn-search"><i class="fa fa-search" style="margin:0;"></i></a> <a href="#" class="btn btn-danger btn-sm"><i class="fa fa-trash"  style="margin:0;"></i></a></div>


                        <div class="pickfile-preview" style="padding-top:10px; clear:both;"></div>
                        <div>
                            
                            
                        </div>
                </div>';
    	
    	$xhtml.='
            
<script type="text/javascript">
    
$(document).ready(function(){
    $("#'.$name.'-pickfile .pickfile-preview,#'.$name.'-pickfile .btn-search").click(function(){
        openFileManager'.$name.'("setPickFile'.$name.'");
        return false;
    }); 
    
    $("#'.$name.'-pickfile .btn-danger").click(function(){
        $("#'.$name.'").val("");
        setPickFile'.$name.'("");   
            return false;
    });
    $("#'.$name.'").change(function(){
        
        setPickFile'.$name.'($("#'.$name.'").val());   
    });
    setPickFile'.$name.'($("#'.$name.'").val());   
});
    
    
    
    function setPickFile'.$name.'(url) {
                
                


                $("#'.$name.'").val("");
                obj=$("#'.$name.'-pickfile .pickfile-preview");
                obj.html("");
                if(url==""){
                }
                else{
                    $("#'.$name.'").val(url);
                    var img = new Image();
                    img.src = url;
                    img.onload = function() {
                        obj.html("<img id=\"'.$name.'-pickfile-image\" src=\"" + url + "\" style=\"max-width:100%;\" />");
                    }
                }
    }
    
    function openFileManager'.$name.'(fname) {
        var fileManagerUrl=baseUrl+"/filemanager?callback=javascript&function="+fname;
        win=window.open(fileManagerUrl,
            "fileManagerUrl", "status=0, toolbar=0, location=0, menubar=0, " +
            "directories=0, resizable=1, scrollbars=0, width=1000, height=680"
        );
        win.focus();
    }
    

</script>
';
        
        
        
    	
        return $xhtml;
    }
}
