<?

class Matj_View_Helper_Seo extends Zend_View_Helper_Abstract
{

	protected $_properties=array();


	function seo(){
		return $this;
	}
        
        function setTitle($title){
            return $this->view->headTitle($title);
        }
        
        function setDescription($text){
            if(empty($text))return false;
            return $this->view->headMeta()->setName("description",$text);
        }
        
}