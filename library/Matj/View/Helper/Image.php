<?php

class Matj_View_Helper_Image extends Zend_View_Helper_Abstract
{

    function image($src,$attribs=array()){

    	if(!empty($attribs["type"])){
            if(empty($attribs["width"]))$attribs["width"]=400;
            if(empty($attribs["height"]))$attribs["height"]=300;
            
            //$src='/core/image/'.$attribs["type"].'/'.$attribs["width"].'x'.$attribs["height"].$src;
            $src='/image/'.$attribs["type"].'/'.$attribs["width"].'x'.$attribs["height"].$src;
            
            unset($attribs["width"],$attribs["height"],$attribs["type"]);
        }
        
        if(!empty($attribs["link"]))return $src;
        
        $html='<img ';
            $html.='src="'.$src.'"';
            foreach($attribs as $a=>$v)
                $html.=' '.$a.' = "'.$v.'"';
            
        $html.='/>';
        return $html;
    }
}
