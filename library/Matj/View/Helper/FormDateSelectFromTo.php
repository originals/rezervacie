<?php


require_once 'Zend/View/Helper/FormElement.php';
require_once 'Zend/View/Helper/FormSelect.php';

class Matj_View_Helper_FormDateSelectFromTo extends Zend_View_Helper_FormElement {

    public function formDateSelectFromTo($name, $value = null, $attribs = null)
    {
        
        $name=str_replace("[]","",$name);
//        Zend_Debug::dump($name);exit;
        
        
        if(!is_array($value))$value=array();
        if(empty($value["from"]))$value["from"]="";
        if(empty($value["to"]))$value["to"]="";
        
        
        
        $xhtml='';
        $xhtml.='<div class=filter-dateselect-wrapper>';
    	$xhtml.='<div class=filter-dateselect-element>'.$this->view->formDateSelect($name."[from]",$value["from"],$attribs).'</div>';
    	$xhtml.='<div class=filter-dateselect-element>'.$this->view->formDateSelect($name."[to]",$value["to"],$attribs).'</div>';
        $xhtml.='</div>';
    	
        return $xhtml;
    }
}
