<?php

class Matj_View_Helper_TextBlock extends Zend_View_Helper_Abstract
{
	
	public $textBlock;
	
    function textBlock($url,$attributes=array()){
    	
    	$this->textblock=new Core_Model_Textblock();
		$this->textblock->fetchByVariable($url);
		
		return $this;
    	
    }
	
	
	function getTextBlock(){
		return $this->textblock;	
	}
	
	function __toString(){
		if($this->textblock)
			return ''.$this->textblock->getText();
		else
			return '';
	}
	
} 