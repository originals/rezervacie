<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_View
 * @subpackage Helper
 * @copyright  Copyright (c) 2005-2010 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: Translate.php 20140 2010-01-08 05:21:04Z thomas $
 */

/** Zend_View_Helper_Translate.php */
require_once 'Zend/View/Helper/Translate.php';

/**
 * Translation view helper
 *
 * @category  Zend
 * @package   Zend_View
 * @copyright  Copyright (c) 2005-2010 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd     New BSD License
 */
class Matj_View_Helper_UrlHost extends Zend_View_Helper_Abstract
{

    function urlHost($forceWWW=false){
		$s=Matj_Get::getRequest();
		$url="http://";
		if($s->getServer("HTTP_HTTPS"))
			$url="https://";

		$url.=strip_tags(htmlspecialchars($s->getServer("HTTP_HOST")));

    	return $url;//".
    	//return "http://".Matj_Get::getFrontController()->getRequest()->getServer('HTTP_HOST');
    }
}
