<?php

class Matj_View_Helper_ImageCache extends Zend_View_Helper_Abstract
{

    function imageCache($src,$type=null,$w=null,$h=null){

        
        
    	
        $image = Matj_WideImage::load($src);
        
        $params["w"]=$w;
        $params["h"]=$h;
        
        
        $folder=new Matj_File_Folder('uploads');
        $folder=$folder->getFolder('_cache',true);
        
        $file=new Matj_File_File($src);
        
        
        //$src='http://arnox.originals.sk/uploads/images/products/muzi/sportove-oblecenie/ponozky-podkolienky/ponozky/exp_outdoor_socks-black_blue-1.jpg';
        
        $cacheFilePrefix=Matj_String::convertToUrlFriendly($src,false);
        
        //echo $cacheFilePrefix;exit;

        
        if($type=="delete"){
            $files=$folder->findFiles($cacheFilePrefix);
            
//            echo $cacheFilePrefix;
//            Zend_Debug::dump($files);
            
            foreach($files as $f){
                $f->delete();
            }
            return true;
        }
        
        
//        return;        
        
        $cacheFile=$folder->getUrl().DIRECTORY_SEPARATOR.$cacheFilePrefix."-".$type."-".$w."x".$h.".".$file->getExtension();
        
        
        
        
        if(file_exists($cacheFile)){
            
            return $this->view->baseUrl().DIRECTORY_SEPARATOR.$cacheFile;
        }
        
        //echo "generate image<br>";
        
        
        if($type=="crop"){
                $add=0;
                $image->resize($params["w"]+$add, $params["h"]+$add, 'outside')
                ->crop('center', 'center', $params["w"]-$add, $params["h"]-$add)
                ->saveToFile($cacheFile);
        
                return $this->view->baseUrl().DIRECTORY_SEPARATOR.$cacheFile;
        }
        
        if($type=="resize"){
            $image->resize($params["w"], $params["h"], 'inside','down')
                ->saveToFile($cacheFile,80);
            return $this->view->baseUrl().DIRECTORY_SEPARATOR.$cacheFile;
        }
        
        
        if($type=="fill"){
            $white=$image->allocateColor(255,255,255);
            $image->resize($params["w"]+$add, $params["h"]+$add, 'inside')
                ->resizeCanvas($params["w"]-$add, $params["h"]-$add,'center', 'center',$white)
                    ->saveToFile($cacheFile);
            return $this->view->baseUrl().DIRECTORY_SEPARATOR.$cacheFile;
        }
        
        
        
    }
}
