<?php


require_once 'Zend/View/Helper/FormElement.php';
require_once 'Zend/View/Helper/FormSelect.php';

class Matj_View_Helper_FormKcimage extends Zend_View_Helper_FormElement {

    public function formKcimage($name, $value = null, $attribs = null)
    {
    	
    	
       
    	$helper=new Zend_View_Helper_FormText();
    	$helper->setView($this->view);
    	$text=$helper->formText($name,$value,$attribs);
    	
        $placeholder=$emptyplaceholder='<span>Kliknutím vyber obrázok<br>Click to select image</span>';
        
        if($value!=""){
            $placeholder='<img src="'.$value.'" />';
        }
        
    	$xhtml='<div class="kcimage" id="'.$name.'-kcimage">
                        '.$text.'
                        <div class="kcimage-preview">'.$placeholder.'</div>
                        <a href="#" class="btn btn-danger btn-mini"><i class="icon-trash icon-white"></i></a>
                </div>';
    	
    	$xhtml.='
            
<script type="text/javascript">
    
$(document).ready(function(){
    $("#'.$name.'-kcimage .kcimage-preview").click(function(){
        openKCFinder'.$name.'($(this));
        return false;
    }); 
    
    $("#'.$name.'-kcimage .btn-danger").click(function(){
        $("#'.$name.'").val("");
        setKcFinderImage'.$name.'($("#'.$name.'-kcimage .kcimage-preview"),"");   
    });
    $("#'.$name.'").change(function(){
        
        setKcFinderImage'.$name.'($("#'.$name.'-kcimage .kcimage-preview"),$("#'.$name.'").val());   
    });
    
});
    
    
    
    function setKcFinderImage'.$name.'(obj,url) {
                if(url==""){
                   obj.html("'.$emptyplaceholder.'"); 
                }
                else{
                    window.KCFinder = null;
                    obj.html("<div class=kcimage-loading>Loading...</div>");
                    var img = new Image();
                    img.src = url;
                    img.onload = function() {
                        obj.html("<img id=\"'.$name.'-kcimage-image\" src=\"" + url + "\" />");
                    }
                }
    }
    
    function openKCFinder'.$name.'(obj) {
        window.KCFinder = {
            callBack: function(url) {
                setKcFinderImage'.$name.'(obj,url);
                $("#'.$name.'").val(url);
            }
        };
        window.open(CKEDITOR.config.filebrowserImageBrowseUrl,
            "kcfinder_image", "status=0, toolbar=0, location=0, menubar=0, " +
            "directories=0, resizable=1, scrollbars=0, width=800, height=600"
        );
    }
    

</script>
';
        
        
        
    	
        return $xhtml;
    }
}
