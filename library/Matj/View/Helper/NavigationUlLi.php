<?php

require_once 'Zend/View/Helper/Abstract.php';

class Matj_View_Helper_NavigationUlLi extends Zend_View_Helper_Abstract {

	public $recursive = true;
	public $linkRecursive = true;
	
	
	function setOptions($options){
		foreach($options as $k=>$d){
			$this->$k=$d;
		}
		return $this;
	}
	
	
	function navigationUlLi ($menu,$options=array(),$level=0) {
		$this->setOptions($options);
		
		if($menu instanceof Matj_Navigation){
			$pages=$menu->getPages();
		}
		elseif(is_array($menu)){
			$pages=$menu;
		}
		else{
			throw new Matj_Navigation_Exception('menu in helper must be array or object');
		}
		

		
		/////UL
		if(empty($options["attributes"]["ul"]))
		$options["attributes"]["ul"]["class"]="level".$level;
		
		
		$html='<ul';
			foreach($options["attributes"]["ul"] as $k=>$d){
				$html.=' '.$k.'="'.$d.'"';	
			}
		$html.='>';

		
			$i=0;
		
			foreach($pages as $k=>$page){
				
				$text=$page->getLabel();
				if(!empty($options["span"])){
					$text='<span>'.$text.'</span>';
				}
				
				if(!empty($options["auth"])){
					$testUrlParams=$urlParams;
					if(!$resetUrl){
						$params=Matj_Get::getRequest()->getParams();
						$testUrlParams=Matj_Util::mergeArrays($params,$testUrlParams);
					}
					if(!Matj_Auth::isAllowed($testUrlParams)){
						continue;
					}
				}				
				
				
				////// LI
				$liAttr=$options["attributes"]["li"];
				if(empty($liAttr))$liAttr=array();
				
				if($page->isActive())
					$liAttr["class"]=(empty($liAttr["class"]) ? "" : $liAttr["class"]." ")."active";
		
				if($i==0)
					$liAttr["class"]=(empty($liAttr["class"]) ? "" : $liAttr["class"]." ")."first";
		
				$html.='<li';
					foreach($liAttr as $k=>$d){
						$html.=' '.$k.'="'.$d.'"';	
					}
				$html.='>';
						
				
				$html.=$this->view->a($text,$page->getLinkAttributes());

				if($this->recursive && count($page->getPages()) ){
					$html .= $this->navigationUlLi($page->getPages(),$options,$level+1);
				}
				$html .= '</li>';
				$i++;
			}
			$html.='</ul>';
		
			
		return $html;
	}
	
	

}
