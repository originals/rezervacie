<?
class Matj_View_Helper_ImageUpload extends Zend_View_Helper_FormFile {

  public function imageUpload($name, $value, $attribs, $options) {
    $str = parent::formFile($name, $attribs = null);
	
	
    if(!empty($options[$name])) {
      $str .= $this->getImagePreview($name, $options[$name]);
    } else {
      $str .= $this->getEmptyPreview();
    }
    
    return $str;
  }

  private function getImagePreview($name, $path) {
    $img = ($this->view->doctype()->isXhtml())
       ? '<img src="/'.$path.'" alt="'.$name.'" />'
       : '<img src="/'.$path.'" alt="'.$name.'">';
    
    return '<p class="imageUploadPreview">'.$img.'</p>';
  }
  
  private function getEmptyPreview() {
    return '<p class="preview">'.Matj_Get::getTranslator()->_("core.image.noimageuploaded").'</p>';
  }
}