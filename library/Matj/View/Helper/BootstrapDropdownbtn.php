<?php

require_once 'Zend/View/Helper/Abstract.php';

class Matj_View_Helper_BootstrapDropdownbtn extends Zend_View_Helper_Abstract {

	function bootstrapDropdownbtn ($data,$options=array()) {
		$html="";
		$html.='<div class="btn-group">';

		if(!array_key_exists('split', $options))$options['split']=true;
		if(!array_key_exists('first', $options))$options['first']=true;

		if(!empty($data[0])){
			$c="btn";
			if(!$options["split"]){
				$c.=" dropdown-toggle";
				$fa=$data[0];
				$fa["data-toggle"]="dropdown";
				$fa["text"].=' <span class="caret"></span>';
				$fa["href"]="#";
			}
			else{
				$fa=$data[0];
			}
			
				


			$fa["class"]=(!empty($fa["class"]) ? $fa["class"]." " : ""). $c;

			$html.=$this->view->a($fa);
		}
		if(!$options["first"])unset($data[0]);

		if(!empty($data) && count($data)>0){
			if(	($options["split"]))
				$html.='<a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>';


			$html.=$this->view->ulLiMenu($data,array('class'=>'dropdown-menu'));
		}
	    $html.='</div>';



		return $html;
	}



} 