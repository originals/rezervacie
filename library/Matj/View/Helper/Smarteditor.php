<?
class Matj_View_Helper_Smarteditor extends Zend_View_Helper_FormTextarea {

  public function smarteditor($name, $value, $attribs, $options) {
	  
	$id="smarteditor-".$name;
	  
    $str="<div id='".$id."' class='smarteditor'>";
	$str.="<div class='smarteditorPreview' style=''>".($value)."</div>";
	$str.='<div class="smarteditorMenu btn-group">
			<a href="#'.$id.'-editor" class="btn edit"><i class="icon-pencil"></i> Edit</a>
			<a href="#" class="btn expand"><i class="icon-zoom-in"></i> Expand</a>
			<a href="#" class="btn reduce" style="display:none;"><i class="icon-zoom-out"></i> Reduce</a>
			<a href="#" class="btn clear"><i class="icon-trash"></i> Clear</a>
		  </div>';
	
	$str.= '<div style="display:none;"><div id="'.$id.'-editor"><div id="'.$id.'-data">'.parent::formTextarea($name,$value, $attribs,$options)."</div></div></div>";
	
	$str.='</div>';
	
	$this->view->headScript()->appendScript('
		$(function(){
			$("#'.$id.' .expand").click(function(){ 
				$("#'.$id.' .smarteditorPreview").css("height","400px"); 
				$(this).hide(); 
				$("#'.$id.' .reduce").show(); 
				return false;
			});
			$("#'.$id.' .reduce").click(function(){ 
				$("#'.$id.' .smarteditorPreview").css("height","100px"); 
				$(this).hide(); 
				$("#'.$id.' .expand").show(); 
				return false;
			});
			
			$("#'.$id.' .clear").click(function(){ 
				if(confirm("Isto zmazať cely text?")){
					$("#'.$id.' .smarteditorPreview").html(""); 
					$("#'.$name.'").html(""); 
				}
				return false;
			});
			
			$("#'.$id.' .edit").fancybox({
				autoDimensions:false,
				width:800,
				height:500,
				"onComplete":function(){
					$("#'.$name.'").ckeditor({skin: "kama",toolbar:"Basic",width:"98%",height:"400px"});
				},
				"onCleanup":function(){
					$("#'.$id.' .smarteditorPreview").html($("#'.$name.'").val());
					$("#'.$name.'").ckeditor(function(){ this.destroy(); });
				}
				
			});
			
			
		});
	
	
	
	');
	
	
    return $str;
  }

}