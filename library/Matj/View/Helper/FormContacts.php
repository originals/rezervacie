<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_View
 * @subpackage Helper
 * @copyright  Copyright (c) 2005-2010 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: FormButton.php 22285 2010-05-25 14:22:11Z matthew $
 */


/**
 * Abstract class for extension
 */
require_once 'Zend/View/Helper/FormElement.php';


/**
 * Helper to generate a "button" element
 *
 * @category   Zend
 * @package    Zend_View
 * @subpackage Helper
 * @copyright  Copyright (c) 2005-2010 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Matj_View_Helper_FormContacts extends Zend_View_Helper_FormElement
{
    /**
     * Generates a 'button' element.
     *
     * @access public
     *
     * @param string|array $name If a string, the element name.  If an
     * array, all other parameters are ignored, and the array elements
     * are extracted in place of added parameters.
     *
     * @param mixed $value The element value.
     *
     * @param array $attribs Attributes for the element tag.
     *
     * @return string The element XHTML.
     */
    public function formContacts($name, $value = null, $attribs = null)
    {
        $info    = $this->_getInfo($name, $value, $attribs);
        extract($info); // name, id, value, attribs, options, listsep, disable, escape

        
        $types=array();
        foreach(Matj_Get::getBootstrap()->getOption('contactTypes') as $c=>$s){
            $types[$c]=$this->view->translate('offer.contactType.'.$c);
        }
        
        
        $xhtml.='<div class="contacts-form" id="'.$name.'">';
        
        for($i=0;$i<=10;$i++){
            $xhtml.='<div class="contacts-row">'
                    .$this->view->formSelect($name.'-type-'.$i,@$value[$name."-type-".$i],array('class'=>'contacts-type input-medium'),$types)
                    .' '
                    .$this->view->formText($name.'-text-'.$i,@$value[$name."-text-".$i],array('class'=>'contacts-text input-medium'))
                    .' '
                    .$this->view->formCheckbox($name.'-delete-'.$i,@$value[$name."-delete-".$i],array('class'=>'contacts-delete'))
                    .' <i class="icon-trash"></i>'
                    .$this->view->formHidden($name.'-id-'.$i,@$value[$name."-id-".$i])
                    .'</div>';
        }
        
        $xhtml.='<div class="contacts-row"><a href="javascript:void();" class="btn contacts-add">'.$this->view->translate('offer.contactType.add').'</a></div></div>';
        return $xhtml;
    }
}
