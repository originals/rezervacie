<?

class Matj_View_Helper_OpenGraph extends Zend_View_Helper_Abstract
{

	protected $_properties=array();


	function openGraph(){
		if(!$this->getUrl()){
			$url=$this->urlHost()
				.Matj_Get::getView()->escape(Matj_Get::getRequest()->getRequestUri());
			$this->setUrl($url);
		}

		if(!$this->getType()){
			$this->setType("article");
		}

		return $this;
	}

	function setProperty($key,$name){
		$key=strtolower($key);
		if(in_array($key,array('image','url'))){
			if(!preg_match('/^http/',$name) && preg_match('/^\//',$name)){
				$name=$this->urlHost().$name;
			}
		}

		$this->_properties[$key]=$name;
		return $this;
	}

	function getProperty($key){
		$key=strtolower($key);
		if(key_exists($key,$this->_properties)){
			return $this->_properties[$key];
		}
		return null;
	}


	function renderProperties(){
		$html='';
		foreach($this->_properties as $k=>$d){
			if($k=="admins"){
				$k="fb:".$k;
			}
			else{
				$k="og:".$k;
			}
			$html.='<meta property="'.$k.'" content="'.$d.'"/>'."\n";
   		}
		return $html;
	}




	function __call ($method, $params) {
		if (preg_match('/^get/', $method)) {
			$col = strtolower(preg_replace('/^get/', '', $method));
			return $this->getProperty($col);
		}
		if (preg_match('/^set/', $method)) {
			$col = strtolower(preg_replace('/^set/', '', $method));
			return $this->setProperty($col,$params[0]);
		}
		throw new Matj_Model_Exception("Method $method not exists in  " . get_class($this));
	}





	function __toString(){
		return $this->renderProperties();
	}



	function urlHost(){
		return Matj_Get::getView()->urlHost();
	}

}