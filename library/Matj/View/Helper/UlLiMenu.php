<?php

require_once 'Zend/View/Helper/Abstract.php';

class Matj_View_Helper_UlLiMenu extends Zend_View_Helper_Abstract {
	
	function ulLiMenu ($menu,$options=array(),$level=0) {
                if(empty($options["levels"]))$options["levels"]=99;
            
                if(is_array($menu) && !empty($menu)){
			$class="";
			if(!empty($options["class"])){
				$class=" ".$options["class"];
				unset($options["class"]);
			}
			
			
			$html='<ul class="level'.$level.$class.'">';
			foreach($menu as $k=>$d){
				
				
				
				
				
				$tprefix="menu.";
				if(!empty($options["tprefix"])){
					$options["tprefix"]=$options["tprefix"];
				}
				
				$liclass="";
				if(!empty($d["liclass"])){
					$liclass=" ".$d["liclass"];
					unset($d["liclass"]);
				}
				
				if(!empty($d["text"]))
					$text=$d["text"];
				else 
					$text=$this->view->translate($tprefix.$k);
				
				
				if(!empty($options["span"])){
					$text='<span>'.$text.'</span>';
				}
				
				
                                
                                if(empty($d["href"])){
                                    $urlParams=$d;

                                    $resetUrl=$urlParams["resetUrl"] ? $urlParams["resetUrl"] : null;
                                    $route=$urlParams["route"] ? $urlParams["route"] : null;

                                    $attributes=$urlParams["attributes"];
                                    $aAttr=array();
                                    if(!empty($attributes["a"]) && is_array($attributes["a"])){
                                            $aAttr=$attributes["a"];
                                    }

                                    unset($urlParams["submenu"]);
                                    unset($urlParams["route"]);
                                    unset($urlParams["resetUrl"]);
                                    unset($urlParams["attributes"]);
                                    unset($urlParams["text"]);



                                    if(!empty($options["auth"])){
                                            $testUrlParams=$urlParams;
                                            if(!$resetUrl){
                                                    $params=Matj_Get::getRequest()->getParams();
                                                    $testUrlParams=Matj_Util::mergeArrays($params,$testUrlParams);
                                            }
                                            if(!Matj_Auth::isAllowed($testUrlParams)){
                                                    continue;
                                            }
                                    }				
                                    
                                    if(!empty($d["class"]))$aAttr["class"]=$d["class"];
                                    
                                    $aAttr['href']=$this->view->url($urlParams,$route,$resetUrl);
                                }
                                else{
                                    $aAttr=array();
                                    $aAttr['href']=$d["href"];
                                    if($d["target"])$aAttr['target']=$d["target"];
                                    
                                }
                                
				if($liclass): $html.='<li class="'.$liclass.'">'.$this->view->a($text,$aAttr); else: $html.='<li>'.$this->view->a($text,$aAttr); endif;
				if (! empty($d["submenu"]) && $level<$options["levels"])
					$html .= $this->ulLiMenu($d["submenu"],$options,$level+1);
				$html .= '</li>';
			}
			$html.='</ul>';
		}
		return $html;
	}
	
	

}
