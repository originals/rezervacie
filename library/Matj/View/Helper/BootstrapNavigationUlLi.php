<?php

require_once 'Zend/View/Helper/Abstract.php';

class Matj_View_Helper_BootstrapNavigationUlLi extends Zend_View_Helper_Abstract {

	public $recursive = true;
	public $linkRecursive = true;
	
	
	function setOptions($options){
		foreach($options as $k=>$d){
			$this->$k=$d;
		}
		return $this;
	}
	
	
	function bootstrapNavigationUlLi ($menu,$options=array(),$level=0) {
		$this->setOptions($options);
		
		if($menu instanceof Matj_Navigation){
			$pages=$menu->getPages();
		}
		elseif(is_array($menu)){
			$pages=$menu;
		}
		else{
			throw new Matj_Navigation_Exception('menu in helper must be array or object');
		}
		

		
		/////UL
		if(empty($options["attributes"]["ul"]))
		$options["attributes"]["ul"]["class"]="level".$level;
		
		
		$html='<ul';
			foreach($options["attributes"]["ul"] as $k=>$d){
				$html.=' '.$k.'="'.$d.'"';	
			}
		$html.='>';

		
			$i=0;
		
			foreach($pages as $k=>$page){
				
				$text=$page->getLabel();
				if(!empty($options["span"])){
					$text='<span>'.$text.'</span>';
				}
				
				if(!empty($options["auth"])){
					$testUrlParams=$urlParams;
					if(!$resetUrl){
						$params=Matj_Get::getRequest()->getParams();
						$testUrlParams=Matj_Util::mergeArrays($params,$testUrlParams);
					}
					if(!Matj_Auth::isAllowed($testUrlParams)){
						continue;
					}
				}				
				
				
				////// LI
				$liAttr=isset($options["attributes"]["li"]) ? $options["attributes"]["li"] : array(); 
				if(empty($liAttr))$liAttr=array();
				
				if($page->isActive())
					$liAttr["class"]=(empty($liAttr["class"]) ? "" : $liAttr["class"]." ")."active";
		
				if($i==0)
					$liAttr["class"]=(empty($liAttr["class"]) ? "" : $liAttr["class"]." ")."first";
				
				
				$aAttr=$page->getLinkAttributes();
				
				
				$isSub=$this->recursive && count($page->getPages()) ;
				if($isSub){
					$liAttr["class"]=(empty($liAttr["class"]) ? "" : $liAttr["class"]." ")."dropdown";
					$aAttr["class"]=(empty($aAttr["class"]) ? "" : $aAttr["class"]." ")."dropdown-toggle";
					$aAttr["data-toggle"]="dropdown";
					$aAttr["href"]="#";
					$text.="<b class='caret'></b>";
				}
				
				$html.='<li';
                                	foreach($liAttr as $k=>$d){
						$html.=' '.$k.'="'.$d.'"';	
					}
				$html.='>';
				
                                if($page->icon!=null && preg_match('/^fa/',$page->icon)){
                                    $text='<i class="fa '.$page->icon.'"></i> '.$text;
                                }
                               
                                
				
				$html.=$this->view->a($text,$aAttr);

				if($isSub){
					//Zend_Debug::dump($options);exit;
					if(!empty($options["attributes"]["ul"]["class"]) && preg_match('/nav/',$options["attributes"]["ul"]["class"])){
						$options["attributes"]["ul"]["class"]=str_replace('nav','dropdown-menu',$options["attributes"]["ul"]["class"]);
					}
					
					$html .= $this->bootstrapNavigationUlLi($page->getPages(),$options,$level+1);
				}
				$html .= '</li>';
				$i++;
			}
			$html.='</ul>';
		
			
		return $html;
	}
	
	

}
