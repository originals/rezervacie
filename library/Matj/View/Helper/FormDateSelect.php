<?php


require_once 'Zend/View/Helper/FormElement.php';
require_once 'Zend/View/Helper/FormSelect.php';

class Matj_View_Helper_FormDateSelect extends Zend_View_Helper_FormElement {

    public function formDateSelect($name, $value = null, $attribs = null)
    {
       
    	$t=Matj_Get::getTranslator();
    	
    	$val=explode(" ",$value);
    	$vd=explode("-",$val[0]);
    	$vt=explode(":",@$val[1]);
    	
    	$helper=new Zend_View_Helper_FormSelect;
    	$helper->setView($this->view);
    	
    	
    	
    	if(!empty($attribs["blankOptions"]) || !empty($attribs["emptyOptions"]))$blank=true;
    	unset($attribs["blankOptions"]);
    	unset($attribs["emptyOptions"]);
    	
    	
    	
    	if(!empty($blank))$days[""]="";
    	foreach(Matj_Util::arrayFromTo(1,31) as $k=>$d){
    	    $days[$k]=$d;
    	}
    	$attr=$attribs;
    	$attr["class"]=(empty($attr["class"]) ? "" : $attr["class"]." ")."dateselect dateselect-day";
    	$attr["id"]=$name."-day";
    	unset($attr["years"],$attr["hours"]);
    	$day=$helper->formSelect($name."[day]",@$vd[2]+0,$attr,$days);
    	
    	

    	if(!empty($blank))$months[""]="";
    	foreach(Matj_Util::arrayFromTo(1,12,'core.month.') as $k=>$d){
    	    $months[$k]=(!empty($d) ? $t->_($d) : '');
    	}
    	$attr=$attribs;
    	$attr["class"]=(empty($attr["class"]) ? "" : $attr["class"]." ")."dateselect dateselect-month";
    	$attr["id"]=$name."-month";
    	    	unset($attr["years"],$attr["hours"]);
    	$month=$helper->formSelect($name."[month]",@$vd[1]+0,$attr,$months);
    	
    	
    	$attr=$attribs;
    	if(!empty($blank))$years[""]="";
    	if(empty($attr["years"])){
        	foreach(Matj_Util::arrayFromTo(date("Y")-2,date("Y")+2) as $k=>$d){
        	    $years[$k]=$d;
        	}
    	}
    	else{
    	    foreach($attr["years"] as $k=>$d){
        	    $years[$k]=$d;
        	}
    	}
    	$attr["class"]=(empty($attr["class"]) ? "" : $attr["class"]." ")."dateselect dateselect-year";
    	$attr["id"]=$name."-year";
    	    	unset($attr["years"],$attr["hours"]);
    	$year=$helper->formSelect($name."[year]",@$vd[0]+0,$attr,$years);
    	
    	
    	$xhtml=$day.$month.$year;
    	
    	
    	if(!empty($attribs["time"])){
	    	
	    	if(!empty($blank))$hours[""]="";
	    	if(empty($attr["hours"])){
	        	foreach(Matj_Util::arrayFromTo(0,23) as $k=>$d){
	        	    $hours[$k]=sprintf("%02d",$d);
	        	}
	    	}
	    	else{
	    	    foreach($attr["hours"] as $k=>$d){
	        	    $hours[$k]=$d;
	        	}
	    	}
	    	$attr=$attribs;
	    	$attr["id"]=$name."-hour";
	    	$attr["class"]=(empty($attr["class"]) ? "" : $attr["class"]." ")."dateselect dateselect-hour";
    		    	unset($attr["years"],$attr["hours"]);
	    	$hour=$helper->formSelect($name."[hour]",@$vt[0]+0,$attr,$hours);
	    	
	    	
	    	if(!empty($blank))$minutes[""]="";
	    	if(!empty($attr["allminutes"])){
	    		$attr["minutes"]=array();
	    		for($i=0;$i<60;$i++)$attr["minutes"][$i]=sprintf('%02d',$i);
	    	}
	        
	    	
	    	if(empty($attr["minutes"])){
	        	foreach(array(0,15,30,45) as $k=>$d){
	        	    $minutes[$d]=sprintf("%02d",$d);
	        	}
	    	}
	    	else{
	    	    foreach($attr["minutes"] as $k=>$d){
	        	    $minutes[$k]=$d;
	        	}
	    	}
	    	$attr=$attribs;
	    	$attr["id"]=$name."-minutes";
	    	$attr["class"]=(empty($attr["class"]) ? "" : $attr["class"]." ")."dateselect dateselect-minutes";
    		    	unset($attr["years"],$attr["hours"]);
	    	$minutes=$helper->formSelect($name."[minutes]",@$vt[1]+0,$attr,$minutes);
	    	
	    	$xhtml.=$hour." <span class='dateselect-divider'>:</span> ".$minutes;
    	}
    	
    	
        return $xhtml;
    }
}
