<?php


require_once 'Zend/View/Helper/FormElement.php';
require_once 'Zend/View/Helper/FormSelect.php';

class Matj_View_Helper_FormBankAccount extends Zend_View_Helper_FormElement {

    public function formBankAccount($name, $value = null, $attribs = null)
    {
    	
    	
       
    	$t=Matj_Get::getTranslator();
    	
    	$v=explode("/",$value);
    	$v2=explode("-",$v[0]);
    	
    	$helper=new Zend_View_Helper_FormText();
    	$helper->setView($this->view);
    	
    	$attr=$attribs;
    	$attr["class"]=(empty($attr["class"]) ? "" : $attr["class"]." ")."bankAccount-prefix";
    	$attr["maxlength"]=6;
    	$prefix=$helper->formText($name."[prefix]",$v2[0],$attr);
    	
    	$attr=$attribs;
    	$attr["class"]=(empty($attr["class"]) ? "" : $attr["class"]." ")."bankAccount-account";
    	$attr["maxlength"]=10;
    	$account=$helper->formText($name."[account]",$v2[1],$attr);
    	
    	$attr=$attribs;
    	$attr["class"]=(empty($attr["class"]) ? "" : $attr["class"]." ")."bankAccount-bank";
    	$attr["maxlength"]=4;
    	$bank=$helper->formText($name."[bank]",$v[1],$attr);
    	
    	$xhtml='<div class="bankAccount">'.$prefix."-".$account."/".$bank.'</div>';
    	
    	
    	
        return $xhtml;
    }
}
