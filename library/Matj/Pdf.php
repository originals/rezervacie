<?php

require_once('Pdf/tcpdf/tcpdf.php'); 

/**
 * Creates an empty pdf document using TCPDF
 * @package DPdf
 * @author Lukas Zemcak
 */
class Matj_Pdf extends TCPDF
{

	const CS = "UTF-8";
	
	const MM = 1;
	
	const LEFT = 20;
	
	const RIGHT = 190;
	
	const LINE_WIDTH_NORMAL = 0.22;
	
	const LINE_WIDTH_BOLD = 0.38;
	
	const TOP = 20;
	
	const A4_WIDTH = 210;
	
	const A4_HEIGHT = 297;
	
	const DOWN = 278;
	
	protected $t;
	
	protected $view;
	
	public function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false){
		parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
		$this->setPDFVersion('1.4');
		$this->init();			
	}
	
	public function init(){
		$this->SetCreator(PDF_CREATOR);
		$this->SetAuthor('www.matj.sk');
//		$pdf->SetTitle('TCPDF Example 001');
//		$pdf->SetSubject('TCPDF Tutorial');
//		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$this->SetFont('arial');

		//set margins
		$this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$this->setPrintHeader(false);
		$this->setPrintFooter(PDF_MARGIN_FOOTER);
		//$this->currency = Zend_Registry::getInstance()->conf->mena;
				
		//set auto page breaks
		$this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);		
	}
	
	// Page footer
    public function Footer() {
        $this->SetY(-15);
        $this->SetFont('arial', '', 9);
       		 $this->Cell(0, 0, $this->getTranslator()->_("core.pdf.page",
        	array ('page'=>$this->getAliasNumPage(),'numPages'=>$this->getAliasNbPages())), 0, 0, 'R');
    }
    
	/**
 	 * return translator for this pdf 
     */ 	
	public function getTranslator()	{
		return Delphi_Get::getTranslator();	
	}
	
	public function drawTextTr($text,	$x, $y, $float = 'L', $height = null, $width = null){
		return $this->drawText($this->getTranslator()->_($text), $x, $y, $float, $height, $width);
	}
	
	public function drawText($text,	$x=null, $y=null, $float = 'L', $height = null, $width = null, $stretch = 0){
		if($x===null)$x=$this->getX();
		if($y===null)$y=$this->getY();
		
		if($width == null) $width = $this->GetStringWidth($text);
		if($height == null) $height = $this->getFontSize();		
		$this->SetXY($x, $y);
		return $this->Cell($width,$height, $text, 0, 0,$float,0,'',$stretch);		
	}
	
	
}

?>
