<?php

require_once 'Zend/Filter/Interface.php';


class Matj_Filter_DecimalComma implements Zend_Filter_Interface
{
	
	
	protected $_floatFilter = true;
	
	function __construct($floatFilter=true){
		$this->_floatFilter=$floatFilter;
	}
	
	public function filter($value)
    {
            
            
        $valueFiltered=str_replace(',','.',trim($value));
    	if($this->_floatFilter)
	        $valueFiltered=preg_replace('/[^0-9.]/','',$valueFiltered);
        else
	       	$valueFiltered=(float)preg_replace('/[^0-9.]/','',$valueFiltered);
        
        return $valueFiltered;
    }
}