<?php

require_once "Zend/Filter/Interface.php";

class Matj_Filter_RemoveAccents implements Zend_Filter_Interface
{
    public function filter($value)
    {
        
		return Matj_String::removeAccents($value);
    }
}