<?

/**
 * najjednoduchsi mozny dekorator
 */
class Matj_Decorator_Text extends Zend_Form_Decorator_Abstract  {
 	
	/**
	 * vkladany text
	 *
	 */
	const TEXT = "text";
	
	public function render($content) {
		$result = $this->_options[self::TEXT];
		if($this->getPlacement()==self::PREPEND){
			return $result.$content;	
		}else{
			return $content.$result;			
		}		
	}
}

