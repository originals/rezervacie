<?
require_once 'Matj/Facebook/src/facebook.php';

class Matj_Facebook_Facebook extends Facebook{

	function getLang () {
		return 'sk_SK';
	}

	protected $_configData=array();
        function setConfig($config){
            $this->_configData=$config;
        }
        
        
        
        
	function getConfig ($name = null) {
		$c=$this->_configData;
//                Zend_Debug::dump($c);
                if(empty($c))
                    $this->_configData=$c = Matj_Get::getBootstrap()->getOption("facebook");
		
                if ($name) {
			return $c[$name];
		}
		return $c;
	}


	function getApiUser(){
		if($this->getUser())
			return $this->api('/me');
	}
	
	
	function apiFeed($params,$options=array()){
		
		if(is_array($params)){
			$data=$params;
		}
		elseif(is_string($params)){
			if(preg_match('/{/',$params)){
				$data=Zend_Json::decode($params);
			}
			else{
				$data["message"]=$params;
			}
		}
		
		if(empty($data))return false;
		
		if(!empty($options["access_token"]))
			$data["access_token"]=$options["access_token"];
		
		//echo $this->getAppId();exit;
		
		
		if(!empty($options['disableErrors'])){
			try{
				return $this->api("/me/feed/","POST",$data);
			}
			catch(Exception $e){
				return false;
			}
		}
		else{
			if(!empty($options["debug"])){
				try{
					return $this->api("/me/feed/","POST",$data);
				}
				catch(Exception $e){
					Zend_Debug::dump($params);
					Zend_Debug::dump($data);
					Zend_Debug::dump($e);
					exit;
				}		
			}
			
			return $this->api("/me/feed/","POST",$data);
		}
	}
	

        function getFriends($params=array()){
            
            return $this->api("/me/friends","GET",$params);
	}
        
        
}
