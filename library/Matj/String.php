<?php
class Matj_String {
	
	static function jsSafeText($text){
		return str_replace(array('"',"'", "\r", "\n", "\0"), array('\"','\\\'','\r', '\n', '\0'), $text);
	}
	
	
	static function removeAccents($text) { 
	  $text = HTMLSpecialChars($text); 
	  $text = StripSlashes($text); 
	  $text = NL2BR($text); 
	  
	  $s=array("á","ä","č","ď","é","ě","ë","í","ľ","ň","ô","ó","ö","ŕ","ř","š","ť","ú","ů","ü","ý","ž","Á","Ä","Č","Ď","É","Ě","Ë","Í","Ľ","Ň","Ó","Ö","Ô","Ř","Ŕ","Š","Ť","Ú","Ů","Ü","Ý","Ž");
	  
	  $bez=array("a","a","c","d","e","e","e","i","l","n","o","o","o","r","r","s","t","u","u","u","y","z","A","A","C","D","E","E","E","I","L","N","O","O","O","R","R","S","T","U","U","U","Y","Z");
	  $text=str_replace($s,$bez,$text);
	  
	  
	  return $text; 
	}

	
	static function convertToUrlFriendly($name,$numsFilter = true){
    	$result=self::removeAccents($name);
    	$result=strtolower($result);
    	
    	
    	$replace_what = array('   ','  ',	'_', ' - ', ' ',', ', ',','.', '/','----','---','--'  );
        $replace_with = array(' ',	' ',	'-', '-'  , '-', ',', '-','-', '-','-','-','-'   );
  		$result=str_replace($replace_what,$replace_with,$result);
		$result = preg_replace("/[^a-zA-Z0-9-_\s]/", "", $result); // odstrani neziaduce znaky
  		
		$result = preg_replace("/(^-.|-$)/","", $result); // odstrani pomlcny zo zaciatku a konca
  		
		
		if($numsFilter){
	  		//if(preg_match("/^[0-9]/",$result)){
	  		//	$result="-".$result;
	  		//}
	  		if(preg_match("/[0-9]$/",$result)){
	  			$result=$result."-";
	  		}
		}
	  	return $result;
    }
	
	
	static function shorten($textOriginal,$lenght=50,$type = ""){
		$dlzka = mb_strlen($textOriginal,'utf-8');
		$text=$textOriginal;
		if($dlzka>$lenght){
			$text=mb_substr($text,0,$lenght)."...";
		}
		
		if($type == "span"){
			$text = '<span title="'.$textOriginal.'">'.$text.'</span>';
		}
		return $text;
	}
	
	
  public  function regtext($text)
  // funkcia prekonvertuje vstupny text po jednom znaku tak ze vzhlada vstupny
  // znak v $sources a do vystupu zapise jeho ekvivalent z $replace. Ak znak nenajde
  // do vystupu zapise zdrojovy znak
  {
   $encoding='UTF-8';
   //$encoding='latin1';
   
    $sources = array("aáäAÁÄ","bB","cCčČ","dDďĎ","eéeëEÉEË",
                     "fF","gG","hH","iIíÍ","jJ","kK","lĺľLĹĽ",
                     "mM","nNňŇ","oóöôOÓÖÔ","pP","qQ","rŕřRŔŘ",
                     "sšSŠ","tťTŤ","uúuüUÚUÜ","vV","wW","xX",
                     "yýYÝ","zžZŽ");
    $replace = array("[a,á,ä,A,Á,Ä]","[b,B]","[c,C,č,Č]","[d,D,ď,Ď]","[e,é,e,ë,E,É,E,Ë]",
                     "[f,F]","[g,G]","[h,H]","[i,I,í,Í]","[j,J]","[k,K]","[l,ĺ,ľ,L,Ĺ,Ľ]",
                     "[m,M]","[n,N,ň,Ň]","[o,ó,ö,ô,O,Ó,Ö,Ô]","[p,P]","[q,Q]","[r,ŕ,ř,R,Ŕ,Ř]",
                     "[s,š,S,Š]","[t,ť,T,Ť]","[u,ú,u,ü,U,Ú,U,Ü]","[v,V]","[w,W]","[x,X]",
                     "[y,ý,Y,Ý]","[z,ž,Z,Ž]");  
    $regtext = "";
    for ($j = 0; $j < mb_strlen($text,$encoding); $j++)
    {
      for ($k = 0; $k < count($sources); $k++)
      {
        $Decoded = false; 
        if (mb_stristr($sources[$k], mb_substr($text,$j,1,$encoding)) != false)
        {
          $regtext = $regtext.$replace[$k];        
          $Decoded = true;
          break;
        }     
      }
      if (!$Decoded)
      {
        $regtext = $regtext."[".mb_substr($text,$j,1,$encoding)."]";
      }      
    }
    return $regtext."";  
  }

  static function fromUtf($text,$to="WINDOWS-1250"){
	return iconv("UTF-8", $to , $text);
  }
  

  static function cleanText($text,$alnum = false){
      // Zend_Loader::loadClass("Zend_Filter_StripTags");
     //$filter=new Zend_Filter_StripTags(array('img','table','tr','td','div'));
     // $text=$filter->filter($text);
      
      if(is_array($text))throw new Exception("a");

       
      $text=str_replace("&nbsp;"," ",$text);
      $text=html_entity_decode($text);
      
      $text=str_replace(" ","",$text);
      $text=str_replace(array("<p>","</p>",chr(160),chr(194)),"",$text);
      
      Zend_Loader::loadClass("Zend_Filter_StringTrim");
      $filter=new Zend_Filter_StringTrim();
      $text=$filter->filter($text);
      
      
      $alnum=false;
      if($alnum){
      Zend_Loader::loadClass("Zend_Filter_Alnum");
      $filter=new Zend_Filter_Alnum();
      $text=$filter->filter($text);
      }

      
      
      return $text;
  }
  
  
  static function isEmptyCleanText($text,$alnum = false){
      $text=self::cleanText($text,$alnum);
      return empty($text);
  }
  
  
  static function inflector($intCount, $w1, $w2, $w3) {
    switch($intCount) {
      case 1:
        return $w1;
      case 2:
      case 3:
      case 4:
        return $w2;
      default:
        return $w3;
    }
  }

  
  
  function urlToView($text){
      
      $text=str_replace(array('http://','https://'),"",$text);
      $text=trim($text,"/");
      
      return $text;
  }
  
  
}
