<?php

/** 
 * @author Delphi
 * @package Delphi
 * 
 */
class Matj_Get {
	/**
	 * @var Matj_Get
	 */
	private static $instance = null;
	
	/**
	 * @var array stored models
	 */
	private static $storedModels = array ();
	
	/**
	 * @var array stored db tables
	 */
	private static $storedTables = array ();
	
	/**
	 * @var Zend_Controller_Action_Helper_FlashMessenger
	 */
	private static $flashMessenger;
	
	/**
	 * @var Zend_View
	 */
	private static $view = null;
	
	/**
	 * vracia instanciu singletonu
	 * @return Matj_Get
	 */
	public static function getInstance () {
		if (self::$instance == NULL) {
			self::$instance == $this;
		}
		return self::$instance;
	}
	
	/**
	 * getting entry from registry 
	 * eq. Zend_Registry::get($index)
	 * @param string $name
	 */
	public static function getRegistry ($index) {
		return Matj_Registry::get($index);
	}
	/**
	 * return locale object
	 * @return Zend_Locale
	 */
	public static function getLocale () {
		return self::getRegistry("Zend_Locale");
	}
        
        /**
	 * return locale object
	 * @return Zend_Locale
	 */
	public static function getLocaleId () {
            $key='Locale_'.self::getLocale().'_id';
            if(!Zend_Registry::isRegistered($key)){
                $id=self::getDbAdapter()->fetchOne('SELECT language_id FROM languages WHERE locale = "'.$locale.'"');
                if(!$id)throw new Exception('No ID for language '.$locale);
                Zend_Registry::set($key,$id);
            }
            return self::getRegistry($key);
	}
        
        
	/**
	 * return translate object
	 * @return Zend_Translate
	 */
	public static function getTranslator () {
		return self::getRegistry("Zend_Translate");
	}
	/**
	 * 
	 * @return Zend_Db_Adapter_Mysqli
	 */
	public static function getDbAdapter () {
		return Matj_Db_Table_Abstract::getDefaultAdapter();
	}
	
	
        /**
         * 
         * @return DibiConnection
         */
	public static function getDibiAdapter () {
		return Zend_Registry::get(Matj_Application_Resource_Dibi::DEFAULT_REGISTRY_KEY);
	}
	
	
	
	
	public static function getDbTable($params){
		$tableName=$params["name"];
		if (empty(self::$storedTables[$tableName])) {
			$t = new Matj_Db_Table_Abstract($params);
			self::$storedTables[$tableName] = $t;
		}
		return self::$storedTables[$tableName];
	}
	
	
	/**
	 * vrati front controller
	 * @return Zend_Controller_Front
	 */
	public static function getFrontController () {
		return Zend_Controller_Front::getInstance();
	}
	/**
	 * @return Matj_Model
	 */
	public static function getModel ($model) {
		if (empty(self::$storedModels[$model])) {
			
			$m = new $model();
			if ($m instanceof Matj_ModelMapper) {
				self::$storedModels[$model] = $m;
			}
			else {
				return $m;
			}
		}
		return self::$storedModels[$model];
	}
	/**
	 * @return Zend_Filter
	 */
	public static function getFilter ($filterName) {
		$filter = new Zend_Filter();
		if (! is_array($filterName)) {
			$filterName = array ( 
				$filterName 
			);
		}
		foreach ($filterName as $f) {
			$fname = "Zend_Filter_" . ucfirst($f);
			Zend_Loader::loadClass($fname);
			$filter->addFilter(new $fname());
		}
		return $filter;
	}
	

	public static function getFlashMessenger () {
		if (null === self::$flashMessenger) {
			self::$flashMessenger = new Matj_Helper_FlashMessenger();
		}
		return self::$flashMessenger;
	}
	public static function setFlashMessenger ($messenger) {
            self::$flashMessenger=$messenger;
        }
        
        
	/**
	 * 
	 * @return Matj_Auth
	 */
	public static function getAuth () {
            
		return Matj_Auth::getInstance();
	}
	
        /**
         * 
         * @return Core_Model_User
         */
	public static function getAuthUser () {
		return Matj_Auth::getInstance()->getIdentity();
	}
	
	
	/**
	 * 
	 * @return Zend_View
	 */
	public static function getView () {
		if (null === self::$view) {
			require_once 'Zend/Controller/Action/HelperBroker.php';
			$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
			self::$view = $viewRenderer->view;
		}
		return self::$view;
	}
	

	/**
	 * Return the request object.
	 *
	 * @return Zend_Controller_Request_Http
	 */
	public static function getRequest () {
		return self::getFrontController()->getRequest();
	}
	
	/**
	 * 
	 * @return Bootstrap
	 */
	public static function getBootstrap(){
		return self::getFrontController()->getParam('bootstrap');
	}


	public static function isBeta(){
		if(preg_match('/^beta*/',$_SERVER["HTTP_HOST"]) or $_SERVER["REMOTE_ADDR"]=="127.0.0.1")return true;
		return false;	
	}
	
	
	public static function formDefaults($formName){
		$options=self::getBootstrap()->getOptions();
		if(!empty($options["formDefaults"][$formName])){
			return $options["formDefaults"][$formName];
		}
		return null;
	}

	
	public static function getLang(){
		return 1;
	}
        
        
        
        public static function getTaxRate()
        {
            return 20;
        }
        
        
        /**
         * 
         * @param type $name
         * @return Zend_Cache
         */
        public static function getCache($name){
            $cacheManager=Matj_Get::getBootstrap()->getPluginResource('cachemanager')->getCacheManager();
            return $cacheManager->getCache($name);
            
        }
        
        
        public static function url(array $urlOptions = array(), $name = null, $reset = false, $encode = true)
        {
            $router = Zend_Controller_Front::getInstance()->getRouter();
            return $router->assemble($urlOptions, $name, $reset, $encode);
        } 
        
        
        public static function getBaseUrl($server=false){
            
            $url=Zend_Controller_Front::getInstance()->getBaseUrl();
            
            if(!$server){
                if (!isset($_SERVER['SCRIPT_NAME'])) {
                    return $url;
                }

                if (($pos = strripos($url, basename($_SERVER['SCRIPT_NAME']))) !== false) {
                    $url = substr($url, 0, $pos);
                }

                return $url; 
            }
            
            return $url;
            
            
        }
        
}
