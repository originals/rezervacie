<?php

throw new Exception('not complete');

class Matj_Bootstrap3_BackendAction extends Zend_Controller_Action {
    

        /**
         * @var Zend_Controller_Action_Helper_FlashMessenger
         */
        public $msg;

        /**
         * View object
         * @var Zend_View
         */
        public $view;


        public $setHeader = true;



        /**
         * 
         * Zapnutie kontroly prihlasovania
         * 
         * @var boolean
         */
        protected $_authEnabled = true;

        /**
         * 
         * Kontrolovať podľa ACL zoznamu práv
         * 
         * @var boolean
         */
        protected $_authAcl = true;

        protected $menuEnabled = true;


	public $menuActiveId;
	public $menuRecursive = true;
	 
	protected $showHeader = true;
	protected $headerParams = array();
	protected $header = null;


        function init() {
            parent::init();
            $this->_checkAuth();

            $this->view->layout()->setLayout('backend');

        }	
	
	function postDispatch(){
		
                    $menu = $this->menuEnabled;
                    if ($this->_getParam("ajax", null)) {
                        $this->_helper->layout()->setLayout("ajax");
                        $menu = false;

                    }
                    if ($this->_getParam("json", null)) {
                        $this->_helper->layout()->disableLayout();
                        $this->_helper->viewRenderer->setNoRender(true);
                        $menu = false;
                    }


                    if ($menu) {
                        $this->initNavigation();
                    }



                    if($this->setHeader)
                        $this->getResponse()->setHeader('Content-Type', 'text/html; charset=UTF-8', true);


            
            
		if(!$this->_getParam("json") && !$this->_getParam("ajax") && empty($this->view->header)){
		
			if($this->showHeader){	
				if(empty($this->header)){
					$this->header=''.$this->_getParam("module").".header.".$this->_getParam("controller").".".$this->_getParam("action");
				}
				$this->view->header=$this->translate($this->header,$this->headerParams);
			}
		}
                
                
                $kcfinder=(Matj_Get::getBootstrap()->getOption('kcfinder'));
                if(!empty($kcfinder)){
                    $sess=new Zend_Session_Namespace('KCFINDER');
                    $sess->disabled = false;
                    $sess->uploadURL = $kcfinder["uploadURL"];
                    $sess->uploadDir = $kcfinder["uploadDir"];
                }
		
	}



	function initNavigation(){
		$navigationConfig = new Zend_Config_Xml(APPLICATION_PATH.'/configs/backend-navigation.xml');

		$menu=new Matj_Navigation($navigationConfig);//$navigationConfig->toArray();
		foreach($menu->getPages() as $k=>$d){
			if(($d->isActive(true) or (!empty($this->menuActiveId) && $d->getId() == $this->menuActiveId)) && !$this->menuRecursive){
				$subMenu=$d->getPages();
			}
		}


		$options=array('submenuKey'=>'pages','recursive'=>$this->menuRecursive);
		$options["attributes"]["ul"]["class"]='nav navbar-nav';
		$this->view->placeholder('menu')->set($this->view->bootstrapNavigationUlLi($menu,$options));

		if(!empty($subMenu)){
			$options=array('submenuKey'=>'pages','recursive'=>false);
			$options["attributes"]["ul"]["class"]='nav nav-pills';
			$this->view->placeholder('submenu')->set($this->view->bootstrapNavigationUlLi($subMenu,$options));
		}
		
		
		

	}


    /**
     * @return Zend_Controller_Request_Http
     */
    public function getRequest() {
        return $this->_request;
    }

    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array()) {
        parent::__construct($request, $response, $invokeArgs);

        $this->msg = Matj_Get::getFlashMessenger();
    }


    function _checkAuth() {

        if ($this->_authEnabled) {
            if (Matj_Get::getAuth()->hasIdentity()) {
                $id = Matj_Get::getAuth()->getIdentity()->getId();
                $options["user_id"] = $id;

                $allow = Matj_Get::getAuth()->isAllowed($this->getRequest(), $options);
                
                if($this->_authAcl)
                    if (!$allow) {
                        throw new Exception($this->translate('exception.notPrivs'));
                    }
                
                
                
            } else {
                    //throw new Exception($this->translate('exception.notAuth'));
                    $this->addMessage('exception.notAuth', "error");
                    return $this->_redirect(Matj_Get::getView()->url(array('module' => 'core', 'controller' => 'login', 'action' => 'login'), 'default', true));
            
                    
            }
        }
    }

    
    function preDispatch() {

    

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_setParam('ajax', true);
        }
        
    }

    

    function addMessage($msg, $class = "", $params = array()) {
        $flashMessenger = Matj_Get::getFlashMessenger();

        if (is_array($msg)) {
            foreach ($msg as $m) {
                $flashMessenger->addMessage((string) $this->translate($m, $params), $class);
            }
            return $flashMessenger;
        } else {
            return $flashMessenger->addMessage($this->translate($msg, $params), $class);
        }
    }

    function translate($msg, $params = array()) {
        return Matj_Get::getTranslator()->_($msg, $params);
    }

    protected function _redirect($url, array $options = array()) {
        $bu = $this->view->baseUrl();
        if (!empty($bu)) {
            if (substr($url, 0, strlen($bu)) == $bu) {
                $url = substr($url, strlen($bu));
            }
        }
        $this->_helper->redirector->gotoUrl($url, $options);
    }

    function _isPost() {
        return $this->getRequest()->isPost();
    }

    function _getPost($name = null, $default = null) {
        return $this->getRequest()->getPost($name, $default);
    }


}