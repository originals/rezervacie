<?php



class Matj_Bootstrap3_Action_BackendSession extends Matj_Bootstrap3_Action {
    
        protected $setHeader = true;

        protected $menuEnabled = true;

				protected $menuActiveId;
				protected $menuRecursive = true;

				protected $showHeader = true;
				protected $headerParams = array();
				protected $header = null;

        
        protected $auth_group = null;
				protected $clients= array();
        
        /**
        *
        * @var Model_User
        */
        protected $user;
        
        /**
        *
        * @var Matj_Auth_Session
        */
        protected $auth;
        
        /**
         *
         * @var Zend_Session_Namespace
         */
        protected $session;
    
        
        

        function init() {
            parent::init();
            
            $this->initAuth();
            $this->session=$this->auth->getSession();
            
            $this->view->layout()->setLayout('backend');
            
            
            
        }	
        
        function preDispatch() {
            parent::preDispatch();
            
            
            
            if(!$this->auth->isAuthenticated()){
                return $this->_redirect($this->auth->getLoginUrl());
            }
            
            if($this->auth->getUser()->group_id<1 || $this->auth->getUser()->group_id>$this->auth_group){
                $this->addMessage('account.privledges','danger');
                return $this->_redirect($this->auth->getLoginUrl());
            }
            
            
            $this->view->user=$this->user=$this->auth->getUser();
						$this->initClients();
            
            
            
        }
	
        
       
        
        
        function initAuth(){
            
            if($this->auth_group===null)
                throw new Exception('In BackendSession Action you need set $auth_group parameter in your controller');
            
            $this->auth=new Matj_Auth_Session();
            
            
            
            //if($this->auth_group)
            
            
        }
        
        
        
        
        
				function postDispatch(){
            parent::postDispatch();
            
            $menu = $this->menuEnabled;
            if($this->_getParam("ajax", null))$menu = false;
            if($this->_getParam("json", null))$menu = false;
            
            if($menu)$this->initNavigation();



            
            
            if(!$this->_getParam("json") && !$this->_getParam("ajax") && empty($this->view->header)){
                    if($this->showHeader){	
                            if(empty($this->header)){
                                    //$this->header=''.$this->_getParam("module").".header.".$this->_getParam("controller").".".$this->_getParam("action");
                                    $this->header=$this->_getParam("controller").".header.".$this->_getParam("action");
                                    $this->view->header=$this->translate($this->header,$this->headerParams);
                            }
                            else{
                                $this->view->header=Matj_Util::parseParams($this->header,$this->headerParams);
                            }
                    }
            }

            $this->initFilemanager();
				}

        
        function initFilemanager(){
            $kcfinder=(Matj_Get::getBootstrap()->getOption('kcfinder'));
            if(!empty($kcfinder)){
                $sess=new Zend_Session_Namespace('KCFINDER');
                
                
                
                if($this->user && $this->user->getGroupId()<=$kcfinder["group_id"]){
                    $sess->disabled = false;
                    $sess->uploadURL = $kcfinder["uploadURL"];
                    $sess->uploadDir = $kcfinder["uploadDir"];
                }
                else{
                    $sess->disabled = true;
                }
            }
        }
	
				function initClients(){
            $this->clients=$this->user->fetchAccessClients();            
        }


				function initNavigation(){
					$navigationConfig = new Zend_Config_Xml(APPLICATION_PATH.'/configs/backend-navigation.xml');

											$mc=$navigationConfig->toArray();
											$gid=$this->user->group_id+0;

											foreach($mc as $k=>$d){

													//Zend_Debug::dump(($d["group_id"]>=$this->user->group_id ));

													if(!empty($d["group_id"]) && ( $gid > ($d["group_id"]+0)  || empty($gid))){
															unset($mc[$k]);
													}
											}






											$menu=new Matj_Navigation($mc);//$navigationConfig->toArray(); 

											foreach($menu->getPages() as $k=>$d){
						if(($d->isActive(true) or (!empty($this->menuActiveId) && $d->getId() == $this->menuActiveId)) && !$this->menuRecursive){
							$subMenu=$d->getPages();
						}
					}

					$options=array('submenuKey'=>'pages','recursive'=>$this->menuRecursive);
					$options["attributes"]["ul"]["class"]='nav navbar-nav';



					$this->view->placeholder('menu')->set($this->view->bootstrapNavigationUlLi($menu,$options));

					if(!empty($subMenu)){
						$options=array('submenuKey'=>'pages','recursive'=>false);
						$options["attributes"]["ul"]["class"]='nav nav-pills';
						$this->view->placeholder('submenu')->set($this->view->bootstrapNavigationUlLi($subMenu,$options));
					}
				}

}