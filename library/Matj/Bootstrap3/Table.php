<?php


class Matj_Bootstrap3_Table extends Matj_SmartTable2 {
           protected $_options = array (
		'form' => true,
		'header' => true,
		'sortable' => true,
		'autoHeaders' => false,
		'popupAlwaysVisible' => true,
		'actionUrl' => '',

		'sortKey' => 'sort',
		'sortDefault' => null,
		'sortDefaultDesc' => false,
		'ajax' => true,

		'paginator' => true,
		'paginatorClass' => "Matj_Bootstrap3_Paginator",
		'paginatorPosition' => 'bottom',

		'translatePrefix' => '',

		'pageKey' => 'page',
		'perpageKey' => 'perpage',
                'perpageDefault' => 50,

		'filter' => true,
                'columnOptions'=>false,
		'columnOptionsDefault'=>array(),
		'class'=>'smartTable table table-striped table-bordered table-condensed'
	);
           
           
           
       function ajaxRender(Matj_Bootstrap3_Action $controller){
           
           if($this->getOption('ajax')!=true)return;
           
           $request=$controller->getRequest();
           if($request->isXmlHttpRequest()){
               $request->setParam('json',true);
               $controller->getResponse()->setBody($this->__toString());
           }
           
       }    
       
       
       /**
	 * vrati order by klauzulu
	 *
	 * @return unknown
	 */
	public function getOrderBy () {
		$request = Matj_Get::getRequest();
		$col = $this->getSortColumn();
                if ($column = $this->getColumn($col)) {
			return array($column->getSortSql(),($this->isSortAsc() ? "ASC" : "DESC"));
		}

	}
    
}