<?php

/**
 * Rozsirenie Zend_Form o translator a view
 * @package Library
 * @author Matus Jancik
 * @since 11.9.2009
 */
class Matj_Bootstrap3_Form extends Matj_Form {

        const elementWrapperClass='col-sm-9';
        const elementLabelClass='col-sm-3 text-sm-right';
        protected $elementButtonsWrapperClass='col-sm-9 col-sm-offset-3';
       
	protected $_elementDecorators = array (
                'ViewHelper',
                array (
                        'Description',
                        array (
                                'tag' => 'p',
                                'escape' => false,
                                'class' => 'help-block'
                        )
                ),
                array('Errors',array('placement'=>'append')),
                array (
                        array (
                                'data' => 'HtmlTag'
                        ),
                        array (
                                'tag' => 'div',
                                'class' => self::elementWrapperClass
                        )
                ),
                array(
                    array('labelDtClose' => 'HtmlTag'), 
                    array('tag' => 'div', 'closeOnly' => true, 'placement' => 'prepend')),
                    'Label',
                    array(array('labelDtOpen' => 'HtmlTag'), 
                    array('tag' => 'div', 'openOnly' => true, 'placement' => 'prepend', 'class' => self::elementLabelClass)),

                array (
                        array (
                                'Row' => 'HtmlTag'
                        ),
                        array (
                                'tag' => 'div',
                                'class' => 'row'
                        )
                ),
                array (
                        array (
                                'RowGroup' => 'HtmlTag'
                        ),
                        array (
                                'tag' => 'div',
                                'class' => 'form-group'
                        )
                )
        );



	public function __construct ($options = null) {
                
            
            
            
            
                parent::__construct($options);
		
		$this->setAttrib('class','block-form');
		
	}


        function addElement ($element, $name = null, $options = null) {
            
            
            if(!preg_match('/(checkbox|radio)/',  strtolower($element))){
                $options["class"]=isset($options["class"]) ? $options["class"]." "."form-control" : "form-control";
                
                    
            }
            
            $element=parent::addElement($element, $name, $options);
            
            
            if(!empty($options["required"])){
                $d=$this->getElement($name)->getDecorator('label')->setOption('class','required');
            }
            
            /*
            if(!empty($options["col-size"])){
                $decorators=$this->_elementDecorators;
                $decorators[3][1]["class"]=$options["col-size"];
                $dec=$this->setElementDecorators($decorators,array($name));
            }
            */
            
            
            return $element;
        }
        
        function setColSize($class,array $elements){
            $decorators=$this->_elementDecorators;
            $decorators[3][1]["class"]=$class;
            $this->setElementDecorators($decorators,$elements);
        }


	function addButtons($options=array()){

		$buttons='<div class="row"><div class=col-sm-12><hr></div></div><div class="row"><div class="'.$this->elementButtonsWrapperClass.'">';

		
		if(!empty($options["submit"]["label"])){
                    $text=$options["submit"]["label"];
                }
		else{
                    $text=$this->getTranslator()->_("core.submit");
		}
		
		if(!empty($options["submit"]["class"])){
                        $class=$options["submit"]["class"];
                }
		else{
			$class='btn-primary';
		}
		
		
                if(!isset($image))$image="";	
                $buttons.='<button class="btn  btn-form-submit '.$class.'" type="submit">
                           '.$image.''.$text.'
                          </button>';

                $back=$this->getView()->url(array('action'=>null,'id'=>null));
                if(!empty($options["back"]["url"])){
                        $back=$options["back"]["url"];
                }

                if(!empty($options["back"]["show"])){
                        if(!empty($options["back"]["label"])){
                            $text=$options["back"]["label"];
                        }
                        else{
                            $text=$this->getTranslator()->_("core.cancel");
                        }
                        $buttons.='<span class="secondbtn"> '.$this->getTranslator()->_("core.or")
                                .' <a class="btn btn-sm btn-default btn-form-back" href="'.$back.'">'.$text.'</a></span>';
                }

                if(!empty($options["delete"]["show"])){
                        $delete=$this->getView()->url(array('action'=>'delete'));
                                if(!empty($options["delete"]["url"])){
                                $delete=$options["delete"]["url"];
                        }
                        if(!empty($options["delete"]["label"]))
                            $text=$options["delete"]["label"];
                        else
                            $text=$this->getTranslator()->_("core.delete");
                        
                        
                        $buttons.=' <a class="btn btn-danger btn-form-delete pull-right confirm_submit" alt="Skutočne chcete vymazať túto položku?" href="'.$delete.'">'.$text.'</a>';
                }


                        if(!empty($options["append"])){
                        $buttons.=$options["append"];
                }

                if(!empty($options["prepend"])){
                        $buttons=$options["prepend"].$buttons;
                }

        


		$buttons.='
                  </div> </div>
		';
		$this->addHidden('buttons');
    $this->buttons->setIgnore(true);
		$this->parse('buttons',$buttons);
	}
}