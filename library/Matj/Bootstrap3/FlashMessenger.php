<?php

class Matj_Bootstrap3_FlashMessenger extends Matj_Helper_FlashMessenger{
    
    
    public function __toString(){		
        $result="";
        if($this->hasMessages()){
                
                $closeBtn='<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>';
            

                $result = "<div class=\"flash ".$this->_namespace."\">";
                foreach ($this->getMessages() as $k => $value){
                        switch($value["class"]){
                                case "success": $class="notice"; break;
                                default: $class=$value["class"]; break;
                        }

                        $result.='<div class="alert alert-'.$value["class"].'">'.$closeBtn.$value["msg"].'</div>';
                }		
                $result.="</div>";

        }
        $this->clearMessages();	
        return $result;
		
    }
    

}