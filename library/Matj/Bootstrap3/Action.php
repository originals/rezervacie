<?php

class Matj_Bootstrap3_Action extends Matj_Controller_Action {
    
    
    /**
    *
    * @var Model_User
    */
    protected $user;

    /**
    *
    * @var Matj_Auth_Session
    */
    protected $auth;

    protected $seoIsSet;
    
    
    function preDispatch() {
        parent::preDispatch();
        $this->auth=new Matj_Auth_Session();
        
        if($this->auth->isAuthenticated()){
            $this->view->user=$this->user=$this->auth->getUser(); 
        }     
        
    }
    
    
    public function postDispatch() {
        parent::postDispatch();
        
        if($this->view->header){
            $h=$this->view->header;
            if(is_array($h)){
                if(count($h)>1){
                    $this->view->header="<small>".$h[0].'</small><br>'.$h[1];
                }
                else{
                    $this->view->header=implode(" - ",$h);
                }
            }
            else{
                $h=array($this->view->header);
            }
            
            $this->view->headTitle(strip_tags(implode(" - ",$h)),true);
        }
        
    }
    
    
    function init() {
        parent::init();
        
        
    }

    function initFlashMessenger(){
        $this->msg = new Matj_Bootstrap3_FlashMessenger();
        Matj_Get::setFlashMessenger($this->msg);
    }

    
    
    function _setSeo($title,$description=null,$image=null,$url=null,$openGraph=array()){
        $this->seoIsSet=true;
        if(empty($description)){
            $description=$title;
            $ogdescription=$title;
        }
        else{
            $ogdescription=$description;
        }
            
        
        if(empty($image))
            $image=$this->view->urlHost().'/assets/img/logo.png';
        
        
        
        $this->view->seo()->setTitle($title);
        $this->view->seo()->setDescription($description);
        
        $this->view->openGraph()->setTitle($title);
        $this->view->openGraph()->setDescription($ogdescription);
        if($image)$this->view->openGraph()->setImage($image);
        if($url)$this->view->openGraph()->setUrl($url);
        $this->view->openGraph()->setAdmins('1479832080');
        
        if($this->facebook)
            $this->view->openGraph()->setProperty('app_id',$this->view->facebook()->getConfig('appId'));
        //$this->view->openGraph()->setPicture($this->view->urlHost().'/images/doska_logo3.jpg');
        
        foreach($openGraph as $k=>$v){
            $this->view->openGraph()->setProperty($k,$v);
        }
        
        
        
        
    }
}