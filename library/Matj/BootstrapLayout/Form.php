<?

/**
 * Rozsirenie Zend_Form o translator a view
 * @package Library
 * @author Matus Jancik
 * @since 11.9.2009
 */
class Matj_BootstrapLayout_Form extends Matj_Form {


	protected $_elementDecorators = array (
		'ViewHelper',
		array (
			'Description',
			array (
				'tag' => 'p',
				'class' => 'help-block',
                                'escape'=>false
			)
		),
		array('Errors',array('placement'=>'append')),
		array (
			array (
				'data' => 'HtmlTag'
			),
			array (
				'tag' => 'div',
				'class' => 'controls'
			)
		),
		array (
			'Label',
			array (
				'class' => 'control-label'
			)
		),
		array (
			array (
				'Row' => 'HtmlTag'
			),
			array (
				'tag' => 'div',
				'class' => 'control-group'
			)
		)
	);



	public function __construct ($options = null) {
		parent::__construct($options);
		
		$this->setAttrib('class','form-horizontal');
		
	}





	function addButtons($options=array()){

		$buttons='<div class="form-actions">';

		/*if($options["button"]["src"]){
			$image="";
		}else{
			$image='<img src="'.$this->getView()->baseUrl().'/images/ic_tick.png" alt="Save">';
		}*/

		if(!empty($options["submit"]["label"])){
        	$text=$options["submit"]["label"];
    	}
		else{
			$text=$this->translate("submit");
		}
		
		if(!empty($options["submit"]["class"])){
        	$class=$options["submit"]["class"];
    	}
		else{
			$class='btn-primary';
		}
		
		
	if(!isset($image))$image="";	
        $buttons.='<button class="btn '.$class.'" type="submit">
                   '.$image.''.$text.'
                  </button>';

        $back=$this->getView()->url(array('action'=>null,'id'=>null));
        if(!empty($options["back"]["url"])){
        	$back=$options["back"]["url"];
    	}

        if(!empty($options["back"]["show"])){
        	$buttons.='<span class="secondbtn"> alebo <a class="btn" href="'.$back.'">návrat</a></span>';
        }

        if(!empty($options["delete"]["show"])){
        	$delete=$this->getView()->url(array('action'=>'delete'));
			if(!empty($options["delete"]["url"])){
	        	$delete=$options["delete"]["url"];
	    	}

        	$buttons.=' <a class="btn btn-danger pull-right confirm_submit" alt="Skutočne chcete vymazať túto položku?" href="'.$delete.'">Vymazať</a>';
        }

		
		if(!empty($options["append"])){
        	$buttons.=$options["append"];
        }

        if(!empty($options["prepend"])){
        	$buttons=$options["prepend"].$buttons;
        }

        


		$buttons.='
                  </div>
		';
		$this->addHidden('buttons');
                $this->buttons->setIgnore(true);
		$this->parse('buttons',$buttons);
	}
}