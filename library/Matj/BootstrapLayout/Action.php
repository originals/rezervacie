<?php



class Matj_BootstrapLayout_Action extends Matj_Controller_Action {
    

	public $menuActiveId;
	public $menuRecursive = true;
	 
	protected $showHeader = true;
	protected $headerParams = array();
	protected $header = null;

   
   	public function init () {
		parent::init();
		/* Initialize action controller here */
		$this->view->layout()->setLayout('backend-bootstrap');
		
		
		
	}
	
	function preDispatch(){
		parent::preDispatch();
		$this->header=null;
		
	}
	
	function postDispatch(){
		parent::postDispatch();
		
		if(!$this->_getParam("json") && !$this->_getParam("ajax") && empty($this->view->header)){
		
			if($this->showHeader){	
				if(empty($this->header)){
					$this->header=''.$this->_getParam("module").".header.".$this->_getParam("controller").".".$this->_getParam("action");
				}
				$this->view->header=$this->translate($this->header,$this->headerParams);
			}
		}
                
                
                $kcfinder=(Matj_Get::getBootstrap()->getOption('kcfinder'));
                if(!empty($kcfinder)){
                    $sess=new Zend_Session_Namespace('KCFINDER');
                    $sess->disabled = false;
                    $sess->uploadURL = $kcfinder["uploadURL"];
                    $sess->uploadDir = $kcfinder["uploadDir"];
                }
		
	}



	function initNavigation(){
		$navigationConfig = new Zend_Config_Xml(APPLICATION_PATH.'/configs/backend-navigation.xml');

		$menu=new Matj_Navigation($navigationConfig);//$navigationConfig->toArray();
		foreach($menu->getPages() as $k=>$d){
			if(($d->isActive(true) or (!empty($this->menuActiveId) && $d->getId() == $this->menuActiveId)) && !$this->menuRecursive){
				$subMenu=$d->getPages();
			}
		}


		$options=array('submenuKey'=>'pages','recursive'=>$this->menuRecursive);
		$options["attributes"]["ul"]["class"]='wat-cf nav';
		$this->view->placeholder('menu')->set($this->view->bootstrapNavigationUlLi($menu,$options));

		if(!empty($subMenu)){
			$options=array('submenuKey'=>'pages','recursive'=>false);
			$options["attributes"]["ul"]["class"]='wat-cf nav nav-pills';
			$this->view->placeholder('submenu')->set($this->view->bootstrapNavigationUlLi($subMenu,$options));
		}
		
		
		

	}

	   
}