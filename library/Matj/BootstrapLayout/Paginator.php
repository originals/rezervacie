<?php

require_once 'Matj/Paginator/Abstract.php';

class Matj_BootstrapLayout_Paginator extends Matj_Paginator_Abstract {

    public $showPages = 7;
    public $showPagesLabel = true;
    public $showPages2 = 3;
    public $renderPerPage=true;

    public $css = array(
                'wrapper'=>'pagination'
    );


    function render(){
        
        if($this->parent instanceof Matj_SmartTable2 && $this->parent->getOption("ajax")){
                $this->parent->addJavascript('paginatorPlugin','
                        $(document).ready(function(){
                                $("#'.$this->parent->getId().' .'.$this->css['wrapper'].' a").click(function(){
                                                return delphi.smarttable.runUrl($(this).attr("href"),"'.$this->parent->getId().'");
                                });
                        });
                ');
        }
		
        
        
        $html='<div class="'.$this->css["wrapper"].'">';
        //$html.=$this->renderFound();
        if($this->renderPerPage)$html.=$this->renderPerPage();
        $html.=$this->renderPages();
        $html.='</div>';
        return $html;
    }


    function renderPerPage(){
        
        $from=$this->getOffset()+1;
        $to=$from+$this->getLimit()-1;
        
        
        
        $html='<ul class="perpage">'
        	.'<li><a href="#" class="disabled">'.$this->translate($this->t["found"],array('count'=>$this->getCount(),'from'=>$from,'to'=>$to)).' '.$this->translate($this->t["perpage"]).'</a></li>';

        foreach($this->perpageOptions as $k=>$d){

			$html.='<li '.($this->getPerPage()==$d ? 'class = "active"' : '').' ><a href="'.$this->url(array('perpage'=>$d,'page'=>null)).'"  >'.$d.'</a></li>';

/*			if($this->getPerPage()==$d)
					$html.='<span class="current">'.$d.'</span>';
            	else
  */
//			$html.='<a href="'.$this->url(array('perpage'=>$d,'page'=>null)).'" '.($this->getPerPage()==$d ? 'class="current"' : "").'>'.$d.'</a>';
        }


        $html.='</ul>';
        return $html;
    }

    function renderFound(){
        $html='<span class="found">'
        	.'<span class="label">'.$this->translate($this->t["found"],array('count'=>$this->getCount())).'</span>';
        $html.='</span>';
        return $html;
    }

    function renderPages(){
        if($this->getPages()<=$this->showPages){
			$f=1;$l=$this->getPages();
			$fl=false;
		}
		else{
			if($this->getPage()<=$this->showPages2){
				$f=1;
				$l=$this->showPages;
				$fl=true;
			}
			elseif($this->getPage()>=($this->getPages()-$this->showPages2)){
				$f=$this->getPages()-$this->showPages+1;
				$l=$this->getPages();
				$fl=true;
			}
			else{
				$f=$this->getPage()-$this->showPages2;
				$l=$this->getPage()+$this->showPages2;
				$fl=true;
			}

		}
		$html='<ul class="pages pull-right">';

        if($this->showPagesLabel)
        $html.='<li ><a href="#" class="disabled">'.$this->translate($this->t["page"],array('page'=>$this->getPage(),'pages'=>$this->getPages())).'</a></li>';


			if($this->getPage()>1){
				if($fl){
					$i=1; $html.='<li><a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">&lt;&lt;</a></li>';
				}
				$i=$this->getPage()-1; $html.='<li><a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">&lt;</a></li>';
			}

			for($i=$f;$i<=$l;$i++){
				/*if($this->getPage()==$i)
					$html.='<li><span class="current">'.$i.'</span></li>';
            	else*/
                	$html.='<li '.($this->getPage()==$i ? 'class = "active"' : '').'><a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">'.$i.'</a></li>';
            }

			if($this->getPage()<$this->getPages()){
				$i=$this->getPage()+1; $html.='<li><a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">&gt;</a></li>';
				if($fl){
					$i=$this->getPages(); $html.='<li><a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">&gt;&gt;</a></li>';
				}
			}


        $html.='</span>';
        return $html;
    }



    function __toString(){
        return $this->render();
    }


 	function url($array=array(),$route="default",$clear=null,$amp=null){
	    $array=Matj_Util::mergeArrays($array, $this->urlParams);
            return Matj_Get::getView()->url($array,$route,$clear,$amp);
	}

}