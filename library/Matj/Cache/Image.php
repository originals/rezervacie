<?


class Matj_Cache_Image {

    
   public $view;
   
   function __construct(Zend_View $view) {
       $this->view=$view;
   }
    
   
   static function baseUrl(){
       return Zend_Controller_Front::getInstance()->getBaseUrl();
   }
    
   static function image($src,$type=null,$w=null,$h=null){
       
     //  return 'http://arnox.originals.sk/uploads/_cache/uploads-images-products-muzi-sportove-oblecenie-rolaky-firn-navy-1-jpg-fill-51x68.jpg';
        
        if(empty($src) || empty($type))return false;
       
       
        
        $params["w"]=$w;
        $params["h"]=$h;
        
        $src=urldecode($src);
        
        
        //$folder=new Matj_File_Folder('uploads');
        //$folder=$folder->getFolder('_cache',true);
        
        $folder='uploads/_cache';
        $ext = pathinfo($src, PATHINFO_EXTENSION);
        
        
        
        try{
            $file=new Matj_File_File($src);
        }
        catch(Exception $e){
            return null;
        }
        
        
        
        $cacheFilePrefix=Matj_String::convertToUrlFriendly($src,false);
//        echo $cacheFilePrefix;
        
        if($type=="delete"){
            $folder=new Matj_File_Folder($folder);
            $files=$folder->findFiles($cacheFilePrefix);
            foreach($files as $f){
                $f->delete();
            }
            return true;
        }
        
        
        
        $cacheFile=$folder.DIRECTORY_SEPARATOR.$cacheFilePrefix."-".$type."-".$w."x".$h.".".$ext;
        
        
        
        
        if(file_exists($cacheFile)){
            return self::baseUrl().DIRECTORY_SEPARATOR.$cacheFile;
        }
        
        //echo "generate image<br>";
        
        try{
            $image = Matj_WideImage::load($src);
        }
        catch(Exception $e){
            return null;
        }
        
        
        if($type=="crop"){
                $add=0;
                $image->resize($params["w"]+$add, $params["h"]+$add, 'outside','down')
                ->crop('center', 'center', $params["w"]-$add, $params["h"]-$add)
                ->saveToFile($cacheFile);
        
                return self::baseUrl().DIRECTORY_SEPARATOR.$cacheFile;
        }
        
        if($type=="resize"){
            $image->resize($params["w"], $params["h"], 'inside','down')
                ->saveToFile($cacheFile,80);
            return self::baseUrl().DIRECTORY_SEPARATOR.$cacheFile;
        }
        
        
        if($type=="fill"){
            $white=$image->allocateColor(255,255,255);
            $image->resize($params["w"]+$add, $params["h"]+$add, 'inside')
                ->resizeCanvas($params["w"]-$add, $params["h"]-$add,'center', 'center',$white)
                    ->saveToFile($cacheFile);
            return self::baseUrl().DIRECTORY_SEPARATOR.$cacheFile;
        }
        
        
        
    }
    
    
    
    
    
    
}