<?php

abstract class Matj_Paginator_Abstract {
    
    public $page = 1;
    public $count = 0;
    public $perpage = 20;
    public $perpageOptions = array(10,20,50,100);
    public $urlParams = array();    
    
    public $t = array(
    		'perpage'=>"paginator.perpage",
                'found'=>"paginator.found",
                'page'=>"paginator.page",
                );
    public $css = array(
                'wrapper'=>'paginator'
    ); 
                
    
    public $parent;
    
    function __construct($params=array(),$parent=null) {
        $this->parent=$parent;
    }
    
    
    /**
     * @return Matj_Paginator_Abstract
     */
    public function setPage($page){
		if($page>0){// && $page<=$this->getPages()){
           $this->page = $page;
		}
		return $this;
	}
	function getPage(){
	    return $this->page;
	}
    /**
     * @return Matj_Paginator_Abstract
     */
	public function setCount($count){
		$this->count = $count;
		return $this;
	}
	public function getCount(){
	    return $this->count;
	}
    /**
     * @return Matj_Paginator_Abstract
     */
	public function setPerPage($page){
		$this->perpage = $page;
		return $this;
	}
	public function getPerPage(){
		return $this->perpage;
	}
	/**
     * @return Matj_Paginator_Abstract
     */
	public function setPerPageOptions(array $d){
		$this->perpageOptions = $d;
		return $this;
	}
	function getPages(){
	    return ceil($this->getCount()/$this->getPerPage());
	}
	
	function getOffset(){
	    return ($this->getPage()-1)*$this->getPerPage();
	}
	function getLimit(){
	    return $this->getPerPage();
	}
	
	
	
	function url($array=array(),$file=null,$clear=null,$amp=null){
            return Matj_Get::getView()->url($array,$file,$clear,$amp);
	}
	
	
	function translate($text,$parse=array()){
	    return Matj_Get::getTranslator()->_($text,$parse);
	}
    
	function setCss($key,$value=null){
	    if(is_array($key)){
	        $this->css=$key;
	    }
	    else{
	        $this->css[$key]=$value;
	    }
	    return $this;
	}
	
	
}