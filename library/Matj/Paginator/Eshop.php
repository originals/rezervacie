<?php

require_once 'Matj/Paginator/Abstract.php';

class Matj_Paginator_Eshop extends Matj_Paginator_Abstract {

    public $showPages = 5;
    public $showPages2 = 1;
    
    public $perpage = 24;
    public $perpageOptions = array(12,24,48,64);
    
    
    public $sort;
    public $sort2;
    
    
    function render(){
        $html='<div class="eshop-paginator row">';
        $html.=$this->renderPages();
        $html.=$this->renderPerPage();
        $html.=$this->renderOrder();
      //  $html.=$this->renderFound();
        //$html.='<div class="clearfix"></div>'; 
        $html.='</div>';
        return $html;
    }
    
    
//// '<li><a href="#" class="disabled">'.$this->translate($this->t["found"],array('count'=>$this->getCount())).' '.$this->translate($this->t["perpage"]).'</a></li>';
    
    function renderOrder(){
        
        $order[]=array('sort'=>'rank','sort2'=>null,'text'=>$this->translate('arnox.sort.rank'));
        $order[]=array('sort'=>'name','sort2'=>null,'text'=>$this->translate('arnox.sort.name'));
        $order[]=array('sort'=>'name','sort2'=>'desc','text'=>$this->translate('arnox.sort.name.desc'));
        $order[]=array('sort'=>'price','sort2'=>null,'text'=>$this->translate('arnox.sort.price'));
        $order[]=array('sort'=>'price','sort2'=>'desc','text'=>$this->translate('arnox.sort.price.desc'));
        $order[]=array('sort'=>'new','sort2'=>null,'text'=>$this->translate('arnox.sort.new'));
        $order[]=array('sort'=>'sell','sort2'=>null,'text'=>$this->translate('arnox.sort.sell'));
        
        $selected=$order[0];
        foreach($order as $k=>$o){
             if($this->sort==$o["sort"])
                 if(!$o["sort2"]&& !$this->sort2){
                     $selected=$o;
                 }
                 elseif($o["sort2"] && $this->sort2){
                     $selected=$o;
                 }
        }
        //Zend_Debug::dump($this->sort);
        
        
        
        $html='<div class="col-sm-5 order">
                    <div class="btn-group">
                        <button class="btn disabled">'.$this->translate('paginator.order').':</button>
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                          '.$selected["text"].'
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                          ';
                            foreach($order as $k=>$d){
                                $html.='<li><a href="'.$this->url(array('sort'=>$d["sort"],'sort2'=>$d["sort2"])).'"  >'.$d["text"].'</a></li>';
                            }
             $html.='
                        </ul>
                    </div>

                </div>';
        return $html;
    }
    
    
    
    
    function renderPerPage(){
        $html='<div class="col-sm-3 perpage">
            
                    <div class="btn-group">
                        <button class="btn disabled">'.$this->translate('paginator.perpage').':</button>
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                          '.$this->getPerPage().'
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                          ';
                            foreach($this->perpageOptions as $k=>$d){
                                        $html.='<li '.($this->getPerPage()==$d ? 'class = "active"' : '').' ><a href="'.$this->url(array('perpage'=>$d,'page'=>null)).'"  >'.$d.'</a></li>';
                            }
             $html.='
                        </ul>
                    </div>

                </div>';
        return $html;
 
    }
    
    function renderFound(){
        $from=$this->getOffset()+1;
        $to=$from+$this->getLimit()-1;
        
        $html='<div class="found col-sm-3">'.$this->translate($this->t["found"],array('count'=>$this->getCount(),'from'=>$from,'to'=>$to)).'</div>';
        return $html;
    }
    
    function renderPages(){
        
        if($this->getPages()<2)return '';
        
        if($this->getPages()<=$this->showPages){
			$f=1;$l=$this->getPages();
			$fl=false;
		}
		else{
			if($this->getPage()<=$this->showPages-2){
				$f=1;
				$l=$this->showPages;
				$fl=true;
			}
			elseif($this->getPage()>=($this->getPages()-$this->showPages2)){
				$f=$this->getPages()-$this->showPages+1;
				$l=$this->getPages();
				$fl=true;
			}
			else{
				$f=$this->getPage()-$this->showPages2;
				$l=$this->getPage()+$this->showPages2;
				$fl=true;
			}
			
		}
		$html='<div class="col-sm-4"><ul class="nav nav-pills">';
        //$html.='<li ><a href="#" class="disabled">'.$this->translate($this->t["page"],array('page'=>$this->getPage(),'pages'=>$this->getPages())).'</a></li>';
            
			
			if($this->getPage()>1){ 
				$i=$this->getPage()-1; $html.='<li><a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'" class=prev>&lt;</a></li>'; 
				if($this->getPage()>$this->showPages){
					$i=1; $html.='<li><a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">'.$i.'</a><li><a href="#">...</a></li></li>'; 
				}
                        }
            
            for($i=$f;$i<=$l;$i++){

                            $html.='<li '.($this->getPage()==$i ? 'class = "active"' : '').'><a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">'.$i.'</a></li>';
            }	
			
			if($this->getPage()<$this->getPages()){ 
				if($this->getPages()>$this->showPages && $this->getPage()<$this->getPages()-$this->showPages2){
					$i=$this->getPages(); $html.='<li><a href="#">...</a></li><li><a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">'.$i.'</a></li>'; 
				}
				$i=$this->getPage()+1; $html.='<li><a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'" class=next>&gt;</a></li>'; 
			}
			
			
        $html.='</ul></div>';
        return $html;
    }
    
    
    
    function __toString(){
        return $this->render();
    }
    
}