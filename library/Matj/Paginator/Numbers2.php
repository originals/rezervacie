<?php

require_once 'Matj/Paginator/Abstract.php';

class Matj_Paginator_Numbers2 extends Matj_Paginator_Abstract {

    public $showPages = 7;
    public $showPages2 = 3;

    public $css = array(
                'wrapper'=>'pagination'
    ); 
   
    
    function render(){
        $html='<div class="'.$this->css["wrapper"].'">';
        $html.=$this->renderFound();
        $html.=$this->renderPerPage();
        $html.=$this->renderPages();
        $html.='</div>';
        return $html;
    }
    
    
    function renderPerPage(){
        $html='<span class="perpage">'
        	.'<span class="label">'.$this->translate($this->t["perpage"]).'</span>';
        
        foreach($this->perpageOptions as $k=>$d){
            if($this->getPerPage()==$d)
					$html.='<span class="current">'.$d.'</span>';
            	else 
                	$html.='<a href="'.$this->url(array('perpage'=>$d,'page'=>null)).'">'.$d.'</a>';
//			$html.='<a href="'.$this->url(array('perpage'=>$d,'page'=>null)).'" '.($this->getPerPage()==$d ? 'class="current"' : "").'>'.$d.'</a>';
        }	
        	
        
        $html.='</span>';
        return $html;
    }
    
    function renderFound(){
        $html='<span class="found">'
        	.'<span class="label">'.$this->translate($this->t["found"],array('count'=>$this->getCount())).'</span>';
        $html.='</span>';
        return $html;
    }
    
    function renderPages(){
        if($this->getPages()<=$this->showPages){
			$f=1;$l=$this->getPages();
			$fl=false;
		}
		else{
			if($this->getPage()<=5){
				$f=1;
				$l=$this->showPages;
				$fl=true;
			}
			elseif($this->getPage()>=($this->getPages()-$this->showPages2)){
				$f=$this->getPages()-$this->showPages+1;
				$l=$this->getPages();
				$fl=true;
			}
			else{
				$f=$this->getPage()-$this->showPages2;
				$l=$this->getPage()+$this->showPages2;
				$fl=true;
			}
			
		}
		$html='<span class="pages">';
        $html.='<span class="label">'.$this->translate($this->t["page"],array('page'=>$this->getPage(),'pages'=>$this->getPages())).'</span>';
            
			
			if($this->getPage()>1){ 
				if($fl){
					$i=1; $html.='<a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">&lt;&lt;</a>'; 
				}
				$i=$this->getPage()-1; $html.='<a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">&lt;</a>'; 
			}
            
			for($i=$f;$i<=$l;$i++){
				if($this->getPage()==$i)
					$html.='<span class="current">'.$i.'</span>';
            	else 
                	$html.='<a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">'.$i.'</a>';
            }	
			
			if($this->getPage()<$this->getPages()){ 
				$i=$this->getPage()+1; $html.='<a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">&gt;</a>'; 
				if($fl){
					$i=$this->getPages(); $html.='<a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">&gt;&gt;</a>'; 
				}
			}
			
			
        $html.='</span>';
        return $html;
    }
    
    
    
    function __toString(){
        return $this->render();
    }
    
}