<?php

require_once 'Matj/Paginator/Abstract.php';

class Matj_Paginator_Numbers extends Matj_Paginator_Abstract {

    public $showPages = 7;
    public $showPages2 = 3;
                
    function render(){
        $html='<span class="'.$this->css["wrapper"].'">';
        $html.=$this->renderFound();
        $html.=$this->renderPerPage();
        $html.=$this->renderPages();
        $html.='</span>';
        return $html;
    }
    
    
    function renderPerPage(){
        $html='<span class="perpage">'
        	.'<span>'.$this->translate($this->t["perpage"]).'</span>';
        
        foreach($this->perpageOptions as $k=>$d){
            $html.='<a href="'.$this->url(array('perpage'=>$d,'page'=>null)).'" '.($this->getPerPage()==$d ? 'class="selected"' : "").'>'.$d.'</a>';
        }	
        	
        
        $html.='</span>';
        return $html;
    }
    
    function renderFound(){
        
        $from=$this->getOffset()+1;
        $to=$from+$this->getLimit()-1;
        
        $html='<span class="found">'
        	.'<span>'.$this->translate($this->t["found"],array('count'=>$this->getCount(),'from'=>$from,'to'=>$to)).'</span>';
        $html.='</span>';
        return $html;
    }
    
    function renderPages(){
        if($this->getPages()<=$this->showPages){
			$f=1;$l=$this->getPages();
			$fl=false;
		}
		else{
			if($this->getPage()<=5){
				$f=1;
				$l=$this->showPages;
				$fl=true;
			}
			elseif($this->getPage()>=($this->getPages()-$this->showPages2)){
				$f=$this->getPages()-$this->showPages+1;
				$l=$this->getPages();
				$fl=true;
			}
			else{
				$f=$this->getPage()-$this->showPages2;
				$l=$this->getPage()+$this->showPages2;
				$fl=true;
			}
			
		}
		$html='<span class="pages">';
        $html.='<span>'.$this->translate($this->t["page"],array('page'=>$this->getPage(),'pages'=>$this->getPages())).'</span>';
            
			
			if($this->getPage()>1){ 
				if($fl){
					$i=1; $html.='<a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">&lt;&lt;</a>'; 
				}
				$i=$this->getPage()-1; $html.='<a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">&lt;</a>'; 
			}
            
			for($i=$f;$i<=$l;$i++){
                $html.='<a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'" '.($this->getPage()==$i ? 'class="selected"' : "").'>'.$i.'</a>';
            }	
			
			if($this->getPage()<$this->getPages()){ 
				$i=$this->getPage()+1; $html.='<a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">&gt;</a>'; 
				if($fl){
					$i=$this->getPages(); $html.='<a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">&gt;&gt;</a>'; 
				}
			}
			
			
        $html.='</span>';
        return $html;
    }
    
    
    
    function __toString(){
        return $this->render();
    }
    
}