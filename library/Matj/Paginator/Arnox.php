<?php

require_once 'Matj/Paginator/Abstract.php';

class Matj_Paginator_Arnox extends Matj_Paginator_Abstract {

    public $showPages = 5;
    public $showPages2 = 1;
    
    public $perpage = 24;
    public $perpageOptions = array(12,24,48,64);
    
    
    public $sort;
    public $sort2;
    
    
    function render(){
        $html='<div class="arnox-paginator">';
        $html.=$this->renderOrder();
        $html.=$this->renderFound();
        $html.=$this->renderPerPage();
        $html.=$this->renderPages();
        //$html.='<div class="clearfix"></div>'; 
        $html.='</div>';
        return $html;
    }
    
    
//// '<li><a href="#" class="disabled">'.$this->translate($this->t["found"],array('count'=>$this->getCount())).' '.$this->translate($this->t["perpage"]).'</a></li>';
    
    function renderOrder(){
        
        $order[]=array('sort'=>'rank','sort2'=>null,'text'=>$this->translate('arnox.sort.rank'));
        $order[]=array('sort'=>'name','sort2'=>null,'text'=>$this->translate('arnox.sort.name'));
        $order[]=array('sort'=>'name','sort2'=>'desc','text'=>$this->translate('arnox.sort.name.desc'));
        $order[]=array('sort'=>'price','sort2'=>null,'text'=>$this->translate('arnox.sort.price'));
        $order[]=array('sort'=>'price','sort2'=>'desc','text'=>$this->translate('arnox.sort.price.desc'));
        $order[]=array('sort'=>'new','sort2'=>null,'text'=>$this->translate('arnox.sort.new'));
        $order[]=array('sort'=>'sell','sort2'=>null,'text'=>$this->translate('arnox.sort.sell'));
        
        $selected=$order[0];
        foreach($order as $k=>$o){
             if($this->sort==$o["sort"])
                 if(!$o["sort2"]&& !$this->sort2){
                     $selected=$o;
                 }
                 elseif($o["sort2"] && $this->sort2){
                     $selected=$o;
                 }
        }
        //Zend_Debug::dump($this->sort);
        
        
        
        $html='<div class="order-wrapper pull-left">
                <strong>'.$this->translate('paginator.order').':</strong>
                    <div class=perpage-selected>
                        <span>'.$selected["text"].'</span>'
                  .'<ul class="perpage">';
                        foreach($order as $k=>$d){
                            $html.='<li><a href="'.$this->url(array('sort'=>$d["sort"],'sort2'=>$d["sort2"])).'"  >'.$d["text"].'</a></li>';
                        }	
        $html.='</ul></div></div>';
        return $html;
    }
    
    
    
    
    function renderPerPage(){
        $html='<div class="perpage-wrapper pull-left">
                <strong>'.$this->translate('paginator.perpage').':</strong>
                    <div class=perpage-selected>
                        <span>'.$this->getPerPage().'</span>'
                  .'<ul class="perpage">';
                        foreach($this->perpageOptions as $k=>$d){

                                        $html.='<li '.($this->getPerPage()==$d ? 'class = "active"' : '').' ><a href="'.$this->url(array('perpage'=>$d,'page'=>null)).'"  >'.$d.'</a></li>';
                        }	
        $html.='</ul></div></div>';
        return $html;
    }
    
    function renderFound(){
        $from=$this->getOffset()+1;
        $to=$from+$this->getLimit()-1;
        
        $html='<div class="found  pull-left">'.$this->translate($this->t["found"],array('count'=>$this->getCount(),'from'=>$from,'to'=>$to)).'</div>';
        return $html;
    }
    
    function renderPages(){
        
        if($this->getPages()<2)return '';
        
        if($this->getPages()<=$this->showPages){
			$f=1;$l=$this->getPages();
			$fl=false;
		}
		else{
			if($this->getPage()<=$this->showPages-2){
				$f=1;
				$l=$this->showPages;
				$fl=true;
			}
			elseif($this->getPage()>=($this->getPages()-$this->showPages2)){
				$f=$this->getPages()-$this->showPages+1;
				$l=$this->getPages();
				$fl=true;
			}
			else{
				$f=$this->getPage()-$this->showPages2;
				$l=$this->getPage()+$this->showPages2;
				$fl=true;
			}
			
		}
		$html='<ul class="pages pull-right">';
        //$html.='<li ><a href="#" class="disabled">'.$this->translate($this->t["page"],array('page'=>$this->getPage(),'pages'=>$this->getPages())).'</a></li>';
            
			
			if($this->getPage()>1){ 
				$i=$this->getPage()-1; $html.='<li><a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'" class=prev></a></li>'; 
				if($this->getPage()>$this->showPages){
					$i=1; $html.='<li><a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">'.$i.'</a><li><span>...</span></li></li>'; 
				}
                        }
            
            for($i=$f;$i<=$l;$i++){

                            $html.='<li '.($this->getPage()==$i ? 'class = "active"' : '').'><a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">'.$i.'</a></li>';
            }	
			
			if($this->getPage()<$this->getPages()){ 
				if($this->getPages()>$this->showPages && $this->getPage()<$this->getPages()-$this->showPages2){
					$i=$this->getPages(); $html.='<li><span>...</span></li><li><a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'">'.$i.'</a></li>'; 
				}
				$i=$this->getPage()+1; $html.='<li><a href="'.$this->url(array('page'=>($i == 1 ? null : $i ))).'" class=next></a></li>'; 
			}
			
			
        $html.='</ul>';
        return $html;
    }
    
    
    
    function __toString(){
        return $this->render();
    }
    
}