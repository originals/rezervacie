<?php


require_once dirname(__FILE__).'/dibi.php';


class Matj_Dibi_Model extends Matj_Model_Abstract{
    
    protected $tableName;
    protected $idColumn="id";
            
    protected $tableCreateOptions;
    
    protected $createdColumn=false;
    protected $modifiedColumn=false;
    protected $loadIdColumn=false;
    
    protected $logChanges=false;

    
    
    protected $_data;
    
    /**
     *
     * @var MongoId
     */
    protected $_id;
    
    /**
     * 
     * @return DibiConnection
     */
    function getAdapter(){
        return Zend_Registry::get(Matj_Application_Resource_Dibi::DEFAULT_REGISTRY_KEY);
    }
    
    
    function count($options=array()){
        $options["returnCursor"]=true;
        return $this->fetchAll($options)->count();
    }
    
    
    
    
    function fetchAll($options=array()){
        $query=array();
        
        if(!empty($options["count"])){
            unset($options["limit"]);
            unset($options["group"]);
            $query[] = 'SELECT count(*) FROM ['.$this->tableName.']';
            $query=$this->buildQuery($query,$options);
            return $this->getAdapter()->query($query)->fetchSingle();
        }
        
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS * FROM ['.$this->tableName.']';
        $query=$this->buildQuery($query,$options);
        
        
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        
        
        $data=array();
        
        
        foreach($result as $k=>$r){
            
            
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }
            
            
        }
        
        return $data;
    }
    
    
    
    function __construct($data=null) {
        if(is_array($data)){
            $this->setData($data);
            return $this;
        }
        if(is_string($data)){
            if($this->loadIdColumn){
                $obj=$this->fetchOne(array($this->loadIdColumn=>$data));
                if($obj)return $obj;
            }
            
            return $this->fetchOne(array($this->idColumn=>($data)));
        }
        if(is_int($data)){
            return $this->fetchOne(array($this->idColumn=>($data)));
        }
    }
    
    
    function setData($data){
        
        if(isset($data["_id"])){
            $this->setId($data["_id"]);
        }
        foreach($data as $k=>$d){
            $this->_data[$k]=$d;
        }
    }
    
    function getData(){
        return $this->_data;
    }
    
    function setDataFromForm($data){
        return $this->setData($data);
    }
    
    
    
    function setId($id){
        $this->id=$id;
        
    }
    
    function getId(){
        
        $idColumn=$this->idColumn;
        
        return $this->{$idColumn};
    }
    
    
    
    
    protected $_columns;
    function getTableColumns(){
        if($this->_columns===null){
            $meta=$this->getTableMeta();
            $this->_columns=$meta["columns"];
        }
            
        return $this->_columns;
    }
    
    function getTableMeta(){
        
        if(!Zend_Registry::isRegistered("tablemeta_".$this->tableName)){

            $meta=array('columns'=>array(),'name'=>$this->tableName);
            foreach($this->getAdapter()->query('DESCRIBE ['.$this->tableName.']')->fetchAll() as $k=>$m){
                $meta["columns"][$m->Field]=$m->toArray();
            }
            
            Zend_Registry::set("tablemeta_".$this->tableName,$meta);
            
            
        }
        return Zend_Registry::get("tablemeta_".$this->tableName);
    }
    
    
    
    protected function _prepareSaveData($data=null){
        
        if(!$data)$data=$this->getData();
        unset($data["id"]);
        
        if($this->createdColumn && !$this->getId()){
            $data["created"]=new DateTime();
        }
        
        if($this->modifiedColumn){
            $data["modified"]=new DateTime();
        }
        
        $columns=$this->getTableColumns();
        foreach($data as $k=>$d){
            if(!key_exists($k, $columns))
                unset($data[$k]);
        }
        
        
       //Zend_Debug::dump($data);exit;
        return $data;
    }
    
    
    
    
    function save(){
        
        if($this->getId()){
            $data=$this->_prepareSaveData();
            
            if(method_exists($this, "updateCallback")){
                $before=$this->getAdapter()->fetch("SELECT * FROM  [".$this->tableName."] WHERE %and",array($this->idColumn=>$this->getId()))->toArray();
            }
            
            
            $this->update($data,array($this->idColumn=>$this->getId()));
            
            if(method_exists($this, "updateCallback")){
                $after=$this->getAdapter()->fetch("SELECT * FROM  [".$this->tableName."] WHERE %and",array($this->idColumn=>$this->getId()))->toArray();

                $diff=array_diff($after,$before);
                unset($diff["modified"]);
                
                $this->updateCallback($after,$before,$diff);
            }
            
            
            return $this;
        }
        else{
            $data=$this->_prepareSaveData();
            //Zend_Debug::dump($this->insert($data));exit;
            if(($id=$this->insert($data))>0 || empty($this->idColumn)){
                
                if(!empty($this->idColumn))
                    $data[$this->idColumn]=$id;
                
                $this->setData($data);
                return $this;
            }
            else{
                throw new Exception('Database INSERT error');
            }
        }
        
    }
    
    function getFoundRows(){
        return $this->getAdapter()->query("SELECT FOUND_ROWS()")->fetchSingle();
    }
    
    
    function fetchOne($where,$options=array()){
        
        $query=array();
        $query[] = 'SELECT * FROM ['.$this->tableName.']';
        
        $query=$this->buildQuery($query,array('where'=>$where));
        
        
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        
        $data=$result->fetch();
        if($data){
            $this->setData($data);
            return $this;
        }
        return false;
    }
    
    
    function delete(){
        if($this->getId()){
            
            $query=array();
            $query[] = 'DELETE FROM ['.$this->tableName.'] ';
            $where=array($this->idColumn => $this->getId());
            $options["where"]=$where;
            
            if($this->logChanges){
                $data=$this->fetchOne($where);
                if(!empty($data))
                    $data=$data->toArray ();
                $this->saveLogChanges($this->tableName,$this->getId(),'delete',array('where'=>$where,'data'=>$data));
            }

            $query=$this->buildQuery($query, $options);
            $this->getAdapter()->query($query);
            
            return $this->getAdapter()->getAffectedRows();

        }
    }
    
    
    function getApiData(){
        return $this->toArray();
    }
    
    
    
    function toArray($data=null){
        
        if(empty($data)){
            $data=$this->_data;
        }
        
        if(empty($data))
            return array();
        
        foreach($data as $k=>$d){
            if(is_object($d)){
                if($d instanceof MongoDate)
                    $data[$k]=Matj_Format::formatDate ($d,'Y-m-d H:i:s');
                elseif($d instanceof DateTime)
                    $data[$k]=$d->format('Y-m-d H:i:s');
                else
                    $data[$k]=$d."";
            }
            elseif(is_array($d) && !empty($d)){
                $data[$k]=$this->toArray($d);
            }
            else{
                $data[$k]=$d;
            }
        }
        
        return $data;
    }
    
    
    function __toString() {
        throw new Exception("Not defined __toString");
    }
    
    function getFormData(){
        $data=$this->toArray();
        
        return $data;
    }
    
    function getTableData(){
        $data=$this->toArray();
        
        if(empty($data["id"]) && isset($data["_id"]))
            $data["id"]=$data["_id"];
        
        return $data;
    }
    
    
    function isEmpty(){
        return empty($this->_data) && empty($this->_id);
    }
    
    function __get($name) {
        if(is_array($this->_data))
            if(key_exists($name,$this->_data))
                    return $this->_data[$name];
        
        if($name=="id")
            return $this->getId();
        
        return null;
    }
    function __set($name,$value) {
        $this->_data[$name]=$value;
    }
    
    
    function update($data,$where=null){
        
        if($where===null){
            
            if($this->getId()>0){
                $where=array($this->idColumn=>$this->getId());
            }
            else{
                throw new Exception('Update ALL / PREVENTION');
            }
        }
        
        
        $query=array();
        $query[] = 'UPDATE ['.$this->tableName.'] SET ';
        $query[]=$data;
        
        $options["where"]=$where;
        $query=$this->buildQuery($query, $options);
        $this->getAdapter()->query($query);
        
        
        if($this->logChanges){
            $this->saveLogChanges($this->tableName,$this->getId(),'update',array('where'=>$where,'data'=>$data));
        }
        
        
        
        return $this->getAdapter()->getAffectedRows();
        
    }
    
    function insert($data){
        
        $query=array();
        $query[] = 'INSERT INTO ['.$this->tableName.'] ';
        $query[]=$data;
        
        $this->getAdapter()->query($query);
        
        if($this->idColumn)
            $id=$this->getAdapter()->getInsertId();
        else
            $id="NoIdColumn";
        
        if($this->logChanges){
            $this->saveLogChanges($this->tableName,$id,'insert',array('data'=>$data));
        }
        
        return $id;
        
    }
    
    
    function saveLogChanges($tableName,$id,$operation,$data){
        $log=new Model_LogChanges();
        $log->table=$tableName;
        $log->row_id=$id;
        $log->operation=$operation;
        
        if(is_array($data)){
            $log->data=json_encode($data);
        }
        else{
            $log->data=print_r($data,true);
        }
        $log->time=date('Y-m-d H:i:s');
        
        
        
        $auth=new Matj_Auth_Session();
        
        if($auth->isAuthenticated()){
            $user=$auth->getSession()->user;
            if(!empty($user["id"]))
                $log->user_id=$user["id"];
        }
        
        $log->save();
        
        
    }
    
    
    function buildQuery($query,$options){
        
        return Matj_Dibi_Util::buildQuery($query, $options);
    }

    
    
}
