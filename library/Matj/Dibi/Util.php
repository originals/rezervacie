<?php

class Matj_Dibi_Util {
    
    
    
    public static function buildQuery($query,$options){
        
        if (!empty($options["where"])){
            
            if(is_array($options["where"])){
                $where=array();
                foreach($options["where"] as $k=>$w){
                    if(is_string($k)){
                        if(preg_match('/[\[\]\=\<\>]+/',$k)){
                            $where[]=array($k,$w);
                        }
                        else{
                            $where[$k]=$w;
                        }
                    }
                    else{
                        $where[]=$w;
                    }
                }
                
                array_push($query, 'WHERE %and', $where);
            }
            else{
                $query[]='WHERE '.$options["where"];
                
            }
            
            
            
        }
        
        
        if (!empty($options["group"])){
            
            if(is_array($options["group"]) && !is_array($options["group"][0])){
                $options["group"]=array($options["group"]);
            }
            
            array_push($query, 'GROUP BY %by', $options["group"]);

        }
        
        if (!empty($options["order"])){
            
            if(is_array($options["order"]) && !is_array($options["order"][0])){
                $options["order"]=array($options["order"]);
            }
            
            array_push($query, 'ORDER BY %by', $options["order"]);

        }
        
        if (!empty($options["limit"])){
            array_push($query, 'LIMIT %i', $options["limit"]);
        }
        if (!empty($options["offset"])){
            array_push($query, 'OFFSET %i', $options["offset"]);
        }
        
        
        
        return $query;
    }
    
}