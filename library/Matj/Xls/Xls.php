<?php

class Matj_Xls_Xls {
    
       
    function getDataFromExcel($file, $sheet, $rows, $cols){
        // COM CREATE
        fwrite(STDOUT, "—————————————-\r\n");
        $excel = new COM("Excel.application") or die ("ERROR: Unable to instantaniate COM!\r\n");
        fwrite(STDOUT, "Application name: {$excel->Application->value}\r\n") ;
        fwrite(STDOUT, "Loaded version: {$excel->Application->version}\r\n");
        fwrite(STDOUT, "—————————————-\r\n\r\n");

        // DATA RETRIEVAL
        $Workbook = $excel->Workbooks->Open($file) or die("ERROR: Unable to open " . $file . "!\r\n");
        $Worksheet = $Workbook->Worksheets($sheet);
        $Worksheet->Activate;
        $i = 0;
        foreach ($rows as $row)
        {
            $i++; $j = 0;
            foreach ($cols as $col)
            {
                $j++;
                $cell = $Worksheet->Range($col . $row);
                $cell->activate();
                $matrix[$i][$j] = $cell->value;
            }
        }

        // COM DESTROY
        $Workbook->Close();
        unset($Worksheet);
        unset($Workbook);
        $excel->Workbooks->Close();
        $excel->Quit();
        unset($excel);
        return $matrix;
    
    }

}
?>
