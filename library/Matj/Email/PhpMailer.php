<?php

/**
 * Trieda pre posielanie mailov
 * @package Core
 * @author Lukas Zemcak
 * @since 11.9.2009
 */

require_once 'class.phpmailer.php';

class Matj_Cms_Email_PhpMailer extends PHPMailer_5_1{
		
	function __construct($exceptions = false){
		parent::__construct($exceptions);
	}


	function send(){
		exit;
	}
	
    function sendEmail($data){
		exit;
		try { 
	        if(!is_array($data["to"])){
				if(preg_match("/\,/",$data["to"])){
				    $to=array();
				    foreach(explode(",",$data["to"]) as $m){
				        $to[]=$m;
				    }   
				}
	            else{
    				$to=array($data["to"]);
	            }
			}
			else{
			    $to=$data["to"];
			}
			
			
			$mail=$this;
			
			$mail->Subject=$data["subject"];
            $mail->IsHTML(true);
            $mail->CharSet="utf-8";
			$this->exceptions =true;
            
            if(!empty(Zend_Registry::get("config")->email->smtp->enable)){
                $mail->IsSMTP();
                $mail->SMTPAuth = !empty(Zend_Registry::get("config")->email->smtp->auth);
                $mail->Host = Zend_Registry::get("config")->email->smtp->host;
                $mail->Username = Zend_Registry::get("config")->email->smtp->username;
                $mail->Password = Zend_Registry::get("config")->email->smtp->password;
            }        
        
            $mail->From = $data["from"];
            $mail->FromName = $data["from_name"];
            $mail->Body=$data["body"];
    				
    			foreach($to as $val){
    	        	$mail->ClearAddresses();
                    $mail->ClearBCCs();
                    $mail->addAddress($val);
                
    		        if(!empty($data["file"])){
    		            if(is_array($data["file"])){
    		                foreach($data["file"] as $k=>$d){
    		                    if(file_exists($d)){
    		                        $att=$mail->createAttachment(file_get_contents($d));
    		                        $f=explode("/",$d);
    		                        $filename=$f[count($f)-1];
        		                    $att->filename=$filename;
    		                    }
    		                }
    		            }
    		        }
    		        if(!$mail->Send()){
    		            $error=1;
						echo $mail->ErrorInfo;
    		        }
                
    			}
    	    } catch (Zend_Exception $e) { 
    	       	$this->setError($e->getMessage());
    	       	return false;
    		} 
    	    return true;
        }
        
        
        
	
	
	
}
