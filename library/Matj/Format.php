<?php
/** 
 * @author Delphi
 * @package Delphi
 * 
 */
class Matj_Format
{

    const dateTimeFormat = "d.m.Y H:i:s";
	
	
    static function toDate($date,$format=null){
           if(!$format)$format=Zend_Locale_Format::getDateFormat(Matj_Get::getLocale());
           $date=new Zend_Date($date,null,"en");
       	   return $date->toString($format);
    }

    static function getDate($date,$format=null){
       //$format="yyyy-MM-dd";
	   if(empty($format))$format="Y-m-d";
	   

           if(is_array($date)){
               throw new Exception('Date must be a string');
           }
           
           
	   if(preg_match('/[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,4}/',$date)){
               	$h=explode(" ",$date);
			$d=explode(".",@$h[0]);
			$c=explode(":",@$h[1]);
			if($date=="")return '';
			
			$timestamp=mktime(@$c[0]+0,@$c[1]+0,@$c[2]+0,@$d[1]+0,@$d[0]+0,@$d[2]+0);
			$date=date($format,$timestamp);
			return $date;
	   }
	   elseif(preg_match('/[0-9]{1,4}-[0-9]{1,2}-[0-9]{1,4}/',$date)){
               return self::formatDate($date,$format);
           }
	   
       /*$date=new Zend_Date($date,null,Matj_Get::getLocale());
        return $date->toString($format);*/
    }
    
    static function formatDate($date,$format=null){
    	// return self::toDate(self::getDate($date),$format);
    	
    	if(!$format)$format="d.m.Y";
    	
    	if($date==0)return "";
		
		$h=explode(" ",$date);
		$d=explode("-",@$h[0]);
		$c=explode(":",@$h[1]);
		
		$timestamp=mktime(@$c[0]+0,@$c[1]+0,@$c[2]+0,@$d[1]+0,@$d[2]+0,@$d[0]+0);
		$date=date($format,$timestamp);
		return $date;
	
    	
    }
    
    
    static function getFloat($input){
       $input=preg_replace('/,/','.',$input);
       return preg_replace('/[^0-9.]/','',$input);
    }
    
    static function toFloat($input){
       return Zend_Locale_Format::toFloat($input,array('locale'=>Matj_Get::getLocale()));
    }
    
    static function toPrice($int,$options=array()){
		$session=new Zend_Session_Namespace('locale');
		$config=Matj_Get::getCurrencyRate();
		$currency="€";
		if($config && $session->currency=="czk"){
			$int=$int*$config;
			$currency="KČ";
		}
		
			if(!empty($options["dash"]) && round($int)==$int){
				$html=round($int).',-';
			}	
			else{
				$html=number_format((float)$int,(key_exists('decimal',$options) ? $options["decimal"] : 2 ),',',' ');
			}

			if(!empty($options["noHtml"])){
				
				$html.=' '.$currency;
			}
			else{
				$html.=' <span class="currency">'.(empty($session->currency) ? '&euro;' : $currency ).'</span>';
			}
		
    	return $html;
    }
    /*
    public static function __callStatic($method, $args)
    {
        if(method_exists(Zend_Locale_Format,$method)){
            return Zend_Locale_Format::$method($args[0],array('locale'=>Matj_Get::getLocale()));
        }    
    }
	*/
    
    function dateToTimestamp($date){
    	$d=explode(" ",$date);
		$datum=explode("-",$d[0]);
		$cas=explode(":",@$d[1]);
		$res=mktime($cas[0]+0,@$cas[1]+0,@$cas[2]+0,@$datum[1]+0,@$datum[2]+0,@$datum[0]+0);
		return $res;
    }
    
	
	function convertFacebookTime($t){
		$date=substr($t,0,10)." ".substr($t,11,8);
		return Matj_Util::addDate($date,array('hour'=>1),'Y-m-d H:i:s');
	}
	
	
}