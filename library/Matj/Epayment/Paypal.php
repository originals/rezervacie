<?php

/** 
 * @author Matus Jancik
 * 
 * 
 */
class Matj_Epayment_Paypal {

    private $email ='';
    
    
    
	function __construct ($options=array()) {
		
            $this->email=$options["email"];
            $this->url="https://www.paypal.com/cgi-bin/webscr";
            
            
           // $this->email="testcompany1@originals.sk";
           // $this->url="https://www.sandbox.paypal.com/cgi-bin/webscr";
            
	}
	
	
	

	/**
	 * 
	 * Enter description here ...
	 * @var Eshop_Model_Order
	 */
	protected $order;
	
    function setOrder(Eshop_Model_Order $order){
            $this->order=$order;
    }
        
	public function getPaymentForm(){
		
            $view=Matj_Get::getView();
                $html='<p class="paymentnote">'.Matj_Get::getTranslator()->_('eshop.paymentnote.paypal').'</p>';
                $returnUrl = $view->urlHost().$view->url(array('action'=>'paymentgate','param1'=>'paypal'),'order',true);
  
                $html.='
                        <form action="'.$this->url.'" method="post">  
                            <input type="hidden" name="cmd" value="_xclick">  
                            <input type="hidden" name="business" value="'.$this->email.'">  
                            <input type="hidden" name="item_name" value="Objednavka c.'.$this->order->getVs().'">  
                            <input type="hidden" name="item_number" value="1">  
                            <input type="hidden" name="custom" value="'.$this->order->getVs().'">  
                            <input type="hidden" name="amount" value="'.round($this->order->getCurrencyPrice(),2).'">  
                            <input type="hidden" name="no_shipping" value="0">  
                            <input type="hidden" name="no_note" value="1">  
                            <input type="hidden" name="currency_code" value="'.strtoupper($this->order->getCurrency()).'">  
                            <input type="hidden" name="return" value="'.$returnUrl.'">  
                            <input type="hidden" name="cancel_return" value="'.$returnUrl.'?cancel='.$this->order->getVs().'">  
                            <input type="hidden" name="notify_url" value="'.$returnUrl.'?notify='.$this->order->getVs().'">  
                            <input type="hidden" name="lc" value="AU">  
                            <input type="hidden" name="bn" value="PP-BuyNowBF">  
                            <input type="submit" class="btn btn-order" border="0" name="submit" value="'.Matj_Get::getTranslator()->_('eshop.paymentbutton.paypal').'" >  
                            <img alt="" border="0" src="https://www.paypal.com/en_AU/i/scr/pixel.gif" width="1" height="1">  
                        </form> 
                ';
          return $html;  
        }
	
        function receiveResponse($request){
            //$status=$this->receive_response();
            
            
            $get=$request->getParams();
            if(isset($get["cancel"])){
                
                return array('status'=>false,'vs'=>$get["cancel"]+0,'message'=>"Canceled by user");
            }
            
            
            $params=$request->getPost();
            
            
            $status=FALSE; 
            
            if(!empty($params["custom"])){
                $vs=$params["custom"]+0;
                if($params["payment_status"]=="Completed"){
                    $status=true;
                    $message="SUCCESS";
                }
                elseif($params["payment_status"]=="Pending"){
                    $status=false;
                    $vs=null;
                    $message="Payment status ".$params["payment_status"];
                }
                else{
                    $message="Payment status ".$params["payment_status"];
                }
            }
            
            
            
            return array('status'=>$status,'vs'=>$vs,'message'=>$message);
        }
        
}
