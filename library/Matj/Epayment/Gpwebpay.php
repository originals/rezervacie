<?php

/** 
 * @author Matus Jancik
 * 
 * 
 */
class Matj_Epayment_Gpwebpay {
	
	var $private_key, $password, $public_key, $digest;
	
	public $params = array ();
	

	var $options = array ();
	
        
	private $GP_WEBPAY_MID;
	private $GP_WEBPAY_URL;
	private $GP_WEBPAY_PRIVATE_KEY;
	private $GP_WEBPAY_PASSWORD;
	private $GP_WEBPAY_PUBLIC_KEY = '';
	private $GP_WEBPAY_PGW = '';
	
	const TEST = 0;
	
	
	private $statusCodeConverter = array(
		'1'=>'1',
		'2'=>'1',
		'3'=>'1',
		'4'=>'2',
		'5'=>'7',
		'6'=>'11',
		'7'=>'3',
		'8'=>'3',
		'9'=>'9',
		'10'=>'10',
		'13'=>'11'
	);
	
	public $lastResponse;
	
	function __construct ($options=array()) {
		
            
                
                
            
		$this->GP_WEBPAY_MID=$options["MID"];
		$this->GP_WEBPAY_URL= "https://3dsecure.gpwebpay.com/csobsk/order.do";
		//$this->GP_WEBPAY_PUBLIC_KEY=PATH_PREFIX.$options["PUBLIC_KEY"];
		$this->GP_WEBPAY_PASSWORD=$options["PASSWORD"];
		$this->GP_WEBPAY_PRIVATE_KEY=$options["PRIVATE_KEY"];
		
		
		//$this->GP_WEBPAY_PRIVATE_KEY=APPLICATION_PATH . '/keys/neri.pem';
		//$this->GP_WEBPAY_PUBLIC_KEY=APPLICATION_PATH . '/keys/muzo.signing_test.pem';
		
                $includePaths=Matj_Get::getBootstrap()->getOption('includePaths');
                $library=$includePaths["keys"];
                //Zend_Debug::dump($library);
                
        $this->GP_WEBPAY_PUBLIC_KEY=$library.'/muzo.signing_prod.pem';
		$this->GP_WEBPAY_PGW=$library.'/pgw-standard.wsdl';
		
		if(!empty($options["TEST"])){
			$this->GP_WEBPAY_URL = "https://test.3dsecure.gpwebpay.com/csobsk/order.do";
			//$this->GP_WEBPAY_PUBLIC_KEY=APPLICATION_PATH . '/keys/muzo.signing_test.pem';
			//$this->GP_WEBPAY_PGW=APPLICATION_PATH."/keys/pgw-standard_test.wsdl";
			
			$this->GP_WEBPAY_PUBLIC_KEY=$library.'/muzo.signing_test.pem';
			$this->GP_WEBPAY_PGW=$library.'/Matj/Epayment/Gpwebpay/pgw-standard_test.wsdl';
		
		}
		
		
		
		$this->options = array ( 
			'MERCHANTNUMBER' => $this->GP_WEBPAY_MID, 
			'OPERATION' => 'CREATE_ORDER', 
			'CURRENCY' => '978', 
			'DEPOSITFLAG' => 1, 
			'DESCRIPTION' => $options["DESCRIPTION"], 
			'MD' =>$options["MD"] 
		);
	
	}
	
	
	
	
	
	function sign ($key, $password) {
		$fp = fopen($key, "r");
		$this->private_key = fread($fp, filesize($key));
		fclose($fp);
		$this->password = $password;
		
                
                
		$digestValue = implode('|', $this->params);
		
		$keyid = openssl_get_privatekey($this->private_key, $this->password);
		if (empty($keyid)) {
                        Zend_Debug::dump($this);exit;
                        throw new Exception("Cant sign GP payment");
		}
		
		openssl_sign($digestValue, $this->digest, $keyid);
		$this->digest = base64_encode($this->digest);
		openssl_free_key($keyid);
	}
	
	function getSign(){
		$this->sign($this->GP_WEBPAY_PRIVATE_KEY, $this->GP_WEBPAY_PASSWORD);
		return $this->digest;
		
	}
	
	
	function verify ($key) {
		$fp = fopen($key, "r");
		$this->public_key = fread($fp, filesize($key));
		fclose($fp);
		
		
		$pubcertid = openssl_get_publickey($this->public_key);
		
		
		//Zend_Debug::dump($pubcertid);
		
		$digest = base64_decode($this->digest);
		$data = implode('|', $this->params);
		$result = openssl_verify($data, $digest, $pubcertid);
		openssl_free_key($pubcertid);
		return (($result == 1) ? true : false);
	}
	
	function send_request ($data) {
		
		$this->params['MERCHANTNUMBER'] = $this->options['MERCHANTNUMBER'];
		$this->params['OPERATION'] = $this->options['OPERATION'];
		$this->params['ORDERNUMBER'] = $data['vs'];
		$this->params['AMOUNT'] = $data['price'] * 100;
		$this->params['CURRENCY'] = $this->options['CURRENCY'];
		$this->params['DEPOSITFLAG'] = isset($data["deposit"]) ? $data["deposit"] : $this->options['DEPOSITFLAG'];
		$this->params['MERORDERNUM'] = $data['vs'];
		$this->params['URL'] = $data["redirect_url"];
		$this->params['DESCRIPTION'] = $this->options['DESCRIPTION'];
		$this->params['MD'] = base64_encode($this->options['MD']);
		
		$this->sign($this->GP_WEBPAY_PRIVATE_KEY, $this->GP_WEBPAY_PASSWORD);
		
		if (isset($this->digest)) {
			$this->params['DIGEST'] = $this->digest;
			$this->params = array_map('urlencode', $this->params);
			array_walk($this->params, create_function('&$val, $key', '$val = "$key=$val";'));
			
			return $this->GP_WEBPAY_URL . "?" . implode('&', $this->params);
		}
		else {
			return false;
		}
	}
	
	function receive_response () {
		
		$this->params = array ();
		$this->params['OPERATION'] = isset($_GET['OPERATION']) ? $_GET['OPERATION'] : '';
		$this->params['ORDERNUMBER'] = isset($_GET['ORDERNUMBER']) ? $_GET['ORDERNUMBER'] : '';
		$this->params['MERORDERNUM'] = isset($_GET['MERORDERNUM']) ? $_GET['MERORDERNUM'] : '';
		$this->params['MD'] = isset($_GET['MD']) ? $_GET['MD'] : '';
		$this->params['PRCODE'] = isset($_GET['PRCODE']) ? $_GET['PRCODE'] : '';
		$this->params['SRCODE'] = isset($_GET['SRCODE']) ? $_GET['SRCODE'] : '';
		$this->params['RESULTTEXT'] = isset($_GET['RESULTTEXT']) ? $_GET['RESULTTEXT'] : '';
		
		$this->digest = isset($_GET['DIGEST']) ? $_GET['DIGEST'] : '';
		

		//Zend_Debug::dump(GP_WEBPAY_PUBLIC_KEY);
		//Zend_Debug::dump($this->verify(GP_WEBPAY_PUBLIC_KEY));
		
		
		
		
		$status= ($this->verify($this->GP_WEBPAY_PUBLIC_KEY) && ($this->params['PRCODE'] == "0") && ($this->params['SRCODE'] == "0")) ? true : false;
		return $status;
	}
	
	
	
	
		
	function checkStatus($orderNumber){
		//$orderNumber=2011000013;
		
		$params=array();
		$params["merchantNumber"]=$this->GP_WEBPAY_MID."";
		$params["orderNumber"]=$orderNumber;
		//$params["amount"]=number_format($order->getPrice(),2,"","");
		$this->params=$params;
		$params["digest"]=$this->getSign();

		
		
		//Zend_Debug::dump($params);exit;
		$soap=$this->getSoap();
		
		try{
			$return=$soap->queryOrderState($params["merchantNumber"],$params["orderNumber"],$params["digest"]);
			
			
			$this->lastResponse=$return;
			if($return->state>0){
				if(key_exists($return->state,$this->statusCodeConverter)){
					$status= $this->statusCodeConverter[$return->state];
				}
				else{
					$status= -2;
				}
			}
			else{
				$status= -3;
			}
		}
		catch(Exception $e){
			$status= -1;
			Zend_Debug::dump($e);exit;
			$this->lastResponse=$e;
				
		}
		//Zend_Debug::dump($this->lastResponse);exit;

		
		//echo $soap->__getLastRequest();
		//exit;
		//$this->savePaymentHistory($orderNumber,$params,$return,$e,$status);
		return $status;
	}
	
	function cancel(Frontend_Model_Order $order){
		
		$params=array();
		$params["merchantNumber"]=$this->GP_WEBPAY_MID."";
		$params["orderNumber"]=$order->getNumberbank();
		//$params["amount"]=number_format($order->getPrice(),2,"","");
		$this->params=$params;
		$params["digest"]=$this->getSign();
		
		$soap=$this->getSoap();
		
		try{
			$return=$soap->approveReversal($params["merchantNumber"],$params["orderNumber"],$params["digest"]);
			$this->lastResponse=$return;
			if($return->ok){
				$status = 1;
			}
			else{
				$status = 0;
			}
		}
		catch(Exception $e){
			$status = -1;
			$this->lastResponse=$e;
		}
		
		$this->savePaymentHistory($order,$params,$return,$e,$status);
		return $return;
	}
	
	function deposit(Frontend_Model_Order $order){
		
		$params=array();
		$params["merchantNumber"]=$this->GP_WEBPAY_MID."";
		$params["orderNumber"]=$order->getNumberbank();
		$params["amount"]=number_format($order->getPrice(),2,"","");
		$this->params=$params;
		$params["digest"]=$this->getSign();
		
		$soap=$this->getSoap();
		
		try{
			$return=$soap->deposit($params["merchantNumber"],$params["orderNumber"],$params["amount"],$params["digest"]);
			$this->lastResponse=$return;
			if($return->ok){
				$status = 1;
			}
			else{
				$status = 0;
			}
		}
		catch(Exception $e){
			$status = -1;
			$this->lastResponse=$e;
		}
		
		$this->savePaymentHistory($order,$params,$return,$e,$status);
		return $return;
	}
	
	
	function depositReversal(Frontend_Model_Order $order){
		
		$params=array();
		$params["merchantNumber"]=$this->GP_WEBPAY_MID."";
		$params["orderNumber"]=$order->getNumberbank();
		//$params["amount"]=number_format($order->getPrice(),2,"","");
		$this->params=$params;
		$params["digest"]=$this->getSign();
		
		$soap=$this->getSoap();
		
		try{
			$return=$soap->depositReversal($params["merchantNumber"],$params["orderNumber"]/*,$params["amount"]*/,$params["digest"]);
			$this->lastResponse=$return;
			if($return->ok){
				$status = 1;
			}
			else{
				$status = 0;
			}
		}
		catch(Exception $e){
			$status = -1;
			$this->lastResponse=$e;
		}
		
		$this->savePaymentHistory($order,$params,$return,$e,$status);
		return $return;
	}
	
	
	
	
	function savePaymentHistory($order,array $params,$return,$e,$status){
		//Zend_Debug::dump(print_r($e,true));exit;
		$model=new Frontend_Model_Order_PaymentHistory();
		
		if($order instanceof Frontend_Model_Order)$model->setData("order_id",$order->getId());
		else $model->setData("order_id",$order);
		
		$model->setUri($_SERVER["REQUEST_URI"]);
		$model->setData("order_paymenthistory_data",print_r($params,true));
		if(!empty($this->lastResponse)){
			$model->setResponse(print_r($this->lastResponse,true));
		}
		$model->setStatus($status);
		$model->save();
		return $this;
	}	
	
	
	/**
	 * @return SoapClient
	 */
	function getSoap(){
		$url='https://test.3dsecure.gpwebpay.com/webservices/services/pgw';		
		
		$soap=new SoapClient($this->GP_WEBPAY_PGW,
					array("trace"      => 1, 
    					  "uri"      => $url, 
    					  "exceptions" => 1));
		return $soap;
	}
	
	/**
	 * 
	 * Enter description here ...
	 * @var Eshop_Model_Order
	 */
	protected $order;
	
        function setOrder(Eshop_Model_Order $order){
            $this->order=$order;
        }
        
	public function getPaymentForm(){
		
		$gpWebpay=$this;
		//Zend_Debug::dump($this->order->getData());exit;
		$request = array( 
		   'price' => $this->order->getPrice("order_total_int"), 
		   'deposit' => $this->order->getDepositflag(), 
		   'vs' => $this->order->getNumberbank(), 
		   'redirect_url' => Matj_Get::getView()->urlHost().
                                     Matj_Get::getView()->url(array('module'=>'eshop','controller'=>'order','action'=>'paymentgate','epayment'=>'gpwebpay'),'default',true)
		); 
		$link=$gpWebpay->send_request($request);
		$text=Matj_Get::getTranslator()->_('epayment.gpwebpay.paybutton.text');

		return '<p><a href="'.$link.'" class="btn btn-large btn-info pay-button pay-gpwebpay">'.$text.'</a></p>';
	}
	
        function receiveResponse($request){
            $status=$this->receive_response();
            return array('status'=>$status,'vs'=>$this->params["ORDERNUMBER"],'message'=>$this->params["RESULTTEXT"]);
        }
        
}
