<?php
/*
	Copyright 2009 MONOGRAM Technologies

	This file is part of MONOGRAM EPayment libraries

	MONOGRAM EPayment libraries is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	MONOGRAM EPayment libraries is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with MONOGRAM EPayment libraries.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once dirname(__FILE__).'/EPaymentMessage.class.php';

abstract class EPaymentAesSignedMessage extends EPaymentMessage {
    public function computeSign($sharedSecret) {
        if (!$this->isValid)
            throw new Exception(__METHOD__.": Message was not validated.");

        try {
            
            $base=$this->GetSignatureBase();
            
         //   $base="99991234.5097811110308https://moja.tatrabanka.sk/cgi-bin/e-commerce/start/example.jsp";
         //   $sharedSecret="3132333435363738393031323334353637383930313233343536373839303132";
            
            $hash = substr (mhash(MHASH_SHA1, $base ), 0, 16 );
            $td = mcrypt_module_open(
                    MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_ECB, ''
            );
            $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size ($td), MCRYPT_RAND);
            mcrypt_generic_init($td, hex2bin($sharedSecret), $iv);
            $crypt= mcrypt_generic ($td, $hash);
            mcrypt_generic_deinit ($td);
            $sign = strtoupper(bin2hex($crypt));
            
            
        } catch (Exception $e) {
            
            echo $e;exit;
            return false;
        }
        return $sign;
    }
    
    protected function getRawSharedSecret($sharedSecret) {
        
        
        
				if (strlen($sharedSecret) == 64) {
						return pack('A*', $sharedSecret);
				} elseif (strlen($sharedSecret) == 128) {
						return pack('A*', pack('H*', $sharedSecret));
				} else {
						throw new Exception(__METHOD__.": Invalid shared secret format.");
				}
		}
}


if ( !function_exists( 'hex2bin' ) ) {
    function hex2bin( $str ) {
        $sbin = "";
        $len = strlen( $str );
        for ( $i = 0; $i < $len; $i += 2 ) {
            $sbin .= pack( "H*", substr( $str, $i, 2 ) );
        }

        return $sbin;
    }
}