<?php

/** 
 * @author Matus Jancik
 * 
 * 
 */
class Matj_Epayment_Cardpay {

        protected $options;
    
    
	function __construct ($options=array()) {
	
            
            if(!empty($options["test"])){
                $options["MID"]="1joa";
                $options["SHAREDSECRET"]="43218765";
                $options["REDIRECTURLBASE"]='http://epaymentsimulator.monogram.sk/TB_CardPay.aspx';
            }
            else{
                $options["REDIRECTURLBASE"]="https://moja.tatrabanka.sk/cgi-bin/e-commerce/start/e-commerce.jsp";
            }
            
            $this->options=$options;

            
	}
	
	
	

	/**
	 * 
	 * Enter description here ...
	 * @var Eshop_Model_Order
	 */
	protected $order;
	
        function setOrder(Eshop_Model_Order $order){
            $this->order=$order;
        }
        
	public function getPaymentForm(){
		
            $protocol = 'http';
            if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
              $protocol = 'https';
            }   
            
            $view=Matj_Get::getView();
            
            $returnUrl = $view->urlHost().$view->url(array('action'=>'paymentgate','param1'=>'cardpay'),'order',true);
  
            $sporopayFields = array(
              'MID' => $this->options["MID"],
              'AMT' => number_format($this->order->getPrice(),2),
              'CURR' => '978',
              'VS' => $this->order->getVs(),
              'CS' => '0308',
              'RURL' => $returnUrl,
              'IPC' => $_SERVER['REMOTE_ADDR'],
              'NAME' => Matj_String::removeAccents($this->order->getUser()->getFullname()),
              'PT' => null,
              'RSMS' => $this->options["RSMS"],
              'REM' => $this->options["REM"],
              'DESC' => null,
              'AREDIR' => null,
              'LANG' => null
            );
            
            
//            Zend_Debug::dump($sporopayFields);exit;
            
            require_once 'EPaymentMerchant_PHP/TB_CardPay/CardPayPaymentRequest.class.php';
    
            $pr = new CardPayPaymentRequest();
            foreach ($sporopayFields as $key => $value) {
              $outValue = $value;

              if (!($outValue === null)) {
                $pr->$key = $outValue;
              }
            }

            $pr->SetRedirectUrlBase($this->options["REDIRECTURLBASE"]);
            
            
//           Zend_Debug::dump($pr);exit;
            
            $validationResult = $pr->validate();
            if ($validationResult) {
              //echo "Validation OK<br />";

              $pr->SignMessage($this->options["SHAREDSECRET"]);

              //echo "Message signed<br />";

              $prurl = $pr->GetRedirectUrl();

              //echo "Payment request: <a href=\"{$prurl}\">{$prurl}</a><br />";
              $html='<p class="paymentnote">'.Matj_Get::getTranslator()->_('eshop.paymentnote.cardpay').'</p>';
              $html.='<a href="'.$prurl.'" class="btn btn-order cardpay_link"><span>'.Matj_Get::getTranslator()->_('eshop.paymentbutton.cardpay').'</span></a>';
              
            } else {
              $html='ERROR';
            }
            
            
            
          return $html;  
        }
	
        function receiveResponse($request){
            
            $fields = array('VS', 'AC', 'RES', 'SIGN');
            $status=false;
            require_once 'EPaymentMerchant_PHP/TB_CardPay/CardPayPaymentHttpResponse.class.php';
            $pres = new CardPayPaymentHttpResponse();

            if ($pres->Validate()) {
              
              if ($pres->VerifySignature($this->options["SHAREDSECRET"])) {
                $result = $pres->GetPaymentResponse();

                if ($result == IEPaymentHttpPaymentResponse::RESPONSE_SUCCESS) {
                  $message='SUCCESS';
                  $status=true;
                } else if ($result == IEPaymentHttpPaymentResponse::RESPONSE_FAIL) {
                  $message='FAIL';
                } else if ($result == IEPaymentHttpPaymentResponse::RESPONSE_TIMEOUT) {
                  $message='TIMEOUT';
                }
                
              } else {
                $message='Received message failed integrity and authenticity check.';
              }
            } else {
              $message='Received message failed validation.';
            }
            
            
            return array('status'=>$status,'vs'=>$pres->VS,'message'=>$message);
        }
        
}
