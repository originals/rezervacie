<?php

require_once APPLICATION_PATH.'/library/Matj/Epayment/EPaymentMerchant_PHP/VUB_EPlatba2_HMAC/EPlatbaPaymentRequest.class.php';
require_once APPLICATION_PATH.'/library/Matj/Epayment/EPaymentMerchant_PHP/VUB_EPlatba2_HMAC/EPlatbaPaymentHttpResponse.class.php';

class Matj_Epayment_Vubpay {

	public $bankName = "VUB";
    public $paymentName = "VubPay";
    public $configVariable = "vubpay";
	
	//const PRODUCTION_URI = 'https://ib.trustpay.eu/mapi/pay.aspx';
    //const TEST_URI = "http://epaymentsimulator.monogram.sk/TB_TatraPay.aspx";
    const MODE_TEST = 'mode.test';
    const MODE_PRODUCTION = 'mode.production';
    //const TEST_IP = '81.89.63.19';
    //const PRODUCTION_IP = '81.89.63.16';
	/**
     * aid  Merchant account ID
     * ID of account assigned by VUBPay
     * @var string
     */
    private $mid;

    /**
     * Signing key
     * @var type @var string
     */
    private $key;
    
    
    private $rem;

    /**
     * Currency of the payment same as currency of merchant account
     * @var string
     */
    private $currency;

    /** @var string */
    private $baseUri;
    private $mode;

    
    protected $config;
	
	public function __construct($config)
    {
        
			$this->config=$config;
            
            $this->mid = $config["MID"];
            $this->key = $config["SHAREDSECRET"];
            //$this->rem = $config["REM"];
            
            $this->currency = $currency;

            if (empty($tc["test"])) {
                $this->mode=self::MODE_PRODUCTION;
                    $this->baseUri = null;
            } else {
                $this->mode=self::MODE_TEST;
                    $this->baseUri = self::TEST_URI;
            }

            
            
    }
	/**
	 * 
	 * Enter description here ...
	 * @var Eshop_Model_Order
	 */
	protected $order;
	
    function setOrder(Eshop_Model_Order $order){
            $this->order=$order;
    }
        
	public function getPaymentForm(){
		
            $view=Matj_Get::getView();
            $view=Matj_Get::getView();
            $html='<p class="paymentnote">'.Matj_Get::getTranslator()->_('eshop.paymentnote.vubpay').'</p>';
            $returnUrl = $view->urlHost().$view->url(array('action'=>'paymentgate','param1'=>'vubpay'),'order',true);
  			$epayments=Matj_Get::getBootstrap()->getOption('epayments'); 
			$tc=$epayments["vubpay"];
			//Zend_Debug::dump($tc);exit;
				$module=new Matj_Epayment_Vubpay($tc);
				$transaction=$module->generatePaymentURI(number_format($this->order->getPrice(), 2, '.', ''), 'sk',$this->order->getVS(),'', $returnUrl,$returnUrl,null,$returnUrl);
				$payReq = $transaction->GetPaymentRequestFields();
				$html.='<form action="'.$transaction->GetUrlBase().'" method="post">'; 
                            foreach ($payReq as $k => $v) {
								$html.= " <input type=\"hidden\" name=\"$k\" value=\"$v\" />";
							}
							$html.='<input type="submit" value="'.Matj_Get::getTranslator()->_('send.payment.request').'" />';
				$html.='</form>'; 

          return $html;  
    }
	
	public function generatePaymentURI($amount, $lang = Language::SLOVAK, $ref = '', $description = '', $successUrl = NULL, $cancelUrl = NULL, $errorUrl = NULL, $notificationUrl = NULL)
    {
            
            $pr=new EplatbaPaymentRequest();
            $pr->AMT = number_format($amount,2,'.',''); // suma (v â‚¬)
            $pr->VS = $ref; // variabilnĂ˝ symbol platby
          //  $pr->SS = $order["vs"]; // ĹˇpecifickĂ˝ symbol platby
            $pr->CS = "0308"; // constant symbol
            //$pr->CURR = "978"; // constant symbol
            //$pr->param = "order_id=".$order_id; 
            $pr->RURL = $successUrl;
            
            $pr->MID = $this->mid;
            
            if($this->rem)$pr->REM = $this->rem;
            
            
            $secret=$this->key;
            if(!empty($this->baseUri)){
                    $pr->SetRedirectUrlBase($this->baseUri);
            }

            if ($pr->validate()) {

                    if($this->mode == self::MODE_TEST){
                                    $pr->MID = "aoj";
                                    $secret = "87651234";
                                    $pr->SetRedirectUrlBase(self::TEST_URI);
                    }
                    
                    //Zend_Debug::dump($secret);exit;
                    $pr->Validate();
                    $pr->SignMessage($secret); 	
                    
                    $l=$pr;
                    return $l;
            }
            else{
                    Zend_Debug::dump($pr);		
                    echo "error validate";
                    return false;
            }
            
            
    }
    
    function response(){
        $error='';
        $status='';
        $vs='';
        
        
        $secret=$this->mid;
        
        if($this->mode = self::MODE_TEST){
            $secret = "87651234";
        }
        
        
        $fields = array('SS', 'VS', 'RES', 'SIGN');
 					$pres = new TatraPayPaymentHttpResponse();
					 if ($pres->Validate()) {
					    if ($pres->VerifySignature($secret)) {
					      $result = $pres->GetPaymentResponse();
					      
					      $vs = $pres->VS+0;
					      
					      if ($result == IEPaymentHttpPaymentResponse::RESPONSE_SUCCESS) {
					      	$status='SUCCESS';
					      } else if ($result == IEPaymentHttpPaymentResponse::RESPONSE_FAIL) {
					      	$status='FAIL';
					      } else if ($result == IEPaymentHttpPaymentResponse::RESPONSE_TIMEOUT) {
					        $status='TIMEOUT';
					      }
					     } else {
					      $error="err_auth";
					    }
					  } else {
						$error="err_valid";
					  }
					 
	    return array("status"=>$status,"error"=>$error,"vs"=>$vs);
    }
	
}


