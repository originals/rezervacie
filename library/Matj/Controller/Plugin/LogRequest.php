<?php
require_once ('Zend/Controller/Plugin/Abstract.php');
class Matj_Controller_Plugin_LogRequest extends Zend_Controller_Plugin_Abstract
{
    
    
    function dispatchLoopStartup($request){
    	try{
	    	$user=Matj_Auth::getInstance()->getIdentity();
	    	$log=new Frontend_Model_Log_Request();
	    	if($user){
	    		$log->setData('user_id',$user->getId());
	    	}
	    	$log->setIp($this->getRequest()->getServer('REMOTE_ADDR'));
		    $log->setUri(substr($this->getRequest()->getRequestUri(),0,250));
		    $log->setClient($this->getRequest()->getServer('HTTP_USER_AGENT'));
		    $log->save();
    	}
    	catch(Exception $e){
    	}	       
    }
    
    
    
}
?>