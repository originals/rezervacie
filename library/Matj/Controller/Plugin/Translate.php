<? 
require_once ('Zend/Controller/Plugin/Abstract.php');
class Matj_Controller_Plugin_Translate
    extends Zend_Controller_Plugin_Abstract
{
    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
         
         
         $translate = new Zend_Translate(
              array(
                  'adapter' => 'Matj_Translate_Adapter_Db',
                  'data' => 'Matj_Translate_DbTable',
                  'dbTable' => 'Matj_Translate_DbTable',
                  'locale'  => 'sk'
              )
          );
         
          Zend_Registry::set('Zend_Translate',$translate);

		       
    }
}
?>