<?php
require_once ('Zend/Controller/Plugin/Abstract.php');
class Matj_Controller_Plugin_Cms extends Zend_Controller_Plugin_Abstract
{
    
    /* */
    private $_siteNamespace;
    
    
    function _initSite(){
        require_once 'sites/sites.php';
        $this->_siteNamespace=$site;

        $config=$this->getSiteConfig();
        if(!empty($config->resources->db)){
            $adapter=Zend_Db::factory($config->resources->db->adapter, $config->resources->db->params->toArray());
            Matj_Db_Table_Abstract::setDefaultAdapter($adapter);
            
            //Matj_Db_Table_Abstract::setDefaultMetadataCache(Matj_Get::getCache('file'));
            
            
            if(Matj_Get::getFrontController()->hasPlugin("ZFDebug_Controller_Plugin_Debug")){
                $zfdebug = Matj_Get::getFrontController()->getPlugin("ZFDebug_Controller_Plugin_Debug");
                $database=  new ZFDebug_Controller_Plugin_Debug_Plugin_Database(array('adapter'=>$adapter));
                $zfdebug->registerPlugin($database);
                
            }
            
            
            unset($config->resources->db);
        }
        
        $options=Matj_Get::getBootstrap()->getOptions();

        $config=$config->toArray();
        foreach($config as $k=>$d){
            if(!empty($d)){
                if(isset($options[$k]))
                    $options[$k]=Matj_Util::mergeArrays($d, $options[$k]);
                else
                    $options[$k]=$d;
            }
        }
        
        Matj_Get::getBootstrap()->setOptions($options);
        
        
        
    }
    
    
    function getSiteConfig(){
        $path='sites/'.$this->getSiteNamespace().'/config.php';
        if(!file_exists($path)){
            throw new Exception('Config file '.$path.' not exists');
        }
        require_once($path);
        $siteConfig=new Zend_Config($config['site'],$allowModifications=true);
        return $siteConfig;
    }
    
    
    
    
    //public function route
    function getSiteNamespace(){
        return $this->_siteNamespace;
        
    }
    
    
    public function routeStartup(Zend_Controller_Request_Abstract $request)
    {
        $this->_initSite();
        $this->_initTranslate();
        
    }
    
    function _initTranslate(){
        
        //$cache=Matj_Get::getCache('file');
        //Matj_Translate::setCache($cache);
        
        
        try{
        $translate = new Matj_Translate(
            array(
                'adapter' => 'Matj_Translate_Adapter_Db',
                'dbTable' => 'Matj_Translate_DbTable',
               // 'cache'=> $cache
            )
        );
        }
        catch(Exception $e){
            Zend_Debug::dump($e);
        }
        Zend_Registry::set("Zend_Translate",$translate);
        
    }
    

    
    function postDispatch(Zend_Controller_Request_Abstract $request) {
        parent::postDispatch($request);
        $layout=Matj_Get::getBootstrap()->getOption('layout');
        if(!empty($layout["path"])){
            if($request->getModuleName()=="default" && $request->getControllerName()!="error"){
                Matj_Get::getView()->layout()->setLayoutPath($layout["path"]);
                //Matj_Get::getView()->addScriptPath($layout["path"]);
            }
        }
        
    }
    
    
    
    
}
?>