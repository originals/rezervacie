<? 
require_once ('Zend/Controller/Plugin/Abstract.php');
class Matj_Controller_Plugin_Language
    extends Zend_Controller_Plugin_Abstract
{
    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
         $lang = $request->getParam('lang', null);

        $front  = Matj_Get::getFrontController();
        //if(empty($front))return null;
            $router = $front->getRouter();
         
        if(empty($lang) && Zend_Registry::isRegistered('DefaultLang')){
            $lang=Zend_Registry::get('DefaultLang');
        }    
            
            $router->setGlobalParam('lang', $lang);    
    }
}
?>