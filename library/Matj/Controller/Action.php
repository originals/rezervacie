<?php



class Matj_Controller_Action extends Zend_Controller_Action {
    

        /**
         * @var Zend_Controller_Action_Helper_FlashMessenger
         */
        public $msg;

        /**
         * View object
         * @var Zend_View
         */
        public $view;

        function init() {
            parent::init();
        }	
	
	function postDispatch(){
            if ($this->_getParam("ajax", null)) {
                $this->_helper->layout()->setLayout("ajax");
                $menu = false;

            }
            if ($this->_getParam("json", null)) {
                $this->_helper->layout()->disableLayout();
                $this->_helper->viewRenderer->setNoRender(true);
                $menu = false;
            }
		
	}

        
        
        
    function initFlashMessenger(){
        $this->msg = Matj_Get::getFlashMessenger();
    }    


    /**
     * @return Zend_Controller_Request_Http
     */
    public function getRequest() {
        return $this->_request;
    }

    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array()) {
        parent::__construct($request, $response, $invokeArgs);

        $this->initFlashMessenger();
    }


    function preDispatch() {

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_setParam('ajax', true);
        }
        
    }

    

    function addMessage($msg, $class = "", $params = array()) {
        $flashMessenger = Matj_Get::getFlashMessenger();

        if (is_array($msg)) {
            foreach ($msg as $m) {
                $flashMessenger->addMessage((string) $this->translate($m, $params), $class);
            }
            return $flashMessenger;
        } else {
            return $flashMessenger->addMessage($this->translate($msg, $params), $class);
        }
    }

    function translate($msg, $params = array()) {
        return Matj_Get::getTranslator()->_($msg, $params);
    }

    protected function _redirect($url, array $options = array()) {
        $bu = $this->view->baseUrl();
        if (!empty($bu)) {
            if (substr($url, 0, strlen($bu)) == $bu) {
                $url = substr($url, strlen($bu));
            }
        }
        $this->_helper->redirector->gotoUrl($url, $options);
    }

    function _isPost() {
        return $this->getRequest()->isPost();
    }

    function _getPost($name = null, $default = null) {
        return $this->getRequest()->getPost($name, $default);
    }
    
    
    function json(array $data){
        $this->_setParam('json',true);
        $this->getResponse()->setBody(json_encode($data));
    }
    
    function html($data){
        $this->_setParam('json',true);
        $this->getResponse()->setBody($data);
    }
    
    function csv(array &$array,$filename)
    {
       $this->_helper->layout()->disableLayout();
       $this->_helper->viewRenderer->setNoRender(true);

        
       $this->download_send_headers($filename);
       
       if (count($array) == 0) {
         return null;
       }
       ob_start();
       $df = fopen("php://output", 'w');
       
       fprintf($df, chr(0xEF).chr(0xBB).chr(0xBF));
       fputcsv($df, array_keys(reset($array)),";");
       
       
       foreach ($array as $row) {
          fputcsv($df, $row,";");
       }
       fclose($df);
       
       $this->getResponse()->setBody(ob_get_clean());
    }

    function download_send_headers($filename) {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
    
    

}