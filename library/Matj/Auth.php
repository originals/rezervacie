<?php
/** 
 * @author Delphi
 * @package Delphi
 * 
 */
class Matj_Auth extends Zend_Auth
{
    /**
     * Singleton instance
     *
     * @var Zend_Auth
     */
    protected static $_instance = null;
    /**
     * log user
     *
     * @var Frontend_Model_User
     */
    protected static $logUser = null;
    
    
    protected $storedIdentities=array();
    
    
    /**
     * Returns an instance of Zend_Auth
     *
     * Singleton pattern implementation
     *
     * @return Matj_Auth
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
	/**
     * Singleton pattern implementation makes "new" unavailable
     *
     * @return void
     */
    protected function __construct()
    {}

    /**
     * Singleton pattern implementation makes "clone" unavailable
     *
     * @return void
     */
    protected function __clone()
    {}
	private static $authAdapter;
	
	
        /**
         * 
         * @return Zend_Auth_Adapter_DbTable
         */
	public static function getAdapter(){
	    if (null === self::$authAdapter) {
                self::$authAdapter = new Matj_Auth_Adapter_DbTable(
                                        Matj_Get::getDbAdapter(),
                                        'users',
                                        'user_email',
                                        'user_password',
                                        'MD5(?)'
                                    );
        }
        
        return self::$authAdapter;
        
        
        
	}
	

	/**
	 * @return Frontend_Model_User
	 */
	public function getIdentity(){
	    $identity=parent::getIdentity();
	    if($identity){
    	    if(empty($this->storedIdentities[$identity])){
		    	$user=Matj_Get::getModel("Core_Model_User")->findByEmail($identity);
	            if($user instanceof Core_Model_User){
					$this->setLogUser($user);
	    	        $this->storedIdentities[$identity]=$this->getLogUser();
				}
    	    }
            if(empty($this->storedIdentities[$identity]))return null;
            return $this->storedIdentities[$identity];
	    }
	    $this->clearIdentity();
	    return null;
	}
	
	public function clearIdentity()
    {
    	$this->storedIdentities=array();
    	return parent::clearIdentity();
    }
    
	function setLogUser(Core_Model_User $user){
	    self::$logUser=$user;
	    return $this;
	}
	
	function getLogUser(){
	    return self::$logUser;
	}

	function clearLogUser(){
	    self::$logUser=null;
	    return $this;
	}


	
	static function isAllowed($request,$options=array()){
		if(empty($options)){
			$user=Matj_Get::getAuth()->getLogUser();
			if($user)
				$options["user_id"]=$user->getId();
			else{
				$options["user_id"]=0;
			}
		}
		
		return self::getInstance()->getModelAuth()->isAllowed($request,$options);
	}
	
	/**
	 * 
	 * @return Core_Model_Acl
	 */
	function getModelAuth(){
		return Matj_Get::getModel("Core_Model_Acl");
	}
	
	
	public static function getLoginReturnUrl(){
		$sess=new Zend_Session_Namespace("rurl");
		if(empty($sess->returnUrl)){
			return Matj_Get::getView()->url(array('module'=>'eshop','controller'=>'products'),'default',true);
		}
		else{
			return $sess->returnUrl;		
		}
	}
	
	public static function setLoginReturnUrl($url=null){
		if($url===null){
			$url=Matj_Get::getView()->url(array());
		}
		$sess=new Zend_Session_Namespace("rurl");
		$sess->returnUrl=$url;		
		return $this;
	}
	
	
	public static function getLoginUrl(){
		return Matj_Get::getView()->url(array('controller'=>'user'),'default',true);
	}	
	
	
}
