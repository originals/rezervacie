<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_Db
 * @subpackage Adapter
 * @copyright  Copyright (c) 2005-2010 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: Mysql.php 20096 2010-01-06 02:05:09Z bkarwin $
 */


/**
 * @see Zend_Db_Adapter_Pdo_Abstract
 */
require_once 'Zend/Db/Adapter/Pdo/Mysql.php';


/**
 * Class for connecting to MySQL databases and performing common operations.
 *
 * @category   Zend
 * @package    Zend_Db
 * @subpackage Adapter
 * @copyright  Copyright (c) 2005-2010 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Matj_Db_Adapter_Pdo_Mysql extends Zend_Db_Adapter_Pdo_Mysql
{
	
	
	function delete($table,$where=''){
		return parent::delete($table,$where);
	}
	
	
	function deleteByArray($table,array $where){
		foreach($where as $k=>$d){
			$w='`'.$k.'`';
			if($d === null){
				$w.=" IS NULL ";
			}
			elseif(is_numeric($d)){
				$w.=" = ".$d;
			}
			else{
				$w.=" '".$d."'";
			}
			$wA[]=$w;
		}
		if(!empty($wA)){
			$where=implode(" AND ",$wA);
			return $this->delete($table,$where);
		}
		else{
			throw new Exception("empty where in deleteByArray");
		}
	}
	
	function insertMulti($table,$data){
		$first=current($data);
		$keys=array_keys($first);
		foreach($keys as $d){
			$cols[]="`".$d."`";
		}
		
		$sqlPrepend="INSERT INTO ".$table." ( ".implode(",",$cols)." ) VALUES ";
		
		$values=array();
		foreach($data as $k=>$d){
			$colsData=array();
			foreach($keys as $key){
				$colsData[$key]="'".$d[$key]."'";
			}
			$values[]="( ".implode(",",$colsData)." )";

			if(count($values)>200){
				$sql=$sqlPrepend.implode(",",$values);
				$this->query($sql);
				$values=array();
			}
		}
		
		if(!empty($values)){
			$sql=$sqlPrepend.implode(",",$values);
			$this->query($sql);
		}
		return true;
    }
    
    
    
    /**
     * Prepares an SQL statement.
     *
     * @param string $sql The SQL statement with placeholders.
     * @param array $bind An array of data to bind to the placeholders.
     * @return PDOStatement
     */
    
  //  protected $_defaultStmtClass = 'Matj_Db_Statement_Pdo';

    
    
    public function X___prepare($sql)
    {
        $this->_connect();
        $stmtClass = $this->_defaultStmtClass;
        if (!class_exists($stmtClass)) {
            require_once 'Zend/Loader.php';
            Zend_Loader::loadClass($stmtClass);
        }
        
        //Zend_Debug::dump($this->_fetchMode);
        
        $stmt = new $stmtClass($this, $sql);
        $stmt->setFetchMode($this->_fetchMode);
        return $stmt;
    }
    
    
    
    
    /**
     * Special handling for PDO query().
     * All bind parameter names must begin with ':'
     *
     * @param string|Zend_Db_Select $sql The SQL statement with placeholders.
     * @param array $bind An array of data to bind to the placeholders.
     * @return Zend_Db_Statement_Pdo
     * @throws Zend_Db_Adapter_Exception To re-throw PDOException.
     */
    public function X___query($sql, $bind = array())
    {
        
   /* 
        
        if (empty($bind) && $sql instanceof Zend_Db_Select) {
            $bind = $sql->getBind();
        }

        
        //var_dump($sql);exit;
        
        if (is_array($bind)) {
            foreach ($bind as $name => $value) {
                if (!is_int($name) && !preg_match('/^:/', $name)) {
                    $newName = ":$name";
                    unset($bind[$name]);
                    $bind[$newName] = $value;
                }
            }
        }
   */
        try {
            // connect to the database if needed
            $this->_connect();

/*
            // is the $sql a Zend_Db_Select object?
            if ($sql instanceof Zend_Db_Select) {
                if (empty($bind)) {
                    $bind = $sql->getBind();
                }

                $sql = $sql->assemble();
            }
*/
            // make sure $bind to an array;
            // don't use (array) typecasting because
            // because $bind may be a Zend_Db_Expr object
            if (!is_array($bind)) {
                $bind = array($bind);
            }

         //  echo ($sql)."<br>";

            
            // prepare and execute the statement with profiling
            $stmt = $this->prepare($sql);
            $stmt->execute($bind);

            // return the results embedded in the prepared statement object
            $stmt->setFetchMode($this->_fetchMode);
            return $stmt;
        } catch (PDOException $e) {
            /**
             * @see Zend_Db_Statement_Exception
             */
            require_once 'Zend/Db/Statement/Exception.php';
            throw new Zend_Db_Statement_Exception($e->getMessage(), $e->getCode(), $e);
        }
    }
    
    
    /**
     * Fetches all SQL result rows as a sequential array.
     * Uses the current fetchMode for the adapter.
     *
     * @param string|Zend_Db_Select $sql  An SQL SELECT statement.
     * @param mixed                 $bind Data to bind into SELECT placeholders.
     * @param mixed                 $fetchMode Override current fetch mode.
     * @return array
     */
    public function X___fetchAll($sql, $bind = array(), $fetchMode = null)
    {
        
        //throw new Exception;
        if ($fetchMode === null) {
            $fetchMode = $this->_fetchMode;
        }
        $stmt = $this->query($sql, $bind);
        $result = $stmt->fetchAll($fetchMode);
        return $result;
    }
    
    
}
