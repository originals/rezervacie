<?php
/**
 * Trieda Table Abstract
 *
 * v pripade ze bude treba nastavovat prefix pre tabulky.. urobime to tu
 */
require_once 'Zend/Db/Table/Abstract.php';
class Matj_Db_Table_Abstract extends Zend_Db_Table_Abstract {

    //protected $_sequence = false;

    protected $dbupdatelog;


	function __construct($config = array()){
		if(!empty($config["dbupdatelog"]))$this->dbupdatelog=1;
        /*$c = Matj_Get::getFrontController()
            ->getParam('bootstrap')
            ->getPluginResource('cachemanager')
            ->getCacheManager()->getCache('file');

        $config["metadataCache"]=  $c;

            */
		return parent::__construct($config);
	}

    protected function _setupTableName() {
		$prefix="";
        $this->_name = $prefix . $this->_name;
	}

	public function insertData(array $data,$unsetId = true)
    {
    	if(!isset($data["date_add"]))$data["date_add"]=date("Y-m-d H:i:s");
    	$fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }
        if($unsetId)unset($data[$this->getPrimary()]);
        if(count($data)==0)return null;
        return parent::insert($data);
    }


    function exists(){
       try{
         $this->info();
         return true;
       }
       catch(Exception $e){
           return false;
       }
    }



    function isColumn($name){
       $cols=$this->info("cols");
       if(in_array($name,$cols)){
           return true;
       }
       return false;
    }





    public function updateData(array $data)
    {
    	$fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }



        $where="".$this->getPrimary()." = '".($data[$this->getPrimary()]+0)."'";
        $rowId=$data[$this->getPrimary()];
        unset($data[$this->getPrimary()]);
        if(count($data)==0)return null;

        if($this->dbupdatelog){
            $cht=Matj_Get::getDbTable(array('name'=>'dbupdatelog'));
            $old=$this->fetchRow($where);

            $db["row_id"]=$rowId;
            if($i=Matj_Get::getAuth()->getIdentity())
            $db["user_id"]=$i->getId();
            $db["table"]=$this->info('name');
            $db["action"]='update';


            foreach($data as $k=>$d){
                if($old[$k]!=$d){
                    $db["before"]=$old[$k];
                    $db["after"]=$d;
                    $db["column"]=$k;
                    $cht->insertData($db);
                }
            }
        }
        return parent::update($data,$where);
    }

    public function deleteDataById($id)
    {
        $where="".$this->getPrimary()." = '".($id+0)."'";

        $rowId=$id;
        if($this->dbupdatelog){
            $cht=Matj_Get::getDbTable(array('name'=>'dbupdatelog'));
            $old=$this->fetchRow($where);

            $db["row_id"]=$rowId;
            if($i=Matj_Get::getAuth()->getIdentity())
            $db["user_id"]=$i->getId();
            $db["table"]=$this->info('name');
            $db["action"]='delete';
            foreach($old as $k=>$d){
                $db["before"]=$d;
                //$db["after"]=$d;
                $db["column"]=$k;
                $cht->insertData($db);

            }
        }




        return parent::delete($where);
    }


    public function deleteDataByArray(array $array)
    {
    	$where="";
    	foreach($array as $k=>$d){
			$w=" `".$k."`".($d===null ? " IS NULL " : " ='".$d."' ");
    		$where=Matj_Cms_Util::joinWheres($where,$w);
    	}
    	if($where!=""){
    		return parent::delete($where);
    	}
    	return null;
    }


    function fetchRowById($id){
        $data=$this->fetchRow(' '.$this->getPrimary().' = "'.($id+0).'"');
    	if(!$data){
    		return array();
    	}
    	return $data->toArray();
    }

    function getRank($where=null){
    	$sql="SELECT max(rank) as rank FROM ".$this->info("name")." ";
    	$sql=Matj_Cms_Util::addSqlParams($sql,$where,"rank DESC","",0,1);
    	$data=$this->getAdapter()->fetchRow($sql);
    	return (@$data["rank"]+0)+1;
    }



    function checkDuplicite($col,$value,$id){
    	$data=$this->fetchAll("`{$col}`= '".$value."' AND '.$this->getPrimary().'!='".(@$id+0)."'");
			if($data->count()>0){
				throw new Exception("db.error.duplicite.".$this->_name.".".$col);
				return false;
			}
			else{
				return true;
			}


    }


    function getPrimary(){
        $primary=current($this->info("primary"));
        return $primary;
    }

    function fetchAllData($where=null,$order=null,$offset=null,$limit=null,$primaryAsKey = true){
    	$sql="SELECT * FROM ".$this->info("name")." ";
    	$sql=Matj_Util::addSqlParams($sql,$where,$order,null,$offset,$limit);
    	$data=$this->getAdapter()->fetchAll($sql);
    	if(empty($data))return array();

    	if(!$primaryAsKey)return $data;
    	$result=array();
    	foreach($data as $k=>$d){
    		$result[$d[$this->getPrimary()]]=$d;
    	}
    	return $result;
    }

    function fetchRowData($where=null,$order = null,$offset=null,$limit = null){
    	$sql="SELECT * FROM ".$this->info("name")." ";
    	$sql=Matj_Util::addSqlParams($sql,$where,$order,$offset,$limit);



    	$data=$this->getAdapter()->fetchRow($sql);
    	if(empty($data))return array();
    	$result=$data;
    	return $result;
    }





}