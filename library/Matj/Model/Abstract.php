<?php

abstract class Matj_Model_Abstract{
    
    
    
    
    function access(Model_User $user,$type="read"){
        
    }
    
    
    
    protected $_valueFilter;
            
    
    /**
     * 
     * @return Zend_Filter_Input
     */
    function getDataFilter(){
        if($this->_valueFilter===null){
            $this->_valueFilter=new Zend_Filter();
            $this->_valueFilter->addFilter(new Zend_Filter_StringTrim);
            $this->_valueFilter->addFilter(new Zend_Filter_StripTags);
        }
        return $this->_valueFilter;    
    }
    
    
    function setDataFromPost(array $data){
        
        foreach($data as $k=>$d){
            $data[$k]=$this->filterValue($k,$d);
        }
        
        
        $time=preg_replace('/[^0-9\:]/','',str_replace(array(',','.'),':',$data["time"]));
        if(!preg_match('/\:/',$time)){
            if(strlen($time)>=4){
                $time=substr($time,0,2).":".substr($time,2,2);
            }
            elseif(strlen($time)<4){
                $time=substr($time,0,2).":".substr($time,2,2);
            }
        }
        
       
        
        $data["date"]=Matj_Format::getDate($data["date_sk"])." ".$time;
        
        
        return $this->setData($data);
    }

    public function filterValue($key, $value) {
        return $this->getDataFilter()->filter($value);
    }

}