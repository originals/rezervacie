<?

/**
 * Rozsirenie Zend_Form o translator a view
 * @package Library
 * @author Matus Jancik
 * @since 11.9.2009
 */
class Matj_Form extends Zend_Form {


	protected $_elementDecorators = array (
		'ViewHelper',
		array('Errors',array('placement'=>'prepend')),
		array (
			array (
				'data' => 'HtmlTag'
			),
			array (
				'tag' => 'div',
				'class' => 'element'
			)
		),
		array (
			'Label',
			array (
				'class' => 'label'
			)
		),
		array (
			'Description',
			array (
				'tag' => 'div',
				'class' => 'description'
			)
		),
		array (
			array (
				'Row' => 'HtmlTag'
			),
			array (
				'tag' => 'div',
				'class' => 'group'
			)
		)
	);





	function getTranslator () {
		return Matj_Get::getTranslator();
	}

	function getView(){
		return Matj_Get::getView();
	}


	public function __construct ($options = null) {
		//$this->_view->setHelperPath('Ews/View/Helper', 'Ews_View_Helper');



		$this->addElementPrefixPath("Matj_Filter", "Matj/Filter", "filter");
		$this->addElementPrefixPath("Matj_Validate", "Matj/Validate", "validate");
		$this->addPrefixPath("Matj_Form_Element", "Matj/Form/Element", Zend_Form::ELEMENT);
		$this->addPrefixPath("Matj_Decorator", "Matj/Decorator", Zend_Form::DECORATOR);

		//$view->addHelperPath('Ews/View/Helper', 'Ews_View_Helper');
		//$this->addPrefixPath('Ews_Form','Ews/Form');



		$this->setDecorators(array (
			'FormElements',
			'Form'
		));

		parent::__construct($options);
	}

	public function addElement ($element, $name = null, $options = null) {
		if ($options == null) {
			$options = array ();
		}

		if (!is_object($element)) {
			if (! key_exists("label", $options) && ! key_exists("Label", $options)) {
				$options["label"] = $this->translate($name);
			}

			if (! key_exists("filters", $options) && ! key_exists("Filters", $options)) {
				$options["Filters"] = array (
					'StripTags',
					'StringTrim'
				);
			}
		}



		if(empty($options["multiOptions"]) && is_string($element) && strtolower($element) == "yesno"){
			$element="Select";
				$options["multiOptions"] = array (
					'0' => $this->getTranslator()->_("core.no"),
					 1 => $this->getTranslator()->_("core.yes")
				);

		}


		if($this->getAttrib('elementClass')){

			$cssClass="";
			if(is_string($element)){
				switch($element){
					case "text":
					case "password":
					case "bankAccount":
						$cssClass="text_field";
					break;
					case "textarea":
						$cssClass="text_area";
					break;
					case "checkbox":
						$cssClass="checkbox";
					break;
				}
				$options["class"]=(empty($options["class"]) ? "" : $options["class"]." ").$cssClass;
			}
		}
			//Zend_Debug::dump($options );
		//exit;


		return parent::addElement($element, $name, $options);

	}

	function translate ($text, $params = array()) {
		/*if ($text == "")
			throw new Exception("x");*/
		return $this->getTranslator()->_($this->getAttrib('prefix') . $text, $params);
	}


	function createSubForm ($options = null) {
		return new Matj_Form_SubForm($options);
	}


	function __toString () {
		$this->removeAttrib('prefix');
		return parent::__toString();
	}


	function addHashElement () {
		$element = new Zend_Form_Element_Hash(
			'___h', array (
				'disableLoadDefaultDecorators' => true,
				'ignore' => true
			));
		$element->setSalt($this->getName() . get_class($this))->setLabel($this->getTranslator()->_("core.HASH"))->addDecorator(
			'ViewHelper')->addErrorMessage('Form must not be resubmitted');
		$this->addElement($element,'___h');
	}


	/**
	 * Set form state from options array
	 *
	 * Array params:
	 *
	 * prefix - Translate prefix
	 * prefixPath
	 * elementPrefixPath
	 * displayGroupPrefixPath
	 * elementDecorators
	 * elementDecorators
	 * defaultDisplayGroupClass
	 * displayGroupDecorators
	 * elementsBelongTo
	 * attribs
	 *
	 *
	 *
	 * @param  array $options
	 * @return Zend_Form
	 */
	public function setOptions (array $options) {
		return parent::setOptions($options);
	}



	protected $_columnStatus;
	protected $_columnClass;
	protected $_columnOpened;
	protected $_columnColumns;

	function columnStart($class=""){
		$this->_columnStatus=1;
		$this->_columnClass=$class;
	}

	function columnStop(){
		$this->_columnStatus=2;
	}



	function parse($name,$text){
		$e=$this->getElement($name);
		if(empty($e)){
			throw new Zend_Form_Exception($name." element not exists");
		}
		$e->addDecorator(new Matj_Decorator_Parse(array('text'=>$text)));


	}

	function addHidden($name,$value=null){
		return parent::addElement('hidden',$name,array('value'=>$value,'decorators'=>array('ViewHelper')));
	}


	function addButtons($options=array()){

		$buttons='<div class="group navform wat-cf">';

		if($options["button"]["src"]){
			$image="";
		}else{
			$image='<img src="'.$this->getView()->baseUrl().'/images/ic_tick.png" alt="Save">';
		}


		
		if(!empty($options["submit"]["label"])){
        	$text=$options["submit"]["label"];
    	}
		else{
			$text=$this->translate("ulozit");
		}
		
        $buttons.='<button class="button" type="submit">
                    '.$image.''.$text.'
                  </button>';


        $back=$this->getView()->url(array('action'=>null,'id'=>null));
        if(!empty($options["back"]["url"])){
        	$back=$options["back"]["url"];
    	}

        if(!empty($options["back"]["show"])){
        $buttons.='<span class="text_button_padding">alebo</span>
        		          <a class="text_button_padding link_button" href="'.$back.'">návrat</a>
                  ';
        }

        if(!empty($options["delete"]["show"])){
        	$delete=$this->getView()->url(array('action'=>'delete'));
			if(!empty($options["delete"]["url"])){
	        	$delete=$options["delete"]["url"];
	    	}

        	$buttons.='<a class="button delete_button confirm_submit" alt="Skutočne chcete vymazať túto položku?" href="'.$delete.'">Vymazať</a>';
        }




		$buttons.='
                  </div>
		';
		$this->addHidden('buttons');
		$this->parse('buttons',$buttons);
	}
        
        
        
        function addHtml($name,$text){
		
		if($e=$this->getElement($name)){
			$e->clearDecorators();
			$e->addDecorator(new Matj_Decorator_Text(array('text' => $text)));
		}
		else{		
			$e = $this->createElement('text', $name, array(
			    'ignore'=>true,    
				'decorators' => array(
						new Matj_Decorator_Text(array('text' => $text))			
					)			
			    ));
		}
		$this->addElement($e);
		return $this;
	}
}