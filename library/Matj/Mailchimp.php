<?php

/**
 * Trieda pre posielanie mailov
 * @package Core
 * @author Matus Jancik
 * @since 2011
 */

require_once 'Mailchimp/MCAPI.class.php';

class Matj_Mailchimp{
		
	protected $mcapi;
	protected $settings;
	
	function __construct($settings = array()){
            if(empty($settings)){
                $settings=Matj_Get::getBootstrap()->getOption('mailchimp');
            }
            $this->settings=$settings;
            $this->mcapi=new MCAPI($settings["apikey"]);
        }

	/**
         * 
         * @return MCAPI
         */
        public function getApi(){
            return $this->mcapi;
        }
        
        function getSettings(){
            return $this->settings;
        }
        
        function getSetting($key){
            $s=$this->settings;
            return $s[$key];
        }
        
        
        /**
         * return listMembers from settings list id 
         * @param string $listId listId if null get from settings
         */
        function getMembers($listId=null){
            if($listId==null)
                $listId=$this->getSetting('listId');
            
            return $this->mcapi->listMembers($listId);
        }
        
        
        function unsubscribe(array $data){
            if(empty($data["listId"]))
                $listId=$this->getSetting('listId');
            else
                $listId=$data["listId"];
            
            return $this->mcapi->listUnsubscribe($listId, $data["user_email"],false,false);
        }
        
        
        function subscribe(array $data){
            if(empty($data["listId"]))
                $listId=$this->getSetting('listId');
            else
                $listId=$data["listId"];
            
            $email=$data["user_email"];
            $validator=new Matj_Validate_EmailAddress();
            if(!$validator->isValid($email))
                throw new Matj_MailchimpException('No email in subscribe data');
            
            unset($data["user_email"]);
            unset($data["listId"]);
            
            $merge_vars=array();
            if(!empty($data["user_name"]))$merge_vars["FNAME"]=$data["user_name"];
            if(!empty($data["user_surname"]))$merge_vars["LNAME"]=$data["user_surname"];
            
//            var_dump($listId);exit;
            
            return $this->mcapi->listSubscribe($listId, $email, $merge_vars,'html',false);
            
        }
        
        function updateMember(array $data){
            if(empty($data["listId"]))
                $listId=$this->getSetting('listId');
            else
                $listId=$data["listId"];
            
            $email=$data["user_email"];
            $validator=new Matj_Validate_EmailAddress();
            if(!$validator->isValid($email))
                throw new Matj_MailchimpException('No email in subscribe data');
            
            unset($data["user_email"]);
            unset($data["listId"]);
            
            $merge_vars=array();
            $merge_vars["FNAME"]=$data["user_name"];
            $merge_vars["LNAME"]=$data["user_surname"];
            
            
            return $this->mcapi->listUpdateMember($listId, $email, $merge_vars);
        }
        
        
        function memberInfo(array $data){
            if(empty($data["listId"]))
                $listId=$this->getSetting('listId');
            else
                $listId=$data["listId"];
            
            return $this->mcapi->listMemberInfo($listId, $data["user_email"]);
        }
        
 }

 
 class Matj_MailchimpException extends Zend_Exception{
     
 }