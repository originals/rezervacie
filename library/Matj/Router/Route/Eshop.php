<?php

/** Zend_Controller_Router_Route_Abstract */
require_once 'Zend/Controller/Router/Route/Abstract.php';

/**
 * Module Route
 *
 * Default route for module functionality
 *
 * @package    Zend_Controller
 * @subpackage Router
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @see        http://manuals.rubyonrails.com/read/chapter/65
 */
class Matj_Router_Route_Eshop extends Zend_Controller_Router_Route_Module
{
    /**
     * Default values for the route (ie. module, controller, action, params)
     * @var array
     */
    protected $_defaults;

    protected $_values      = array();
    protected $_moduleValid = false;
    protected $_keysSet     = false;

    protected $_keyProduct=array('sk'=>"produkty",'en'=>'products');
        
    
    
    /**#@+
     * Array keys to use for module, controller, and action. Should be taken out of request.
     * @var string
     */
    protected $_moduleKey     = 'module';
    protected $_controllerKey = 'controller';
    protected $_actionKey     = 'action';
    /**#@-*/

    /**
     * @var Zend_Controller_Dispatcher_Interface
     */
    protected $_dispatcher;

    /**
     * @var Zend_Controller_Request_Abstract
     */
    protected $_request;

    /**
     * @var Matj_Translate
     */
    protected $_translator;

    /**
     * @var Matj_Db_Adapter_Pdo_Mysql
     */
    protected $_db;

    
    
    
    public function getVersion() {
        return 1;
    }

    /**
     * Instantiates route based on passed Zend_Config structure
     */
    public static function getInstance(Zend_Config $config)
    {
        $frontController = Zend_Controller_Front::getInstance();

        $defs       = ($config->defaults instanceof Zend_Config) ? $config->defaults->toArray() : array();
        $dispatcher = $frontController->getDispatcher();
        $request    = $frontController->getRequest();

        return new self($defs, $dispatcher, $request);
    }

    /**
     * Constructor
     *
     * @param array $defaults Defaults for map variables with keys as variable names
     * @param Zend_Controller_Dispatcher_Interface $dispatcher Dispatcher object
     * @param Zend_Controller_Request_Abstract $request Request object
     */
    public function __construct(
                Zend_Controller_Dispatcher_Interface $dispatcher = null,
                Zend_Controller_Request_Abstract $request = null)
    {
        
        $defaults=array('module'=>'eshop','controller'=>'view','action'=>'index');
        
        $this->_defaults = $defaults;

        if (isset($request)) {
            $this->_request = $request;
        }

        if (isset($dispatcher)) {
            $this->_dispatcher = $dispatcher;
        }
        
        $this->_db = Matj_Get::getDbAdapter();
        
        $this->_translator=Matj_Get::getTranslator();
        
    }

    /**
     * Set request keys based on values in request object
     *
     * @return void
     */
    protected function _setRequestKeys()
    {
        if (null !== $this->_request) {
            $this->_moduleKey     = $this->_request->getModuleKey();
            $this->_controllerKey = $this->_request->getControllerKey();
            $this->_actionKey     = $this->_request->getActionKey();
        }

        if (null !== $this->_dispatcher) {
            $this->_defaults += array(
                $this->_controllerKey => $this->_dispatcher->getDefaultControllerName(),
                $this->_actionKey     => $this->_dispatcher->getDefaultAction(),
                $this->_moduleKey     => $this->_dispatcher->getDefaultModule()
            );
        }

        $this->_keysSet = true;
    }

    /**
     * Matches a user submitted path. Assigns and returns an array of variables
     * on a successful match.
     *
     * If a request object is registered, it uses its setModuleName(),
     * setControllerName(), and setActionName() accessors to set those values.
     * Always returns the values as an array.
     *
     * @param string $path Path used to match against this routing map
     * @return array An array of assigned values or a false on a mismatch
     */
    public function match($path, $partial = false)
    {
        $this->_setRequestKeys();

        
        
        
        $values = array();
        $params = array();

        if (!$partial) {
            $path = trim($path, self::URI_DELIMITER);
        } else {
            $matchedPath = $path;
        }

        
        
        
        
        $path = explode(self::URI_DELIMITER, $path);

        
        $first=$path[0];
        
        
        
        
        if($first=="search"){
            $lang="sk";            
        }
        elseif($first=="akcie"){
            $lang="sk";
            array_shift($path);
            $this->_values["discount"]=1;
        }
		elseif($first=="novinky"){
            $lang="sk";
            array_shift($path);
            $this->_values["new"]=1;
        }
        elseif(preg_match('/^category\-([a-z]{2})\-([0-9]+)$/',$first,$m)){
            $lang=$m[2];
            $categoryId=$m[1];
        }
        else{
            
            $category=new Eshop_Model_Category($params["category"]);

            $where=' category_url_sk = "'.$first.'" OR  category_url_en = "'.$first.'" ';
            $category->fetchEntry(null, $where);
            
            if($category->isEmpty()){
                return false;
            }
            else{
                if($category->getData('category_url_sk')==$first){
                    $lang='sk';
                }
                elseif($category->getData('category_url_en')==$first){
                    $lang='en';
                }
            }
        }        
        
        /*
        $first=array_shift($path);
        
         * if(($lang=array_search($first,$this->_keyProduct)) == false)
            return false;
        else
            $this->_values["lang"]=$lang;
        */
        
        //Zend_Debug::dump($path);exit;
        
        $curl=array();
        foreach ($path as $k=>$d){
            
            if(preg_match('/\.html$/',$d)){
                
                if(preg_match('/([0-9_]+)\.html$/',$d,$m)){
                    $x=explode("_",$m[1]);
                    if(!empty($x[0]))
                        $this->_values["product"]=$x[0]; 
                    if(!empty($x[1]))
                        $this->_values["variation"]=$x[1]; 
                        
                }

                $this->_values["action"]='product'; 
            }
            else{
                $curl[]=$d;
            }
            
        }
        
        
        if(!empty($categoryId)){
            $this->_values["category"]=$categoryId;
        }
        elseif(!empty($curl)){
            
            
            $category=new Eshop_Model_Category($params["category"]);
            $category->fetchByUrl(implode("/",$curl),$this->_values["lang"]);
            
            $this->_values["category"]=$category->isEmpty() ? null : $category->getId();
        }
        
        
        $allGetValues = $_GET;
        foreach($_GET as $k=>$d){
            $this->_values[htmlspecialchars(strip_tags(urldecode($k)))]=htmlspecialchars(strip_tags(urldecode($d)));
        }
        
        return $this->_values + $this->_defaults;
    }

    /**
     * Assembles user submitted parameters forming a URL path defined by this route
     *
     * @param array $data An array of variable and value pairs used as parameters
     * @param bool $reset Weither to reset the current params
     * @return string Route path with user submitted parameters
     */
    public function assemble($data = array(), $reset = false, $encode = true, $partial = false)
    {
        if (!$this->_keysSet) {
            $this->_setRequestKeys();
        }

        
        
        $params = (!$reset) ? $this->_values : array();
        
        unset($params["page"]);
        
        foreach ($data as $key => $value) {
            if ($value !== null) {
                $params[$key] = $value;
            } elseif (isset($params[$key])) {
                unset($params[$key]);
            }
        }

        
        //nepotrebujem default
        //$params += $this->_defaults;

        
        $url = '';
        $urlPart=array();

        
        $lang=$params["lang"];
        if(empty($lang))
            $lang=Matj_Get::getLocale().'';
        
        unset($params["lang"]);
        
        
       // $urlPart[]=$this->_keyProduct[$lang];
        
        if(!empty($params["discount"])){
            $urlPart[]="akcie";
            unset($params["discount"]);
        }
		if(!empty($params["new"])){
            $urlPart[]="novinky";
            unset($params["new"]);
        }
        
        if(!empty($params["category"])){
            
            
            $category=new Eshop_Model_Category();
            
            $urlPart[]=$category->getFullurlCache($params["category"],$params["lang"]);
            unset($params["category"]);
        }
        else{
            if(empty($urlPart))$urlPart[]='search';
        }
        
        
        
        if(!empty($params["product"])){
            $urlPart[]=$params["product"].'.html';
            unset($params["product"]);
        }
        
        unset($params["variation"]);
        unset($params["action"]);

        $addurl='';
        
        foreach ($params as $key => $value) {
            $key = ($encode) ? urlencode($key) : $key;
            
            if(!empty($addurl))$addurl.='&amp;';
            
            if (is_array($value)) {
                foreach ($value as $key2=>$arrayValue) {
                    $arrayValue = ($encode) ? urlencode($arrayValue) : $arrayValue;
                    $addurl .= $key.'['.$key2.']';
                    $addurl .= '='.$arrayValue;
                }
            } else {
                if ($encode) $value = urlencode($value);
                $addurl .= $key.'='.$value;
            }
        }

        $url=implode(self::URI_DELIMITER,$urlPart);
        
        
        
        //Zend_Debug::dump($urlPart);
        
        if(!empty($addurl))
            $url.='?'.$addurl;
        
        
        return ltrim($url, self::URI_DELIMITER);
    }

    /**
     * Return a single parameter of route's defaults
     *
     * @param string $name Array key of the parameter
     * @return string Previously set default
     */
    public function getDefault($name) {
        if (isset($this->_defaults[$name])) {
            return $this->_defaults[$name];
        }
    }

    /**
     * Return an array of defaults
     *
     * @return array Route defaults
     */
    public function getDefaults() {
        return $this->_defaults;
    }
    
    
    
    
    function _getObject($url,$parent=null,$options=array()){
        $object=new Objects_Model_Object();
        
        $object->fetchByUrl($url,$parent,$options);
        if($object->isEmpty()){
            return null;
        }
        return $object;
    }
    
    function _getCity($url,$parent=null){ 
        $object=new Objects_Model_ObjectCity();
        
        $object->fetchEntry(null,'city_url = "'.$url.'"',$parent=null);
        if($object->isEmpty()){
            return null;
        }
        return $object;
    }
    
    
}
