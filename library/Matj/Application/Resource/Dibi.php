<?php

require_once 'Matj/Dibi/Model.php';

class Matj_Application_Resource_Dibi extends Zend_Application_Resource_ResourceAbstract
{
    
    const DEFAULT_REGISTRY_KEY = 'Dibi';

    
    
    public function init()
    {
        return $this->loadDibi();
    }
    
    public function loadDibi(){
        
        $options=$this->getOptions();
        
        $params=$options["params"];
        
        $dibi=new DibiConnection($params);
        
        if(!empty($params["queries"]))
            foreach($params["queries"] as $q)
                $dibi->query($q);
        
        
        Zend_Registry::set(self::DEFAULT_REGISTRY_KEY,$dibi);
        return $dibi;
    }
    
}
