<?php

class Matj_Application_Resource_Mongo extends Zend_Application_Resource_ResourceAbstract
{
    
    const DEFAULT_REGISTRY_KEY = 'MongoDb';

    
    function getMongoClient($seeds = "", $options = array(), $retry = 3) {
            try {
                return new MongoClient($seeds, $options);
            } catch(Exception $e) {
                /* Log the exception so we can look into why mongod failed later */
                //logException($e);
            }
            if ($retry > 0) {
                return $this->getMongoClient($seeds, $options, --$retry);
            }
        throw new Exception("I've tried several times getting MongoClient.. Is mongod really running?");
    }

 
    
    
    public function init()
    {
        return $this->loadMongo();
    }
    
    public function loadMongo(){
        
        $options=$this->getOptions();
        if(empty($options["server"])){
            $server=null;
        }
        else{
            $server=$options["server"];
        }
        
        if (empty($options["db"])) {
            throw new Exception('Not set DB - resources.mongo.db=databasename');
        }

        $db=$options["db"];
        
        try {
            $mongo = $this->getMongoClient($server, array());
        } catch(Exception $e) {
            /* Can't connect to MongoDB! */
            throw $e;
            
        }

        
        
        $database=$mongo->{$db};
        
        
        Zend_Registry::set(self::DEFAULT_REGISTRY_KEY,$database);
        return $mongo;
    }
    
}
