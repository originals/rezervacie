<?php
 /*
 * HighRoller -- PHP wrapper for the popular JS charting library Highcharts
 * Author:       jmaclabs@gmail.com
 * File:         HighRoller.php
 * Date:         Sun Jan 29 20:35:13 PST 2012
 * Version:      1.0.5
 *
 * Licensed to Gravity.com under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  Gravity.com licenses this file to you use
 * under the Apache License, Version 2.0 (the License); you may not this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at 
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
?>
<?php
/**
 * Author: jmaclabs
 * Date: 9/14/11
 * Time: 5:46 PM
 * Desc: HighRoller Parent Class
 *
 * Licensed to Gravity.com under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  Gravity.com licenses this file to you use
 * under the Apache License, Version 2.0 (the License); you may not this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

class Matj_Charts_HighRoller {

  public $chart;
  public $title;
//  public $legend;
//  public $tooltip;
//  public $plotOptions;
  public $series = array();

  function __construct(){

    $this->chart = new Matj_Charts_Lib_HighRollerChart();
    $this->title = new Matj_Charts_Lib_HighRollerTitle();
//    $this->legend = new Matj_Charts_Lib_HighRollerLegend();
//    $this->tooltip = new Matj_Charts_Lib_HighRollerToolTip();
//    $this->plotOptions = new Matj_Charts_Lib_HighRollerPlotOptions($this->chart->type);
    $this->series = new Matj_Charts_Lib_HighRollerSeries();

  }

  /** returns a javascript script tag with path to your HighCharts library source
   * @static
   * @param $location - path to your highcharts JS
   * @return string - html script tag markup with your source location
   */
  public static function setHighChartsLocation($location){
    return $scriptTag = "<!-- High Roller - High Charts Location-->
  <script type='text/javascript' src='" . $location . "'></script>";

  }

  /** returns a javascript script tag with path to your HighCharts library THEME source
   * @static
   * @param $location - path to your highcharts theme file
   * @return string - html script tag markup with your source location
   */
  public static function setHighChartsThemeLocation($location){
    return $scriptTag = "<!-- High Roller - High Charts Theme Location-->
  <script type='text/javascript' src='" . $location . "'></script>";

  }

  /** returns chart object with newly set obj property name
   * @param $objName - string, name of the HighRoller Object you're operating on
   * @param $propertyName - string, name of the property you want to set, can be a new property name
   * @param $value - mixed, value you wish to assign to the property
   * @return HighRoller
   */
  public function setProperty($objName, $propertyName, $value){
    $this->$objName->$propertyName = $value;
    return $this;
  }

  /** add data to plot in your chart
   * @param $chartdata - array, data provided in 1 of 3 HighCharts supported array formats (array, assoc array or mult-dimensional array)
   * @return void
   */
  public function addData($chartdata){
    if(!is_array($chartdata)){
      die("HighRoller::addData() - data format must be an array.");
    }
    $this->series = array($chartdata);
  }

  /** add series to your chart
   * @param $chartdata - array, data provided in 1 of 3 HighCharts supported array formats (array, assoc array or mult-dimensional array)
   * @return void
   */
  public function addSeries($chartData){
    if(!is_object($chartData)){
      die("HighRoller::addSeries() - series input format must be an object.");
    }

    if(is_object($this->series)){   // if series is an object
      $this->series = array($chartData);
    } else if(is_array($this->series)) {                        // else
      array_push($this->series, $chartData);
    }
  }

  /** enable auto-step calc for xAxis labels for very large data sets.
   * @return void
   */
  public function enableAutoStep(){

    if(is_array($this->series)) {
      $count = count($this->series[0]->data);
      $step = number_format(sqrt($count));
      if($count > 1000){
        $step = number_format(sqrt($count/$step));
      }

      $this->xAxis->labels->step = $step;
    }

  }

  /** returns new Matj_Charts_Lib_Highcharts javascript
   * @return string - highcharts!
   */
  function renderChart($engine = 'jquery'){
    $options = new Matj_Charts_Lib_HighRollerOptions();   // change file/class name to new Matj_Charts_Lib_HighRollerGlobalOptions()

    if ( $engine == 'mootools')
      $chartJS = 'window.addEvent(\'domready\', function() {';
    else
      $chartJS = '$(document).ready(function() {';

    $chartJS .= "\n\n    // HIGHROLLER - HIGHCHARTS UTC OPTIONS ";

    $chartJS .= "\n    Highcharts.setOptions(\n";
    $chartJS .= "       " . json_encode($options) . "\n";
    $chartJS .= "    );\n";
    $chartJS .= "\n\n    // HIGHROLLER - HIGHCHARTS '" . $this->title->text . "' " . $this->chart->type . " chart";
    $chartJS .= "\n    var " . $this->chart->renderTo . " = new Highcharts.Chart(\n";
    $chartJS .= "       " . $this->getChartOptionsObject() . "\n";
    $chartJS .= "    );\n";
    $chartJS .= "\n  });\n";
    return trim($chartJS);
  }

  /** returns valid Highcharts javascript object containing your HighRoller options, for manipulation between the markup script tags on your page`
   * @return string - highcharts options object!
   */
  function getChartOptionsObject(){
    return trim(json_encode($this));
  }

  /** returns new Highcharts.Chart() using your $varname
   * @param $varname - name of your javascript object holding getChartOptionsObject()
   * @return string - a new Highcharts.Chart() object with the highroller chart options object
   */
  function renderChartOptionsObject($varname){
    return "new Highcharts.Chart(". $varname . ")";
  }

}
?><?php

class Matj_Charts_Lib_HighRollerAxisLabel {

  public $style;

  function __construct(){
    $this->style = new Matj_Charts_Lib_HighRollerStyle();
  }

}
?><?php

class Matj_Charts_Lib_HighRollerAxisTitle {

  public $style;

  function __construct(){
    $this->style = new Matj_Charts_Lib_HighRollerStyle();
  }

}
?><?php

 
class Matj_Charts_Lib_HighRollerBackgroundColors {

  public $linearGradient;
  public $stops;
  function __construct(){

  }
  
}
?><?php


class Matj_Charts_Lib_HighRollerChart {

  public $renderTo;
//  public $animation;

  function __construct(){
    $this->renderTo = null;
//    $this->animation = new Matj_Charts_Lib_HighRollerChartAnimation();
  }

}
?><?php


class Matj_Charts_Lib_HighRollerChartAnimation {

  function __construct(){
  }

}
?>
<?php


class Matj_Charts_Lib_HighRollerCredits {

  function __construct(){

  }

}
?><?php

 
class Matj_Charts_Lib_HighRollerDataLabels {

  function __construct(){
    $this->style = new Matj_Charts_Lib_HighRollerStyle();
  }

}
?><?php

 
class Matj_Charts_Lib_HighRollerDateTimeLabelFormats {

  function __construct(){

  }

}
?><?php

 
class Matj_Charts_Lib_HighRollerEngine {

  public $type;

  function __construct(){
    $this->type = "jquery";
  }

}
?><?php
 
class Matj_Charts_Lib_HighRollerFormatter {

  public $formatter;
  
  function __construct(){
    $this->formatter = "";
  }

}
?>
<?php


class Matj_Charts_Lib_HighRollerLegend {

  public $style;
  public $backgroundColor = null;

  function __construct(){
    $this->style = new Matj_Charts_Lib_HighRollerStyle();
  }

}
?><?php


class Matj_Charts_Lib_HighRollerOptions {

  public $global;

  function __construct(){
    $this->global = new Matj_Charts_Lib_HighRollerOptionsGlobal();
  }

}
?><?php

 
class Matj_Charts_Lib_HighRollerOptionsGlobal {

  public $useUTC;

  function __construct(){
    $this->useUTC = true;
  }

}
?><?php

 
class Matj_Charts_Lib_HighRollerPlotLines {

  function __construct(){
    $this->label = new Matj_Charts_Lib_HighRollerAxisLabel();
  }

}
?>

<?php

 
class Matj_Charts_Lib_HighRollerPlotOptions {

  public $series;

  function __construct($chartType){
    $this->series = new Matj_Charts_Lib_HighRollerSeriesOptions();
    if($chartType == 'area'){ $this->area = null; }
    else if($chartType == 'bar'){ $this->bar = null; }
    else if($chartType == 'column'){ $this->column = null; }
    else if($chartType == 'line'){ $this->line = null; }
    else if($chartType == 'pie'){ $this->pie = null; }
    else if($chartType == 'scatter'){ $this->scatter = null; }
    else if($chartType == 'spline'){ $this->spline = null; }
  }

}
?><?php



class Matj_Charts_Lib_HighRollerPlotOptionsByChartType {

  public $dataLabels;
  public $formatter;

  function __construct($type){
    $this->dataLabels = new Matj_Charts_Lib_HighRollerDataLabels();
    $this->formatter = new Matj_Charts_Lib_HighRollerFormatter();
  }

}
?><?php


class Matj_Charts_Lib_HighRollerSeries {

  function __construct(){
  }

}
?><?php
/**
 * Author: jmac
 * Date: 9/23/11
 * Time: 12:40 PM
 * Desc: HighRoller Series Data Options
 *
 * Licensed to Gravity.com under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  Gravity.com licenses this file to you use
 * under the Apache License, Version 2.0 (the License); you may not this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
 
class Matj_Charts_Lib_HighRollerSeriesOptions {

  public $dataLabels;

  function __construct(){
    $this->dataLabels = new Matj_Charts_Lib_HighRollerDataLabels();
  }

}
?><?php
/**
 * Author: jmac
 * Date: 9/24/11
 * Time: 1:28 AM
 * Desc: HighRoller Style
 *
 * Licensed to Gravity.com under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  Gravity.com licenses this file to you use
 * under the Apache License, Version 2.0 (the License); you may not this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
 
class Matj_Charts_Lib_HighRollerStyle {

  function __construct(){

  }
}
?><?php
/**
 * Author: jmac
 * Date: 9/21/11
 * Time: 1:07 PM
 * Desc: HighRoller Title Class
 *
 * Licensed to Gravity.com under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  Gravity.com licenses this file to you use
 * under the Apache License, Version 2.0 (the License); you may not this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

class Matj_Charts_Lib_HighRollerTitle {

  public $text;
//  public $style;

  function __construct(){
//    $this->style = new Matj_Charts_Lib_HighRollerStyle();
  }
  
}
?><?php
/**
 * Author: jmac
 * Date: 9/21/11
 * Time: 11:46 PM
 * Desc: HighRoller Tool Tip
 *
 * Licensed to Gravity.com under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  Gravity.com licenses this file to you use
 * under the Apache License, Version 2.0 (the License); you may not this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
 
class Matj_Charts_Lib_HighRollerToolTip {

  public $backgroundColor = null;

  function __construct(){

  }

}
?>
<?php
/**
 * Author: jmac
 * Date: 9/21/11
 * Time: 1:10 PM
 * Desc: HighRoller xAxis Class
 *
 * Licensed to Gravity.com under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  Gravity.com licenses this file to you use
 * under the Apache License, Version 2.0 (the License); you may not this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

class Matj_Charts_Lib_HighRollerXAxis {

  public $labels;
  public $title;
  public $categories = array();
  public $plotLines = array();    // @TODO instantiating a new plotLines object isn't working, setting as an array
  public $formatter;

  function __construct(){
    $this->labels = new Matj_Charts_Lib_HighRollerXAxisLabels();
    $this->title = new Matj_Charts_Lib_HighRollerAxisTitle();
    $this->dateTimeLabelFormats = new Matj_Charts_Lib_HighRollerDateTimeLabelFormats();
    $this->formatter = new Matj_Charts_Lib_HighRollerFormatter();
  }

}
?><?php
/**
 * Author: jmac
 * Date: 9/21/11
 * Time: 8:56 PM
 * Desc: HighRoller xAxis Labels
 *
 * Licensed to Gravity.com under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  Gravity.com licenses this file to you use
 * under the Apache License, Version 2.0 (the License); you may not this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
 
class Matj_Charts_Lib_HighRollerXAxisLabels {

  public $style;

  function __construct(){
    $this->style = new Matj_Charts_Lib_HighRollerStyle();
  }

}
?><?php
/**
 * Author: jmac
 * Date: 9/21/11
 * Time: 8:44 PM
 * Desc: HighRoller yAxis
 *
 * Licensed to Gravity.com under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  Gravity.com licenses this file to you use
 * under the Apache License, Version 2.0 (the License); you may not this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
 
class Matj_Charts_Lib_HighRollerYAxis {

  public $labels;
  public $title;
  public $plotLines = array();    // @TODO instantiating a new plotLines object isn't working, setting as an array
  public $formatter;

  function __construct(){
    $this->labels = new Matj_Charts_Lib_HighRollerAxisLabel();
    $this->title = new Matj_Charts_Lib_HighRollerAxisTitle();
    $this->plotLines = array();   // @TODO need to revisit why declaring this as an empty class or a hydrated class isn't working    $this->dateTimeLabelFormats = new Matj_Charts_Lib_HighRollerDateTimeLabelFormats();
    $this->formatter = new Matj_Charts_Lib_HighRollerFormatter();
  }
  
}
?>