<?php

class Matj_Util {
	
	/**
	 * tato metoda prechadza $msg a nahradzuje vyskyty {$key} ich hodnotou v $params[$key] 
	 */
	public static function parseParams ($msg, $params = array()) {
		if (is_array($params)) {
			foreach ($params as $key => $param) {
				if (! is_array($param)) {
					$msg = str_replace('{' . $key . '}', $param, $msg);
					$msg = str_replace('%' . $key . '%', $param, $msg);
				}
			}
		}
		return $msg;
	}
	
	
	static function arrayToObject($data) {
 	   $object = (object) $data;
	   foreach ($object as $property) {
        if (is_array($property)) self::arrayToObject($property);
    	}
		return $object;
	}
	
	
	/**
	 * vrati pole s cislami od do
	 * 
	 * @param int $from
	 * @param int $to
	 * @param string $valuePrefix
	 * @return string
	 */
	public static function arrayFromTo ($from, $to, $valuePrefix = "") {
		$array = array ();
		if ($from < $to) {
			for ($i = $from; $i <= $to; $i ++) {
				$array[$i] = $valuePrefix . $i;
			}
		}
		else {
			for ($i = $from; $i >= $to; $i --) {
				$array[$i] = $valuePrefix . $i;
			}
		}
		return $array;
	}
	
	/**
	 * 
	 */
	public static function addSqlParams ($select, $where = "", $order = NULL, $group = "", $from = 0, $count = null, $having = "") {
		if (is_array($where)) {
			$o = $where;
			unset($where);
			if (isset($o["where"]))
				$where = $o["where"];
			if (isset($o["orderBy"]))
				$order = $o["orderBy"];
			if (isset($o["order"]))
				$order = $o["order"];
			if (isset($o["offset"]))
				$from = $o["offset"];
			if (isset($o["limit"]))
				$count = $o["limit"];
			if (isset($o["having"]))
				$having = $o["having"];
			if (isset($o["group"]))
				$group = $o["group"];
			
		}
		else {
			$where = $where;
		}
		if (! empty($where)) {
			$select .= "WHERE $where ";
		}
		if (! empty($group)) {
			$select .= "GROUP BY $group ";
		}
		if (! empty($having)) {
			$select .= " HAVING $having ";
		}
		if (! empty($order)) {
			$select .= "ORDER BY $order ";
		}
		if (! empty($count)) {
			$select .= " LIMIT " . ($count) . " OFFSET " . ($from + 0);
		}
		return $select;
	}
	
	/**
	 * spoji dve where klauzule pomocou operatora v $join
	 *
	 * @param string $where1
	 * @param string $where2
	 * @param string $join
	 * @return string
	 */
	public static function joinWheres ($where1, $where2, $join = 'AND') {
		if (! empty($where1) && ! empty($where2)) {
			return $where1 . " " . $join . " " . $where2;
		}
		else {
			return $where1 . $where2;
		}
	}
	

	static function human_time_diff ($from, $to = '') {
		if (preg_match("/\-/", $from)) {
			$from = self::dateToMktime($from);
		}
		
		if (empty($to))
			$to = time();
		$diff = (int) abs($to - $from);
		if ($diff <= 3600) {
			$mins = round($diff / 60);
			if ($mins <= 1) {
				$mins = 1;
			}
			$since = sprintf(Matj_Get::getTranslator()->_('%s min ago'), $mins);
		}
		else 
			if (($diff <= 86400) && ($diff > 3600)) {
				$hours = round($diff / 3600);
				if ($hours <= 1) {
					$hours = 1;
				}
				$since = sprintf(Matj_Get::getTranslator()->_('%s hour'), $hours);
			}
			elseif ($diff >= 86400) {
				$days = round($diff / 86400);
				if ($days <= 1) {
					$days = 1;
				}
				$since = sprintf(Matj_Get::getTranslator()->_('%s day'), $days);
			}
		return $since;
	}
	
	function timeago ($referencedate = 0, $timepointer = '', $measureby = '', $autotext = true) { ## Measureby can be: s, m, h, d, or y
		
		if (preg_match("/\-/", $referencedate)) {
			$referencedate = self::dateToMktime($referencedate);
		}

		if ($timepointer == '')
			$timepointer = time();
		else {
			if (preg_match("/\-/", $timepointer)) {
				$timepointer = self::dateToMktime($timepointer);
			}
		}
		$Raw = $timepointer - $referencedate; ## Raw time difference
		$Clean = abs($Raw);
		$calcNum = array ( 
			array ( 
				's', 
				60 
			), 
			array ( 
				'm', 
				60 * 60 
			), 
			array ( 
				'h', 
				60 * 60 * 60 
			), 
			array ( 
				'd', 
				60 * 60 * 60 * 24 
			), 
			array ( 
				'w', 
				60 * 60 * 60 * 24 * 7
			), 
			array ( 
				'y', 
				60 * 60 * 60 * 24 * 365 
			) 
		); ## Used for calculating
		$calc = array ( 
			's' => array ( 
				1, 
				'second', 
				'seconds' 
			), 
			'm' => array ( 
				60, 
				'minute', 
				'minutes' 
			), 
			'h' => array ( 
				60 * 60, 
				'hour', 
				'hours' 
			), 
			'd' => array ( 
				60 * 60 * 24, 
				'day', 
				'days' 
			), 
			'w' => array ( 
				60 * 60 * 24 * 7, 
				'week', 
				'weeks' 
			), 
			'y' => array ( 
				60 * 60 * 24 * 365, 
				'year', 
				'years' 
			) 
		); ## Used for units and determining actual differences per unit (there probably is a more efficient way to do this)
		


		if ($measureby == '') { ## Only use if nothing is referenced in the function parameters
			$usemeasure = 's'; ## Default unit
			


			for ($i = 0; $i < count($calcNum); $i ++) { ## Loop through calcNum until we find a low enough unit
				if ($Clean <= $calcNum[$i][1]) { ## Checks to see if the Raw is less than the unit, uses calcNum b/c system is based on seconds being 60
					$usemeasure = $calcNum[$i][0]; ## The if statement okayed the proposed unit, we will use this friendly key to output the time left
					$i = count($calcNum); ## Skip all other units by maxing out the current loop position
				}
			}
		}
		else {
			$usemeasure = $measureby; ## Used if a unit is provided
		}
		
		$datedifference = floor($Clean / $calc[$usemeasure][0]); ## Rounded date difference
		

		if ($autotext == true && ($timepointer == time())) {
			if ($Raw < 0) {
				$prospect = 'from now';
			}
			else {
				$prospect = 'ago';
			}
		}
		

		if ($referencedate != 0) { ## Check to make sure a date in the past was supplied
			if ($datedifference == 1) { ## Checks for grammar (plural/singular)
				

				$format = '%s ' . $calc[$usemeasure][1] . ' ' . $prospect;
				$format = Matj_Get::getTranslator()->_(trim($format));
				$return=sprintf($format, $datedifference);
				$return='<abbr title="'.date(Matj_Format::dateTimeFormat,$referencedate).'">'.$return.'</abbr>';
				
				return $return;
			}
			else {
				/*
				foreach($calc as $k=>$d){
					$prospect="";
					$format='%s ' . $d[1] . ' ' . $prospect;
					Matj_Get::getTranslator()->_(trim($format));
					$format='%s ' . $d[2] . ' ' . $prospect;
					Matj_Get::getTranslator()->_(trim($format));
					
					$prospect=" ago";
					$format='%s ' . $d[1] . ' ' . $prospect;
					Matj_Get::getTranslator()->_(trim($format));
					$format='%s ' . $d[2] . ' ' . $prospect;
					Matj_Get::getTranslator()->_(trim($format));
					
				}
				exit;*/
				$format = '%s ' . $calc[$usemeasure][2] . ' ' . $prospect;
				$format = Matj_Get::getTranslator()->_(trim($format));
				$return=sprintf($format, $datedifference);
				
				$return='<abbr title="'.date(Matj_Format::dateTimeFormat,$referencedate).'">'.$return.'</abbr>';
				
				return $return;
			}
		}
		else {
			return '- - -';
		}
	}
	
	static function dateToMktime ($date) {
		$d = explode(" ", $date);
		$datum = explode("-", $d[0]);
		$cas = explode(":", @$d[1]);
		$res = mktime($cas[0] + 0, @$cas[1] + 0, @$cas[2] + 0, @$datum[1] + 0, @$datum[2] + 0, @$datum[0] + 0);
		return $res;
	}
	

	static function multisort ($array, $sort_by, $key1, $key2 = NULL, $key3 = NULL, $key4 = NULL, $key5 = NULL, $key6 = NULL) {
		// sort by ?
		foreach ($array as $pos => $val)
			$tmp_array[$pos] = $val[$sort_by];
		asort($tmp_array);
		
		// display however you want
		foreach ($tmp_array as $pos => $val) {
			$return_array[$pos][$sort_by] = $array[$pos][$sort_by];
			$return_array[$pos][$key1] = $array[$pos][$key1];
			if (isset($key2)) {
				$return_array[$pos][$key2] = $array[$pos][$key2];
			}
			if (isset($key3)) {
				$return_array[$pos][$key3] = $array[$pos][$key3];
			}
			if (isset($key4)) {
				$return_array[$pos][$key4] = $array[$pos][$key4];
			}
			if (isset($key5)) {
				$return_array[$pos][$key5] = $array[$pos][$key5];
			}
			if (isset($key6)) {
				$return_array[$pos][$key6] = $array[$pos][$key6];
			}
		}
		return $return_array;
	}
	
	
	static function removeFromArray($key,&$array){
		foreach($array as $j=>$i){
			if($i == $key){
				unset($array[$j]);
				return true;
			}
		}
	}


	static function mergeArrays($array1,$array2){
		$array=$array1;
		foreach($array2 as $k=>$d){
			if(is_array($d) and isset($array[$k]) and is_array($array[$k])){
				$array[$k]=self::mergeArrays($array[$k],$d);			
			}
			else{
				$array[$k]=$d;	
			}
			if($d===null){
				unset($array[$k]);	
			}
		}
		return $array;
	}

	public static function convertToUserDate($dateIn){
		if($dateIn == "0000-00-00" or $dateIn == "0000-00-00 00:00:00")return null;
		
		$userDateFormat = Zend_Registry::getInstance()->conf->dateFormat;
		$data = explode("-",$dateIn);
		$dataPrepare = explode(" ",$data[2]);
		$dataTime = explode(":",$dataPrepare[1]);
		
		$date = new Zend_Date(array('year'=>$data[0],'month'=>$data[1],'day'=>$data[2],'hour'=>$dataTime[0],'minutes'=>$dataTime[1],'seconds'=>$dataTime[3]));
		return strftime("%d.%m.%Y",$date->getTimestamp());
	}
	
	public static function getRemainTime($time=null){
		$date1=time();
		$date2=Matj_Format::dateToTimestamp($time);
		
		//$date2=Delphi_Format::dateToTimestamp('2011-05-05 18:00:00');
		
		$time=$date2-$date1;
		if($time<=0)return '00:00:00:00';
		
		$r["d"]=floor($time/(24*60*60));
		$z=($time-($r["d"]*(24*60*60)));
		$r["h"]=floor($z/(60*60));
		$z=$z-($r["h"]*60*60);
		$r["m"]=floor($z/60);
		$r["s"]=$z-($r["m"]*60);
		
		//Zend_Debug::dump($r);exit;
		
		return sprintf('%02d:%02d:%02d:%02d',$r["d"],$r["h"],$r["m"],$r["s"]);
	}
	
	
	
	static function addDate($date,$array=array(),$format = "Y-m-d"){
        $timestamp=self::dateToMktime($date);
        $add=0;
        if(!empty($array["day"])){
            $add+=( $array["day"] * 24 * 60 * 60 );
        }
        if(!empty($array["hour"])){
            $add+=( $array["hour"] * 60 * 60 );
        }
        if(!empty($array["minute"])){
            $add+=( $array["minute"] * 60);
        }
        $nT=$timestamp + $add;
        return date($format,$nT);
    }

    static function subDate($date,$array=array(),$format = "Y-m-d"){
        $timestamp=self::dateToMktime($date);
        $add=0;
        if(!empty($array["day"])){
            $add+=( $array["day"] * 24 * 60 * 60 );
        }
        if(!empty($array["hour"])){
            $add+=( $array["hour"] * 60 * 60 );
        }
        if(!empty($array["minute"])){
            $add+=( $array["minute"] * 60);
        }
        $nT=$timestamp - $add;
        return date($format,$nT);
    }

    
    
    function base64encode($filename="",$filetype="") {
        if ($filename) {
            $imgbinary = fread(fopen($filename, "r"), filesize($filename));
            return 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
        }
    }
    
    
    function image($src,$opt,$w=null,$h=null){
        if(is_string($opt)){
            $opt=array('type'=>$opt,'link'=>1);
            if($w>0)$opt["width"]=$w;
            if($h>0)$opt["height"]=$h;
        }
        
        return Matj_View_Helper_Image::image($src,$opt);
    }
    
    static function parseSkDate($d){
            $x=explode(".",$d);
            foreach($x as $k=>$d){
                if($d>0)
                    $dateArray[]=$d;
            }
            $date=null;
            if(count($dateArray)==3){
                if($dateArray[2]<=date('y'))
                    $dateArray[2]+=2000;
                if($dateArray[2]>date('y') && $dateArray[2]<100)
                    $dateArray[2]+=1900;
            
                if(checkdate($dateArray[1], $dateArray[0],$dateArray[2]))
                    $date=sprintf("%04d",$dateArray[2]).'-'.sprintf("%02d",$dateArray[1]).'-'.sprintf("%02d",$dateArray[0]);
            }
            elseif(count($dateArray)==2){
                if(checkdate($dateArray[1], $dateArray[0],date('Y')))
                    $date=sprintf("%04d",date('Y')).'-'.sprintf("%02d",$dateArray[1]).'-'.sprintf("%02d",$dateArray[0]);
            }

            
            
            return $date;
    }
    
    
    static function color_name_to_hex($color_name)
{
    // standard 147 HTML color names
    $colors  =  array(
        'aliceblue'=>'F0F8FF',
        'antiquewhite'=>'FAEBD7',
        'aqua'=>'00FFFF',
        'aquamarine'=>'7FFFD4',
        'azure'=>'F0FFFF',
        'beige'=>'F5F5DC',
        'bisque'=>'FFE4C4',
        'black'=>'000000',
        'blanchedalmond '=>'FFEBCD',
        'blue'=>'0000FF',
        'blueviolet'=>'8A2BE2',
        'brown'=>'A52A2A',
        'burlywood'=>'DEB887',
        'cadetblue'=>'5F9EA0',
        'chartreuse'=>'7FFF00',
        'chocolate'=>'D2691E',
        'coral'=>'FF7F50',
        'cornflowerblue'=>'6495ED',
        'cornsilk'=>'FFF8DC',
        'crimson'=>'DC143C',
        'cyan'=>'00FFFF',
        'darkblue'=>'00008B',
        'darkcyan'=>'008B8B',
        'darkgoldenrod'=>'B8860B',
        'darkgray'=>'A9A9A9',
        'darkgreen'=>'006400',
        'darkgrey'=>'A9A9A9',
        'darkkhaki'=>'BDB76B',
        'darkmagenta'=>'8B008B',
        'darkolivegreen'=>'556B2F',
        'darkorange'=>'FF8C00',
        'darkorchid'=>'9932CC',
        'darkred'=>'8B0000',
        'darksalmon'=>'E9967A',
        'darkseagreen'=>'8FBC8F',
        'darkslateblue'=>'483D8B',
        'darkslategray'=>'2F4F4F',
        'darkslategrey'=>'2F4F4F',
        'darkturquoise'=>'00CED1',
        'darkviolet'=>'9400D3',
        'deeppink'=>'FF1493',
        'deepskyblue'=>'00BFFF',
        'dimgray'=>'696969',
        'dimgrey'=>'696969',
        'dodgerblue'=>'1E90FF',
        'firebrick'=>'B22222',
        'floralwhite'=>'FFFAF0',
        'forestgreen'=>'228B22',
        'fuchsia'=>'FF00FF',
        'gainsboro'=>'DCDCDC',
        'ghostwhite'=>'F8F8FF',
        'gold'=>'FFD700',
        'goldenrod'=>'DAA520',
        'gray'=>'808080',
        'green'=>'008000',
        'greenyellow'=>'ADFF2F',
        'grey'=>'808080',
        'honeydew'=>'F0FFF0',
        'hotpink'=>'FF69B4',
        'indianred'=>'CD5C5C',
        'indigo'=>'4B0082',
        'ivory'=>'FFFFF0',
        'khaki'=>'F0E68C',
        'lavender'=>'E6E6FA',
        'lavenderblush'=>'FFF0F5',
        'lawngreen'=>'7CFC00',
        'lemonchiffon'=>'FFFACD',
        'lightblue'=>'ADD8E6',
        'lightcoral'=>'F08080',
        'lightcyan'=>'E0FFFF',
        'lightgoldenrodyellow'=>'FAFAD2',
        'lightgray'=>'D3D3D3',
        'lightgreen'=>'90EE90',
        'lightgrey'=>'D3D3D3',
        'lightpink'=>'FFB6C1',
        'lightsalmon'=>'FFA07A',
        'lightseagreen'=>'20B2AA',
        'lightskyblue'=>'87CEFA',
        'lightslategray'=>'778899',
        'lightslategrey'=>'778899',
        'lightsteelblue'=>'B0C4DE',
        'lightyellow'=>'FFFFE0',
        'lime'=>'00FF00',
        'limegreen'=>'32CD32',
        'linen'=>'FAF0E6',
        'magenta'=>'FF00FF',
        'maroon'=>'800000',
        'mediumaquamarine'=>'66CDAA',
        'mediumblue'=>'0000CD',
        'mediumorchid'=>'BA55D3',
        'mediumpurple'=>'9370D0',
        'mediumseagreen'=>'3CB371',
        'mediumslateblue'=>'7B68EE',
        'mediumspringgreen'=>'00FA9A',
        'mediumturquoise'=>'48D1CC',
        'mediumvioletred'=>'C71585',
        'midnightblue'=>'191970',
        'mintcream'=>'F5FFFA',
        'mistyrose'=>'FFE4E1',
        'moccasin'=>'FFE4B5',
        'navajowhite'=>'FFDEAD',
        'navy'=>'000080',
        'oldlace'=>'FDF5E6',
        'olive'=>'808000',
        'olivedrab'=>'6B8E23',
        'orange'=>'FFA500',
        'orangered'=>'FF4500',
        'orchid'=>'DA70D6',
        'palegoldenrod'=>'EEE8AA',
        'palegreen'=>'98FB98',
        'paleturquoise'=>'AFEEEE',
        'palevioletred'=>'DB7093',
        'papayawhip'=>'FFEFD5',
        'peachpuff'=>'FFDAB9',
        'peru'=>'CD853F',
        'pink'=>'FFC0CB',
        'plum'=>'DDA0DD',
        'powderblue'=>'B0E0E6',
        'purple'=>'800080',
        'red'=>'FF0000',
        'rosybrown'=>'BC8F8F',
        'royalblue'=>'4169E1',
        'saddlebrown'=>'8B4513',
        'salmon'=>'FA8072',
        'sandybrown'=>'F4A460',
        'seagreen'=>'2E8B57',
        'seashell'=>'FFF5EE',
        'sienna'=>'A0522D',
        'silver'=>'C0C0C0',
        'skyblue'=>'87CEEB',
        'slateblue'=>'6A5ACD',
        'slategray'=>'708090',
        'slategrey'=>'708090',
        'snow'=>'FFFAFA',
        'springgreen'=>'00FF7F',
        'steelblue'=>'4682B4',
        'tan'=>'D2B48C',
        'teal'=>'008080',
        'thistle'=>'D8BFD8',
        'tomato'=>'FF6347',
        'turquoise'=>'40E0D0',
        'violet'=>'EE82EE',
        'wheat'=>'F5DEB3',
        'white'=>'FFFFFF',
        'whitesmoke'=>'F5F5F5',
        'yellow'=>'FFFF00',
        'yellowgreen'=>'9ACD32');

    $color_name = strtolower($color_name);
    if (isset($colors[$color_name]))
    {
        return ('#' . $colors[$color_name]);
    }
    else
    {
        return ($color_name);
    }
}
	
	public static function getIp(){
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
				$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
    

}



