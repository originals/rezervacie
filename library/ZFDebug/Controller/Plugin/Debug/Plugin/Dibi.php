<?php
/**
 * ZFDebug Zend Additions
 *
 * @category   ZFDebug
 * @package    ZFDebug_Controller
 * @subpackage Plugins
 * @copyright  Copyright (c) 2008-2009 ZF Debug Bar Team (http://code.google.com/p/zfdebug)
 * @license    http://code.google.com/p/zfdebug/wiki/License     New BSD License
 * @version    $Id$
 */

/**
 * @category   ZFDebug
 * @package    ZFDebug_Controller
 * @subpackage Plugins
 * @copyright  Copyright (c) 2008-2009 ZF Debug Bar Team (http://code.google.com/p/zfdebug)
 * @license    http://code.google.com/p/zfdebug/wiki/License     New BSD License
 */
class ZFDebug_Controller_Plugin_Debug_Plugin_Dibi 
    extends ZFDebug_Controller_Plugin_Debug_Plugin 
    implements ZFDebug_Controller_Plugin_Debug_Plugin_Interface
{

    /**
     * Contains plugin identifier name
     *
     * @var string
     */
    protected $_identifier = 'database';

    /**
     * @var array
     */
    protected $_db = array();
    
    protected $_explain = false;

    /**
     * Create ZFDebug_Controller_Plugin_Debug_Plugin_Variables
     *
     * @param Zend_Db_Adapter_Abstract|array $adapters
     * @return void
     */
    public function __construct(array $options = array())
    {
        
        if ($options['adapter'] instanceof DibiConnection ) {
            $this->_db[0] = $options['adapter'];
            
        }
        
        
        if (isset($options['explain'])) {            
            $this->_explain = (bool)$options['explain'];
        }
    }

    /**
     * Gets identifier for this plugin
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->_identifier;
    }
    
    /**
     * Returns the base64 encoded icon
     *
     * @return string
     **/
    public function getIconData()
    {
        return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAEYSURBVBgZBcHPio5hGAfg6/2+R980k6wmJgsJ5U/ZOAqbSc2GnXOwUg7BESgLUeIQ1GSjLFnMwsKGGg1qxJRmPM97/1zXFAAAAEADdlfZzr26miup2svnelq7d2aYgt3rebl585wN6+K3I1/9fJe7O/uIePP2SypJkiRJ0vMhr55FLCA3zgIAOK9uQ4MS361ZOSX+OrTvkgINSjS/HIvhjxNNFGgQsbSmabohKDNoUGLohsls6BaiQIMSs2FYmnXdUsygQYmumy3Nhi6igwalDEOJEjPKP7CA2aFNK8Bkyy3fdNCg7r9/fW3jgpVJbDmy5+PB2IYp4MXFelQ7izPrhkPHB+P5/PjhD5gCgCenx+VR/dODEwD+A3T7nqbxwf1HAAAAAElFTkSuQmCC';
    }

    /**
     * Gets menu tab for the Debugbar
     *
     * @return string
     */
    public function getTab()
    {
       
        
        $log=Zend_Registry::isRegistered('DibiLog') ? Zend_Registry::get('DibiLog') : array();
        $t=0;
        foreach ($log as $l) {
            $t+=$l->time;
        }
        
            $adapterInfo[] = count($log) . ' in ' 
                           . round($t*1000, 2) . ' ms';
        
        $html = implode(' / ', $adapterInfo);

        return $html;
    }

    /**
     * Gets content panel for the Debugbar
     *
     * @return string
     */
    public function getPanel()
    {
        
        $html = '<h4>Database queries';
        
        // @TODO: This is always on?
        /*if (Zend_Db_Table_Abstract::getDefaultMetadataCache()) {
            $html .= ' – Metadata cache ENABLED';
        } else {
            $html .= ' – Metadata cache DISABLED';
        }*/
        $html .= '</h4>';

        return $html . $this->getProfile();
    }
    
    public function getProfile()
    {
        
        $log=Zend_Registry::isRegistered('DibiLog') ? Zend_Registry::get('DibiLog') : array();
        $t=0;
            
                $queries .='<table cellspacing="0" cellpadding="0" width="100%">';
                foreach ($log as $k=>$l) {
                    $queries .= "<tr>\n<td style='text-align:right;padding-right:2em;' nowrap>\n" 
                           . sprintf('%0.2f', $l->time*1000) 
                           . "ms</td>\n<td>";
                    
                    $queries .= nl2br(htmlspecialchars($l->sql));
                    $queries .= "</td>"
                              . "<td>Rows: ".$l->count;
                    $queries .= "</td>\n</tr>\n";
                }
                $queries .= "</table>\n";
            
        
        return $queries;
    }

    // For adding quotes to query params
    protected function _addQuotes(&$value, $key) 
    {
        $value = "'" . $value . "'";
    }
    
    
    
    
    static function dibiLogger($evt){
        
        $log=Zend_Registry::isRegistered('DibiLog') ? Zend_Registry::get('DibiLog') : array();
        $log[]=$evt;
        Zend_Registry::set('DibiLog',$log);
        
    }
    
    
}