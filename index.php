<?php


// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));





// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/app'));

// Define application environment
defined('APPLICATION_ENV') 
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));


defined('LIBRARY_PATH')
    || define('LIBRARY_PATH', realpath(dirname(__FILE__) . '/library'));





// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(LIBRARY_PATH),
    "."
)));
 
define('PUBLIC_PATH',realpath(dirname(__FILE__)));

/** Zend_Application */
require_once 'Zend/Application.php';

require_once 'Zend/Config/Ini.php';
require_once 'Zend/Debug.php';

//ini_set('mongo.native_long', 1);

//ini_set('display_errors',1);
//error_reporting(E_ALL);

$applicationConfig=new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini',APPLICATION_ENV,array('allowModifications'=>true));




$application = new Zend_Application(null,$applicationConfig->toArray());



require_once LIBRARY_PATH.'/utils.php';



$application->bootstrap()
            ->run();