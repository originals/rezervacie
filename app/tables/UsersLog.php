<?php

class Table_UsersLog extends Matj_Bootstrap3_Table {
	
	function init () {
		$this->setOptions(array ( 
			'sortDefault' => 'created',
			'autoHeaders' => true,
                        'sortDefaultDesc'=>true,
                        'translatePrefix' => 'user.',
		));
		 
//Zend_Debug::dump($this->_options);exit;
                
                $this->addColumn(array ( 
			'column' => 'created', 
			'sort' => 'l.created',
                        'plugins'=>array('dateTime','filterDateFromTo'=>array('column'=>'l.created'))
                      
		));
		
                
		$this->addColumn(array ( 
			'column' => 'login', 
			'sort' => 'login' ,
                        'header'=>'Login',
			'width' => '60' ,
			'plugins'=>array('filterText')
		));
		
                
		
		$this->addColumn(array ( 
			'column' => 'name', 
			'sort' => 'name',
                        'plugins'=>array('filterText'=>array('column'=>'l.created'))
                      
		));
		
		$this->addColumn(array ( 
			'column' => 'group_name', 
			'sort' => 'group_name',
                        'plugins'=>array('filterText'=>array('column'=>'g.name'))
		));
		/*
		$this->addColumn(array ( 
			'column' => 'browser', 
			'sort' => 'browser',
                        'plugins'=>array('filterText','short')
		));
		*/
		$this->addColumn(array ( 
			'column' => 'ip', 
			'sort' => 'ip',
                        'plugins'=>array('filterSubmitReset')
		));
		
        }
}