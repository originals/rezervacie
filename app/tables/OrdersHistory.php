<?php

class Table_OrdersHistory extends Matj_Bootstrap3_Table {
	
        /**
         *
         * @var Model_Order
         */
        public $order;
    
    
	function init () {
		$this->setOptions(array ( 
			'sortDefault' => 'created',
			'autoHeaders' => true,
                        'sortDefaultDesc'=>true,
                        'translatePrefix' => 'order.',
		));
		 
//Zend_Debug::dump($this->_options);exit;
                
                $this->addColumn(array ( 
			'column' => 'created', 
			'width' => '175',
                        'sort' => 'h.created',
                        'plugins'=>array('filterDateFromTo'=>array('column'=>'h.created'),'DateTime'=>array('format'=>'d.m.y H:i'))
		));
                $this->addColumn(array ( 
			'column' => 'time', 
			'sort' => 'time',
			'width' => '175',
                        'plugins'=>array('filterDateFromTo',
                             'DateTime'=>array('format'=>'d.m.y H:i'),'parse'=>array('text'=>'{_value} / {loadtime} min.'),
                             'link' => array ( 
						'url' => array ( 
							'action' => 'edit' ,
                                                     'controller' => 'calendar' 
						), 
						'columnParams' => array ( 
							'id' => 'order_id',
							
						), 
						'resetSort' => true,
                                                //'text'=>'Upraviť',
                                                //'class'=>'btn btn-primary btn-xs'
                            ),
                            )
		));
		
                $this->addColumn(array ( 
			'column' => 'customer_name', 
			'sort' => 'customer_name',
                        'plugins'=>array('filterText'=>array('column'=>'CONCAT(c.name," ",c.shortname)'),'parse'=>array('text'=>'<span class="label" style="background:{customer_color};color:{customer_textcolor};">{customer_shortname}</span>'))
                ));
		
		
		
                $this->addColumn(array ( 
			'column' => 'carcode', 
			'sort' => 'carcode',
                        'plugins'=>array('filterText','parse'=>array('text'=>'<img src="/assets/images/{cartype}.png" /> {_value} '))
		));
		
                $this->addColumn(array ( 
			'column' => 'user_name', 
			'sort' => 'u.name',
                        'header'=>'Používateľ',
                        'plugins'=>array('filterText'=>array('column'=>'u.name'))
		));
		
		$this->addColumn(array ( 
			'column' => 'description', 
			'header'=>'Zmeny',
                        'sort' => 'description',
                        'plugins'=>array('filterSubmitReset')
		));
		
		
        }
}