<?php

class Table_Customers extends Matj_Bootstrap3_Table {
	
        /**
         *
         * @var Model_Order
         */
        public $order;
    
    
	function init () {
		$this->setOptions(array ( 
			'sortDefault' => 'company',
			'autoHeaders' => true,
                        'sortDefaultDesc'=>true,
                        'translatePrefix' => 'user.',
		));
		 
//Zend_Debug::dump($this->_options);exit;
                
                
		$this->addColumn(array ( 
			'column' => 'companyid', 
			'sort' => 'companyid' ,
                        'header'=>'IČO',
			'width' => '60' ,
			'plugins'=>array('filterText')
		));
		
                
		
		
                $this->addColumn(array ( 
			'column' => 'company', 
			'sort' => 'company',
                        'plugins'=>array('filterText'=>array('column'=>'u.company'))
                ));
		
		
                $this->addColumn(array ( 
			'column' => 'city', 
			'sort' => 'city',
                        'plugins'=>array('filterText')
                ));
		
                $this->addColumn(array ( 
			'column' => 'email', 
			'sort' => 'email',
                        'plugins'=>array('filterText')
                ));
		
		
                $this->addColumn(array ( 
			'column' => 'website', 
			'sort' => 'website',
                        'plugins'=>array('filterText')
                ));
		
		
                
		
		
		 $this->addColumn(
			array ( 
				'column' => 'edit', 
				'sort' => false,
                                'header'=>'',
				'plugins' => array ( 
					'link' => array ( 
						'url' => array ( 
							'action' => 'view' 
						), 
						'columnParams' => array ( 
							'id' => 'id' 
						), 
						'resetSort' => true,
                                                'text'=>'Zobraziť',
                                                'class'=>'btn btn-success btn-xs'
					),
					'filterSubmitReset'=>array('name'=>'submit'),
				) 
			)
		);
        }
}