<?php

class Table_Objects extends Matj_Bootstrap3_Table {
	
	/**
	 *
	 * @var Model_Center
	 */
	public $center;    
    
	function init () {
		$this->setOptions(array ( 
			'sortDefault' => 'time',
			'autoHeaders' => true,
                        'sortDefaultDesc'=>true,
                        'translatePrefix' => 'object.',
		));
		 
//Zend_Debug::dump($this->_options);exit;
                
                /*
		$this->addColumn(array ( 
			'column' => 'id', 
			'sort' => 'id' ,
                        'header'=>'ID',
			'width' => '60' ,
			'plugins'=>array('filterText')
		));
		*/
                
		
		
		$this->addColumn(array ( 
			'column' => 'name', 
			'sort' => 'name',
                        'plugins'=>array('filterText'=>array('column'=>'name'))
    ));
                
    
                
                
		
		
		 $this->addColumn(
			array ( 
				'column' => 'edit', 
				'sort' => false,
                                'header'=>'',
				'plugins' => array ( 
					'link' => array ( 
						'url' => array ( 
							'action' => 'addobject' 
						), 
						'columnParams' => array ( 
							'hash' => 'hash',
							'id'=>'id'
						), 
						'resetSort' => true,
                                                'text'=>'Upraviť',
                                                'class'=>'btn btn-primary btn-xs'
					),
					'filterSubmitReset'=>array('name'=>'submit'),
				) 
			)
		);
        }
        
    
}