<?php

class Table_Centers extends Matj_Bootstrap3_Table {
	
	/**
	 *
	 * @var Model_Center
	 */
	public $center;    
    
	function init () {
		$this->setOptions(array ( 
			'sortDefault' => 'time',
			'autoHeaders' => true,
                        'sortDefaultDesc'=>true,
                        'translatePrefix' => 'center.',
		));
		 
Zend_Debug::dump($this->_options);exit;
                
                /*
		$this->addColumn(array ( 
			'column' => 'id', 
			'sort' => 'id' ,
                        'header'=>'ID',
			'width' => '60' ,
			'plugins'=>array('filterText')
		));
		*/
                
		
		$this->addColumn(array ( 
			'column' => 'time', 
			'sort' => 'time',
                        'plugins'=>array('filterDateFromTo','DateTime'=>array('format'=>'d.m.y H:i'),'parse'=>array('text'=>'{_value} <small class="label label-info">{loadtime} min.</small>'))
		));
		$this->addColumn(array ( 
			'column' => 'ramp_name', 
			'sort' => 'ramp_name',
                        'plugins'=>array('filterText'=>array('column'=>'r.name'))
                ));
                
                
                $this->order=new Model_Order;
                $options=array();
                foreach($this->order->getStatuses() as $s)
                    $options[$s->code]=$s->name;
                
                
		$this->addColumn(array ( 
			'column' => 'status', 
			'sort' => 's.rank',
                        'plugins'=>array('filterSelect'=>array('column'=>'o.status','options'=>$options),
                                         'parse'=>array('text'=>'<span class="fc-history label" style="background:{status_color};color:{status_textcolor};">{status_name}</span>'))
                ));
		
                $this->addColumn(array ( 
			'column' => 'customer_name', 
			'sort' => 'customer_name',
                        'plugins'=>array('filterText'=>array('column'=>'CONCAT(c.name," ",c.shortname)'),'parse'=>array('text'=>'<span class="label" style="background:{color};color:{textcolor};" title="{customer_name}">{customer_shortname}</span>'))
                ));
		
		
		
                $this->addColumn(array ( 
			'column' => 'carcode', 
			'sort' => 'carcode',
                        'plugins'=>array('filterText','parse'=>array('text'=>'<img src="/assets/images/{cartype}.png" /> {_value} '))
		));
		
		$this->addColumn(array ( 
			'column' => 'load', 
			'sort' => false,
                        'plugins'=>array()
		     
		));
		
		 $this->addColumn(
			array ( 
				'column' => 'edit', 
				'sort' => false,
                                'header'=>'',
				'plugins' => array ( 
					'link' => array ( 
						'url' => array ( 
							'action' => 'edit' 
						), 
						'columnParams' => array ( 
							'id' => 'id' 
						), 
						'resetSort' => true,
                                                'text'=>'Upraviť',
                                                'class'=>'btn btn-primary btn-xs'
					),
					'filterSubmitReset'=>array('name'=>'submit'),
				) 
			)
		);
        }
        
        
        function setData($data){
            $loadColumn=$this->getColumn('load');
            
            foreach($data as $k=>$d){
            
                if($loadColumn)
                    $d->load=$d->getLoadTimeString();
                
                $data[$k]=$d->getTableData();
            }
            parent::setData($data);
        }
}