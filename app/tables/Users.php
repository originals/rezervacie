<?php

class Table_Users extends Matj_Bootstrap3_Table {
	
	function init () {
		$this->setOptions(array ( 
			'sortDefault' => 'created',
			'autoHeaders' => true,
                        'sortDefaultDesc'=>true,
                        'translatePrefix' => 'user.',
		));
		 
//Zend_Debug::dump($this->_options);exit;
                
                
		$this->addColumn(array ( 
			'column' => 'login', 
			'sort' => 'login' ,
                        'header'=>'Login',
			'width' => '60' ,
			'plugins'=>array('filterText')
		));
		
                
		$this->addColumn(array ( 
			'column' => 'name', 
			'sort' => 'name',
                        'plugins'=>array('filterText'=>array('column'=>'u.created'))
                      
		));
		
		$this->addColumn(array ( 
			'column' => 'email', 
			'sort' => 'email',
                        'plugins'=>array('filterText')
		));
		
		
		
		$this->addColumn(array ( 
			'column' => 'group_name', 
			'sort' => 'group_name',
                        'plugins'=>array('filterText'=>array('column'=>'g.name'))
		));
		
		$this->addColumn(array ( 
			'column' => 'created', 
			'sort' => 'created',
                        'plugins'=>array('dateTime')
                      
		));
		
		 $this->addColumn(
			array ( 
				'column' => 'edit', 
				'sort' => false,
                                'header'=>'',
				'plugins' => array ( 
					'link' => array ( 
						'url' => array ( 
							'action' => 'edit' 
						), 
						'columnParams' => array ( 
							'id' => 'id' 
						), 
						'resetSort' => true,
                                                'text'=>'Upraviť',
                                                'class'=>'btn btn-primary btn-xs'
					),
					'filterSubmitReset'=>array('name'=>'submit'),
				) 
			)
		);
        }
}