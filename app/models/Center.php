<?php

class Model_Center extends Matj_Dibi_Model{
    
    protected $tableName = "centers";
    protected $createdColumn=true;
    protected $modifiedColumn=true;
    
    protected $logChanges=true;
    
       
    function fetchAll($options=array()){
        $query=array();    
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS ce.* FROM ['.$this->tableName.'] ce';
        //$query[] = 'LEFT JOIN [groups] g ON g.id = u.group_id';        
        //$options["group"]=array('o.id');        
        // $options["test"]=1;
        $query=$this->buildQuery($query,$options);
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        $data=array();        
        
        foreach($result as $k=>$r){
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }    
        }        
        return $data;
    }
  
    function fetchOpeningHours($options=array()){
        $query=array();    
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS h.* FROM [hours] h';
        $query[] = 'LEFT JOIN [hours_interval] i ON i.id = h.interval_id';        
        $options["group"]=array('h.id');  
        $options["order"]=array("h.wday,h.hour");
        //$options["test"]=1;
        $query=$this->buildQuery($query,$options);
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        $data=array();        
        
        foreach($result as $k=>$r){
            if(!empty($options["output"]) && $options["output"]=="array"){
              //Zend_Debug::dump($r);
              if($this->unit==60){
                $r->hour=date("H:i:s",strtotime($r->hour));
                $r->divider=1;
                $data[date("H:i",strtotime($r->hour))]=($r->toArray());
              }
              if($this->unit<60){
                //$data[date("H:i",strtotime($r->hour))]=($r->toArray());
                $div=round(60/$this->unit);
                //Zend_Debug::dump($sec);
                $sec=$this->unit;
                $minutes=0;
                $hour=date("H",strtotime($r->hour));
                for($i=0;$i<$div;$i++){  
                  $key=$hour.":".$minutes;
                  $r->divider=$div;
                  $r->hour=date("H:i",strtotime($hour.":".$minutes.":00")); 
                  $data[$r->hour]=($r->toArray());
                  $minutes+=$sec;
                }              
               //exit;
                //$data[date("H:i",strtotime($r->hour))]=($r->toArray());
              }
              
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }    
        } 
        ksort($data);
        //Zend_Debug::dump($data);exit;
        return $data;
    }
    
  
    function getCenterByHash($data){        
        if(!empty($data["center"])){
            $id=$data["center"];
            return $this->fetchOne(array('hash'=>$id));
        }    
        
    }
  
    function fetchObjects($options=array()){
        $query=array();        
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS o.* FROM [objects] o';  
        $query[]=' WHERE o.center_id="'.$this->id.'" ';
        $optons["group"]=array('o.id');
        
        $query=$this->buildQuery($query,$options);          
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }       
        
        $data=array();        
        
        foreach($result as $k=>$r){ 
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }
            
            
        }
        //zend_Debug::dump($data);
        return $data;
    }
  
    function access(Model_User $user,$id) {
        //Zend_Debug::dump($id);exit;
        if($user->getGroupId()>0 && $user->getGroupId()<=3)
          $return=true;        
        
        $query=array();        
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS c.* FROM [clients] c';        
        $query[] = 'LEFT JOIN [users_joins] j ON j.client_id = c.id';
        //$options["test"]=1;
        $query[]=' WHERE c.id="'.$this->client_id.'" AND j.user_id="'.$user->id.'" ';
      
        //$query[]=' LIMIT 1';
        
        $query=$this->buildQuery($query,$options);       
        
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }  
        $data=array();        
        
        foreach($result as $k=>$r){
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data=new $class($r->toArray());
            }
            
            
        }
        
        return $data;
        
    }
    
    
    
}