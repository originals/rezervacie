<?php

class Model_UserAccess extends Matj_Dibi_Model{
    
    protected $tableName = "users_access";
    protected $modifiedColumn=true;   
    
   
    function fetchAll($options=array()){
        $query=array();
        
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS u.*,c.*,a.* FROM [users_access] a';
        $query[] = 'LEFT JOIN [users] a ON a.user_id = u.id';
        $query[] = 'LEFT JOIN [clients] c ON a.client_id = j.id';
        
        //$options["group"]=array('o.id');
        
       // $options["test"]=1;
        
        $query=$this->buildQuery($query,$options);
        
        
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        
        
        $data=array();
        
        
        foreach($result as $k=>$r){
            
            
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }
            
            
        }
        
        return $data;
    }
   
    
    function getApiData() {
        $data=parent::getApiData();
        $data["access_id"]=$data["id"];
        
        return $data;
    }
}