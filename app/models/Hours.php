<?php

class Model_Hours extends Matj_Dibi_Model{
    
    protected $tableName = "hours";
    protected $createdColumn=true;
    protected $modifiedColumn=true;
    
    protected $logChanges=true; 
    
    function fetchAll($options=array()){
        $query=array();
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS h.* FROM ['.$this->tableName.'] h'; 
        $query[] = 'LEFT JOIN [hours_interval] i ON i.id = h.interval_id';
        
        $query=$this->buildQuery($query,$options);
      
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }    
        
        $data=array();
      
        foreach($result as $k=>$r){
            
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }
        }        
        return $data;
    }

    
    
    function getApiData() {
        $data=parent::getApiData();
//         $data["communication_id"]=$data["id"];
//         $data["date_sk"]=Matj_Format::getDate($data["date"], "d.m.Y");
//         $data["time"]=Matj_Format::getDate($data["date"], "H:i");
//         $data["title"]=$data["customer_company"];
//         $data["text_short"]=Matj_String::shorten($data["text"],100,array('noHtml'=>true));
//         $data["start"]=$data["date"];
//         $data["end"]=Matj_Util::addDate($data["date"],array('hour'=>1),'Y-m-d H:i:s');
        
        return $data;
    }
  
    
}