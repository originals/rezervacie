<?php

class Model_Reservationtime extends Matj_Dibi_Model{
    
    protected $tableName = "reservations_times";
    
    protected $createdColumn=true;
    protected $modifiedColumn=true;
    
    protected $logChanges=true; 
       
   function getApiData() {
        $data=parent::getApiData();      
        return $data;
   }
  
  
  
     function fetchAll($options=array()){
        $query=array();
        
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS t.*,r.first_name,r.last_name,r.email,r.phone,r.color FROM ['.$this->tableName.'] t';
        $query[] = 'LEFT JOIN [reservations] r ON r.id = t.reservations_id';        
                
        $query=$this->buildQuery($query,$options);  
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
            
        $data=array();
        
        foreach($result as $k=>$r){
            
            if(!empty($options["output"]) && $options["output"]=="array"){
                
                $data[$r->object_id][date("H:i",$r->timestamp)]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }      
        }
        return $data;
    }
  
  function getByHash($hash=null){        
        if(!empty($hash)){
            return $this->fetchOne(array('hash'=>$hash));
        }
    }
  
    
    
}