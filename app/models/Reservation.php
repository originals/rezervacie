<?php

class Model_Reservation extends Matj_Dibi_Model{
    
    protected $tableName = "reservations";
    protected $tableNameTimes = "reservations_times";
    
    protected $createdColumn=true;
    protected $modifiedColumn=true;
    
    protected $logChanges=true; 
       
   
    
    function access(Model_User $user, $type) {
        
        if($user->getGroupId()>0 && $user->getGroupId()<=2)
            return true;
        
        
        
        $access=new Model_UserAccess();
        return $access->fetchOne(array('user2_id'=>$this->customer_id,'user_id'=>$user->getId()));
        
        
    }
    
    
    
    
    
    
    function fetchAll($options=array()){
        $query=array();
        
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS r.*,MIN(t.timestamp) as start, MAX(t.timestamp) as end,c.name as center_name, o.name as object_name FROM ['.$this->tableName.'] r';
        $query[] = 'LEFT JOIN [reservations_times] t ON t.reservations_id = r.id';        
        $query[] = 'LEFT JOIN [centers] c ON c.id = r.center_id';
        $query[] = 'LEFT JOIN [objects] o ON o.id = r.object_id';
        $query=$this->buildQuery($query,$options);  
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
            
        $data=array();
        
        foreach($result as $k=>$r){
            
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }      
        }
        return $data;
    }
  
    function fetchTimeStamp($options=array()){
        $query=array();
        
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS t.*,u.first_name,u.last_name,u.email,u.phone,r.status FROM ['.$this->tableNameTimes.'] t';
        $query[] = 'LEFT JOIN ['.$this->tableName.'] r ON t.reservations_id = r.id';     
        $query[] = 'LEFT JOIN [users] u ON u.id = r.user_id'; 
      
             
        $query=$this->buildQuery($query,$options);  
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
            
        $data=$result->fetch();
        if($data){
            $this->setData($data);
            return $this;
        }
        return false;
    }
  
  

    function getReservationBySession($data=array()){        
        if(!empty($data["session"])){
            $id=$data["session"];
            return $this->fetchOne(array('session'=>$id));
        }
        
    }
  
    function getByHash($hash=null){        
        if(!empty($hash)){
            return $this->fetchOne(array('hash'=>$hash));
        }
    }
  
    function saveTimestamp($params){
        $orderTime=new Model_Reservationtime();
        $orderTime->setData(array(
          'session'=>$params["session"],
          'client_id'=>$params["client"],
          'center_id'=>$params["center"],
          'object_id'=>$params["object"],
          'timestamp'=>$params["timestamp"],
          'date'=>$params["date"],
          'reservations_id'=>$params["reservation_id"]
        ));
        return $orderTime->save();      
    }
    
    
    function getApiData() {
        $data=parent::getApiData();
        $data["reservation_id"]=$data["id"];
        $data["date_sk"]=Matj_Format::getDate($data["date"], "d.m.Y");
        $data["time"]=Matj_Format::getDate($data["date"], "H:i");
        $data["title"]=$data["customer_company"];
        $data["text_short"]=Matj_String::shorten($data["text"],100,array('noHtml'=>true));
        $data["start"]=$data["date"];
        $data["end"]=Matj_Util::addDate($data["date"],array('hour'=>1),'Y-m-d H:i:s');
        
        return $data;
    }
  
    
    
}