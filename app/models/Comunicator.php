<?php

class Model_Comunicator extends Matj_Dibi_Model{
	
	protected $tableName = "communications";
    protected $createdColumn=true;
    protected $modifiedColumn=true;
    
    protected $logChanges=true;

	/**
	 * @return Matj_Email
	 */
	function getMailInstance(){
		return new Matj_Email(true);
	}



	  
  public function sendEmail(array $options){

		$mail=$this->getMailInstance();
    $mailOptions["to"]=$options["to"];
		
		$mailOptions["from"]=$options["from"];
		$mailOptions["from_name"]=$options["fromName"];
		$mailOptions["subject"]=$options["subject"];

		if(!empty($options["body"])){
			$mailOptions["body"]=Matj_Util::parseParams($options["body"],$options["data"]);                        
		}
		
		if(empty($options["altBody"])){
			$mailOptions["altBody"]=strip_tags(Matj_Util::parseParams($options["body"],$options["data"]));	
		}
		
		if(!empty($options["template"])){
			$file=APPLICATION_PATH.DIRECTORY_SEPARATOR.'layouts'.DIRECTORY_SEPARATOR.'email'.DIRECTORY_SEPARATOR.$options["template"].".phtml";
			if(!file_exists($file))
				throw new Exception("Mail template ".$file." not exists");

			$mailOptions["body"]=$this->parseTemplate($file,array("data"=>$options["data"],"body"=>$mailOptions["body"],"title"=>$options["subject"]));
		
    }
		
		
		//Zend_Debug::dump($mailOptions);exit;
		//echo $mailOptions["body"];exit;
              
			if($mail->sendEmail($mailOptions)){			
				return true;
			}else{
				return false;
			}
		
	}
		


	function parseTemplate($file,$data=array()){
		ob_start();
    include($file);
		return ob_get_clean(); // filter output
	}
	
	


}