<?php

class Model_Interval extends Matj_Dibi_Model{
    
    protected $tableName = "hours_interval";
    protected $createdColumn=true;
    protected $modifiedColumn=true;
    
    protected $logChanges=true;
    
       
   function _prepareSaveData($data = null) {
        $data=parent::_prepareSaveData($data);
        
        
        if(key_exists('main',$data) && empty($data["main"])){
            $data["main"]=1;
        }   
        
        $data["start"]=date("Y-m-d",strtotime($data["start"]));
        $data["end"]=date("Y-m-d",strtotime($data["end"]));        
        
        return $data;
    }
  
    function setDataFromForm($data){
        
       if(key_exists('main',$data) && empty($data["main"])){
            $data["main"]=1;
        }   
        
        $data["start"]=date("Y-m-d",strtotime($data["start"]));
        $data["end"]=date("Y-m-d",strtotime($data["end"]));
        
        
        return $this->setData($data);
    }
    
    function access(Model_User $user) {  
        $valid=false;
        if($user->getGroupId()>0 && $user->getGroupId()<=3) $valid=true;
          //Zend_Debug::dump($user->getGroupId());
        if($valid){
          $access=new Model_UserAccess();
          $access->fetchOne(array('client_id'=>$this->client_id,'user_id'=>$user->id));
          if(!$access->isEmpty()){
            $valid=true;
          }
          else{
            $valid=false;
          }
        }
      //Zend_Debug::dump($valid);exit;
        return $valid;
    }
    
    
   
    
    
    
    function fetchAll($options=array()){
        $query=array();
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS i.* FROM ['.$this->tableName.'] i';
        
        
        
        
        $query=$this->buildQuery($query,$options);
        
        
        
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        
        
        $data=array();
        
        
        foreach($result as $k=>$r){
            
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }
            
            
        }
        
        return $data;
    }

    
    
    function getApiData() {
        $data=parent::getApiData();
//         $data["communication_id"]=$data["id"];
//         $data["date_sk"]=Matj_Format::getDate($data["date"], "d.m.Y");
//         $data["time"]=Matj_Format::getDate($data["date"], "H:i");
//         $data["title"]=$data["customer_company"];
//         $data["text_short"]=Matj_String::shorten($data["text"],100,array('noHtml'=>true));
//         $data["start"]=$data["date"];
//         $data["end"]=Matj_Util::addDate($data["date"],array('hour'=>1),'Y-m-d H:i:s');
        
        return $data;
    }
  
  
    function getDays($options=array()){
        $hours=new Model_Hours();
        $saved=$hours->fetchAll(array("where"=>'h.interval_id="'.$this->id.'"'));
        $h=array();
        foreach($saved as $k=>$v){
          $h[$v->wday][date("H:i",strtotime($v->hour))]=$v->price;          
        }
        return $h;
    }
    
}