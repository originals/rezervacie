<?php

class Model_Object extends Matj_Dibi_Model{
    
    protected $tableName = "objects";
    protected $createdColumn=true;
    protected $modifiedColumn=true;
    
    protected $logChanges=true;
    
       
    function fetchAll($options=array()){
        $query=array();    
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS o.* FROM ['.$this->tableName.'] o';
        //$query[] = 'LEFT JOIN [groups] g ON g.id = u.group_id';        
        //$options["group"]=array('o.id');        
        // $options["test"]=1;
        $query=$this->buildQuery($query,$options);
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        $data=array();        
        
        foreach($result as $k=>$r){
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }    
        }        
        return $data;
    }
  
    
  
    function getByHash($data){        
        if(!empty($data["object"])){
            $id=$data["object"];
            return $this->fetchOne(array('hash'=>$id));
        }    
        
    }    
    
    
    
}