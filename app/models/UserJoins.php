<?php

class Model_UserJoins extends Matj_Dibi_Model{
    
    protected $tableName = "users_joins";
    protected $modifiedColumn=true;   
    
   
    function fetchAll($options=array()){
        $query=array();
        
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS u.*,c.*,j.* FROM [users_joins] j';
        $query[] = 'LEFT JOIN [users] j ON j.user_id = u.id';
        $query[] = 'LEFT JOIN [clients] c ON j.client_id = j.id';
        
        //$options["group"]=array('o.id');
        
       // $options["test"]=1;
        
        $query=$this->buildQuery($query,$options);
        
        
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        
        
        $data=array();
        
        
        foreach($result as $k=>$r){
            
            
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }
            
            
        }
        
        return $data;
    }
   
    
    function getApiData() {
        $data=parent::getApiData();
        $data["access_id"]=$data["id"];
        
        return $data;
    }
}