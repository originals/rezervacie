<?php

class Model_Client extends Matj_Dibi_Model{
    
    protected $tableName = "clients";
    protected $createdColumn=true;
    protected $modifiedColumn=true;
    
    protected $logChanges=true;
    
       
    function fetchAll($options=array()){
        $query=array();    
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS c.* FROM ['.$this->tableName.'] c';
        //$query[] = 'LEFT JOIN [groups] g ON g.id = u.group_id';        
        //$options["group"]=array('o.id');        
        // $options["test"]=1;
        $query=$this->buildQuery($query,$options);
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        $data=array();        
        
        foreach($result as $k=>$r){
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }    
        }        
        return $data;
    }
    
  
    function getClientByHash($data){        
        if(!empty($data["client"])){
            $id=$data["client"];
            return $this->fetchOne(array('hash'=>$id));
        }    
        
    }
  
    function fetchCenters($options=array()){
        $query=array();        
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS c.* FROM [centers] c';  
        $query[]=' WHERE c.client_id="'.$this->id.'" ';
        $optons["group"]=array('c.id');
        
        $query=$this->buildQuery($query,$options);          
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }       
        
        $data=array();        
        
        foreach($result as $k=>$r){ 
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }
            
            
        }
        //zend_Debug::dump($data);
        return $data;
    }
  
    
    
    
    
}