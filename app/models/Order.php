<?php

class Model_Order extends Matj_Dibi_Model{
    
    protected $tableName = "orders";
    protected $createdColumn=true;
    protected $modifiedColumn=true;
    
    protected $logChanges=true;
    
    /**
     *
     * @var Model_User
     */
    public $authUser;
    
 
    public function getRamps() {
        $registry=Zend_Registry::getInstance();
        if(!$registry->isRegistered('order_ramps')){
            $statuses=array();
            foreach($this->getAdapter()->fetchAll('SELECT * FROM ramps ORDER BY rank') as $s)
                $statuses[$s->code]=$s;
            
            $registry->set('order_ramps',$statuses);
        }
        return $registry->get('order_ramps');
    }

    
    function getTableData(){
        $data=$this->toArray();
        $data["textcolor"]=$this->get_brightness($this->color)<130 ? '#ffffff' : '#000000';
        $data["status_textcolor"]=$this->get_brightness($this->status_color)<130 ? '#ffffff' : '#000000';
        
        if(empty($data["id"]) && isset($data["_id"]))
            $data["id"]=$data["_id"];
        return $data;
    }
    
    
    function fetchAll($options=array()){
        $query=array();
        
        if(!empty($options["count"])){
            unset($options["limit"]);
            unset($options["group"]);
            $query[] = 'SELECT count(*) FROM ['.$this->tableName.']';
            $query=$this->buildQuery($query,$options);
            return $this->getAdapter()->query($query)->fetchSingle();
        }
        
        
        if(!empty($options["user"]) && $options["user"]->group_id>3){
            $cond='o.status NOT IN ("odisiel","storno","delete")';
            if(is_array($options["where"]))
                $options["where"][]=array($cond);
            else
                $options["where"]=  Matj_Util::joinWheres($options["where"],$cond);
        }
        
        if(is_array($options["where"])){
            $options["where"][]=array('o.status !="delete"');
        }
        else{
            if(!preg_match('/delete/',$options["where"]))
                $options["where"]=  Matj_Util::joinWheres($options["where"],'o.status !="delete"');
        }
        
        
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS o.*,c.name as customer_name,c.shortname as customer_shortname,r.name as ramp_name,s.name as status_name,s.color as status_color,IFNULL(GROUP_CONCAT(oh.description SEPARATOR "|"),"") as comments FROM ['.$this->tableName.'] o';
        $query[] = 'LEFT JOIN [customers] c ON c.id = o.customer_id';
        $query[] = 'LEFT JOIN [ramps] r ON o.ramp = r.code';
        $query[] = 'LEFT JOIN [statuses] s ON o.status = s.code';
        $query[] = 'LEFT JOIN [orders_history] oh ON ( o.id = oh.order_id AND oh.description<>"" )';
        
        $options["group"]=array('o.id');
        
       // $options["test"]=1;
        
        $query=$this->buildQuery($query,$options);
        
        
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        
        
        $data=array();
        
        
        foreach($result as $k=>$r){
            
            
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }
            
            
        }
        
        return $data;
    }
    
    static public function get_brightness($hex) {
        // returns brightness value from 0 to 255
        // strip off any leading #
        $hex = str_replace('#', '', $hex);

        $c_r = hexdec(substr($hex, 0, 2));
        $c_g = hexdec(substr($hex, 2, 2));
        $c_b = hexdec(substr($hex, 4, 2));

        return (($c_r * 299) + ($c_g * 587) + ($c_b * 114)) / 1000;
    }
    
    
    
    
    
    function isWhirlpool(){
        return $this->customer_id==7; 
    }
    
    
    
    function getApiData(){
        
        $comments=$this->comments."";
        if(empty($comments)){
            $comments=array();
        }
        else{
            $comments=explode("|",$this->comments);
        }
        
        $ramps=array($this->ramp);
        if($this->isWhirlpool() && $this->ramp!="exterier"){
            $ramps[]="exterier";
        }
        //Zend_Debug::dump($this->getData());exit;
        
        
        $min=$this->loadtime < 20 ? 20 : $this->loadtime;
        if($min==30)$min=45;
        
        $end=Matj_Util::addDate($this->time,array('minute'=>$min),'Y-m-d H:i:s');
        
        return array(
             'id'=>$this->id,
             'allDay'=>false,
             'resources'=>$ramps,
             'title'=>$this->customer_shortname ?: "N/A",
             'name'=>$this->customer_name ?: "NEZADANÉ",
             'color'=>$this->color,
             'textcolor'=>$this->get_brightness($this->color) > 130 ? "#000000" : "#ffffff",
             'cartype'=>$this->cartype,
             'carcode'=>$this->carcode,
             'status'=>$this->status,
             'status_color'=>$this->status_color,
             'status_textcolor'=>$this->get_brightness($this->status_color) > 130 ? "#000000" : "#ffffff",
             'status_name'=>$this->status_name,
             'priority'=>$this->priority,
             'start'=>"".$this->time,
             'loadtime'=>"".$this->loadtime,
             'end'=>$end,
             'comments'=>$comments
        );
        
        
        
    }
    
    
    
    public function updateCallback($after, $before, $diff) {
        
        
        
        
        if(empty($diff))
            return false;
        
        $befData=array();
        
        foreach($diff as $k=>$d){
            
            $befData[$k]=$before[$k];
        }
        
        
        $history=new Model_OrderHistory();
        $history->order_id=$this->id;
        $history->user_id= $this->authUser ? $this->authUser->id : null;
        $history->changes=json_encode($diff);
        $history->beforechanges=json_encode($befData);
        $history->save();
    }

    public function getStatuses($where=array()) {
        $registry=Zend_Registry::getInstance();
        if(!$registry->isRegistered('order_statuses') || !empty($where)){
            
            if(empty($where))
                $where["1"]=1;
            
            foreach($this->getAdapter()->fetchAll('SELECT * FROM statuses WHERE %and',$where,'ORDER BY rank') as $s)
                $statuses[$s->code]=$s;
            
            //Zend_Debug::dump($statuses);
            
            $registry->order_statuses=$statuses;
        }
        return $registry->order_statuses;
    }

    function getHistory($options=array()){
        $query=array();
        
        $options["where"]["h.order_id"]=$this->id+0;
        $options["order"][]="h.created desc";
        
        if($this->authUser->group_id>3)
            $options["where"][]=array("h.description <>''");
        
        //$options["test"]=1;
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS h.*,u.name as user_name, u.email as user_email FROM [orders_history] h';
        $query[] = 'LEFT JOIN [users] u ON u.id = h.user_id';
        $query=$this->buildQuery($query,$options);
        
        
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        
        
        $data=array();
        
        
        foreach($result as $k=>$r){
            
            
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $model=new Model_OrderHistory($r->toArray());
                $model->order=$this;
                $data[]=$model;
            }
            
            
        }
        
        return $data;
    }

    public function addHistory($description,$changes=array(),$user=null) {
        $history=new Model_OrderHistory();
        
        if($description)
            $history->description=$description;
        
        $history->order_id=$this->id;
        
        if(!empty($changes))
            $history->changes=json_encode($changes);
        
        if(empty($user))
            $history->user_id=$this->authUser ? $this->authUser->id : null;
        else
            $history->user_id=$user->getId();
        
        $history->save();

    }

    public static function getColors() {
        $color["#ffffff"]="#ffffff";  
        $color["#ac725e"]="#ac725e";  $color["#d06b64"]="#d06b64";  $color["#f83a22"]="#f83a22";  $color["#fa573c"]="#fa573c";  $color["#ff7537"]="#ff7537";  $color["#ffad46"]="#ffad46";  $color["#42d692"]="#42d692";  $color["#16a765"]="#16a765";  $color["#7bd148"]="#7bd148";  $color["#b3dc6c"]="#b3dc6c";  $color["#fbe983"]="#fbe983";  $color["#fad165"]="#fad165";  $color["#92e1c0"]="#92e1c0";  $color["#9fe1e7"]="#9fe1e7";  $color["#9fc6e7"]="#9fc6e7";  $color["#4986e7"]="#4986e7";  $color["#9a9cff"]="#9a9cff";  $color["#b99aff"]="#b99aff";  $color["#c2c2c2"]="#c2c2c2";  $color["#cabdbf"]="#cabdbf";  $color["#cca6ac"]="#cca6ac";  $color["#f691b2"]="#f691b2";  $color["#cd74e6"]="#cd74e6";  
        $color["#a47ae2"]="#a47ae2";
        $color["#ff0000"]="#ff0000";
        
        return $color;
    }
    
    
    function getDelayed(){
        
        $where=$options=array();
        //$options["test"]=1;
        $where[]=array("o.time < %s",date('Y-m-d H:i:s'));
        $where[]=array("o.status IN %in",array('vytvorena','meskanie','vratnica'));
        $options["where"]=$where;
        $options["order"]=array('time');
        return $this->fetchAll($options);
    }
    
    function getInArea(){
        
        $where=$options=array();
        //$options["test"]=1;
       // $where[]=array("o.time < %s",date('Y-m-d H:i:s'));
        $where[]=array("s.inarea=1");
        $options["where"]=$where;
        $options["order"]=array('time');
        return $this->fetchAll($options);
    }
    
    
    function getAjaxStatus($last){
        
        $inArea=$this->fetchAll(array('where'=>array('s.inarea'=>1)));
        
        //Zend_Debug::dump($inArea);
        
        $data["inarea"]=count($inArea);
        $data["inareatype"]=array();
        
        foreach($inArea as $o){
            
            if(empty($data["inareatype"][$o->cartype]))
                $data["inareatype"][$o->cartype]=1;
            else
                $data["inareatype"][$o->cartype]++;
        }
        
        
        
        
        $data["delayed"]=count($this->getDelayed());
        
        
        
        
        
        $data["changes"]=$this->getAdapter()->fetchSingle('SELECT count(*) FROM orders WHERE %and',array(array('modified > %s',date('Y-m-d H:i:s',$last))));
        
        $data["time"]=time();
        
        return $data;
        
        
    }
    
    function findCarcodes($q){
        
        return $this->getAdapter()->fetchAll('SELECT carcode as value, carcode as title FROM orders WHERE %and',array(array('carcode collate utf8_general_ci LIKE %s',"%".$q."%")),'GROUP BY [carcode] ORDER BY [time] DESC LIMIT 10');
        
    }
    
    function findCustomers($q){
        
        return $this->getAdapter()->fetchAll('SELECT id as value, CONCAT(shortname," ",name) as title FROM [customers] WHERE %and',array(array('CONCAT(shortname," ",name) collate utf8_general_ci LIKE %s',"%".$q."%")),'ORDER BY [name] DESC LIMIT 10');
        
    }
    
    
    function getLoadTimeString(){
        $id=$this->id+0;
        $select='
            SELECT "naklada" as status,MAX(created) as time FROM `orders_history` WHERE `order_id` = '.$id.' AND changes LIKE "%status%" AND changes LIKE "%naklada%"
            UNION SELECT "nalozene" as status,MAX(created) as time FROM `orders_history` WHERE `order_id` = '.$id.' AND changes LIKE "%status%" AND changes LIKE "%nalozene%"
            UNION SELECT "vyklada" as status,MAX(created) as time FROM `orders_history` WHERE `order_id` = '.$id.' AND changes LIKE "%status%" AND changes LIKE "%vyklada%"
            UNION SELECT "vylozene" as status,MAX(created) as time FROM `orders_history` WHERE `order_id` = '.$id.' AND changes LIKE "%status%" AND changes LIKE "%vylozene%"
        ';
        $data=$this->getAdapter()->fetchAll($select);
        $times=array();
        
        foreach($data as $d){
            $times[$d->status]=Matj_Format::formatDate($d->time,'H:i') ?: '?';
        }
        
        $html='';
        
        if(strlen($times["naklada"].$times["nalozene"])>2)
            $html='<span class="label label-default" title="nákladka"><i class="fa fa-download"></i> '.$times["naklada"].' - '.$times["nalozene"].'</span>';
        
        if(strlen($times["vyklada"].$times["vylozene"])>2)
            $html.=' <span class="label label-default" title="výkladka"><i class="fa fa-upload"></i> '.$times["vyklada"].' - '.$times["vylozene"].'</span>';
        
        return $html;
               
    }

    public static function getLoadTimes() {
        return array(
                                     '10'=>'10 '.translate('core.minutes'),
                                     '20'=>'20 '.translate('core.minutes'),
                                     '30'=>'30 '.translate('core.minutes'),
                                     '45'=>'45 '.translate('core.minutes'),
                                     '60'=>'60 '.translate('core.minutes'),
                                     '90'=>'90 '.translate('core.minutes'),
                                     '120'=>'120 '.translate('core.minutes')
                                     );
    }
    
    
    function _prepareSaveData($data = null) {
        $data=parent::_prepareSaveData($data);
        
        
        if(key_exists('customer_id',$data) && empty($data["customer_id"])){
            $data["customer_id"]=null;
        }
        
        
        return $data;
    }

    
    function setDataFromForm($data){
        
        if($data["status"]=="areal" && $this->status!="areal")
            $data["time"]=date('Y-m-d H:i:00');
        
        
        return $this->setData($data);
    }
    
    
}