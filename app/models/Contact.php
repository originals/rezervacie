<?php

class Model_Contact extends Model_User{

    private $minGroup=21;

    function access(Model_User $user, $type) {
        
        if($user->getGroupId()>0 && $user->getGroupId()<=2)
            return true;
        
        $access=new Model_UserAccess();
        return $access->fetchOne(array('user2_id'=>$this->pid,'user_id'=>$user->getId()));
        
    }
    
    function getApiData() {
        $data=parent::getApiData();
        
        $allowed=array('name','first_name','last_name','position','email','phone','id','created');
        
        foreach($data as $k=>$d){
            
            if(!in_array($k,$allowed))
                unset($data[$k]);
            
        }
        
        $data["contact_id"]=$data["id"];
        
        return $data;
    }
    
    
    function _prepareSaveData($data = null) {
        
        if(!$data)$data=$this->getData();
        
        if($data["group_id"]<$this->minGroup)
            $data["group_id"]=$this->minGroup;
        
        return parent::_prepareSaveData($data);
    }
        
    
    
}