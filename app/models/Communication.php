<?php

class Model_Communication extends Matj_Dibi_Model{
    
    protected $tableName = "communications";
    protected $createdColumn=true;
    protected $modifiedColumn=true;
    
    protected $logChanges=true;
    
       
    public function setDataFromForm($data=null){
        
        if(is_array($data) && key_exists('pid',$data) && empty($data["pid"]))
                $data["pid"]=null;
        
        return parent::setDataFromForm($data);
    }
    
    function access(Model_User $user, $type) {
        
        if($user->getGroupId()>0 && $user->getGroupId()<=2)
            return true;
        
        
        
        $access=new Model_UserAccess();
        return $access->fetchOne(array('user2_id'=>$this->customer_id,'user_id'=>$user->getId()));
        
        
    }
    
    
    function icon(){
        $type=$this->type;
        $icon="comments";
        
        if($type=="meeting")
            $icon="coffee";
        if($type=="email")
            $icon="envelope";
        if($type=="phone")
            $icon="phone";
        if($type=="videomeet")
            $icon="users";
        
        
        return $icon;
    }
    
    
    
    
    function fetchAll($options=array()){
        $query=array();
        
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS c.*,u.name as user_name,u.email as user_email,cont.name as contact_name,cust.company as customer_company FROM ['.$this->tableName.'] c';
        $query[] = 'LEFT JOIN [users] u ON u.id = c.user_id';
        $query[] = 'LEFT JOIN [users] cust ON cust.id = c.customer_id';
        $query[] = 'LEFT JOIN [users] cont ON cont.id = c.contact_id';
        
        //$options["group"]=array('o.id');
        
       // $options["test"]=1;
        
        
        
        
        
        $query=$this->buildQuery($query,$options);
        
        
        
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        
        
        $data=array();
        
        
        foreach($result as $k=>$r){
            
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }
            
            
        }
        
        return $data;
    }

    public static function getTypeOptions() {
        
        $options["meeting"]="Stretnutie";
        $options["call"]="Telefonát";
        $options["email"]="Email";
        $options["videomeet"]="Videohovor";
        $options["chat"]="Chat";
        $options["other"]="Iné";
        return $options;
    }
    
    
    function getApiData() {
        $data=parent::getApiData();
        $data["communication_id"]=$data["id"];
        $data["date_sk"]=Matj_Format::getDate($data["date"], "d.m.Y");
        $data["time"]=Matj_Format::getDate($data["date"], "H:i");
        $data["title"]=$data["customer_company"];
        $data["text_short"]=Matj_String::shorten($data["text"],100,array('noHtml'=>true));
        $data["start"]=$data["date"];
        $data["end"]=Matj_Util::addDate($data["date"],array('hour'=>1),'Y-m-d H:i:s');
        
        return $data;
    }
    
}