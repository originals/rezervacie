<?php

class Model_User extends Matj_Dibi_Model{
    
    protected $tableName = "users";
    protected $createdColumn=true;
    protected $modifiedColumn=true;
    
    protected $logChanges=true;
    
    
    protected $_group;
    
    
    function getGroup(){
        if($this->_group===null)
            $this->_group=new Model_Group($this->group_id);
        return $this->_group;
    }
  
    public function setDataFromForm($data=null){
        /*
        if(is_array($data) && key_exists('pid',$data) && empty($data["pid"]))
                $data["pid"]=null;*/
        
        return parent::setDataFromForm($data);
    }
    
    
    
    function getAuthUser($data){
        
        if(!empty($data["id"])){
            $id=$data["id"];
            return $this->fetchOne(array('id'=>$id));
        }
        
        
    }
    
    
    function authenticate($data){
        //$db=array('email'=>$data["username"],'password'=>$this->encodePassword($data["password"]));
        
        $db=array('email'=>$data["username"],'password'=>$this->encodePassword($data["password"]));
        if(preg_match('/@/',$data["username"])){
            $db=array('email'=>$data["username"],'password'=>$this->encodePassword($data["password"]));
        }
        else{
            $db=array('login'=>$data["username"],'password'=>$this->encodePassword($data["password"]));
        }
        
        
        $user=$this->fetchOne($db,array('test'=>0));
        
        if($user){
            return array('id'=>$user->getId());                    
        }
    }
    
    function encodePassword($pwd){
        return md5($pwd);
    }
    
    function getPassword(){
        return $this->password;
    }
    
    
    function getImage(){
        return $this->image;
    }
    
    function getFullName(){
        return $this->name;
    }
/*
    function isUniqueEmail($data){
        $data=$this->getAdapter()->query('SELECT * FROM ['.$this->tableName.'] WHERE %and',array(array("[id]!= %i",$data["_id"]),"email"=>$data["email"]))->fetch();
        Zend_Debug::dump($data);
        return empty($data);
    }*/
    	
    function isUniqueEmail($data){
        $data=$this->getAdapter()->query('SELECT * FROM ['.$this->tableName.'] WHERE %and',array("email"=>$data["email"]))->fetch();
        Zend_Debug::dump($data);
        return empty($data);
    }
  
    function updateLoginData($data){
        
        $db=array("email"=>$data["email"]);
        
        
        if($data["password"])
            $db["password"]=$this->encodePassword($data["password"]);
        
        return $this->update($db,array("id" => $this->getId()));
        
    }
    
    
    function getGroupId(){
        
        if($this->group_id>0){
            return $this->group_id;
        }
        return 999;
        Zend_Debug::dump($this->getData());exit;
    }
    
    
    function getUniqueIdentifier($data=null,$gender=true,$bornFormat="ymd"){
        if($data===null)$data=$this->toArray();
        $chArr=str_split(strtolower(Matj_String::removeAccents(substr($data["firstname"],0,3)."".substr($data["lastname"],0,3))));
        sort($chArr);
        
        $str="";
        if($gender && !empty($data["gender"]))
            $str.=strtolower($data["gender"]);
        
        $str.=implode($chArr);
        
        if(!empty($data["born"]))
            $str.=Matj_Format::formatDate($data["born"],$bornFormat);
        
        
        return $str;
    }
    
    
    function insert($data) {
        return parent::insert($data);
        
    }
    
    
    function _prepareSaveData($data = null) {
        
        
        $data=parent::_prepareSaveData($data);
        
        if(empty($data["name"]) && !empty($data["first_name"]) && !empty($data["last_name"]))
            $data["name"]=trim($data["last_name"]." ".$data["first_name"]);
        
        return $data;
        
    }
    
    
    function createUser($data){
        
        $data["uniqid"]=$this->getUniqueIdentifier($data);
        
        $user=new self();
        $data=$this->_prepareSaveData($data);
        
        if(!empty($data["password"]))
            $data["password"]=$this->encodePassword ($data["password"]);
        
        //Zend_Debug::dump($data);exit;
        
        if($id=$this->insert($data)){
            $data["id"]=$id;
            $this->setData($data);
            return $this;
        }
        return false;
        
        
        
        
        
        
        
    }
    
    
    protected $_eventAccess=null;
    
    function getEventAccess($event_id=null){
        
        if($this->_eventAccess===null){
            $model=new Model_EventUser();
            $data=$model->fetchAll(array('where'=>array('user_id'=>$this->getId())));
            foreach($data as $k=>$d){
                $ea[$d->event_id]=$d;
            }
            $this->_eventAccess=$ea;
        }
        
        if($event_id)
            return $this->_eventAccess[$event_id];
        else
            return $this->_eventAccess;
        
        
        
    }
    
    
    
    function getUploadSettings(){
        $conf=Matj_Get::getBootstrap()->getOption('upload');
        $settings["path"]=rtrim($conf["folder"],"/").'/users/'.(0+$this->id);
        $settings["url"]=rtrim($conf["url"],"/").'/users/'.(0+$this->id);
        return $settings;
    }
    
    
    function getActivatecode($create=false){
        if($create){
            if($this->activatecode==""){
                $code=md5(print_r($this->getData(),true).time());
                $this->activatecode=$code;
                $this->save();
                
            }
        }
        return $this->activatecode;
    } 

    function getApiData() {
        $data=parent::getApiData();
        $data["born"]=Matj_Format::formatDate($this->born,"Y-m-d");
        unset($data["password"]);
        unset($data["activatecode"]);
        
        
        return $data;
    }

    
    function hasScope($scope){
        $scopes=explode(",",$this->scope);
        $return=false;
        
        if(is_array($scope)){
            foreach($scope as $s)
                if(in_array($s,$scopes))
                    $return=true;
                    
            return $return;        
        }
        
        return in_array($scope,explode(",",$this->scope));
        
        
    }

    public function getGroups($options=array()) {
        
        $query[] = 'SELECT * FROM [groups]';
        $query=$this->buildQuery($query,$options);
        
        $options["output"]='array';
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        
        
        $data=array();
        
        
        foreach($result as $k=>$r){
            
            
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                //$class=get_class($this);
                //$data[]=new $class($r->toArray());
            }
            
            
        }
        
        return $data;
        
    }
  
     public function getClients($options=array()) {
        
        $query[] = 'SELECT * FROM [clients]';
        $query=$this->buildQuery($query,$options);
        
        $options["output"]='array';
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        
        
        $data=array();
        
        
        foreach($result as $k=>$r){
            
            
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                //$class=get_class($this);
                //$data[]=new $class($r->toArray());
            }
            
            
        }
        
        return $data;
        
    }
    
     
    function fetchAll($options=array()){
        $query=array();
        
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS u.*,g.name as group_name FROM ['.$this->tableName.'] u';
        $query[] = 'LEFT JOIN [groups] g ON g.id = u.group_id';
        
        $query=$this->buildQuery($query,$options);
        
        
        
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        
        
        $data=array();
        
        
        foreach($result as $k=>$r){
            
            
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }
            
            
        }
        
        return $data;
    }
   
    function fetchUsersAccessLog($options=array()){
        $query=array();
        
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS u.*,g.name as group_name,l.created as created, l.ip,l.browser FROM ['.$this->tableName.'] u';
        $query[] = 'LEFT JOIN [log_access] l ON l.user_id = u.id';
        $query[] = 'LEFT JOIN [groups] g ON g.id = u.group_id';
        
        //$options["group"]=array('o.id');
        
       // $options["test"]=1;
        
        $query=$this->buildQuery($query,$options);
        
        
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        
        
        $data=array();
        
        
        foreach($result as $k=>$r){
            
            
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }
            
            
        }
        
        return $data;
    }
  
    function fetchAccessClients($options=array()){
        $query=array();        
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS c.* FROM [clients] c';        
        $query[] = 'LEFT JOIN [users_joins] j ON j.client_id = c.id';
        $query[] = 'LEFT JOIN [users] u ON j.user_id = u.id';
        
        //$options["test"]=1;
        $query[]=' WHERE j.user_id="'.$this->id.'" ';
        $query[]=' LIMIT 1';
        
        $query=$this->buildQuery($query,$options);
        
        
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }       
        
        $data=array();
        
        
        foreach($result as $k=>$r){
            //Zend_Debug::dump($r);exit;
            $r->centers=$this->fetchClientsCenters(array("client_id"=>$r->id));
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data=new $class($r->toArray());
            }
            
            
        }
        
        return $data;
    }
  
    function fetchClientsCenters($options=array()){
        $query=array();        
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS c.* FROM [centers] c';        
        $query[] = 'LEFT JOIN [clients] cl ON cl.id = c.client_id';
        $query[] = 'LEFT JOIN [users_access] u ON cl.id = u.client_id';
        $query[]=' WHERE cl.id="'.$options["client_id"].'" AND c.id=u.center_id ';
        $optons["group"]=array('cl.id');
        
        $query=$this->buildQuery($query,$options);   
        
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }       
        
        $data=array();
        
        
        foreach($result as $k=>$r){ 
            $center=new Model_Center($r->id);
            $r->objects=$center->fetchObjects();
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }
            
            
        }
        //zend_Debug::dump($data);
        return $data;
    }
   
   

}