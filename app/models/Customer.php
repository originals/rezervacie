<?php

class Model_Customer extends Model_User{
    
    
    
    function access(Model_User $user, $type) {
        
        if($user->getGroupId()>0 && $user->getGroupId()<=2)
            return true;
        
        
        $access=new Model_UserAccess();
        return $access->fetchOne(array('user2_id'=>$this->getId(),'user_id'=>$user->getId()));
    }
    
    
    function getContacts(){
        $users=new Model_User();
        $options["where"]["g.level"]=6;
        $options["where"]["u.pid"]=$this->getId();
        return $users->fetchAll($options);
    }

    public function getCommunication() {
        $users=new Model_Communication();
        $options["where"]["c.customer_id"]=$this->getId();
        $options["order"][]="c.date DESC";
        //$options["test"]=1;
        return $users->fetchAll($options);
    }

    public function getAccess() {
        $users=new Model_UserAccess();
        $options["where"]["a.user2_id"]=$this->getId();
        $options["order"][]="a.modified DESC";
        //$options["test"]=1;
        return $users->fetchAll($options);
    }
    
    function fetchAll($options=array()){
        $query=array();
        
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS u.*,g.name as group_name FROM ['.$this->tableName.'] u';
        $query[] = 'LEFT JOIN [groups] g ON g.id = u.group_id';
        
        //$options["group"]=array('o.id');
        
        // $options["test"]=1;
        
        
        if(!empty($options["user"])){
            
            $query[] = 'RIGHT JOIN [users_access] a ON a.user2_id = u.id AND a.user_id = '.($options["user"]->id+0);
            
        }
        
        
        
        
        
        $query=$this->buildQuery($query,$options);
        
        
        
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        
        
        $data=array();
        
        
        foreach($result as $k=>$r){
            
            
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $class=get_class($this);
                $data[]=new $class($r->toArray());
            }
            
            
        }
        
        return $data;
    }
    
    
}