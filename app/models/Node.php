<?php

class Model_Node extends Matj_Dibi_Model{
    
    protected $tableName = "nodes";
    protected $createdColumn=true;
    protected $modifiedColumn=true;
    
    protected $logChanges=true;
    
       
    public function setDataFromForm($data=null){
        
        if(is_array($data) && key_exists('pid',$data) && empty($data["pid"]))
                $data["pid"]=null;
        
        return parent::setDataFromForm($data);
    }
}