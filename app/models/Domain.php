<?php

class Model_Domain extends Matj_Dibi_Model{
    
    protected $tableName = "domains";
    protected $createdColumn=true;
    protected $modifiedColumn=true;
    
    protected $logChanges=true;
    
       
    function getPages($options=array()){
        $node=new Model_Node();
        $options["where"]["type"]="page";
        $options["where"]["domain_id"]=$this->id+0;
        $options["order"][]=array('IF(pid>0,pid,id)','ASC');
        $options["order"][]=array('id','ASC');
        
        
        return $node->fetchAll($options);
        
        
        
    }
    
    
    
    
}