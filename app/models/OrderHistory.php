<?php

class Model_OrderHistory extends Matj_Dibi_Model{
    
    protected $tableName = "orders_history";
    protected $createdColumn=true;
    protected $modifiedColumn=true;
    
    protected $logChanges=false;
    
    /**
     *
     * @var Model_Order
     */
    public $order;
    
    
    function getDescription(){
        
        $diff=json_decode($this->changes,true);
        
        $descr=$this->description;
        if(!empty($descr))
            $description[]='<div class=history-description><i class="fa fa-comment"></i> '.$this->description.'</div>';
        
        if(!empty($diff)){
            foreach($diff as $k=>$v){
                
                if(is_array($v) && !empty($v["date"])){
                    $v=Matj_Format::formatDate($v["date"],'d.m.Y H:i');
                }
                
                
                if($k=="status"){
                    $statuses=$this->getOrder()->getStatuses();
                    if(key_exists($v, $statuses))
                        $v=$statuses[$v]->name;
                }
                
                if($k=="ramp"){
                    $ramps=$this->getOrder()->getRamps();
                    if(key_exists($v, $ramps))
                        $v=$ramps[$v]->name;
                }
                
                if($k=="client_id" && $v!=null){
                    $customer=new Model_Client($v);
                    if(!$customer->isEmpty())
                        $v=$customer->name;
                }
                
                if($k=="type")$v=translate('order.type.'.$v);
                if($k=="color")$v='<span class="label" style="background:'.$v.'">'.$v.'</span>';
                if($k=="priority")$v=translate('customer.priority.'.$v);
                
                
                if(empty($v))
                    $v=" - ";
                
                $description[]='<span class="history-change"><i class="fa fa-pencil"></i> <strong>'.translate('order.'.$k).'</strong>: '.$v.'</span>';
            }
        }
        
        
        
        return implode("",$description);
        
        
    }
    
    
    
    function fetchAll($options=array()){
        $query=array();
        
        $query[] = 'SELECT SQL_CALC_FOUND_ROWS o.*,h.*,u.name as user_name, u.email as user_email,c.name as customer_name FROM [orders_history] h';
        $query[] = 'LEFT JOIN [reservations] o ON o.id = h.reservation_id';
        $query[] = 'LEFT JOIN [clients] c ON c.id = o.client_id';
        $query[] = 'LEFT JOIN [users] u ON u.id = h.user_id';
        $query=$this->buildQuery($query,$options);
        
        
        
        if(!empty($options["test"])){
            $result = $this->getAdapter()->test($query);
            exit;
        }
        else{
            $result = $this->getAdapter()->query($query);
        }
        
        
        
        $data=array();
        
        
        foreach($result as $k=>$r){
            
            
            if(!empty($options["output"]) && $options["output"]=="array"){
                $data[]=($r->toArray());
            }
            else{
                $model=new Model_OrderHistory($r->toArray());
                //$model->order=$this;
                $data[]=$model;
            }
            
            
        }
        
        return $data;
    }
    
    function getTableData(){
        $data=$this->toArray();
        $data["customer_textcolor"]=Model_Order::get_brightness($this->customer_color)<130 ? '#ffffff' : '#000000';
        $data["description"]=$this->getDescription();
        return $data;
    }

    public function getOrder() {
        if($this->order===null)
            $this->order=new Model_Reservation($this->reservation_id);
        return $this->order;
    }

}