<?php

 
class SetupController extends Matj_Bootstrap3_Action_BackendSession
{
           
    
    /** od akej user / group_id je povoleny vstup */
    protected $auth_group = 3;   
    function init(){
			parent::init();
			$this->view->clients=$this->clients;
			
		}
    
    
    function indexAction(){
      $this->showHeader=false;
			$this->view->header="Správa Vašich stredísk";
			$this->view->clients=$this->clients;
			$form=new Form_Client();
			$form->client=$this->clients;
			//zend_debug::dump($this->clients->getData());
      $usr=new Model_Client($this->clients->id+0);
			if($this->_isPost()){
				if($form->isValid($this->getRequest()->getPost())){
					$data=$this->_getPost();
					
					$usr->setDataFromForm($this->_getPost());
					if($usr->save()){
						$this->addMessage("credentials.saved","success");
						$this->clients=$usr;
					}else{
						$this->addMessage("credentials.not.valid","danger");
					}
				}else{
					$this->addMessage("form.not.valid","danger");
				}
			}
			$form->populate($this->clients->getData());
			$this->view->form=$form;
			
       
    }
    
   	function intervalAction(){
			$this->view->params=$params=$this->getRequest()->getParams();
			$center=new Model_Center($this->_getParam('center_id'));
			$interval=new Model_Interval($this->_getParam('interval'));
			$this->view->clients=$this->clients;
			$this->view->header="";
			if($this->_isPost()){            
            
            //Zend_Debug::dump($this->_getPost());exit;         
            
            $interval=new Model_Interval($this->_getPost('interval'));
            if(!$interval->isEmpty() && !$interval->access($this->user)){
							exit;
                throw new Exception('No access');
            }             
								
						$interval->setDataFromForm($this->_getPost());
						$interval->center_id=$center->id;
						$interval->client_id=$center->client_id;
						//Zend_Debug::dump($interval->getData());exit;
						$interval->save();
						$this->addMessage("interval.saved","success");
						$this->_redirect($this->view->url(array('controller'=>'setup','action'=>'interval','center_id'=>$center->id,'add'=>null)));
            
     	}
			/* ak chcem mazat interval */
			if($params["delete"]=="1" && $params["interval"]){
					$interval=new Model_Interval($this->_getParam('interval'));	
					$valid=$interval->access($this->user);
					if($valid==true){	
						try{
							$interval->delete();
						}catch(Exception $e){
							//Zend_Debug::dump($e);
						}
						$this->addMessage("interval.deleted","success");	
          }else{
						$this->addMessage("not.authorised.for.this.action","danger");
					}
					$this->_redirect($this->view->url(array('controller'=>'setup','action'=>'interval','center_id'=>$center->id,'delete'=>null,'interval'=>null)));
			}			
			
			/* zobrazenie jednotlivych intervalov pre nastavenie */
			if($interval->id){
				$this->view->interval=$interval;
			}
			$this->view->center=$center;
			$this->view->intervals=$interval->fetchAll(array("where"=>'i.center_id="'.$center->id.'"'));
		}
	
		function savepriceAction(){
			$params=$this->getRequest()->getParams();
			//Zend_Debug::dump($params);
			
			$center=new Model_Center($params["center_id"]+0);
			$interval=new Model_Interval($params["interval"]+0);
			//Zend_Debug::dump($center->id);
			//exit;
			if($center->isEmpty()){
				$this->addMessage("missing.center.param","error");
				$this->_redirect($this->_redirect($this->view->url(array('controller'=>'setup','action'=>'index','center_id'=>null,'delete'=>null,'add'=>null,'interval'=>null,'wday'=>null))));
			}
			if(!$interval->access($this->user)){
				$this->addMessage("not.authorised.for.this.action","danger");
				$this->_redirect($this->_redirect($this->view->url(array('controller'=>'setup','action'=>'index','center_id'=>null,'delete'=>null,'add'=>null,'interval'=>null,'wday'=>null))));
			}
			//Zend_Debug::dump($params["wday"]);exit;
			if($params["center_id"] && $params["interval"] && $params["wday"] || $params["center_id"] && $params["interval"] && $params["wday"]=="0"){
				if($params["price"]){
					foreach($params["price"] as $k=>$v){
						$price=new Model_Hours();
						//Zend_Debug::dump($center->client_id);exit;
						if(empty($v)){
							$price->fetchOne(array("wday"=>$params["wday"],"client_id"=>$center->client_id,"center_id"=>$center->id,"hour"=>$k.":00:00","interval_id"=>$params["interval"]));
							if(!$price->isEmpty()){
								$price->delete();
							}
						}else{
							$price->fetchOne(array("wday"=>$params["wday"],"client_id"=>$center->client_id,"center_id"=>$center->id,"hour"=>$k.":00:00","interval_id"=>$params["interval"]));
							$price->setData(array(
								"wday"=>$params["wday"],
								"client_id"=>$center->client_id,
								"center_id"=>$center->id,
								"interval_id"=>$params["interval"],
								"hour"=>$k.":00:00",
								"price"=>number_format($v,2)
							));
							$price->save();
						}
					}
				}
				$this->addMessage("succesfully.saved","error");
				$this->_redirect($this->_redirect($this->view->url(array('controller'=>'setup','action'=>'interval','center_id'=>$center->id,'interval'=>$params["interval"],'wday'=>$params["wday"]))));
			}else{
				$this->addMessage("missing.params.to.save","error");
				$this->_redirect($this->_redirect($this->view->url(array('controller'=>'setup','action'=>'interval','center_id'=>$center->id,'delete'=>null,'interval'=>null,'wday'=>null))));
			}
			exit;
		}
    
    
    function addcenterAction(){
        $this->editcenterAction();
    }
	
		function editcenterAction(){
			$params=$this->getRequest()->getParams();
			$center=new Model_Center();
			$form=new Form_Center();
			$center->getCenterByHash(array("center"=>$this->_getParam("id")));
			$id=$center->id;
			if($this->user->group_id > 3 && $center->isEmpty()){
        throw new Exception('Access error');
      }
			if(!$center->isEmpty()){
				if(!$center->access($this->user,$center->client_id)){
					$this->addMessage("not.valid.access.to.this.center","danger");
					$this->_redirect($this->_redirect($this->view->url(array('controller'=>'setup','action'=>'interval','center_id'=>null,'delete'=>null,'interval'=>null,'wday'=>null))));
				}else{
					//Zend_Debug::dump($center->getData());
					$form->populate($center->getData());
				}
			}
			if($this->_isPost()){
            if($form->isValid($this->_getPost())){
                $data=$form->getValues();
                $center->setDataFromForm($data);
								$center->hash=md5(date("Y-m-d H:i:s").rand());
								$center->client_id=$this->clients->id;
								
								try{
									$center->save();
									$access=new Model_UserAccess();
									$access->user_id=$this->user->id;
									$access->client_id=$this->clients->id;
									$access->center_id=$center->id;
									$access->save();
									$this->_redirect($this->_redirect($this->view->url(array('controller'=>'setup','action'=>'index','center_id'=>null,'delete'=>null,'interval'=>null,'wday'=>null))));
								}catch(Exception $e){
									Zend_Debug::dump($e);
								}
						}
			}
			
			
      $options["where"]=Matj_Util::joinWheres("o.center_id = ".$center->id."",$options["where"]);
			$object=new Model_Object();
			$data=$object->fetchAll($options);
			$this->view->params=$params;
			$this->view->objects=$data;
			$this->view->form=$form;
		}
	
		function addobjectAction(){
        $this->editobjectAction();
    }
	
		function editobjectAction(){
				$params=$this->getRequest()->getParams();
				$center=new Model_Center();
				$object=new Model_Object();
				$form=new Form_Object();
			
				$center->getCenterByHash(array("center"=>$this->_getParam("id")));
				$object->getByHash(array("object"=>$this->_getParam("object")));
				if(!$center->isEmpty()){
					if(!$center->access($this->user,$center->client_id)){
						$this->addMessage("not.valid.access.to.this.center","danger");
						$this->_redirect($this->_redirect($this->view->url(array('controller'=>'setup','action'=>'interval','center_id'=>null,'delete'=>null,'interval'=>null,'wday'=>null))));
					}
				}
			
				if($this->_isPost()){
            if($form->isValid($this->_getPost())){
                $data=$form->getValues();
                $object->setDataFromForm($data);
								$object->center_id=$center->id;
								if($object->id){
									$object->edited=date("Y-m-d H:i:s");
								}else{
									$object->hash=md5(date("Y-m-d H:i:s").rand());
									$object->created=date("Y-m-d H:i:s");
								}
								try{
									$object->save();
									$this->_redirect($this->_redirect($this->view->url(array('controller'=>'setup','action'=>'addcenter','id'=>$this->_getParam("id"),'delete'=>null,'interval'=>null,'wday'=>null,'object'=>null))));
								}catch(Exception $e){
									Zend_Debug::dump($e);
								}
						}
			}
			
			
				
				if(!$object->isEmpty()){
					$form->populate($object->getData());
				}			
				$this->view->form=$form;
			//$this->_redirect($this->_redirect($this->view->url(array('controller'=>'setup','action'=>'addcenter','id'=>$params["id"],'delete'=>null,'interval'=>null,'wday'=>null))));
		}
    
    function editAction(){
        
        
        $order=new Model_Order($this->_getParam('id'));
        
        if($this->user->group_id > 3 && $order->isEmpty()){
            throw new Exception('Access error');
        }
        
        
        $order->authUser=$this->user;
        $this->view->history=$order->getHistory();
        
        $form=$this->_getOrderForm($order);
        $form->setAction(url(array('delete'=>null,'copy'=>null)));
        
        
        if($this->_getParam('delete')){
            
            if($this->user->group_id>3)
                throw new Exception('Access error');
            
            $order->status="delete";
            $order->save();
            
            $this->addMessage('core.item.deleted','success');
            
            if(!$this->_getParam('ajax')){
                return $this->_redirect(url(array('id'=>null,'delete'=>null,'action'=>'list')));
            }
            else{
                $form="<script> hideEditForm(); </script>";
                $this->view->form=$form;
                return;
            }
        }
        
        
        
        if($this->_isPost()){
            if($form->isValid($this->_getPost())){
                $data=$form->getValues();
                $order->setDataFromForm($data);
                $id=$order->getId();
                if($data["customer_id"]=="add"){
                    $customer=new Model_Customer(array('name'=>$data["customer_name"],'shortname'=>$data["customer_shortname"],'type'=>$data["customer_type"],'color'=>$data["color"],'priority'=>$data["priority"],'loadtime'=>$data["loadtime"]));
                    $customer->save();
                    $order->customer_id=$customer->getId();
                }
                try{
                    if($order->save()){
                        $this->addMessage('core.form.saved','success');
                        if(empty($id)){
                            $order->addHistory(null,array('status'=>$order->status));
                        }
                        if(!empty($data["addnote"]))
                            $order->addHistory($data["addnote"]);

                        if(!$this->_getParam('ajax')){
                            $this->_redirect(url(array('id'=>null,'action'=>'list')));
                        }
                        else{
                            $form="<script> hideEditForm(); </script>";
                        }
                    }   
                }
                catch(Exception $e){
                    
                    $this->addMessage('core.form.error','danger');
                    
                }
            }
            else{
                $this->addMessage('core.form.notvalid','danger');
            }
            
        }
        else{
            
            $data=$order->getFormData();
            
            $change=$this->_getParam('change');
            if(!empty($change["start"]) && strtotime($change["start"])>0 && strtotime($change["start"])<>strtotime($data["time"])){
                $form->time->setDescription('Zmena z <strong>'.Matj_Format::formatDate($data["time"],'d.m.Y H:i').'</strong>');
                $data["time"]=date('Y-m-d H:i:s',strtotime($change["start"]));
            }
            
            if(!empty($change["resources"]) && $change["resources"]!=$data["ramp"]){
                $form->ramp->setDescription('Zmena z <strong>'.$data["ramp"].'</strong>');
                $data["ramp"]=$change["resources"];
            }
            if($this->_getParam('copy')>0){
                $orderCopied=new Model_Order($this->_getParam('copy'));
                $data=$orderCopied->getData();
                $data["status"]=null;
                $data["time"]=Matj_Util::addDate($data["time"],array('minute'=>30),'Y-m-d H:i:s');
                
                $this->addMessage('order.copy.created','success');
            }
            $form->populate($data);
        }
        
        
        $this->view->form=$form;
    }
    
    
    

}

