<?php

class WidgetsController extends Matj_Bootstrap3_Action {
	
		function init(){
			parent::init();
			
			$this->view->layout()->setLayout('widgets');
			
		}
	
    
    
    function indexAction(){
      
        $params=$this->getRequest()->getParams();  
				if(empty($params["currentDay"])) $params["currentDay"]=date("Y-m-d");
        if($params["client"]){
          $client=new Model_Client();
          $center=new Model_Center();
          $this->view->client=$client->getClientByHash($params);          
          $this->view->centers=$center->fetchAll(array("where"=>'ce.client_id="'.$client->getId().'" AND ce.active="1" ','output'=>'array'));        
          
          
        }else{
          $this->_redirect("/");
        }
        $this->view->params=$params;
    }
  
    function centerAction(){      
        $params=$this->getRequest()->getParams();     
        
        $client=new Model_Client();
        $center=new Model_Center();
        $this->view->client=$client->getClientByHash($params);            
        $this->view->center=$model=$center->getCenterByHash($params); 
      
        if(empty($params["currentDay"])) $params["currentDay"]=date("Y-m-d");
			
				if($params["currentDay"]<date("Y-m-d")){
					$this->addMessage("sorry.date.is.already.in.past","danger");
					$this->_redirect($this->view->url(array('action'=>'center','client'=>$params["client"],'center'=>$params["center"],'currentDay'=>date("Y-m-d")),'widget'));
				}
          
        if(!$client->isEmpty() || !$center->isEmpty()){  
          $this->view->intervals=$center->fetchOpeningHours(array('output'=>'array','where'=>'i.center_id="'.$center->id.'" AND i.client_id="'.$center->client_id.'" AND i.start < "'.$params["currentDay"].'" AND i.end > "'.$params["currentDay"].'" AND h.wday="'.date("w",strtotime($params["currentDay"])).'" ')); 
          $this->view->objects=$center->fetchObjects();
          $resTime=new Model_Reservationtime();
          $where[]=array('t.client_id = "'.$client->id.'"');
          $where[]=array('t.center_id = "'.$center->id.'"');
          $where[]=array('t.date = "'.$params["currentDay"].'"');
          $where[]=array('r.status < 3 ');
          $options["where"]=$where;
          $options["output"]='array';
          $this->view->reservations=$resTime->fetchAll($options); 
          unset($options["where"]);
          $session=session_id();
          $where2[]=array('r.session="'.$session.'"');
          $where2[]=array('t.client_id = "'.$client->id.'"');
          //$where2[]=array('t.center_id = "'.$center->id.'"');          
          $where2[]=array('r.status = 3 ');
          $options["where"]=$where2;
          $options["output"]='array';
          //$options["test"]=1;         
					$myreservations["all"]=$resTime->fetchAll($options);
					$where2[]=array('t.date = "'.$params["currentDay"].'"');
					$options["where"]=$where2;
					$myreservations[$params["currentDay"]]=$resTime->fetchAll($options);
					$this->view->myreservations=$myreservations;
          
        }else{
          if($params["client"]){
             $this->_redirect($this->view->url(array('client'=>$params["client"]),'client'));
          }else{
             $this->_redirect("/");
          }         
        }
        $this->view->params=$params;
        
    }
  
    function reservationAction(){
      $params=$this->getRequest()->getParams();
      //Zend_Debug::dump($params);
      $session=session_id();
      $client=new Model_Client();
      $center=new Model_Center();
      $object=new Model_Object();
      $this->view->client=$cl=$client->getClientByHash($params);            
      $this->view->center=$ce=$center->getCenterByHash($params); 
      $this->view->object=$ob=$object->getByHash($params);
      $check=false;
      $resTime=new Model_Reservationtime();
      $where[]=array('t.object_id = "'.$ob->id.'"');
			$where[]=array('t.date = "'.date("Y-m-d",$params["timestamp"]).'"');
      $where[]=array('t.timestamp = "'.$params["timestamp"].'"');
      $where[]=array('r.status < 3');
      $options["where"]=$where;
      if($resTime->fetchAll($options)){
        $check=true;
      }
      
      if($check){
        $this->addMessage("sorry.therm.is.already.booked","danger");
      }else{
        $reservation=new Model_Reservation();
        $reservation->getReservationBySession(array("session"=>$session));
        if($reservation->isEmpty()){
          $reservation->client_id=$cl->id;
          $reservation->session=$session;
          $reservation->center_id=$ce->id;
          $reservation->object_id=$ob->id;
          $reservation->created=date("Y-m-d H:i:s");
					$reservation->hash=md5($session.date("Y-m-d H:i:s").$ob->id.$ce->id.$cl->id);
          $reservation->ip=Matj_Util::getIp();
          $reservation->status=3;
					$reservation->color='#'.Mufin_Color::random_color();
          $reservation->save();
        }
        //Zend_Debug::dump($reservation);exit;
        $resTime->session=$session;
        $resTime->reservations_id=$reservation->id;
        $resTime->date=date("Y-m-d",$params["timestamp"]);
        $resTime->timestamp=$params["timestamp"];
        $resTime->price=$params["price"];
        $resTime->object_id=$ob->id;
        $resTime->center_id=$ce->id;
        $resTime->client_id=$cl->id;
				$resTime->hash=md5($session.$params["timestamp"].$ob->id.$ce->id.$cl->id);
        $resTime->save();       
        
      }
      //exit;
      $this->_redirect(url(array('action'=>'center','center'=>$ce->hash,'client'=>$cl->hash,'object'=>null,'timestamp'=>null),'widget'));
      
    }
	
		function removeAction(){
			$params=$this->getRequest()->getParams();
      
      //$session=session_id();      
      $resTime=new Model_Reservationtime();
			$resTime->getByHash($params["id"]);
			//Zend_Debug::dump($resTime->isEmpty());exit;
      if(!$resTime->isEmpty()){
        $resTime->delete();
      }     
      $this->_redirect(url(array('action'=>'center','center'=>$params["center"],'client'=>$params["client"],'id'=>null),'widget'));
    }
	
		function userAction(){
			$params=$this->getRequest()->getParams();
      //Zend_Debug::dump($params);
      $session=session_id();
			$reservation=new Model_Reservation();
			$reservation->getReservationBySession(array("session"=>$session));
			if($reservation->isEmpty()){
				$this->addMessage("nothing.to.book","bg-pomegranate");
				$this->_redirect($this->view->url(array('action'=>'center','center'=>$ce->hash,'client'=>$cl->hash,'object'=>null,'timestamp'=>null),'widget'));
			}
			$rtime=new Model_Reservationtime();
			$form=new Form_OrderUser();
      $client=new Model_Client();
      $center=new Model_Center();
      $object=new Model_Object();
      $this->view->client=$cl=$client->getClientByHash($params);            
      $this->view->center=$ce=$center->getCenterByHash($params); 
      $this->view->object=$ob=$object->getByHash($params);
			if($this->user){
				$form->populate($this->user->getData());
				$form->removeElement("accout");
			}
			
			
			if($this->_isPost()){
            if($this->_getPost('back')==1){
                $this->_redirect($this->view->url(array('action'=>'center','center'=>$ce->hash,'client'=>$cl->hash,'object'=>null,'timestamp'=>null),'widget'));
            }            
						if($form->isValid($this->_getPost())){
							$user=new Model_User();
							$post=$this->_getPost();
							//Zend_Debug::dump($post);exit;							
							$reservation->edited=date("Y-m-d H:i:s");
							/* Ak chce vytvorit konto */
							if($post["create"]){
								/* overenie jedinecneho emailu */
								$unique=$user->isUniqueEmail($this->_getPost());
								if($unique){
									$user->setDataFromPost($this->_getPost());
									$user->client_id=$cl->id;
									$user->save();
									$reservation->first_name=$user->first_name;
									$reservation->last_name=$user->last_name;
									$reservation->phone=$user->phone;
									$reservation->email=$user->email;
								}else{
									$this->addMessage("account.already.exists.please.login","success");
									$this->_redirect($this->view->url(array(),'login'));
								}
							}else{
								/* pokračujem zapisanim udajov bez konta */
								$reservation->first_name=$post["first_name"];
								$reservation->last_name=$post["last_name"];
								$reservation->phone=$post["phone"];
								$reservation->email=$post["email"];
								
							}
							
							
							$this->view->myreservations=$times=$rtime->fetchAll(array('where'=>'t.reservations_id="'.$reservation->id.'"'));
							$checkout=false;
							foreach($times as $k=>$v){
								$isValid=$rtime->fetchAll(array('where'=>'t.timestamp="'.$v->timestamp.'" AND t.object_id="'.$v->object_id.'" AND r.status="1"'));
								//Zend_Debug::dump($isValid);
								if($isValid){
									$reservations[]=$v;
									$checkout=true;
								}
							}
							//Zend_Debug::dump($checkout);exit;
							if($checkout){
								//$this->addMessage("object.is.already.booked","error");
								$this->view->reservations=$reservations;
								$reservation->status=3;
								$reservation->save();
							}else{
								$reservation->status=2;	
								$reservation->session="";
								$reservation->save();
								$mailer=new Model_Comunicator();
								$notity=$mailer->sendEmail(array(
									'fromName'=>'Rezervacie -'.$cl->name,
									'from'=>$cl->email,
									'to'=>$post["email"],
									'subject'=>'Vaša rezervácia bola odoslaná',
									'body'=>'Odoslali sme Vašu rezerváciu. O jej stave budete informovaný prostredníctvom emailu. Ďakujeme'
								));
								if($notify) $this->addMessage("Odoslali sme Vám email.");
								$this->_redirect($this->view->url(array('action'=>'success','center'=>$ce->hash,'client'=>$cl->hash,'object'=>null,'timestamp'=>null),'widget'));
							}

						}else{
							$this->addMessage("form.is.not.valid","bg-pomegranate");
						}
            
			}
			
			if($this->_getParam("accept")){
				$times=$rtime->fetchAll(array('where'=>'t.reservations_id="'.$reservation->id.'"'));
				foreach($times as $k=>$v){
					$isValid=$rtime->fetchAll(array('where'=>'t.timestamp="'.$v->timestamp.'" AND t.object_id="'.$v->object_id.'" AND r.status="1"'));
					//Zend_Debug::dump($isValid);
					if($isValid){
						$toDelete=new Model_Reservationtime($v->id);
						$toDelete->delete();
					}
				}	
				$form->populate($reservation->getData());
			}
			$this->view->myreservations=$times=$rtime->fetchAll(array('where'=>'t.reservations_id="'.$reservation->id.'"'));
			$this->view->form=$form;
			
		}
	
		function summaryAction(){
			$params=$this->getRequest()->getParams();
      //Zend_Debug::dump($params);
      $session=session_id();
      $client=new Model_Client();
      $center=new Model_Center();
      $object=new Model_Object();
      $this->view->client=$cl=$client->getClientByHash($params);            
      $this->view->center=$ce=$center->getCenterByHash($params); 
      $this->view->object=$ob=$object->getByHash($params);
			
			if(!$client->isEmpty() || !$center->isEmpty()){  
          $this->view->intervals=$center->fetchOpeningHours(array('output'=>'array','where'=>'i.center_id="'.$center->id.'" AND i.client_id="'.$center->client_id.'" AND i.start < "'.$params["currentDay"].'" AND i.end > "'.$params["currentDay"].'" AND h.wday="'.date("w",strtotime($params["currentDay"])).'" ')); 
          $this->view->objects=$center->fetchObjects();
          $resTime=new Model_Reservationtime();
          $where[]=array('t.client_id = "'.$client->id.'"');
          $where[]=array('t.center_id = "'.$center->id.'"');
          $where[]=array('t.date = "'.$params["currentDay"].'"');
          $where[]=array('r.status < 3 ');          
          $options["output"]='array';
					if($this->user):
					$where2[]=array('r.user_id="'.$this->user->id.'"');
					else:
          $session=session_id();
          $where2[]=array('r.session="'.$session.'"');
					endif;
          $where2[]=array('t.client_id = "'.$client->id.'"');
          $where2[]=array('t.center_id = "'.$center->id.'"');          
          $where2[]=array('r.status = 3 ');
          $options["where"]=$where2;
          $options["output"]='array';
          //$options["test"]=1;         
					$myreservations["all"]=$resTime->fetchAll($options);
					$where2[]=array('t.date = "'.$params["currentDay"].'"');
					$options["where"]=$where2;
					$myreservations[$params["currentDay"]]=$resTime->fetchAll($options);
					$this->view->myreservations=$myreservations;
          
			}else{
				if($params["client"]){
					 $this->_redirect($this->view->url(array('client'=>$params["client"]),'client'));
				}else{
					 $this->_redirect("/");
				}         
			}
			$this->view->params=$params;
			
		}
	
		function successAction(){
			$params=$this->getRequest()->getParams();
      //Zend_Debug::dump($params);
      $session=session_id();
      $client=new Model_Client();
      $center=new Model_Center();
      $this->view->client=$cl=$client->getClientByHash($params);            
      $this->view->center=$ce=$center->getCenterByHash($params);
			
		}
    
    
    
    
}