<?php

 
class CalendarController extends Matj_Bootstrap3_Action_BackendSession
{
           
    
    /** od akej user / group_id je povoleny vstup */
    protected $auth_group = 5; 
    
    function init(){
			parent::init();
			$this->view->client=$this->clients;
		}
    
    function indexAction(){
        $this->showHeader=false;
				$params=$this->getRequest()->getParams();
				$session=session_id();
				$this->view->client=$this->clients;
				if(empty($params["currentDay"])) $params["currentDay"]=date("Y-m-d"); 
				$resTime=new Model_Reservationtime();
				$reservations=new Model_Reservation();
        $where[]=array('t.client_id = "'.$this->clients->id.'"');
        $where[]=array('t.date = "'.$params["currentDay"].'"');
        $where[]=array('r.status < 3 ');
        $options["where"]=$where;
        $options["output"]='array';
			  $opt["where"]=$where;
				$opt["group"]=array('r.id');
        $this->view->reservations=$resTime->fetchAll($options);				
				$this->view->admintable=$reservations->fetchAll($opt);
				$this->view->params=$params;
				unset($options);
        $where2[]=array('t.client_id = "'.$this->clients->id.'"');
				$where2[]=array('r.session="'.$session.'"');          
        $where2[]=array('r.status = 3 ');
        $options["where"]=$where2;
        $options["output"]='array';  
				//$options["test"]=1;
				$myreservations["all"]=$resTime->fetchAll($options);
				$where2[]=array('t.date = "'.$params["currentDay"].'"');
				$options["where"]=$where2;
				$myreservations[$params["currentDay"]]=$resTime->fetchAll($options);
				$form=new Form_OrderUseradmin();
				if(!empty($myreservations[$params["currentDay"]])){
					$this->view->form=$form;
				}
			
				if($this->_isPost()){
            if($this->_getPost('back')==1){
                $this->_redirect($this->view->url(array('action'=>'center','center'=>$ce->hash,'client'=>$cl->hash,'object'=>null,'timestamp'=>null),'center'));
            }            
						if($form->isValid($this->_getPost())){
							$user=new Model_User();
							$post=$this->_getPost();
							$reservation=new Model_Reservation();
							$reservation->getReservationBySession(array("session"=>$session));
							//Zend_Debug::dump($post);exit;							
							$reservation->edited=date("Y-m-d H:i:s");
							/* Ak chce vytvorit konto */
							$checkout=false;
							foreach($myreservations[$params["currentDay"]] as $k=>$v){
								$rtime=new Model_Reservationtime();
								$isValid=$rtime->fetchAll(array('where'=>'t.timestamp="'.$v["timestamp"].'" AND t.object_id="'.$v["object_id"].'" AND r.status="1"'));
								//Zend_Debug::dump($isValid);
								if($isValid){
									//$reservations[]=$v;
									$checkout=true;
								}
							}
							//Zend_Debug::dump($checkout);exit;
							if($checkout){
								$this->addMessage("object.is.already.booked","error");
								$reservation->status=3;
								$reservation->save();
							}else{
									$user=new Model_User();
									$unique=false;
									if($post["email"]){
										$unique=$user->isUniqueEmail($this->_getPost());
									}
									if($unique){
										$user->setDataFromPost($this->_getPost());
										$user->save();
										$reservation->first_name=$user->first_name;
										$reservation->last_name=$user->last_name;
										$reservation->phone=$user->phone;
										$reservation->email=$user->email;
									}else{
										/* pokračujem zapisanim udajov bez konta */
										$reservation->first_name=$post["first_name"];
										$reservation->last_name=$post["last_name"];
										$reservation->phone=$post["phone"];
										$reservation->email=$post["email"];

									}
								$reservation->status=1;	
								$reservation->user_id=$this->user->id;
								$reservation->session="";
								$reservation->description=$post["description"];
								$reservation->save();
								try{
									$orderH=new Model_OrderHistory();
									$orderH->user_id=$reservation->user_id;
									$orderH->reservation_id=$reservation->id;
									$orderH->status=$reservation->status;
									$orderH->created=date("Y-m-d H:i:s");
									$orderH->save();
								}catch(Exception $e){
									//Zend_Debug::dump($e);exit;
								}
								$this->addMessage("reservation.is.saved.succesfully","success");
								$this->_redirect($this->view->url(array('action'=>'index','controller'=>'calendar'),'default'));
							}

						}else{
							$this->addMessage("form.is.not.valid","bg-pomegranate");
						}
            
				}
				$this->view->myreservations=$myreservations;
    }
	
		function reservationAction(){
      $params=$this->getRequest()->getParams();
      //Zend_Debug::dump($params);
      $session=session_id();
      $client=new Model_Client();
      $center=new Model_Center();
      $object=new Model_Object();
      $this->view->client=$cl=$client->getClientByHash($params);            
      $this->view->center=$ce=$center->getCenterByHash($params); 
      $this->view->object=$ob=$object->getByHash($params);
      $check=false;
      $resTime=new Model_Reservationtime();
      $where[]=array('t.object_id = "'.$ob->id.'"');
			$where[]=array('t.date = "'.date("Y-m-d",$params["timestamp"]).'"');
      $where[]=array('t.timestamp = "'.$params["timestamp"].'"');
      $where[]=array('r.status < 3');
      $options["where"]=$where;
      if($resTime->fetchAll($options)){
        $check=true;
      }else{
				unset($resTime);
			}
      //Zend_Debug::dump($resTime->fetchAll($options));exit;
      if($check){
        $this->addMessage("sorry.therm.is.already.booked","danger");
      }else{
        $reservation=new Model_Reservation();
        $reservation->getReservationBySession(array("session"=>$session));
        if($reservation->isEmpty()){
          $reservation->client_id=$cl->id;
          $reservation->session=$session;
          $reservation->center_id=$ce->id;
          $reservation->object_id=$ob->id;
          $reservation->created=date("Y-m-d H:i:s");
          $reservation->ip=Matj_Util::getIp();
					$reservation->hash=md5($session.date("Y-m-d H:i:s").$ob->id.$ce->id.$cl->id);
          $reservation->status=3;
					$reservation->color='#'.Mufin_Color::random_color();
          $reservation->save();
					try{
						$orderH=new Model_OrderHistory();
						$orderH->user_id=$this->user->id;
						$orderH->reservation_id=$reservation->id;
						$orderH->status=$reservation->status;
						$orderH->created=date("Y-m-d H:i:s");
						$orderH->save();
					}catch(Exception $e){
						//Zend_Debug::dump($e);exit;
					}
        }
        $resTime=new Model_Reservationtime();
        $resTime->session=$session;
        $resTime->reservations_id=$reservation->id;
        $resTime->date=date("Y-m-d",$params["timestamp"]);
        $resTime->timestamp=$params["timestamp"];
        $resTime->price=$params["price"];
        $resTime->object_id=$ob->id;
        $resTime->center_id=$ce->id;
        $resTime->client_id=$cl->id;
				$resTime->hash=md5($session.$params["timestamp"].$ob->id.$ce->id.$cl->id);
        $resTime->save();            
        
      }
      //exit;
      $this->_redirect(url(array('action'=>'index','controller'=>'calendar','currentDay'=>$params["currentDay"],'timestamp'=>null,'client'=>null,'center'=>null,'price'=>null,'object'=>null),'default'));
    }
	
		function removeAction(){
			$params=$this->getRequest()->getParams();
      //Zend_Debug::dump($params);
      //$session=session_id();      
      $resTime=new Model_Reservationtime();
			$resTime->getByHash($params["id"]);
      if(!$resTime->isEmpty()){
        $resTime->delete();
      }     
      $this->_redirect(url(array('action'=>'index','id'=>null,'controller'=>'calendar'),'default'));
    }
	
    function confirmAction(){
			$params=$this->getRequest()->getParams();
			$reservation=new Model_Reservation();
			$reservation->getByHash($params["id"]);
			if(!$reservation->isEmpty()){
				
        $reservation->status=1;
				$reservation->edited=date("Y-m-d H:i:s");
				$reservation->save();			
				try{
					$orderH=new Model_OrderHistory();
					$orderH->user_id=$this->user->id;
					$orderH->reservation_id=$reservation->id;
					$orderH->status=$reservation->status;
					$orderH->created=date("Y-m-d H:i:s");
					$orderH->save();
				}catch(Exception $e){
					//Zend_Debug::dump($e);exit;
				}
				$data=$reservation->fetchAll(array('where'=>'r.id="'.$reservation->id.'"'));
				//Zend_Debug::dump($data);exit;
				$mailer=new Model_Comunicator();
				$client=new Model_Client($reservation->client_id);
				$center=new Model_Center($reservation->center_id);
				if($reservation->email){
					$mailer=new Model_Comunicator();
					$notify=$mailer->sendEmail(array(
							'fromName'=>'Rezervacie - '.$client->name,
							'from'=>$client->email,
							'to'=>$reservation->email,
							'subject'=>'Vaša rezervácia bola potvrdená',
							'body'=>'Potvrdili sme Vašu rezerváciu v stredisku '.$client->name.' na '.$data[0]->center_name.' - '.$data[0]->object_name.' '.date("d.m. H:i",$data[0]->start).'. Ďakujeme a prajeme príjemný športový zážitok.<br> Tím '.$client->name
					));
					//Zend_Debug::dump($notify);exit;
					if($notify) $this->addMessage("Odoslali sme potvrdzovací email zákazníkovi.","success");
				}
      } 
			$this->_redirect(url(array('action'=>'index','id'=>null,'controller'=>'calendar'),'default'));
		}
	
		function cancelAction(){
			$params=$this->getRequest()->getParams();
			$reservation=new Model_Reservation();
			$reservation->getByHash($params["id"]);
			if(!$reservation->isEmpty()){
				
        $reservation->status=4;
				$reservation->edited=date("Y-m-d H:i:s");
				$reservation->save();
				
				try{
					$orderH=new Model_OrderHistory();
					$orderH->user_id=$this->user->id;
					$orderH->reservation_id=$reservation->id;
					$orderH->status=$reservation->status;
					$orderH->created=date("Y-m-d H:i:s");
					$orderH->save();
				}catch(Exception $e){

				}
				
				$data=$reservation->fetchAll(array('where'=>'r.id="'.$reservation->id.'"'));
				//Zend_Debug::dump($data);exit;
				$mailer=new Model_Comunicator();
				$client=new Model_Client($reservation->client_id);
				$center=new Model_Center($reservation->center_id);
				if($reservation->email){
					$mailer=new Model_Comunicator();
					$notify=$mailer->sendEmail(array(
							'fromName'=>'Rezervacie - '.$client->name,
							'from'=>$client->email,
							'to'=>$reservation->email,
							'subject'=>'Vaša rezervácia bola zrušená',
							'body'=>'Zrušili sme Vašu rezerváciu v stredisku '.$client->name.' na '.$data[0]->center_name.' - '.$data[0]->object_name.' '.date("d.m. H:i",$data[0]->start).'. <br> Tím '.$client->name
					));
					//Zend_Debug::dump($notify);exit;
					if($notify) $this->addMessage("Odoslali sme email o zrušení rezervácie zákazníkovi.","success");
				}
      } 
			$this->_redirect(url(array('action'=>'index','id'=>null,'controller'=>'calendar'),'default'));
		}
		
	
	
	
    function ajaxAction(){   
        
        $params=$this->getRequest()->getParams();
				$session=session_id();
				$resTime=new Model_Reservation();
        
        $where[]=array('t.client_id = "'.$params["client"].'"');
				$where[]=array('t.object_id = "'.$params["object"].'"');
				$where[]=array('t.center_id = "'.$params["center"].'"');
				$where[]=array('t.timestamp = "'.$params["timestamp"].'"');
				
      	//$where[]=array("r.object_id < %s",$params["end"]." 00:00:00");
        
        //$options["user"]=$this->user;
			
        
        $options["where"]=$where;
				//$options["test"]=1;        
        $order=new Model_Reservation();
        $data=$resTime->fetchTimeStamp($options);
				//Zend_Debug::dump($data);
        if(empty($data)){
					$reservation=$order->getReservationBySession(array("session"=>$session));
					if($reservation){
						$params["session"]=$session;
						$params["reservation_id"]=$reservation->id;
						$reservation->saveTimestamp($params);
						$json["client"]=$params["client"];
						$json["reload"]=true;
					}					
				}else{
					if($data->status==3){
						$resTime=new Model_Reservationtime($data->id);
						$resTime->delete();
						$json["client"]=$params["client"];
						$json["reload"]=true;
					}else{
						$json["message"]=$this->translate("already.reserved");
						$json["reload"]=true;
					}
				}        
        
        
        
        $this->json($json);
                
    }
    
    
    function ajaxstatusAction(){
        
        $order=new Model_Order();
        
        
        $this->json($order->getAjaxStatus($this->_getParam('last')+0));
        
        
    }
    
    function ajaxcarcodeAction(){
        
        $order=new Model_Order();
        $this->json(array('data'=>$order->findCarcodes($this->_getParam('q'))));
        
        
    }
    
    function ajaxcustomerAction(){
        
        $q=$this->view->escape($this->_getParam('q'));
        if($q!=null){
            $order=new Model_Order();
            $data=$order->findCustomers($q);
            $data[]=array('value'=>"add",'name'=>$q,'title'=>"Vytvoriť nový záznam - '".$q."'");
            return $this->json(array('data'=>$data));
        }
        
        $id=$this->view->escape($this->_getParam('id'));
        if($id>0){
            $customer=new Model_Customer($id);
            return $this->json(array('customer'=>$customer->getApiData()));
        }
        
        
        
    }
    
    function ajaxdelayedAction(){
        $order=new Model_Order();
        $data=$order->getDelayed();
        $table=new Table_Orders();
        $table->setOptions(array('form' => false,
		'header' => true,
		'sortable' => false,'filter'=>false,'paginator'=>false));
        $table->setData($data);
        
        $table->removeColumn('id');
        $table->removeColumn('created');
        
        
        $this->html($table.'');
    }
    
    function ajaxinareaAction(){
        $order=new Model_Order();
        $data=$order->getInArea();
        $table=new Table_Orders();
        $table->setOptions(array('form' => false,
		'header' => true,
		'sortable' => false,'filter'=>false,'paginator'=>false));
        $table->setData($data);
        
        $table->removeColumn('id');
        $table->removeColumn('created');
        
        
        $this->html($table.'');
    }
    
    
    
    
    function addunknownAction(){
        
        
        $order=new Model_Order();
       
        
        $order->authUser=$this->user;
        
        $form=new Form_OrderUnknown();
        $form->addRamps($order->getRamps());
        $form->addStatuses($order->getStatuses(array(array('groups LIKE "%'.$this->user->group_id.'%"'))));
        $form->setAction(url(array('delete'=>null,'copy'=>null)));
        
        
        
        if($this->_isPost()){
            if($form->isValid($this->_getPost())){
                $data=$form->getValues();
                
                //Zend_Debug::dump($data);exit;
                
                $order->setDataFromForm($data);
                $id=$order->getId();
                
                try{
                    if($order->save()){
                        $this->addMessage('core.form.saved','success');
                        if(empty($id)){
                            $order->addHistory(null,array('status'=>$order->status));
                        }
                        if(!empty($data["addnote"]))
                            $order->addHistory($data["addnote"]);

                        if(!$this->_getParam('ajax')){
                            $this->_redirect(url(array('id'=>null,'action'=>null,'controller'=>'calendar'),null,true));
                        }
                        else{
                            $form="<script> hideEditForm(); </script>";
                        }
                    }   
                }
                catch(Exception $e){
                    $this->addMessage('core.form.notvalid','danger');
                }
            }
            else{
                $this->addMessage('core.form.notvalid','danger');
            }
            
        }
        $this->view->form=$form;
    }
    
    
    
    public function listAction () {
            if ($this->getRequest()->isXmlHttpRequest()) {
                    return $this->_forward('ajaxlist');
            }

            return $this->ajaxlistAction();
    }
    
    
    
    
    /**
     *
     *
     */
    public function ajaxlistAction () {
            $table = new Table_Orders();
            $order=new Model_Order();
            $table->order=$order;
            $options=$table->getModelOptions();
            $options["user"]=$this->user;
        
            
            $data=$order->fetchAll($options);

            $table->setDataCount($order->getFoundRows());
            $table->setData($data);
            $this->view->table = $table;



            //echo $table;exit;


    }

    /**
     * 
     * @param Model_Order $order
     * @return Form_Order
     */
    public function _getOrderForm($order) {
        
        
        if($this->user->group_id>3){
            $form=new Form_OrderSimple();
            $form->addStatuses($order->getStatuses(array(array('groups LIKE "%'.$this->user->group_id.'%"'))));
            if($this->user->group_id==4){//sklad
                $form->carcode
                     ->setAttrib('disabled','disabled')
                     ->setIgnore('ignore',true);
            }
        }
        else{
            $form=new Form_Order();
            $customer=new Model_Customer();
            $form->addCustomers($customer->fetchAll(array('order'=>array('name'))));
            $form->addStatuses($order->getStatuses());

        
        }
        $form->order=$order;
        $form->setAction(url(array()));
        $form->addRamps($order->getRamps());
        
        return $form;
    }

}

