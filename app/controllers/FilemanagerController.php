<?php

class FilemanagerController extends Matj_Bootstrap3_Action_BackendSession
{
    
    protected $auth_group = 3;
    protected $client;
    
    function init(){
			parent::init();
			$this->client=$this->clients;
			
		}
  
    function indexAction(){
       
        if($this->_getParam('callback')){
            $this->_setParam('ajax',true);
            $this->view->callback=$this->_getParam('callback');
        }
        
        $this->showHeader=false;
    }
    
    
    function galleryAction(){
        $form=new Matj_BootstrapLayout_Form;
        $form->addElement('gallery','gallery',array('json'=>false));
        $form->addButtons();
        
        $this->view->form=$form;
        $form->isValid($this->_getPost());
        
        
        Zend_Debug::dump($form->getValues());
        
        
        
    }
    
    
    function connectorAction(){


        //error_reporting(0); // Set E_ALL for debuging

        $elFinderDirectory=PUBLIC_PATH.'/external/elfinder/php/';
        
        include_once $elFinderDirectory.'elFinderConnector.class.php';
        include_once $elFinderDirectory.'elFinder.class.php';
        include_once $elFinderDirectory.'elFinderVolumeDriver.class.php';
        include_once $elFinderDirectory.'elFinderVolumeLocalFileSystem.class.php';
        
        
        
        $settings=$this->_uploadSettings();
        

        $opts = array(
                // 'debug' => true,
                'roots' => array(
                        array(
                                'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
                                'path'          => $settings["folder"],         // path to files (REQUIRED)
                                'URL'           => $settings["url"], // URL to files (REQUIRED)
                                'accessControl' => 'fileAccess'             // disable and hide dot starting files (OPTIONAL)
                        )
                )
        );

        // run elFinder
        $connector = new elFinderConnector(new elFinder($opts));
        $connector->run();
 
    }

    public function _uploadSettings() {
        
        if($this->clients){
          
            if(empty($this->clients)){
                throw new Exception('folder name empty');
            }
            
            $conf=Matj_Get::getBootstrap()->getOption('upload');
            
            
            
            $settings["quarantine"]=rtrim($conf["folder"],"/").'/.quarantine';
            $settings["path"]=rtrim($conf["folder"],"/").'/usr/'.$this->clients->hash;
            $settings["url"]=rtrim($conf["url"],"/").'/usr/'.$this->clients->hash;
            
            if(!is_dir($settings["path"])){
                $status=mkdir($settings["path"],0777,true);
            }
            
            $settings["folder"]=  realpath($settings["path"]);
            
            
            return $settings;

        }
        
        return Matj_Get::getBootstrap()->getOption('upload');
    }

}


        
        function fileAccess($attr, $path, $data, $volume) {
                return strpos(basename($path), '.') === 0       // if file/folder begins with '.' (dot)
                        ? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
                        :  null;                                    // else elFinder decide it itself
        }

