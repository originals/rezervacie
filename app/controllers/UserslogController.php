<?php

 
class UserslogController extends Matj_Bootstrap3_Action_BackendSession
{
            
    protected $auth_group = 3;
    
    public function indexAction () {
		if ($this->getRequest()->isXmlHttpRequest()) {
			return $this->_forward('ajaxindex');
		}

		return $this->ajaxindexAction();
	}
	/**
	 *
	 *
	 */
	public function ajaxindexAction () {
		$table = new Table_UsersLog();
		$users=new Model_User();
		
                $options=$table->getModelOptions();
                
                $options["where"]=Matj_Util::joinWheres($options["where"],'group_id <> 1 AND client_id="'.$this->clients->id.'"');
                
		$data=$users->fetchUsersAccessLog($options);
                
                $table->setData($data);
		$table->setDataCount($users->getFoundRows());
		$this->view->table = $table;
	}
        
	public function historyAction () {
		$table = new Table_OrdersHistory();
		$model=new Model_OrderHistory();
		
                $options=$table->getModelOptions();
                
               // $options["where"]=Matj_Util::joinWheres($options["where"],'group_id <> 1');
                
		$data=$model->fetchAll($options);
                
                $table->setData($data);
		$table->setDataCount($model->getFoundRows());
		$this->view->table = $table;
	}
        

}

