<?php

 
class UploadController extends Matj_Bootstrap3_Action
{
            
    function preDispatch() {
        parent::preDispatch();
    }
    
    function init(){
         
        parent::init();
        $this->view->layout()->setLayout('default');
    }

     
    
    
    public function imageAction()
    {	
        
       $settings=$this->auth->getSession()->uploadSettings; 
        
       if(empty($settings["path"]))throw new Exception ('Wrong upload settings');
       
       $key="file";
       
       
       $name=$this->_getParam('name');
       $crop=$this->_getParam('crop');
       $filename=$this->_getParam('filename');
       
       
       if(!empty($_FILES[$key]["name"])){ 
           
        
        if($filename=="random")   
            $settings["name"]=uniqid(date('ymdhis-'));
        elseif(strlen($filename>0))
            $settings["name"]=$filename;
        else
            $settings["name"]=$name;
        
        
        $info = $this->_upload($key,$settings);
        
        
        if($info["status"]==1){
            
             
            if($crop){
                $size=getimagesize($info["path"]);
                
                $cropSize=explode("x",$crop);
                
                if($cropSize[0]!=$size[0] || $cropSize[1]!=$size[1]){
                    $this->auth->getSession()->cropSettings=$info; 
                    return $this->_redirect(url(array('action'=>'crop','crop'=>$crop,'name'=>$name)));
                }
                
                $this->auth->getSession()->selectImage=$info; 
                return $this->_redirect(url(array('action'=>'select','crop'=>$crop,'name'=>$name)));
                
            }
            else{
                $this->auth->getSession()->selectImage=$info; 
                return $this->_redirect(url(array('action'=>'select','crop'=>$crop,'name'=>$name)));
            }
            
        }
        else{
            $this->addMessage($info["errors"],'danger');
        }
        
        
       }
       
       $this->view->layout()->setLayout('iframe'); 
       
    }
    
    
    function cropAction(){
        $config=$this->auth->getSession()->cropSettings; 
        $this->view->config=$config;
        
        
        $crop=$this->_getParam('crop','100x100');
        $crop=explode("x",$crop);
        $this->view->ratio=$crop[0]/$crop[1];
        $name=$this->_getParam('name');
       
        $this->view->layout()->setLayout('iframe'); 

        if($this->_isPost()){
            $targ_w = $crop[0];
            $targ_h = $crop[1];
            $jpeg_quality = 90;

            $src = $config["path"];
            
            if(preg_match('/png$/',strtolower($config["path"])))
                $img_r = imagecreatefrompng($src);
            else
                $img_r = imagecreatefromjpeg($src);
            
            $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

            imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
                $targ_w,$targ_h,$_POST['w'],$_POST['h']);

            header('Content-type: image/jpeg');
            imagejpeg($dst_r, $config["path"], $jpeg_quality);
            
            $this->auth->getSession()->selectImage=$config; 
            return $this->_redirect(url(array('action'=>'select','crop'=>$crop,'name'=>$name)));
        }
        
        
        
    }
    
    function selectAction(){
        $config=$this->auth->getSession()->selectImage; 
        $this->view->name=$this->_getParam('name');
        $this->view->config=$config;
        $this->view->layout()->setLayout('iframe'); 
        
        
        $this->auth->getSession()->cropSettings=null;
        //$this->auth->getSession()->uploadSettings=;
        //$this->auth->getSession()->selectImage=null;
        
        
    }
    
    
    
    
    
    
        
    function _upload($key="file",$config=null){
            
        $key="file";

        
        if(!file_exists($config["path"])){
            mkdir($config["path"], 0777,true);
        }
        
        $folder=new Matj_File_Folder($config["path"]);
    
        
        
        
        
        $upload = new Zend_File_Transfer();

        // Set a file size with 20 bytes minimum and 20000 bytes maximum
        $upload->addValidator(new Matj_Validate_File_Size(array('min' =>$config["minSize"], 'max' => $config["maxSize"])));
        $upload->addValidator(new Matj_Validate_File_Extension('jpg,jpeg,png'));
        $upload->setDestination($folder->getUrl(true));



        $info=$upload->getFileInfo();


        $x=explode(".",$info[$key]["name"]); 
        $ext=str_replace('jpeg','jpg',strtolower($x[count($x)-1]));
        $return[$key]["oldName"]=$info[$key]["name"];

        $name=date("YmdHis-").uniqid().'.'.$ext;
        if(!empty($config['name']))
            $name=$config['name'].'.'.$ext;
        
        
        //Zend_Debug::dump($folder->getUrl(true));exit;
        
        $upload->addFilter('Rename',array('target'=>$folder->getUrl(true).'/'.$name,'overwrite'=>true));

        $return["status"]=0;

        if ($upload->receive()) {
            foreach($upload->getFileInfo() as $k=>$d){
               $return["file"]=$d["name"];
               $return["destination"]=$d["destination"];
               $return["status"]=1;
               $return["path"]=$d["destination"].'/'.$d["name"];
               
               if(!empty($config["url"]))
                   $return["url"]=rtrim($config["url"],"/")."/".$name;
               
            }

        } else {
            $this->view->errors=$upload->getMessages();
            foreach($this->view->errors as $error)
                    @$return["errors"].=$error." ";

        }

        return $return;
        $this->_setParam('json',true);
    }
     
    
}

