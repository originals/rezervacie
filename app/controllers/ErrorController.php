<?php

class ErrorController extends Zend_Controller_Action
{
	
    protected $_authEnabled = false;

    public function init()
    {
    /* Initialize action controller here */
            parent::init();
            $this->_helper->layout()->setLayout('backend');
            Matj_Get::getView()->layout()->setLayoutPath(APPLICATION_PATH.'/layouts');
    }
	 
    public function errorAction()
    {
    
    	//Header("HTTP/1.0 204 No Content");
		$errors = $this->_getParam('error_handler');
        
        if (!$errors) {
            $this->view->message = 'You have reached the error page';
            return;
        }
        
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
        
                // 404 error -- controller or action not found
              //  $this->getResponse()->setHttpResponseCode(204);
                $this->view->message = 'Page not found';
                break;
            default:
                // application error
             //   $this->getResponse()->setHttpResponseCode(500);
                $this->view->message = 'Application error';
                break;
        }
        
        // Log exception, if logger available
        if ($log = $this->getLog()) {
            $log->crit($this->view->message, $errors->exception);
        }
        
        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }

       
        //Header("HTTP/1.0 204 No Content");
        $this->view->request   = $errors->request;

    //Zend_Debug::dump($errors->exception);exit;
        
    
    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }


}

