<?php

class CronController extends Matj_Bootstrap3_Action {
    
    function init(){
        parent::init();
        $this->_setParam('json',true);
    }
    
    
    function indexAction(){
        
        
        $order=new Model_Order();
        
        $customer=new Model_Customer(7);
        $user=new Model_User(10);
            
        
        for($i=1;$i<=7;$i++){
            
            $day=  Matj_Util::addDate(date('Y-m-d'),array('day'=>$i));
            
            
            $where=array();
            $where[]=array('time >= %s',$day.' 00:00:00');
            $where[]=array('time <= %s',$day.' 23:59:59');
            $where['ramp']='nakladka5';
            $where['customer_id']=7;
            
            
            $orders=$order->fetchAll(array('where'=>$where));
            
            $dayOfWeek=date('N',strtotime($day));
            
            
            
            Zend_Debug::dump($day);
            Zend_Debug::dump($dayOfWeek);
            Zend_Debug::dump(count($orders));
            
            $hours=array();
            if(count($orders)<15){
                //budeme generovat
                if($dayOfWeek<=5){
                    //pracovny den
                    $start=$day.' 06:00:00';
                    
                    for($h=0;$h<15;$h++){
                        $hours[]=Matj_Util::addDate($start,array('minute'=>$h*30),'Y-m-d H:i:s');
                    }
                    
                    $start=$day.' 14:00:00';
                    for($h=0;$h<15;$h++){
                        $hours[]=Matj_Util::addDate($start,array('minute'=>$h*30),'Y-m-d H:i:s');
                    }
                    
                    $start=$day.' 22:00:00';
                    for($h=0;$h<14;$h++){
                        $hours[]=Matj_Util::addDate($start,array('minute'=>$h*30),'Y-m-d H:i:s');
                    }
                    
                    
                    
                }
                elseif($dayOfWeek==6){
                    //sobota
                    $start=$day.' 06:00:00';
                    
                    for($h=0;$h<10;$h++){
                        $hours[]=Matj_Util::addDate($start,array('minute'=>$h*45),'Y-m-d H:i:s');
                    }
                    
                    
                    $start=$day.' 14:00:00';
                    
                    for($h=0;$h<8;$h++){
                        $hours[]=Matj_Util::addDate($start,array('minute'=>$h*45),'Y-m-d H:i:s');
                    }
                    
                    
                }
                
                
            }
            
            
            
            if(!empty($hours)){
                foreach($hours as $h){
                    $order=new Model_Order;
                    $order->setData(array(
                                        'time'=>$h,
                                        'flag'=>'auto',
                                        'status'=>'vytvorena',
                                        'ramp'=>'nakladka5',
                                        'type'=>'in',
                                        'cartype'=>'lorry',
                                        'customer_id'=>$customer->getId(),
                                        'loadtime'=>$customer->loadtime,
                                        'priority'=>$customer->priority,
                                        'color'=>$customer->color,
                         ));
                    $order->save();
                    $order->addHistory(null,array('status'=>'vytvorena'),$user);
                    
                    
                }
                
                
            }
            
            
            Zend_Debug::dump($hours);
            
            
            
        }
        
        
        
        
    }
    
    
    
    
}