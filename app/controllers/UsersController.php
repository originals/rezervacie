<?php

 
class UsersController extends Matj_Bootstrap3_Action_BackendSession
{
            
    protected $auth_group = 2;
    
    public function indexAction () {
		if ($this->getRequest()->isXmlHttpRequest()) {
			return $this->_forward('ajaxindex');
		}

		return $this->ajaxindexAction();
	}
	/**
	 *
	 *
	 */
	public function ajaxindexAction () {
		$table = new Table_Users();
		$users=new Model_User();
		
    $options=$table->getModelOptions();
    $options["where"]=Matj_Util::joinWheres($options["where"],'g.level BETWEEN 1 AND 5 AND g.id > 1');
                //$options["test"]=1;
		$data=$users->fetchAll($options);
                
                $table->setData($data);
		$table->setDataCount($users->getFoundRows());
		$this->view->table = $table;

                
                
		//echo $table;exit;


	}
        
        
        function addAction(){
            $this->_forward('edit');
        }

	function editAction () {
		$form = new Form_User();
		
                $object = new Model_User($this->_getParam('id') + 0);
		
                $form->setGroups($object->getGroups(array('order'=>array('id DESC'),'where'=>array(array('id>1 AND level <= 5')))));
                $form->setClients($object->getClients(array('order'=>array('name DESC'))));
                
                
                $id=(int)$object->getId();
                
		if ($this->getRequest()->isPost()) {
			if($this->getRequest()->getPost("cancel")){
				return $this->_redirect($this->view->url(array('action'=>'index','id'=>null)));
			}
			if ($form->isValid($this->getRequest()->getPost())) {
				 
                                $data=$form->getValues();
                               // $data["group_id"]=3;
                                
                                if(!empty($data["password"]))
                                    $data["password"]=md5($data["password"]);
                                else
                                    unset($data["password"]);
                                
                                
				$object->setDataFromForm($data);
                                 
				$object->setId($id);
				$object->save();
				if($data["client_id"]){
					$access=new Model_UserJoins();
					$access->user_id=$object->id;	
					$access->client_id=$data["client_id"];
					try{
						$access->save();
					}catch(Exception $e){
						
					}
				}
				$this->addMessage('core.edit.success','success');
                                
                                if($this->_getParam('iframe')){
                                    $this->view->iframeCallback=$this->_getParam('iframeCallback');
                                    $this->view->iframeCallbackParam=$this->_getParam('iframeCallbackParam');
                                }
                                else
                                    return $this->_redirect($this->view->url(array('action'=>'index','id'=>null)));
			}
			else{
				$this->addMessage('core.edit.notValid','danger');
			}
		}
		else{
			$form->populate($object->getFormData());
		}
		$this->view->form = $form;
                
                $this->view->editUser=$object;
                
                
               
                
	}
        
        function deleteAction(){
            $object = new Model_User($this->_getParam('id') + 0);
            if($object->getId()>0){
                $object->delete();
                $this->addMessage('core.delete.success','success');
                
            }
            $this->_redirect(url(array('action'=>null,'id'=>null)));
        }
            
}

