<?php

 
class CommunicationController extends Matj_Bootstrap3_Action_BackendSession
{
           
    
    /** od akej user / group_id je povoleny vstup */
    protected $auth_group = 3;
       
    
    function init(){
        parent::init();
        
        head()->appendScriptFile('assets/backend/js/pages/customers.js');
        
    }
    
    
    
    function getjsonAction(){
        
        
        
        $object = new Model_Communication($this->_getParam('id') + 0);
        
        
        if(!$object->isEmpty() && !$this->auth->acl()->write($object)){
            return $this->_redirect($this->auth->getLoginUrl());
        }
        
        $this->json($object->getApiData());
    }
    
    
        
}

