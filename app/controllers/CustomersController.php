<?php

 
class CustomersController extends Matj_Bootstrap3_Action_BackendSession
{
           
    
    /** od akej user / group_id je povoleny vstup */
    protected $auth_group = 3;
       
    
    function init(){
        parent::init();
        
        head()->appendScriptFile('assets/backend/js/pages/customers.js');
        
    }
    
    function indexAction(){
       
        $table = new Table_Customers();
        $users=new Model_Customer();

        $options=$table->getModelOptions();
        $options["where"]=Matj_Util::joinWheres("g.level =5 ",$options["where"]);
        
        if($this->auth->getUser()->getGroup()->level>1)
            $options["user"]=$this->auth->getUser();
        
        $data=$users->fetchAll($options);

        $table->setData($data);
        $table->setDataCount($users->getFoundRows());
        $this->view->table = $table;


       
    }
    
    
    function viewAction(){
        $object = new Model_Customer($this->_getParam('id') + 0);
        if(!$this->auth->acl()->write($object)){
            return $this->_redirect($this->auth->getLoginUrl());
        }
        
        $form = new Form_Customer();
        $form->customer=$object;
        $form->populate($object->getFormData());
        
        $this->view->header=$object->company;
        $this->view->object=$object;
        $this->view->contacts=$object->getContacts();
        
        $employee=new Model_Employee;
        $this->view->communication=$object->getCommunication();
        $this->view->form=$form;
        
        
        if($this->auth->getUser()->getGroup()->level==1){
            $this->view->employees=$employee->fetchAll(array('where'=>array('g.level'=>2),'test'=>0));
            $this->view->access=$object->getAccess();
        }
        
        
    }
    
    function editcontactAction(){
        $object = new Model_Customer($this->_getParam('id') + 0);
        if(!$this->auth->acl()->write($object)){
            return $this->_redirect($this->auth->getLoginUrl());
        }
        
        if($this->_isPost()){
            
            
            //Zend_Debug::dump($this->_getPost());exit;
            
            
            $contact=new Model_Contact($this->_getPost('contact_id'));
            if(!$contact->isEmpty() && !$this->auth->acl()->write($contact)){
                throw new Exception('No access');
            }
            
            if($this->_getPost('delete')==1){
                if(!$contact->isEmpty())
                    $contact->delete();
            }
            else{
            
                $contact->setDataFromPost($this->_getPost());
                $contact->pid=$object->getId();

                $contact->save();
            }
        }
         
        $this->_redirect(url(array('action'=>'view')));
        
        
    }
    
    function editcommunicationAction(){
        $object = new Model_Customer($this->_getParam('id') + 0);
        if(!$this->auth->acl()->write($object)){
            return $this->_redirect($this->auth->getLoginUrl());
        }
        
        if($this->_isPost()){
            
            $communication=new Model_Communication($this->_getPost('communication_id'));
            if(!$communication->isEmpty() && !$this->auth->acl()->write($communication)){
                throw new Exception('No access');
            }
            
            if($this->_getPost('delete')==1){
                if(!$communication->isEmpty())
                    $communication->delete();
            }
            else{
            
                $communication->setDataFromPost($this->_getPost());
                $communication->customer_id=$object->getId();
                if(!$communication->user_id)$communication->user_id = $this->auth->getUser ()->getId();
                $communication->save();
            }
        }
         
        $this->_redirect(url(array('action'=>'view')));
    }
    
    function editaccessAction(){
        $object = new Model_Customer($this->_getParam('id') + 0);
        if(!$this->auth->acl()->write($object)){
            return $this->_redirect($this->auth->getLoginUrl());
        }
        
        if($this->_isPost()){
            
            $access=new Model_UserAccess($this->_getPost('access_id'));
            if(!$access->isEmpty() && !$this->auth->acl()->write(new Model_Customer($access->user2_id))){
                throw new Exception('No access');
            }
            
            
            
            
            if($this->_getPost('delete')==1){
                if(!$access->isEmpty())
                    $access->delete();
            }
            else{
                $access->user_id = $this->_getPost('user_id')+0;
                $access->user2_id = $object->getId();
                
                $access->save();
            }
        }
         
        $this->_redirect(url(array('action'=>'view')));
    }
    
    function getaccessjsonAction(){    
        $object = new Model_UserAccess($this->_getParam('id') + 0);
        if(!$object->isEmpty() && !$this->auth->acl()->write(new Model_Customer($object->user2_id))){
            return $this->_redirect($this->auth->getLoginUrl());
        }
        $this->json($object->getApiData());
    }
    
    
    function getcontactjsonAction(){
        $object = new Model_Contact($this->_getParam('id') + 0);
        if(!$object->isEmpty() && !$this->auth->acl()->write($object)){
            return $this->_redirect($this->auth->getLoginUrl());
        }
        
        $this->json($object->getApiData());
    }
    
    function addAction(){
        $this->_forward('edit');
    }

    function editAction () {
            $form = new Form_Customer();

            $object = new Model_Customer($this->_getParam('id') + 0);
            $form->customer=$object;
            
            
            
            $id=(int)$object->getId();
            
            
            if($this->_getParam('delete')){
                $object->delete();
                $this->addMessage('core.item.deleted','success');
                $this->_redirect(url(array('id'=>null,'delete'=>null,'action'=>null)));
            
            }

            
            
            if ($this->getRequest()->isPost()) {
                    if($this->getRequest()->getPost("cancel")){
                            return $this->_redirect($this->view->url(array('action'=>'index','id'=>null)));
                    }
                    if ($form->isValid($this->getRequest()->getPost())) {

                            $data=$form->getValues();
                            
                            //SKUPINA KLIENT
                            $data["group_id"]=11;
                            
                            
                            
                            $object->setDataFromForm($data);
                            
                            
                            
                            $object->setId($id);
                            $object->save();
                            
                            if(!$id && $this->auth->getUser()->getGroup()->level>1){
                                
                                $access=new Model_UserAccess;
                                $access->user_id=$this->auth->getUser()->getId();
                                $access->user2_id=$object->id;
                                $access->save();
                                
                            }
                            
                            
                            $this->addMessage('core.edit.success','success');

                            
                        //    Zend_Debug::dump($data);exit;
            
                            
                            
                            if($this->_getParam('iframe')){
                                $this->view->iframeCallback=$this->_getParam('iframeCallback');
                                $this->view->iframeCallbackParam=$this->_getParam('iframeCallbackParam');
                            }
                            else
                                return $this->_redirect($this->view->url(array('action'=>'view','id'=>$object->getId())));
                    }
                    else{
                        
                       // Zend_Debug::dump($form->getMessages());exit;
                            $this->addMessage('core.edit.notValid','danger');
                    }
            }
            else{
                    $form->populate($object->getFormData());
            }
            $this->view->form = $form;

            $this->view->customer=$object;




    }

    function deleteAction(){
        $object = new Model_Customer($this->_getParam('id') + 0);
        if($object->getId()>0){
            $object->delete();
            $this->addMessage('core.delete.success','success');

        }
        $this->_redirect(url(array('action'=>null,'id'=>null)));
    }
    
        
}

