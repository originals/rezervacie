<?php

class ToolsController extends Matj_BootstrapLayout_Action
{
    protected $_authEnabled=false;
    
    public function localeAction(){
    	$p=$this->_getPost();
        
        
        if(!Matj_Get::getFrontController()->hasPlugin("ZFDebug_Controller_Plugin_Debug"))
            throw new Exception('ZFDEBUG');
        
        //$table=new Matj_Translate_DbTable;
        $locale=$p["lang"];
        
        foreach($p["newKey"] as $k=>$d){
            if(!empty($d) && !empty($p["newValue"][$k])){
                $p["locale"][$d]=$p["newValue"][$k];
            }
        }
        
        
            foreach($p["locale"] as $k=>$d){
                
                if($k!=$d){
                    $model=new Model_Translations();
                    $data=array('messageId'=>$k,'locale'=>$locale,'message'=>$d);
                    //$res=$model->getCollection()->update(array('messageId'=>$k,'locale'=>$locale), $data, array('upsert'=>true));
                    if($model->fetchOne(array('messageId'=>$k,'locale'=>$locale))){
                        $res=$model->update($data, array('messageId'=>$k,'locale'=>$locale));
                    }
                    else{
                        $res=$model->insert($data);
                    }
                
                }
            }
        
            echo "ok";
            exit;
        
    }
    
    
    
    function uploaduserAction(){
        
        //todo dokoncit
        $user=Matj_Get::getAuthUser();
        
        $key="file";
        $return=array();
            
        $id=0;
        if($user)$id=$user->getId();
        $id=sprintf("%08d",$id);
        
        
        $config=Matj_Get::getBootstrap()->getOption('upload');
        $folder=new Matj_File_Folder($config["userfiles"]);
        $folder=$folder->getFolder($id,true);
        
        
        $upload = new Zend_File_Transfer();

        // Set a file size with 20 bytes minimum and 20000 bytes maximum
        $upload->addValidator(new Matj_Validate_File_Size(array('min' =>$config["minSize"], 'max' => $config["maxSize"])));
        $upload->addValidator(new Matj_Validate_File_IsImage(array()));
        $upload->setDestination($folder->getUrl(true));
        
        
        
        $info=$upload->getFileInfo();
        $x=explode("/",$info[$key]["type"]); 
        $ext=str_replace('jpeg','jpg',$x[1]);
        $return[$key]["oldName"]=$info[$key]["name"];
        
        $upload->addFilter('Rename',array('target'=>$folder->getUrl(true).'/'.date("YmdHis-").uniqid().'.'.$ext));
        
        
        if ($upload->receive()) {
            foreach($upload->getFileInfo() as $k=>$d){
               $file=new Matj_File_File($d["destination"]."/".$d["name"]);
               $return[$k]["file"]=$file->getPublicUrl();
               $return[$k]["name"]=$d["name"];
               $return[$k]["destination"]=$d["destination"];
               
               $db=array();
               $db["photo_name"]=$return[$k]["oldName"];
               $db["photo_image"]=$file->getPublicUrl();
               $db["user_id"]=$user->getId();
               $db["photo_created"]=date('Y-m-d H:i:s');
               $photo=new Core_Model_Photo($db);
               if($photo->save())
                   $return[$k]["photo_id"]=$photo->getId();
               else
                   echo "error";
            }
            Zend_Debug::dump($return);
            exit;
        } else {
            echo "notvalid";
            Zend_Debug::dump($upload->getMessages());
            exit;
        }
    }
    
    
    
    
    
    
    function qrcodeAction(){
        require_once 'Matj/Pdf/tcpdf/2dbarcodes.php';
        
        $text=$this->view->escape($this->_getParam('text'));
        $s=5;
        if($this->_getParam('size')>0)$s=$this->_getParam('size')+0;
        
        $qr=new TCPDF2DBarcode($text,'qrcode');
        $a=$qr->getBarcodePng($s,$s);
        $this->_setParam('json',true);
        //echo $a;exit;
        
        
    }
    
}

