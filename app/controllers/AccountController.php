<?php

 
class AccountController extends Matj_Bootstrap3_Action
{
            
    function preDispatch() {
        parent::preDispatch();
    }
    
    function init(){
         
        parent::init();
        $this->view->layout()->setLayout('backend');
    }

     
    
    
    public function loginAction()
    {	
        
        if($this->_getParam('client')){
          $client=new Model_Client();
          $this->view->client=$client->getClientByHash(array('client'=>$this->_getParam('client')));
				}
        
        
            $form=new Form_Login();
            
            if ($url = $this->_getParam('return')) {
                $this->auth->setReturnUrl($url);
            }

            if($this->_request->isPost()){
                if($this->_getPost('username')){  
                    if($form->isValid($this->_request->getPost())){
                         $data=$form->getValues();

                         $user=$this->auth->authenticate($data);
                         if(!$user){
                             $this->addMessage(implode("<br>",$this->auth->getErrors()),'danger');
                         }
                         else{
                             $user=$this->auth->getUser();
                             $url=$this->auth->getReturnUrl();
                             $this->auth->setReturnUrl('');
                             
                             
                             
                             $this->_redirect($url);
                         }
                     }
                     else{
                         $this->addMessage('user.login.error','danger');
                     }
                }
            
            
            if($this->_getPost('email')){  
           
                
                
                
                    
                    if($formRegistration->isValid($this->_getPost())){
                        
                        //Zemd_Debug::dump($eq);
                        
                        
                         $data=$formRegistration->getValues();
                         
                         
                         $user=new Model_User;
                         //$data["password"]=$user->encodePassword($user->$data["password"]);
                         //$user->setDataFromForm($data);
                         
                         
                         
                         if($user->createUser($data)){
                             $this->addMessage('core.registration.success','success');
                             $this->auth->forceLogin(new Model_User($user->getId()));
                             return $this->_redirect($this->auth->getReturnUrl());
                         }
                         else{
                             $this->addMessage('Neznáma chyba!','danger');
                         }
                     }
                     else{
                         //$this->addMessage('core.edit.notValid','danger');
                         //Zend_Debug::dump($formRegistration->getMessages());
                         foreach($formRegistration->getMessages() as $k=>$d){
                             
                             //Zend_Debug::dump($formRegistration->getElement($k));exit;
                             
                             $this->addMessage($formRegistration->getElement($k)->getLabel()." - ".implode($d),'danger');
                         }
                         
                     }
            }
            
            
            
            
            
            
        }
        else{
        }
        
    	
    	
        
        
        $this->view->form=$form;
        $this->view->formRegistration=$formRegistration;
        
       // echo $this->view->formRegistration."";exit;
        
        
        //$this->view->header=$this->translate('login.header');
        //$this->view->formRegistration=$formRegistration;
        
        $pathData=array();
        $pathData[]=array('text'=>'Úvod','href'=>'/');
        $pathData[]=array('text'=>'Profil','href'=>'/');
        $pathData[]=array('text'=>$this->view->header,'href'=>$this->view->url());
        $this->view->path=($this->view->ulLiMenu($pathData,array('class'=>'pull-right breadcrumb')));

    } 
    
    public function emailAction() {
        $formEmail=new Default_Form_Account_RegistrationEmail;
        
        if($formEmail->isValid($this->getRequest()->getPost())){
            $data=$formEmail->getValues();
                
            $user=new Default_Model_User;
            $user->findByEmail($data["user_email"]);
            
            if(!$user->isEmpty()){
                $this->addMessage("msg.account.emailExists",'error');
                return $this->_redirect($this->view->url(array('action'=>null),'login'));
            }
            else{
                $this->_getSession()->registrationEmail=$data["user_email"];
                return $this->_redirect($this->view->url(array('action'=>'registration'),'login'));
            }
            
        }
        else{
            $this->addMessage("msg.account.notValidLoginForm",'error');
            $this->_getSession()->revalid="formLogin";
            $this->_forward('index');
        }
        
    }
    
    
    function logoutAction(){
				if($url = $this->_getParam('return')) {
          $this->auth->setReturnUrl($url);
        }
				$url=$this->auth->getReturnUrl();
				//Zend_Debug::dump($url);exit;
    		$this->auth->logout();
        $this->addMessage('core.auth.isLogout',"success");
        return $this->_redirect($url);
    			      
    }
    
    
    function profileAction(){
        
        
        if(!$this->user || $this->user->isEmpty()){
            //$this->addMessage('msg.account.needLogin','info');
            return $this->_redirect($this->auth->getLoginUrl(url())); 
        }
     
        return $this->_redirect(url(array('action'=>'manageraces')));
        
        $this->view->header=$this->user->getFullName();
        
        $pathData=array();
        $pathData[]=array('text'=>'Úvod','href'=>'/');
        $pathData[]=array('text'=>'Profil','href'=>url(array(),'profile',true));
        $pathData[]=array('text'=>$this->view->header,'href'=>$this->view->url());
        $this->view->path=($this->view->ulLiMenu($pathData,array('class'=>'pull-right breadcrumb')));
    }
    
    
    function updateAction(){
        
        if(!$this->user || $this->user->isEmpty()){
            $this->addMessage('msg.account.needLogin','info');
            return $this->_redirect($this->auth->getLoginUrl(url())); 
        }
     
        
        $this->auth->getSession()->uploadSettings=$this->user->getUploadSettings();

        
        $this->view->header=$this->user->getFullName();
        
        $this->view->form=$form=new Default_Form_Account_ProfileUpdate;
        $element=$form->getElement("country");
        $country=new Model_Country();
        foreach ($country->fetchAll() as $c) {
            $element->addMultiOption($c->code, $c->name);
        }
        
        $form->populate($this->user->getFormData());
        
        
        
        if($this->_isPost())
            if($form->isValid($this->_getPost())){
                $data=$form->getValues();
                $this->user->setDataFromForm($data);
                if($this->user->save()){
                    $this->addMessage('core.edit.success','success');
                }
            }
            else{
                $this->addMessage('core.edit.notValid','danger');
            }
        
        
        $pathData=array();
        $pathData[]=array('text'=>'Úvod','href'=>'/');
        $pathData[]=array('text'=>'Profil','href'=>url(array(),'profile',true));
        $pathData[]=array('text'=>$this->view->header,'href'=>url(array('action'=>null)));
        $this->view->path=($this->view->ulLiMenu($pathData,array('class'=>'pull-right breadcrumb')));
    }
    
    function updateloginAction(){
        
        if(!$this->user || $this->user->isEmpty()){
            $this->addMessage('msg.account.needLogin','info');
            return $this->_redirect($this->auth->getLoginUrl(url())); 
        }
         
        
        $this->view->header=$this->user->getFullName();
        
        $this->view->form=$form=new Default_Form_Account_LoginUpdate;
        
        if($this->_isPost()){
            $postData=$this->_getPost();
            $postData["_id"]=$this->user->getId();
            $postData["old_pwd"]=$this->user->getPassword();
            
            $form->user=$this->user;
            
            if($form->isValid($postData)){
                $data=$form->getValues();
                if($this->user->updateLoginData($data)){
                    $this->addMessage('core.edit.success','success');
                }
            }
            else{
                $this->addMessage('core.edit.notValid','danger');
            }
        }
        else
            $form->populate($this->user->getFormData());
        
        
        $pathData=array();
        $pathData[]=array('text'=>'Úvod','href'=>'/');
        $pathData[]=array('text'=>'Profil','href'=>url(array(),'profile',true));
        $pathData[]=array('text'=>$this->view->header,'href'=>url(array('action'=>null)));
        $this->view->path=($this->view->ulLiMenu($pathData,array('class'=>'pull-right breadcrumb')));
    }
    
    
   
    function passwordAction(){
        
        $pathData=array();
        $pathData[]=array('text'=>'Úvod','href'=>'/');
        $pathData[]=array('text'=>'Zabudnuté heslo','href'=>url());
        $this->view->path=($this->view->ulLiMenu($pathData,array('class'=>'pull-right breadcrumb')));
        $this->view->header="Zabudnuté heslo";
        
        
        if(null === ($code=$this->_getParam('code'))){
            $formEmail=new Default_Form_Account_RegistrationEmail;
            
            if($this->_isPost() && $formEmail->isValid($this->_getPost())){
                $email=$formEmail->email->getValue();
                $user=new Model_User();
                $user->fetchOne(array('email'=>$email));
                if($user->isEmpty()){
                    $this->addMessage('user.notExists','error');
                }
                else{
                    
                    $data=$user->getFormData();
                    $data["link"]=$this->view->urlHost().$this->view->url(array('action'=>'password'))."?code=".$user->getActivatecode(true);

                    $messenger=new Model_Messenger();
                    $messenger->setData(array('type'=>'email','user_id'=>$user->getId(),'to'=>$data["email"],'subject'=>'Obnovenie zabudnutého hesla','template'=>'email/user_getpassword','template_data'=>$data));
                    $messenger->send();
                    
                    $this->addMessage('core.email.sent','success');
                    $formEmail->reset();
//                    $this->_redirect($this->view->url(array('action'=>null)));
                    
                }
            }
            $this->view->form=$formEmail;
        }
        else{
            // ak je preklik z emailu
            $user=new Model_User();
            $filter=new Zend_Filter_Alnum();
            
            $code=$filter->filter($this->_getParam('code'));
            
            if(empty($code))
                throw new Exception('Activation code error');
            
            $user->fetchOne(array('activatecode'=>$code));
            if($user->isEmpty()){
                $this->addMessage("core.bad.link",'danger');
                return $this->_redirect($this->view->url(array('action'=>'password'),'login',true));
            }
            
            $formPassword=new Default_Form_Account_RegistrationPassword;
            if($this->_isPost() && $formPassword->isValid($this->_getPost())){
                $data=$formPassword->getValues();
                $data["email"]=$user->email;
                $user->updateLoginData($data);
                $user->update(array('activatecode'=>""));
                $this->addMessage("core.getpassword.success",'success');
                return $this->_redirect($this->view->url(array('action'=>null),'login',true));
            }
            
            $this->view->form=$formPassword;
        }
        
    }
    
   
   
    
    function manageracesAction(){
         
        if(!$this->user || $this->user->isEmpty()){
            $this->addMessage('msg.account.needLogin','info');
            return $this->_redirect($this->auth->getLoginUrl(url())); 
        }
        
        if(isset($_GET["create"]))
            $this->_redirect(url(array('action'=>'createevent')));
        
        
        $this->view->header=array($this->user->name,"Správa podujatí");
        $pathData=array();
        $pathData[]=array('text'=>'Úvod','href'=>'/');
        $pathData[]=array('text'=>'Profil','href'=>url(array(),'profile',true));
        $pathData[]=array('text'=>'Správa podujatí','href'=>url(array('action'=>null)));
        $this->view->path=($this->view->ulLiMenu($pathData,array('class'=>'pull-right breadcrumb')));
        
        
        $table=new Races_Table_Races;
        $model=new Model_Race();
        $data=$model->fetchRacesByUser($this->user,$table->getModelOptions());
        $table->setData($data);
        $table->setDataCount($model->getFoundRows());
        $this->view->table = $table;
        
        $table->ajaxRender($this);
        
        
        
        
        
        
    }
    
    
    function createeventAction(){
        $form = new Races_Form_EventSettings();
	
        $form->removeElement('banner');
        $form->removeElement('logo');
        
        if($this->_isPost()){
            if($form->isValid($this->_getPost())){
                $event=new Model_Event();
                $event->setDataFromForm($form->getValues());
                if($event->save())
                    $this->_redirect($event->getUrl());
            }
        }
        
        
        $this->view->header=array($this->user->name,"Vytvorenie nového podujatia");
        $pathData=array();
        $pathData[]=array('text'=>'Úvod','href'=>'/');
        $pathData[]=array('text'=>'Profil','href'=>url(array(),'profile',true));
        $pathData[]=array('text'=>'Správa podujatí','href'=>url(array('action'=>'manageraces')));
        $this->view->path=($this->view->ulLiMenu($pathData,array('class'=>'pull-right breadcrumb')));
        
        
        
        $this->view->form=$form;
    }
    
    
    
}

