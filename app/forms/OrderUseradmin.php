<?php

class Form_OrderUseradmin extends Matj_Bootstrap3_Form {
	
        function init () {
		$this->setName(get_class($this));
		$this->setAttrib("class","form");
		
		$options = array ( 
			'prefix' => 'user.', 
			'elementClass' => true 
		);
		$this->setOptions($options);		
		
    $this->addElement('text', 'first_name', array ( 
			'required' => true 
		));
    $this->addElement('text', 'last_name', array ( 
			'required' => true 
		));
		$this->addElement('text', 'company_name', array ( 
			'required' => false 
		));
		$this->addElement('text', 'company_id', array ( 
			'required' => false 
		));
		$this->addElement('text', 'email', array ( 
			'required' => false 
		));
		$this->addElement('text', 'phone', array ( 
			'required' => false 
		));
		$this->addElement('textarea', 'description', array ( 
			'required' => false ,
			'rows'=>4
		));
		
		$this->addHidden('group_id',11);
			
		$this->addButtons(array(
        'submit'=>array('label'=>$this->translate("save.reservation"),'class'=>'btn-sm btn-success'),
        'back'=>array('show'=>true,'class'=>'btn-sm btn-warning'),
        'delete'=>array('show'=>false)
    ));
		
		$this->initClick();
		
	}
	
	function initClick(){
            $js='
                
             ';

  }
	
	function isValid ($data) {
		$valid = parent::isValid($data);
		//Zend_Debug::dump($data);
		
		return $valid;
	}
        
  function getValues(){
		$data=parent::getValues();
		$data['name']=null;
		return $data;
	}
	
	function populate($data){
       return parent::populate($data);
   }
        
    

}