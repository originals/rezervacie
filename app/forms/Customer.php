<?php

class Form_Customer extends Matj_Bootstrap3_Form {
	
        const elementWrapperClass='col-sm-7';
        const elementLabelClass='col-sm-5 text-sm-right';
        protected $elementButtonsWrapperClass='col-sm-7 col-sm-offset-5';
       
    
    
        function init () {
            
            
		$this->setName(get_class($this));
		$this->setAttrib("class","form");
		
		$options = array ( 
			'prefix' => 'user.', 
			'elementClass' => true 
		);
		$this->setOptions($options);
		
		
		
	
		
			$this->addElement('text', 'company', array ( 
				'required' => true 
			));
                    
                        
                $setting=new Model_Setting();        
                $setting->fetchOne(array('type'=>'global','variable'=>'companytype'));
                $settings=json_decode($setting->value);
                if(!is_array($settings))$settings=array();
                $companytype=array();
                foreach($settings as $s){
                    $companytype[$s->code]=$s->name;
                }
                
                        
                        $this->addElement('select', 'companytype', array ( 
				'required' => false,
                                'multiOptions'=>$companytype
			));
                        
                        
                        $this->addElement('text', 'street', array ( 
				'required' => false
			));
                        
                        $this->addElement('text', 'zip', array ( 
				'required' => false
			));
                        
                        $this->addElement('text', 'city', array ( 
				'required' => false
			));
                        
                        $this->addElement('text', 'companyid', array ( 
				'required' => false
			));
                        
                        $this->addElement('text', 'vatid', array ( 
				'required' => false
			));
                        
                        $this->addElement('text', 'taxid', array ( 
				'required' => false
			));
                        
                        $this->addElement('text', 'email', array ( 
				'required' => false
			));
                        
                        $this->addElement('text', 'phone', array ( 
				'required' => false
			));
                        
                        $this->addElement('text', 'website', array ( 
				'required' => false
			));
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        /*
                        
                        
			$this->addElement('select', 'color', array ( 
				'required' => false,
                                'multiOptions'=>Model_Order::getColors()
			));
                        $this->initSimplecolorpicker();
                        
                        */
                        
			
                        
			
			//$this->parse('buttons',$buttons);
			
			

		//$this->getElement("username")->getDecorator("label")->setTagClass('xx');
		
				
		//$this->addHashElement();
		
		//$this->initTinyMce();
                        
                        
	}
	
	function isValid ($data) {
		$valid = parent::isValid($data);
                
                
                $customer=$this->customer;
                $usr=new Model_Customer;
                if($usr->fetchOne(array('companyid'=>$data["companyid"],array('[id]!=%i',0+$customer->getId())),array('test'=>0))){
                    $this->companyid->addError('IČO už existuje');
                    $valid=false;
                }
                
                
                
		return $valid;
	}
        
        
        function initSimplecolorpicker(){
            
            
            head()->appendScriptFile('assets/simplecolorpicker/jquery.simplecolorpicker.js');
            head()->appendStylesheet('assets/simplecolorpicker/jquery.simplecolorpicker.css');
            
                $js='
                    $(function(){
                        $("select[name=color]").simplecolorpicker({
                            picker: true
                        });
                    });
                    ';
             head()->appendJavascript($js);   
                
                
        }
                
                function initTinyMce(){
            
            
            head()->appendScriptFile('external/tinymce/tinymce.min.js');
            head()->appendScriptFile('external/tinymce/jquery.tinymce.min.js');
            $js='
                
tinymce.init({
    selector: "textarea.tinymce_small",
    menubar: false,
    body_class: "mceBody",
    content_css : ["/assets/css/bootstrap.min.css","/assets/css/style.css"],
    toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | code",
    plugins:"code link",
    entity_encoding : "raw",
    language: "sk",
    relative_urls: false,
    image_class_list: [
        {title: "None", value: ""},
        
    ]
    
 });
 
 tinymce.init({
    selector: "textarea.tinymce_full",
    menubar: false,
    body_class: "mceBody",
    content_css : ["/assets/css/bootstrap.min.css","/assets/css/style.css"],
    toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
    toolbar2: "preview code | link image | forecolor backcolor emoticons",
    plugins: [
        "advlist autolink lists link charmap preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime nonbreaking table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern image"
    ],
    file_browser_callback : elFinderBrowser,
    entity_encoding : "raw",
    language: "sk",
    relative_urls: false,
    image_class_list: [
        {title: "None", value: ""},
        
    ]
    
 });
    

function elFinderBrowser (field_name, url, type, win) {
  tinymce.activeEditor.windowManager.open({
    file: "'.url(array('controller'=>'filemanager'),'default',true).'?callback=tinymce",// use an absolute path!
    title: "Správca súborov",
    width: 900,  
    height: 450,
    resizable: "yes"
  }, {
    setUrl: function (url) {
        if(typeof url =="object")
            url=url.url;
      win.document.getElementById(field_name).value = url;
    }
  });
  return false;
}


                ';
            head()->appendJavaScript($js);
            
        }
        
        
        
        /**
         *
         * @var Model_Customer
         */
        public $customer;
        
        function __toString() {
            
            $show=!$this->customer->isEmpty();
            
            $this->addButtons(array(
                                'submit'=>array('label'=>'Uložiť','class'=>'btn-large btn-success'),
                                'back'=>array('show'=>true,'url'=>url(array('id'=>null,'action'=>null))),
                                'delete'=>array('show'=>$show,'label'=>'<i class="fa fa-trash-o"></i>','url'=>url(array('delete'=>1)))
            ));
            
            
            $html=parent::__toString();
            
            
            
            
            return $html;
        }
        
        
        function getReadRows(){
            
            $rows=array();
            foreach($this->getElements() as $k=>$e){
                
                $rows[$k]=array(
                     'label'=>$e->getLabel(),
                     'value'=>$e->getValue()
                );
                
            }
            return $rows;
            
            
        }
        

}