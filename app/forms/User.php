<?php

class Form_User extends Matj_Bootstrap3_Form {
	
        function init () {
		$this->setName(get_class($this));
		$this->setAttrib("class","form");
		
		$options = array ( 
			'prefix' => 'user.', 
			'elementClass' => true 
		);
		$this->setOptions($options);
		
		
		
		
		
			$this->addElement('text', 'email', array ( 
				'required' => true 
			));
                        
                        
			$this->addElement('text', 'first_name', array ( 
				'required' => true 
			));
                        
                        $this->addElement('text', 'last_name', array ( 
				'required' => true 
			));
                        
                        
                        
      $this->addElement('select', 'group_id', array ( 
				'required' => true,
        'multiOptions'=>array(''=>'')
			));
					
			$this->addElement('select', 'client_id', array ( 
				'required' => true,
        'multiOptions'=>array(''=>'')
			));
                        
			
                        $this->addElement('text', 'login', array ( 
				'required' => true 
			));
                        
			
                        $this->addElement('password', 'password', array ( 
                                'required' => false 
			));
			
			//$this->addHidden('group_id',2);
			
			
                        
			$this->addButtons(array(
                                            'submit'=>array('label'=>'Uložiť','class'=>'btn-large btn-success'),
                                            'back'=>array('show'=>true),
                                            'delete'=>array('show'=>true)
                            ));
			//$this->parse('buttons',$buttons);
			
			

		//$this->getElement("username")->getDecorator("label")->setTagClass('xx');
		
				
		//$this->addHashElement();
		
		
	}
	
	function isValid ($data) {
		$valid = parent::isValid($data);
		return $valid;
	}
        
        function getValues(){
            $data=parent::getValues();
            $data['name']=null;
            return $data;
        }
        
        
    public function setGroups($data) {
        
        foreach($data as $k=>$d)
            $this->getElement('group_id')->addMultiOption($d["id"],$d["name"]);
        
    }
	
		public function setClients($data) {        
        foreach($data as $k=>$d)
            $this->getElement('client_id')->addMultiOption($d["id"],$d["name"]);
        
    }

}