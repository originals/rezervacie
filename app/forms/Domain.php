<?php

class Form_Domain extends Matj_Bootstrap3_Form {
	
        const elementWrapperClass='col-sm-8';
        const elementLabelClass='col-sm-4 text-sm-right';
        protected $elementButtonsWrapperClass='col-sm-8 col-sm-offset-4';
       
    
    
	function init () {
		$this->setName(get_class($this));
		$this->setAttrib("class","form");
		
		$options = array ( 
			'prefix' => 'domain.', 
			'elementClass' => true 
		);
		$this->setOptions($options);
		
		
		
		
		
			$this->addElement('text', 'name', array ( 
				'required' => true 
			));
			
			$this->addElement('yesNo', 'publish', array ('required'=>true)); 
			$this->addElement('text', 'title', array ( 
				'required' => true 
			));
			$this->addElement('text', 'subtitle', array ( 
				'required' => false 
			));
			$this->addElement('select','template',array(
				'multiOptions'=>array("2"=>"Predvolená","1"=>"template 1","2"=>"template 2"),
				'required' => true
			));
		
			$this->addElement('select','menu',array(
				'multiOptions'=>array("1"=>"Default","1"=>"Na vrchu fixné","2"=>"Na vrchu posuvné","3"=>"Vľavo"),
				'required' => true
			));
			
                        
                        
                        $this->addElement('PickFile','banner',array());
                        
                        
                        
                        
                        
                        
			$this->addElement('yesNo', 'footer', array ('required'=>false));
			$this->addElement('textarea', 'footer_content', array ( 
				'required' => true ,
                                'class'=>'tinymce_full',
                             
                                'filters'=>array('StringTrim','StripSlashes')
			));
			
			$this->addElement('text', 'background', array ( 
				'required' => false,
				'class'=>'cpk'
			));
			$this->addElement('text', 'link', array ( 
				'required' => false,
				'class'=>'cpk'
			));
			$this->addElement('text', 'footer_color_background', array ( 
				'required' => false,
				'class'=>'cpk'
			));
			$this->addElement('text', 'footer_color_text', array ( 
				'required' => false,
				'class'=>'cpk'
			));
			$this->addElement('text', 'h1size', array ( 
				'required' => false
			));
			$this->addElement('text', 'h1color', array ( 
				'required' => false,
				'class'=>'cpk'
			));
			$this->addElement('text', 'h2size', array ( 
				'required' => false 
			));
			$this->addElement('text', 'h2color', array ( 
				'required' => false,
				'class'=>'cpk'
			));
			$this->addElement('text', 'h3size', array ( 
				'required' => false 
			));
			$this->addElement('text', 'h3color', array ( 
				'required' => false,
				'class'=>'cpk' 
			));
		
			$this->addElement('text', 'products', array ( 
				'required' => false 
			));
                        
			$this->parse('background','<div class="row"><div class="col-md-12"><h3>Štýly stránky</h3></div></div><div class="row"><div class="col-md-6">{content}</div>'); 
			$this->parse('link','<div class="col-md-6">{content}</div></div>');
			$this->parse('footer_color_background','<div class="row"><div class="col-md-6">{content}</div>'); 
			$this->parse('footer_color_text','<div class="col-md-6">{content}</div></div>'); 
            $this->parse('h1size','<div class="row"><div class="col-md-6">{content}</div>'); 
			$this->parse('h1color','<div class="col-md-6">{content}</div></div>');
			$this->parse('h2size','<div class="row"><div class="col-md-6">{content}</div>'); 
			$this->parse('h2color','<div class="col-md-6">{content}</div></div>');
            $this->parse('h3size','<div class="row"><div class="col-md-6">{content}</div>'); 
			$this->parse('h3color','<div class="col-md-6">{content}</div></div>');
                        
                        
			$this->addButtons();
			//$this->parse('buttons',$buttons);
			
			

		//$this->getElement("username")->getDecorator("label")->setTagClass('xx');
		
				
		//$this->addHashElement();
		$this->initTinyMce();
		
	}
	
	function isValid ($data) {
		$valid = parent::isValid($data);
		return $valid;
	}
    
	function initTinyMce(){
            
            
            head()->appendScriptFile('external/tinymce/tinymce.min.js');
            head()->appendScriptFile('external/tinymce/jquery.tinymce.min.js');
            $js='
                

 
 tinymce.init({
    selector: "textarea.tinymce_full",
    menubar: false,
    body_class: "mceBody",
    content_css : ["/assets/css/bootstrap.min.css","/assets/css/style.css"],
    toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link",
    toolbar2: "preview code | link image | forecolor backcolor emoticons",
    plugins: [
        "advlist autolink lists link charmap preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime nonbreaking table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern image"
    ],
    file_browser_callback : elFinderBrowser,
    entity_encoding : "raw",
    language: "sk",
    relative_urls: false,
    image_class_list: [
        {title: "None", value: ""},
        
    ]
    
 });
    

function elFinderBrowser (field_name, url, type, win) {
  tinymce.activeEditor.windowManager.open({
    file: "'.url(array('controller'=>'filemanager'),'default',true).'?callback=tinymce",// use an absolute path!
    title: "Správca súborov",
    width: 900,  
    height: 450,
    resizable: "yes"
  }, {
    setUrl: function (url) {
        if(typeof url =="object")
            url=url.url;
      win.document.getElementById(field_name).value = url;
    }
  });
  return false;
}


                ';
            head()->appendJavaScript($js);
            
        }
        
 


}