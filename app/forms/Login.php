<?php

class Form_Login extends Matj_Bootstrap3_Form {
	
        const elementWrapperClass='col-sm-4';
        const elementLabelClass='col-sm-4 text-sm-right';
        protected $elementButtonsWrapperClass='col-sm-4 col-sm-offset-4';
       
    
    
	function init () {
		$this->setName(get_class($this));
		$this->setAttrib("class","form");
		
		$options = array ( 
			'prefix' => 'core.', 
			'elementClass' => true 
		);
		$this->setOptions($options);
		
		
		
		
		
			$this->addElement('text', 'username', array ( 
				'required' => true 
			));
			
			$this->addElement('password', 'password', array ('required'=>true)); 
			$this->addElement('checkbox', 'pernament', array ('label'=>'')); 
			

			$this->addButtons(array('submit'=>array('label'=>$this->translate('login'),'class'=>'btn-large btn-success')));
			//$this->parse('buttons',$buttons);
			
			

		//$this->getElement("username")->getDecorator("label")->setTagClass('xx');
		
				
		//$this->addHashElement();
		
		
	}
	
	function isValid ($data) {
		$valid = parent::isValid($data);
		return $valid;
	}


}