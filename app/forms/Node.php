<?php

class Form_Node extends Matj_Bootstrap3_Form {
	
        function init () {
		$this->setName(get_class($this));
		$this->setAttrib("class","form");
		
		$options = array ( 
			'prefix' => 'node.', 
			'elementClass' => true 
		);
		$this->setOptions($options);
		
		
		
		
                    
            $this->addElement('select', 'pid', array ( 
				'required' => false,
                                'multiOptions'=>array(''=>'Najvyššia úroveň')
			));
		
			$this->addElement('text', 'title', array ( 
				'required' => true 
			));
                        
                        
                        
                        
			$this->addElement('textarea', 'content', array ( 
				'required' => true ,
                                'class'=>'tinymce_full',
                                'filters'=>array('StringTrim','StripSlashes')
			));
                        
                        
                        
			$this->addElement('text', 'rank', array ( 
				'required' => false
			));
			$this->addElement('text', 'url', array ( 
				'required' => false
			));
			$this->addElement('text', 'seo_title', array ( 
				'required' => false
			));
			$this->addElement('text', 'seo_description', array ( 
				'required' => false
			));
                        
                        
                        
			
                        
			$this->addButtons(array(
                                            'submit'=>array('label'=>'Uložiť','class'=>'btn-large btn-success'),
                                            'back'=>array('show'=>true),
                                            'delete'=>array('show'=>true)
                            ));
			//$this->parse('buttons',$buttons);
			
			

		//$this->getElement("username")->getDecorator("label")->setTagClass('xx');
		
				
		//$this->addHashElement();
		
		$this->initTinyMce();
                        
                        
	}
	
	function isValid ($data) {
		$valid = parent::isValid($data);
		return $valid;
	}
        
        
                function initTinyMce(){
            
            
            head()->appendScriptFile('external/tinymce/tinymce.min.js');
            head()->appendScriptFile('external/tinymce/jquery.tinymce.min.js');
            $js='
                
tinymce.init({
    selector: "textarea.tinymce_small",
    menubar: false,
    body_class: "mceBody",
    content_css : ["/assets/css/bootstrap.min.css","/assets/css/style.css"],
    toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | code",
    plugins:"code link",
    entity_encoding : "raw",
    language: "sk",
    relative_urls: false,
    image_class_list: [
        {title: "None", value: ""},
        
    ]
    
 });
 
 tinymce.init({
    selector: "textarea.tinymce_full",
    menubar: false,
    body_class: "mceBody",
    content_css : ["/assets/css/bootstrap.min.css","/assets/css/style.css"],
    toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
    toolbar2: "preview code | link image | forecolor backcolor emoticons",
    plugins: [
        "advlist autolink lists link charmap preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime nonbreaking table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern image"
    ],
    file_browser_callback : elFinderBrowser,
    entity_encoding : "raw",
    language: "sk",
    relative_urls: false,
    image_class_list: [
        {title: "None", value: ""},
        
    ]
    
 });
    

function elFinderBrowser (field_name, url, type, win) {
  tinymce.activeEditor.windowManager.open({
    file: "'.url(array('controller'=>'filemanager'),'default',true).'?callback=tinymce",// use an absolute path!
    title: "Správca súborov",
    width: 900,  
    height: 450,
    resizable: "yes"
  }, {
    setUrl: function (url) {
        if(typeof url =="object")
            url=url.url;
      win.document.getElementById(field_name).value = url;
    }
  });
  return false;
}


                ';
            head()->appendJavaScript($js);
            
        }
        
 


}