<?php

class Form_Object extends Matj_Bootstrap3_Form {
	
        const elementWrapperClass='col-sm-8';
        const elementLabelClass='col-sm-4 text-sm-right';
        protected $elementButtonsWrapperClass='col-sm-8 col-sm-offset-4 order-form-btns';
       
    
    
        function init () {
						$this->setName(get_class($this));
						$this->setAttrib("class","form");

						$options = array ( 
							'prefix' => 'object.', 
							'elementClass' => true 
						);
						$this->setOptions($options);
		
						$this->addElement('text', 'name', array ( 
							'required'=>true
						));
						
					
						$this->addElement('YesNo', 'status', array ( 
								//'required'=>true
						));
					
					
                       
            
             /*          
                        $this->addElement('text', 'customer_search', array ( 
                                'label'=>translate('order.customer_id'),
				'data-valuefield'=>'#customer_id',
                                'class'=>'ajaxtypeahead',
                                'data-source'=>'/calendar/ajaxcustomer',
                                'data-valueupdate'=>'updateCustomer',
                                'autocomplete'=>'off'
                                
			));*/
                        
              
                        
                        
	}
	
	function isValid ($data) {
     $valid = parent::isValid($data);
                
                
                
		return $valid;
	}
        
        
        function __toString() {
            
            
            
            $this->addButtons(array(
                                'submit'=>array('label'=>'Uložiť','class'=>'btn-large btn-success'),
                                'back'=>array('show'=>false,'url'=>url(array('id'=>null,'action'=>null))),
                                'delete'=>array('show'=>false,'label'=>'<i class="fa fa-trash-o"></i>','url'=>url(array('delete'=>false)))
            ));
            
            
            $html=parent::__toString();
            
            
            
            
            return $html;
        }
        
        
        function getReadRows(){
            
            $rows=array();
            foreach($this->getElements() as $k=>$e){
                
                $rows[$k]=array(
                     'label'=>$e->getLabel(),
                     'value'=>$e->getValue()
                );
                
            }
            return $rows;
            
            
        }      
   

}