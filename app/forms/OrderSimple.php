<?php

class Form_OrderSimple extends Matj_Bootstrap3_Form {
	
        const elementWrapperClass='col-sm-8';
        const elementLabelClass='col-sm-4 text-sm-right';
        protected $elementButtonsWrapperClass='col-sm-8 col-sm-offset-4 order-form-btns';
       
    
    
        function init () {
		$this->setName(get_class($this));
		$this->setAttrib("class","form");
		
		$options = array ( 
			'prefix' => 'order.', 
			'elementClass' => true 
		);
		$this->setOptions($options);
		
		
		
                        $this->addElement('text', 'time', array ( 
				'disabled'=>true,'ignore'=>true
			));
                        
                        $this->addElement('text', 'customer_name', array ( 
				'disabled'=>true,'ignore'=>true,
                                'label'=>translate('customer.name'),
                        ));
                        
                        
                        $this->addElement('select', 'status', array ( 
				'required'=>true,
			));
                        
                        
                        $this->addElement('text', 'addnote', array ( 
			));
                        
                        
			$this->addElement('text', 'carcode', array ( 
                                
			));
                        
                        
                        
			$this->addElement('select', 'cartype', array (
                             'disabled'=>true,'ignore'=>true,
                             'multiOptions'=>array('lorry'=>translate('order.cartype.lorry'),'tank'=>translate('order.cartype.tank'),'truck'=>translate('order.cartype.truck'),'van'=>translate('order.cartype.van'))
			));
                        
                        
                        
			$this->addElement('select', 'ramp', array ( 
				//'disabled'=>true,'ignore'=>true,
			));
                        
                        $this->addElement('select', 'type', array ( 
				'disabled' => true,'ignore'=>true,
                                'multiOptions'=>array('in'=>translate('order.type.in'),'out'=>translate('order.type.out'))
			));
                        
                        
                        
                        
                        $this->addElement('select', 'loadtime', array ( 
				'disabled'=>true,'ignore'=>true,
                                'value'=>60,
                                'multiOptions'=>Model_Order::getLoadTimes()
			));
                        
                        $this->addElement('select', 'priority', array ( 
				'disabled' => true,'ignore'=>true,
                                'label' => translate('customer.priority'),
                                'multiOptions'=>array('normal'=>translate('customer.priority.normal'),'high'=>translate('customer.priority.high'),'low'=>translate('customer.priority.low'))
			));
                        
                        
                        
                       
                        
                        
                        
                        
                        /*
                        
			$this->addElement('select', 'color', array ( 
				'required' => false,
                                'label' => translate('customer.color'),
                                'multiOptions'=>Model_Order::getColors()
			));
                         
                         */
                        
                       /* 
                        
                        
                        $dec=$this->_elementDecorators+array();
                        ($dec[7][1]["openOnly"]=true);
                        ($dec[8][1]["openOnly"]=true);
                        $this->setElementDecorators($dec,array('carcode'));
                        
                        $dec=$this->_elementDecorators+array();
                        ($dec[7][1]["closeOnly"]=true);
                        ($dec[8][1]["closeOnly"]=true);
                        unset($dec[5]);
                        
                        
                        
                        $this->setElementDecorators($dec,array('cartype'));
                        
                        
                        echo $this->carcode.$this->cartype;exit;
                        */
                        
                        
 //                       exit;
                        
                        
		//$this->addHashElement();
		
		//$this->initTinyMce();
                         
                        $dec=$this->_elementDecorators;
                        $dec[3][1]["class"].=" col-md-6";
                        $dec[6][1]["class"].=" col-md-6";
                        //Zend_Debug::dump($dec);exit;
                        $this->setElementDecorators($dec,array('carcode','cartype','loadtime','priority','ramp','type','color'));
                        
                        
                        $this->carcode->addDecorator('parse',array('text'=>'<hr><div class="row"><div class="col-sm-6">{content}</div>'));
                        $this->cartype->addDecorator('parse',array('text'=>'<div class="col-sm-6">{content}</div></div>'));
                        
                        $this->loadtime->addDecorator('parse',array('text'=>'<div class="row"><div class="col-sm-6">{content}</div>'));
                        $this->priority->addDecorator('parse',array('text'=>'<div class="col-sm-6">{content}</div></div>'));
                        
                        
                        $this->ramp->addDecorator('parse',array('text'=>'<div class="row"><div class="col-sm-6">{content}</div>'));
                        $this->type->addDecorator('parse',array('text'=>'<div class="col-sm-6">{content}</div></div>'));
                        
                        //$this->color->addDecorator('parse',array('text'=>'<div class="row"><div class="col-sm-6">{content}</div></div>'));
                        
                        
                        
	}
        
        
        function populate($data){
            
            $data["time"]=Matj_Format::formatDate($data["time"],'d.m.Y H:i');
            $customer=new Model_Customer($data["customer_id"]);
            $data["customer_name"]=$customer->name;
            
            
            return parent::populate($data);
        }
        
	
	function isValid ($data) {
//                if($data["customer_id"]=="add"){
//                    $this->customer_name->setRequired('true');
//                    $this->customer_shortname->setRequired('true');
//                    $this->customer_type->setRequired('true');
//                }
//                else{
//                    if(empty($data["customer_id"]))
//                        $this->customer_search->setRequired('true');
//                }
                
            
            
                $valid = parent::isValid($data);
                
                
                
		return $valid;
	}
        
        
        function initSimplecolorpicker(){
            $js='
                if(typeof initOrderForm !="function"){ 
                    function initOrderForm(){
                        $(".status_selectors").remove();
                        statusObj=$("#status");
                        
                        statusSelector=$("<div/>",{"class":"status_selectors"});

                        for(var i in statusData){
                            st=statusData[i];
                            act="";
                            if(i==statusObj.val())act="active";
                            
                            btn=$("<button/>",{"type":"button","class":"btn "+act,"text":st.name,"value":i,"css":{"background":st.color,"color":st.textcolor,"margin":"0 5px 5px 0"}})
                            statusSelector.append(btn);
                        }
                        statusSelector.find("button").click(function(){
                            $("#status").val($(this).attr("value"));
                            $("#addnote").focus();
                            initOrderForm();
                        });
                        statusObj.hide();
                        statusObj.parent().append(statusSelector);
                    }
                }
                $(document).ready(function(){
                    initOrderForm();
                });
                ';


            head()->appendJavascript('var statusData = '.json_encode($this->statusData).';','statusdata');
            head()->appendJavascript($js,'initOrderForm');
        }
          
        
        
        
    public $order;
        
    function __toString() {
        
        $this->initSimplecolorpicker();
                        
        $show=!$this->order->isEmpty();

        
//        $this->addButtons(array(
//                            'submit'=>array('label'=>'Uložiť','class'=>'btn-large btn-success'),
//                            'back'=>array('show'=>true,'url'=>url(array('edit'=>null,'action'=>'list'))),
//                            'delete'=>array('show'=>$show,'label'=>'<i class="fa fa-trash-o"></i>','url'=>url(array('delete'=>1)))
//        ));
        
        
        if($show){
            $btnsUpdate='<a class="btn btn-danger btn-form-delete confirm_submit" alt="Skutočne chcete vymazať túto položku?" href="'.url(array('delete'=>1)).'"><i class="fa fa-trash-o"></i></a>';
            $btnsUpdate.=' <a class="btn btn-default btn-form-delete confirm_submit" alt="Skutočne chcete skopírovať túto položku?" href="'.url(array('copy'=>$this->order->getId(),'id'=>null)).'"><i class="fa fa-copy"></i></a>';
        }
        $btnsUpdate=null;
        
        $btns=' <div class="row order-form-btns">
                    <div class="col-sm-8 col-sm-offset-0"><button class="btn btn-form-submit btn-large btn-success" type="submit">Uložiť</button>
                    <span class="secondbtn"> alebo <a class="btn btn-sm btn-default btn-form-back" href="'.url(array('edit'=>null,'id'=>null,'delete'=>null,'copy'=>null,'action'=>'list')).'">zrušiť</a></span> 
                    </div> 
                    <div class="col-sm-4 text-right">'.$btnsUpdate.'</div>
                </div>';
        
        
        $this->addHidden('buttons');
        $this->buttons->setIgnore(true);
        $this->parse('buttons',$btns);
        
        

        $html=parent::__toString();


        //Zend_Debug::dump($this->customersData);exit;
        
        $html.='<script>';
        $html.='var customersData = '.json_encode($this->customersData).';';
        $html.='</script>';
        
        


        return $html;
    }

    
    protected $customersData=array();
    protected $statusData=array();
    
    public function addCustomers(array $customers) {
        foreach($customers as $c){
            $this->customersData[$c->id]=array('color'=>$c->color,'loadtime'=>$c->loadtime);
            if($this->customer_id instanceof Zend_Form_Element_Multi)
            $this->customer_id->addMultiOption($c->id,$c->shortname." - ".$c->name);
        }
    }

    public function addRamps(array $ramps) {
        foreach($ramps as $r)
            $this->ramp->addMultiOption($r->code,$r->name); 
    }

    public function addStatuses($statuses) {
            
        foreach($statuses as $s){
            $this->statusData[$s->code]=array('color'=>$s->color,'name'=>$s->name,'textcolor'=>Model_Order::get_brightness($s->color)<130 ? '#ffffff' : '#000000');
        
            $this->status->addMultiOption($s->code,$s->name); 
        }
    }

}