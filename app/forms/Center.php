<?php

class Form_Center extends Matj_Bootstrap3_Form {
	
        const elementWrapperClass='col-sm-8';
        const elementLabelClass='col-sm-4 text-sm-right';
        protected $elementButtonsWrapperClass='col-sm-8 col-sm-offset-4 order-form-btns';
       
    
    
        function init () {
						$this->setName(get_class($this));
						$this->setAttrib("class","form");

						$options = array ( 
							'prefix' => 'center.', 
							'elementClass' => true 
						);
						$this->setOptions($options);
		
						$this->addElement('text', 'name', array ( 
							'required'=>true
						));
					
						$this->addElement('select', 'type', array ( 
								'multiOptions' => array(''=>'','1'=>'Tenis','2'=>'Badminton','3'=>'Bowling','4'=>'Table tenis'),
								'required'=>true
						));
					
						$this->addElement('YesNo', 'active', array ( 
								//'required'=>true
						));
					
						$this->addElement('select', 'unit', array ( 
								'multiOptions' => array(''=>'','15'=>'15 min.','30'=>'30 min.','60'=>'60 min.'),
								'required'=>false
						));
					
						$this->addElement('text', 'phone', array ( 
							'required'=>true
						));
					$this->addElement('text', 'email', array ( 
							'required'=>false
						));
					$this->addElement('text', 'person', array ( 
							'required'=>false
						));
					$this->addElement('PickFile', 'image', array ( 
						));
					
                       
            
             /*          
                        $this->addElement('text', 'customer_search', array ( 
                                'label'=>translate('order.customer_id'),
				'data-valuefield'=>'#customer_id',
                                'class'=>'ajaxtypeahead',
                                'data-source'=>'/calendar/ajaxcustomer',
                                'data-valueupdate'=>'updateCustomer',
                                'autocomplete'=>'off'
                                
			));*/
                        
              
                        
                        
	}
	
	function isValid ($data) {
     $valid = parent::isValid($data);
                
                
                
		return $valid;
	}
        
        
        function __toString() {
            
            
            
            $this->addButtons(array(
                                'submit'=>array('label'=>'Uložiť','class'=>'btn-large btn-success'),
                                'back'=>array('show'=>false,'url'=>url(array('id'=>null,'action'=>null))),
                                'delete'=>array('show'=>false,'label'=>'<i class="fa fa-trash-o"></i>','url'=>url(array('delete'=>false)))
            ));
            
            
            $html=parent::__toString();
            
            
            
            
            return $html;
        }
        
        
        function getReadRows(){
            
            $rows=array();
            foreach($this->getElements() as $k=>$e){
                
                $rows[$k]=array(
                     'label'=>$e->getLabel(),
                     'value'=>$e->getValue()
                );
                
            }
            return $rows;
            
            
        }      
   

}