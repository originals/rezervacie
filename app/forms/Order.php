<?php

class Form_Order extends Matj_Bootstrap3_Form {
	
        const elementWrapperClass='col-sm-8';
        const elementLabelClass='col-sm-4 text-sm-right';
        protected $elementButtonsWrapperClass='col-sm-8 col-sm-offset-4 order-form-btns';
       
    
    
        function init () {
		$this->setName(get_class($this));
		$this->setAttrib("class","form");
		
		$options = array ( 
			'prefix' => 'order.', 
			'elementClass' => true 
		);
		$this->setOptions($options);
		
		
		
                        $this->addElement('dateSelect', 'time', array ( 
				'time'=>true,
                                'value'=>date('Y-m-d H:i:s'),
                                'allminutes'=>1
			));
                        
                    /*
			$this->addElement('select', 'customer_id', array ( 
				'multiOptions' => array(''=>''),
                                'required'=>true
			));
                     */   
                        $this->addElement('text', 'customer_id', array ( 
				//'multiOptions' => array(''=>''),
                                //'required'=>true
			));
                        
                        $this->addElement('text', 'customer_search', array ( 
                                'label'=>translate('order.customer_id'),
				'data-valuefield'=>'#customer_id',
                                'class'=>'ajaxtypeahead',
                                'data-source'=>'/calendar/ajaxcustomer',
                                'data-valueupdate'=>'updateCustomer',
                                'autocomplete'=>'off'
                                
			));
                        
                        $this->addElement('text', 'customer_name', array ( 
				'required'=>false,
                                'label'=>translate('customer.new.name'),
                                'class'=>'customer_new'
			));
                        
                        $this->addElement('text', 'customer_shortname', array ( 
				'required'=>false,
                                'label'=>translate('customer.shortname'),
                                'class'=>'customer_new'
			));
                        
                        $this->addElement('select', 'customer_type', array ( 
				'required' => false,
                                'class'=>'customer_new',
                                'label'=>translate('customer.type'),
                                'multiOptions'=>array('customer'=>translate('customer.type.customer'),'supplier'=>translate('customer.type.supplier'))
			));
                        
                        
                        
                        
                        
                        $this->addElement('select', 'status', array ( 
				'required'=>true,
			));
                        
                        
                        $this->addElement('text', 'addnote', array ( 
			));
                        
                        
			$this->addElement('text', 'carcode', array ( 
				//'data-valuefield'=>'#carcode',
                                'class'=>'ajaxtypeahead',
                                'data-source'=>'/calendar/ajaxcarcode',
                                //'data-valueupdate'=>'updateCarcode',
                                'data-allowcustom'=>'1',
                                'autocomplete'=>'off'
                                
			));
                        
                        
                        
			$this->addElement('select', 'cartype', array ( 
                             'multiOptions'=>array('lorry'=>translate('order.cartype.lorry'),'tank'=>translate('order.cartype.tank'),'truck'=>translate('order.cartype.truck'),'van'=>translate('order.cartype.van'))
			));
                        
                        
                        
			$this->addElement('select', 'ramp', array ( 
				'required'=>true
			));
                        
                        $this->addElement('select', 'type', array ( 
				'required' => true,
                                'multiOptions'=>array('in'=>translate('order.type.in'),'out'=>translate('order.type.out'))
			));
                        
                        
                        
                        
                        $this->addElement('select', 'loadtime', array ( 
				'required'=>true,
                                'value'=>60,
                                'multiOptions'=>Model_Order::getLoadTimes()
			));
                        
                        $this->addElement('select', 'priority', array ( 
				'required' => false,
                                'label' => translate('customer.priority'),
                                'multiOptions'=>array('normal'=>translate('customer.priority.normal'),'high'=>translate('customer.priority.high'),'low'=>translate('customer.priority.low'))
			));
                        
                        
                        
                       
                        
                        
                        
                        
                        
                        
			$this->addElement('select', 'color', array ( 
				'required' => false,
                                'label' => translate('customer.color'),
                                'multiOptions'=>Model_Order::getColors()
			));
                        $this->initSimplecolorpicker();
                        
                       /* 
                        
                        
                        $dec=$this->_elementDecorators+array();
                        ($dec[7][1]["openOnly"]=true);
                        ($dec[8][1]["openOnly"]=true);
                        $this->setElementDecorators($dec,array('carcode'));
                        
                        $dec=$this->_elementDecorators+array();
                        ($dec[7][1]["closeOnly"]=true);
                        ($dec[8][1]["closeOnly"]=true);
                        unset($dec[5]);
                        
                        
                        
                        $this->setElementDecorators($dec,array('cartype'));
                        
                        
                        echo $this->carcode.$this->cartype;exit;
                        */
                        
                        
 //                       exit;
                        
                        
		//$this->addHashElement();
		
		//$this->initTinyMce();
                         
                        $dec=$this->_elementDecorators;
                        $dec[3][1]["class"].=" col-md-6";
                        $dec[6][1]["class"].=" col-md-6";
                        //Zend_Debug::dump($dec);exit;
                        $this->setElementDecorators($dec,array('carcode','cartype','loadtime','priority','ramp','type','color'));
                        
                        
                        $this->carcode->addDecorator('parse',array('text'=>'<hr><div class="row"><div class="col-sm-6">{content}</div>'));
                        $this->cartype->addDecorator('parse',array('text'=>'<div class="col-sm-6">{content}</div></div>'));
                        
                        $this->loadtime->addDecorator('parse',array('text'=>'<div class="row"><div class="col-sm-6">{content}</div>'));
                        $this->priority->addDecorator('parse',array('text'=>'<div class="col-sm-6">{content}</div></div>'));
                        
                        
                        $this->ramp->addDecorator('parse',array('text'=>'<div class="row"><div class="col-sm-6">{content}</div>'));
                        $this->type->addDecorator('parse',array('text'=>'<div class="col-sm-6">{content}</div></div>'));
                        
                        $this->color->addDecorator('parse',array('text'=>'<div class="row"><div class="col-sm-6">{content}</div></div>'));
                        
                        
                        
	}
	
	function isValid ($data) {
                if($data["customer_id"]=="add"){
                    $this->customer_name->setRequired('true');
                    $this->customer_shortname->setRequired('true');
                    $this->customer_type->setRequired('true');
                }
                else{
                    //if(empty($data["customer_id"]))
                    //    $this->customer_search->setRequired('true');
                }
                
            
            
                $valid = parent::isValid($data);
                
                
                
		return $valid;
	}
        
        
        function initSimplecolorpicker(){
            $registry=Zend_Registry::getInstance();
            if($registry->isRegistered('colorpicker'))
                return false;
            
            $registry->set('colorpicker',true);
            
            
            head()->appendScriptFile('assets/backend/js/bootstrap3-typeahead.js');

            
            head()->appendScriptFile('assets/simplecolorpicker/jquery.simplecolorpicker.js');
            head()->appendStylesheet('assets/simplecolorpicker/jquery.simplecolorpicker.css');
            
                $js='
                    if (typeof updateCustomer != "function") { 
                    
                        function removeCustomer(){
                            $("#customer_id").val("");
                            updateCustomer($("#customer_search"));
                        }
                        
                        function updateCustomer(thObj,item){
                            var $item=item;


                        

                            if(item && item.value=="add"){
                                name=item.name;
                                $("#customer_name").val(name);
                                $(".customer_new").closest(".form-group").show();
                            }
                            

                            
                            idObj=$(thObj.attr("data-valuefield"));
                            idObj.closest(".form-group").hide();

                            if(idObj.val()=="add"){
                                $(".customer_new").closest(".form-group").show();
                            }
                            else{
                                $(".customer_new").closest(".form-group").hide();
                            }
                            
                            if(idObj.val()!=""){
                                $("#customer_search").parent().find(".errors").hide();
                            }

                            if(Number(idObj.val())>0){
                                $.getJSON("/calendar/ajaxcustomer",{id:idObj.val()},function(json){
                                    console.log(json);
                                    if(json.customer.id>0){
                                        html="<div class=\'well customer_info\' style=\'padding:3px 10px; margin:0 0 3px;\'>"+json.customer.name+" <span class=label style=\'background:"+json.customer.color+";color:"+json.customer.textcolor+";\'>"+json.customer.shortname+"</span> <span class=\'btn btn-xs btn-default\' onclick=\'removeCustomer()\'><i class=\'fa fa-trash\'></i></span></div>";
                                        thObj.hide();
                                        thObj.parent().find(".customer_info").remove();
                                        thObj.parent().append(html);
                                        
                                        if($item){
                                            $("#color").val(json.customer.color);
                                            $("#priority").val(json.customer.priority);
                                            $("#loadtime").val(json.customer.loadtime);
                                            initSimplecolorpicker();
                                        }

                                    }
                                    else{
                                        removeCustomer();
                                    }
                                });
                                
                            }
                            else{
                                thObj.show();
                                $(".customer_info").remove();
                            }
                            


                        }
                    }
                    if (typeof initSimplecolorpickerChange != "function") { 
                        function initSimplecolorpickerChange(){ 
                            cid=$("select[name=customer_id]").val();
                            //console.log(customersData[cid]);
                            if(customersData[cid]!=undefined){
                                $("select[name=color]").simplecolorpicker("destroy");
                                $("select[name=color]").val(customersData[cid].color);
                                $("select[name=loadtime]").val(customersData[cid].loadtime);
                                initSimplecolorpicker();
                            }

                        }
                    }
                    if (typeof initSimplecolorpicker != "function") { 
                        function initSimplecolorpicker(){
                            $("select[name=color]").simplecolorpicker({
                                picker: true
                            });
                            
                            $("select[name=customer_id]").change(function(){
                                initSimplecolorpickerChange();
                                
                            });
//console.log("init");
                        }
                    }
                        $(document).ready(function(){
                            initSimplecolorpicker();

                            
                        });
                        
                        $( document ).ajaxComplete(function() {
                     //   console.log("complete");
                              initSimplecolorpicker();
                        });
                    
                    ';
             head()->appendJavascript($js);   
                
                
        }
          
        
        
        
    public $order;
        
    function __toString() {

        $show=!$this->order->isEmpty();

        
//        $this->addButtons(array(
//                            'submit'=>array('label'=>'Uložiť','class'=>'btn-large btn-success'),
//                            'back'=>array('show'=>true,'url'=>url(array('edit'=>null,'action'=>'list'))),
//                            'delete'=>array('show'=>$show,'label'=>'<i class="fa fa-trash-o"></i>','url'=>url(array('delete'=>1)))
//        ));
        
        
        if($show){
            $btnsUpdate='<a class="btn btn-danger btn-form-delete confirm_submit" alt="Skutočne chcete vymazať túto položku?" href="'.url(array('delete'=>1)).'"><i class="fa fa-trash-o"></i></a>';
            $btnsUpdate.=' <a class="btn btn-default btn-form-delete confirm_submit" alt="Skutočne chcete skopírovať túto položku?" href="'.url(array('copy'=>$this->order->getId(),'id'=>null)).'"><i class="fa fa-copy"></i></a>';
        }
        
        $btns=' <div class="row order-form-btns">
                    <div class="col-sm-8 col-sm-offset-0"><button class="btn btn-form-submit btn-large btn-success" type="submit">Uložiť</button>
                    <span class="secondbtn"> alebo <a class="btn btn-sm btn-default btn-form-back" href="'.url(array('edit'=>null,'id'=>null,'delete'=>null,'copy'=>null,'action'=>'list')).'">zrušiť</a></span> 
                    </div> 
                    <div class="col-sm-4 text-right">'.$btnsUpdate.'</div>
                </div>';
        
        
        $this->addHidden('buttons');
        $this->buttons->setIgnore(true);
        $this->parse('buttons',$btns);
        
        

        $html=parent::__toString();


        //Zend_Debug::dump($this->customersData);exit;
        
        $html.='<script>';
        $html.='var customersData = '.json_encode($this->customersData).';';
        $html.='</script>';
        
        


        return $html;
    }

    
    protected $customersData=array();
    
    public function addCustomers(array $customers) {
        foreach($customers as $c){
            $this->customersData[$c->id]=array('color'=>$c->color,'loadtime'=>$c->loadtime);
            if($this->customer_id instanceof Zend_Form_Element_Multi)
            $this->customer_id->addMultiOption($c->id,$c->shortname." - ".$c->name);
        }
    }

    public function addRamps(array $ramps) {
        foreach($ramps as $r)
            $this->ramp->addMultiOption($r->code,$r->name); 
    }

    public function addStatuses($statuses) {
        
            foreach($statuses as $s)
                $this->status->addMultiOption($s->code,$s->name); 
        
    }

}