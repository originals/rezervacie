<?php

class Form_OrderUser extends Matj_Bootstrap3_Form {
	
        function init () {
		$this->setName(get_class($this));
		$this->setAttrib("class","form");
		
		$options = array ( 
			'prefix' => 'user.', 
			'elementClass' => true 
		);
		$this->setOptions($options);		
		
    $this->addElement('text', 'first_name', array ( 
			'required' => true 
		));
    $this->addElement('text', 'last_name', array ( 
			'required' => true 
		));
		$this->addElement('text', 'email', array ( 
			'required' => true 
		));
		$this->addElement('text', 'phone', array ( 
			'required' => true 
		));
		$this->addElement('checkbox', 'create', array ( 
			'required' => false
		));			
    $this->addElement('text', 'login', array ( 
			'required' => false 
		));
    $this->addElement('password', 'password', array ( 
      'required' => false 
		));
		$this->addHidden('group_id',11);
			
		$this->addButtons(array(
        'submit'=>array('label'=>$this->translate("continue.to.checkout"),'class'=>'btn-large btn-success'),
        'back'=>array('show'=>true),
        'delete'=>array('show'=>false)
    ));
		$this->first_name->addDecorator('parse',array('text'=>'<div class="row"><div class="col-sm-6"><h3>'.$this->translate("registration.for").'</h3>{content}'));
		$this->create->addDecorator('parse',array('text'=>'{content}</div>'));
		$this->login->addDecorator('parse',array('text'=>'<div class="col-sm-6" id="accountCreate"><h3>'.$this->translate("create.account").'</h3><p>'.$this->translate("continue.with.creating.account").'</p>{content}'));
		$this->password->addDecorator('parse',array('text'=>'{content}</div></div>'));	
			

		//$this->getElement("username")->getDecorator("label")->setTagClass('xx');
		
				
		//$this->addHashElement();
		$this->initClick();
		
	}
	
	function initClick(){
            $js='
                if(typeof initClick !="function"){ 
                    function initClick(){
                        $("#create").on("change", function() { 
														// From the other examples
														console.log(this.checked);
														if(this.checked) {
															$("#accountCreate").show();
														}else{
															$("#accountCreate").hide();
														}
												});												
                    }
                }
                $(document).ready(function(){
										var check=$("#create");
										if(!check.checked){$("#accountCreate").hide(); };
                    initClick();
                });
             ';


            head()->appendJavascript($js,'initClick');
  }
	
	function isValid ($data) {
		$valid = parent::isValid($data);
		//Zend_Debug::dump($data);
		
		if(empty($data["create"])){
			if($data["login"]){
				$this->login->setRequired('true');
				if(empty($data["password"])){
					$this->password->setRequired('true');
				}
			}
		}
		return $valid;
	}
        
  function getValues(){
		$data=parent::getValues();
		$data['name']=null;
		return $data;
	}
	
	function populate($data){
       return parent::populate($data);
   }
        
    

}