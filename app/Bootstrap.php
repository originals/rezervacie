<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
        
    
        
        
    
	protected function _initRoutes(){
		$front  = Matj_Get::getFrontController();
		//$front->registerPlugin(new Matj_Controller_Plugin_Language());
		$router = $front->getRouter();
		$config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/routes.xml');
		$router->addConfig($config->routes);
		//Zend_Debug::dump($config);
	}
        
        


	protected function _initAutoload () {
            
            
            
                $resourceLoader = new Zend_Loader_Autoloader_Resource(
                    array (
                        'basePath' => APPLICATION_PATH,
                        'namespace' => '',
                        'resourceTypes' => array (
                                'model' => array (
                                        'path' => 'models/',
                                        'namespace' => 'Model'
                                ),
                            )
                    )
                );
        
                $resourceLoader = new Zend_Loader_Autoloader_Resource(
                    array (
                        'basePath' => APPLICATION_PATH,
                        'namespace' => '',
                        'resourceTypes' => array (
                                'model' => array (
                                        'path' => 'reports/',
                                        'namespace' => 'Report'
                                ),
                            )
                    )
                );
        
            
                $resourceLoader = new Zend_Loader_Autoloader_Resource(
                    array (
                        'basePath' => APPLICATION_PATH,
                        'namespace' => '',
                        'resourceTypes' => array (
                                'model' => array (
                                        'path' => 'tables/',
                                        'namespace' => 'Table'
                                ),
                            )
                    )
                );
        
                $resourceLoader = new Zend_Loader_Autoloader_Resource(
                    array (
                        'basePath' => APPLICATION_PATH,
                        'namespace' => '',
                        'resourceTypes' => array (
                                'model' => array (
                                        'path' => 'forms/',
                                        'namespace' => 'Form'
                                ),
                            )
                    )
                );
        
                
                
            
                if(is_dir(APPLICATION_PATH."/modules/")){

                    $f= new Matj_File_Folder(APPLICATION_PATH."/modules/");
                    foreach($f->getFilesAsObjects() as $f){
                    $module=$f->getName();

                    $resourceLoader = new Zend_Application_Module_Autoloader(
                            array (
                                    'basePath' => APPLICATION_PATH . '/modules/'.$module.'/',
                                    'namespace' => ucfirst($module).'_',
                                    'resourceTypes' => array (
                                            'acl' => array (
                                                    'path' => 'acls/',
                                                    'namespace' => 'Acl'
                                            ),
                                            'form' => array (
                                                    'path' => 'forms/',
                                                    'namespace' => 'Form'
                                            ),
                                            'table' => array (
                                                    'path' => 'tables/',
                                                    'namespace' => 'Table'
                                            ),
                                            'model' => array (
                                                    'path' => 'models/',
                                                    'namespace' => 'Model'
                                            ),
                                            'pdf' => array (
                                                    'path' => 'pdf/',
                                                    'namespace' => 'Pdf'
                                            )
                                    )
                            ));
                    }
                }
                else{
                    //no modules
                    
                    
                }


	}

        /*protected function _initSession(){
            $this->bootstrap('dibi');
            
            $session = new Matj_Session_SaveHandler_Dibi(); 
            session_set_save_handler(array(&$session,"open"), 
                                     array(&$session,"close"), 
                                     array(&$session,"read"), 
                                     array(&$session,"write"), 
                                     array(&$session,"destroy"), 
                                     array(&$session,"gc")); 
            
            
        }*/
        
        
        protected function _initLanguage()
	{
			   
          $uri=$_SERVER["REQUEST_URI"];
          $lang="sk";
          $sess=new Zend_Session_Namespace('locale');
          $lg=array("en","pl","sk","hu","de");
          if(preg_match('/^\/([a-z]{2})/',$uri,$m)){
            if (in_array($m[1], $lg)):            
                $lang=$m[1];
                $sess->locale=$lang=$m[1];
            else:
                $lang="sk";
            endif;
          }else{
            if($sess->locale){
                $lang=$sess->locale;
            }
          }
          
          $locale = new Zend_Locale($lang);
          Zend_Registry::set('Zend_Locale',$locale);
          
          Zend_Registry::set('DefaultLang','sk');
		  
	}
        
        
        
        
        
        protected function _initZFDebug()
        {
            /*
                $this->bootstrap('db');
                $db=Matj_Get::getDbAdapter();
            */
            
           
            
            //if(Matj_Get::getAuthUser() && Matj_Get::getAuthUser()->getEmail()=="matus.jancik@gmail.com");
            $sess=new Zend_Session_Namespace('debug');
            if(isset($_GET["debug"]))$sess->debug=strip_tags($_GET["debug"]);
            if($sess->debug){
            
                
                
                $plugins=array('Variables', 
                                    'File',
                                    //'Cache' => array('backend' => $cache->getBackend()), 
                                    'Exception',
                                    'Memory',
                                    'Time',
                                    'Html'
                        );
                
                
                if(isset($db))
                         $plugins['Database']=array('adapter' => $db); 
                
                
                try{
                    $this->bootstrap('dibi');
                    $dibi=Zend_Registry::get('Dibi');
                    
                    $dibi->onEvent[] = array('ZFDebug_Controller_Plugin_Debug_Plugin_Dibi','dibiLogger');
                    
                    //Zend_Registry::set('Dibi',$dibi);

                    
                    $plugins['ZFDebug_Controller_Plugin_Debug_Plugin_Dibi']=array('adapter' => $dibi); 
                }
                catch(Exception $e){
                    echo $e;
                }
                
               
                
                $autoloader = Zend_Loader_Autoloader::getInstance();
                $autoloader->registerNamespace('ZFDebug');
                $options = array(
                    'plugins' => $plugins
                );
                $debug = new ZFDebug_Controller_Plugin_Debug($options);

                $this->bootstrap('frontController');
                $frontController = $this->getResource('frontController');
                $frontController->registerPlugin($debug);
            }
        }
        
        
        
        
        
}
