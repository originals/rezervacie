<?php


require_once '../library/Matj/WideImage.php';

set_include_path(get_include_path().PATH_SEPARATOR.realpath('../library/'));

//require_once '/data/a/r/arnox.eu/web/app/library/Matj/WideImage.php';

class Image {

    public $_srcPrefix = '../';
    
    
    function delete($params){
      
            
            $folder=dirname($params["src"]);
            
            require_once '../library/Matj/File/File.php';
            require_once '../library/Matj/File/Folder.php';
            require_once '../library/Matj/String.php';
            
            
            //$src=str_replace('../','',$params["src"]);
            //$folder=str_replace('../','',$folder);
            
            
            $cacheFilePrefix=Matj_String::convertToUrlFriendly($src,false);
      
            
      
      
            
      
            $folder=new Matj_File_Folder($folder);
            
            $files=$folder->findFiles($cacheFilePrefix);
            foreach($files as $f){
                //echo $f;
                $f->delete();
            }
            
            
            
            
            $folder=new Matj_File_Folder($folder.'/images/');
            
          
            
            foreach($folder->getFilesAsObjects() as $f1){
                if($f1 instanceof Matj_File_Folder)
                    foreach($f1->getFilesAsObjects() as $f2){
                        
                        $furl=$f2->getUrl().(substr($src,0,1)=="/" ? "" : "/").$src;
                        if(file_exists($furl)){
                            $file=new Matj_File_File($furl);
                            $file->delete();
                        }
                    
                    }
            }
            
            
           
            
            return true;
    }
    
    
    
    function __construct(){
  
        //echo __FILE__;
        
        
//        $this->_srcPrefix='../../../web/';
        
        
        $params=$this->getParams();
        
      //var_dump($params);exit;
        
        
        if(isset($_GET["delete"])){
          $this->delete($params);
        }
        
        
        
        
        if($params["cache"] && $params["cached"]){
            
            
            $file=file_get_contents($params["cache_url"]);
            
            
            if(isset($_GET["d"])){
                var_dump($params);exit;
            }

            
            
            if($file){
                header('Pragma: public');
                header('Cache-Control: max-age=86400');
                header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 360));
                header('Content-Type: '.$params["mime"]);
                echo $file;
            }
            return;
        }
        
        
        
        
        $methodName=$params["type"]."Action";
        $this->$methodName();
        
        
        
    }
    
    
    function _getParam($key){
        $p=$this->getParams();
        return $p[$key];
    }
    
    
    
    function resizeAction(){
        $params = $this->getParams();
        $image = Matj_WideImage::load($params["src"]);
		
        $image->resize($params["w"], $params["h"], 'inside')
                ->output($params["format"]);
		
		if($this->_getParam("grayscale")>0){
            $image=$image->asGrayscale();
        }
        
        if($this->_getParam("sepia")>0){
            $image=$image->getHandle();
            imagefilter($image, IMG_FILTER_GRAYSCALE);
            imagefilter($image, IMG_FILTER_COLORIZE, 100, 50, 0);            
        }
		
        if($params["cache"]){
            $this->_prepareCacheDir($params["cache_url"]);
            $image->saveToFile($params["cache_url"]);
        }       
        
        header('Pragma: public');
        header('Cache-Control: max-age=86400');
        header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 360));
        header('Content-Type: '.$params["mime"]);
        $image->output($params["format"]);
    }
    
    function shrinkAction(){ 
        $params = $this->getParams();
        $image = Matj_WideImage::load($params["src"]);
        
        $bgcolor=$image->allocateColor(255,255,255);
        
        if(!in_array(strtolower($params["format"]),array("jpg",'jpeg')))
            $bgcolor=null;
        
        
        $image=$image->resize($params["w"], $params["h"], 'inside')
              ->resizeCanvas($params["w"],$params["h"],"center","center",$bgcolor);
		
		if($this->_getParam("grayscale")>0){
            $image=$image->asGrayscale();
        }
        
        if($this->_getParam("sepia")>0){
            $image=$image->getHandle();
            imagefilter($image, IMG_FILTER_GRAYSCALE);
            imagefilter($image, IMG_FILTER_COLORIZE, 100, 50, 0);            
        }
        
        if($params["cache"]){
            $this->_prepareCacheDir($params["cache_url"]);
            $image->saveToFile($params["cache_url"]);
        }
        
        
        header('Pragma: public');
        header('Cache-Control: max-age=86400');
        header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 360));
        header('Content-Type: '.$params["mime"]);
        
        
        
        $image->output($params["format"]);
        
        
    }

    
    function outsideAction(){
		 
        $params = $this->getParams();
        $image = Matj_WideImage::load($params["src"]);
		
        $image->resize($params["w"], $params["h"], 'outside')
                ->output($params["format"]);
		if($this->_getParam("grayscale")>0){
            $image=$image->asGrayscale();
        }
        
        if($this->_getParam("sepia")>0){
            $image=$image->getHandle();
            imagefilter($image, IMG_FILTER_GRAYSCALE);
            imagefilter($image, IMG_FILTER_COLORIZE, 100, 50, 0);            
        }
		
        if($params["cache"]){
            $this->_prepareCacheDir($params["cache_url"]);
            $image->saveToFile($params["cache_url"]);
        }       
        
        header('Pragma: public');
        header('Cache-Control: max-age=86400');
        header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 360));
        header('Content-Type: '.$params["mime"]);
        $image->output($params["format"]);
    }

    function fillAction(){

//echo "x";exit;

        $params = $this->getParams();
        $image = Matj_WideImage::load($params["src"]);
       // $image->resize($params["w"], $params["h"], 'fill');
        
        $white=$image->allocateColor(255,255,255);
		
        $image=$image->resize($params["w"]+$add, $params["h"]+$add, 'inside')
              ->resizeCanvas($params["w"]-$add, $params["h"]-$add,'center', 'center',$white);
        
		if($this->_getParam("grayscale")>0){
            $image=$image->asGrayscale();
        }
        
        if($this->_getParam("sepia")>0){
            $image=$image->getHandle();
            imagefilter($image, IMG_FILTER_GRAYSCALE);
            imagefilter($image, IMG_FILTER_COLORIZE, 100, 50, 0);            
        }
        
        if(isset($_GET["d"])){
            var_dump($params);exit;
        }
        /*
        if($params["cache"]){
            $this->_prepareCacheDir($params["cache_url"]);
            $image->saveToFile($params["cache_url"]);
        }

       
        
        header('Pragma: public');
        header('Cache-Control: max-age=86400');
        header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));
        header('Content-Type: '.$params["mime"]);        
        
        $image->output($params["format"]);*/
		
		if($params["cache"]){
            $this->_prepareCacheDir($params["cache_url"]);
            $image->saveToFile($params["cache_url"]);
        }       
        
        header('Pragma: public');
        header('Cache-Control: max-age=86400');
        header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 360));
        header('Content-Type: '.$params["mime"]);
        $image->output($params["format"]); 
    }
	
	function cropAction() {
        
        $params = $this->getParams();
        $image = Matj_WideImage::load($params["src"]);
        
        $add=0;
        
        if($this->_getParam("border")>0){
            $add=($this->_getParam("border",10)+0)*2;
            $border=1;
            $color="ffffff";
            if(preg_match('/^[0-9A-Fa-f]{6}$/',$this->_getParam("borderColor"),$m)){
                $color=$m[0];
            }
            
            $rgb=$this->hex2rgb($color);
            $color = $image->allocateColor($rgb[0], $rgb[1], $rgb[2]);
        }        
        
        $image=$image->resize($params["w"]+$add, $params["h"]+$add, 'outside','down')
                ->crop('center', 'center', $params["w"]-$add, $params["h"]-$add);       
        
        
        if(isset($border))
            $image=$image->resizeCanvas($params["w"]+$add,$params["h"]+$add,'center','center',$color);
        
       if($this->_getParam("grayscale")>0){
            $image=$image->asGrayscale();
        }
        
        if($this->_getParam("sepia")>0){
            $image=$image->getHandle();
            imagefilter($image, IMG_FILTER_GRAYSCALE);
            imagefilter($image, IMG_FILTER_COLORIZE, 100, 50, 0);            
        }      
        
        if($params["cache"]){
            $this->_prepareCacheDir($params["cache_url"]);
            $image->saveToFile($params["cache_url"]);
        }       
        
        header('Pragma: public');
        header('Cache-Control: max-age=86400');
        header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 360));
        header('Content-Type: '.$params["mime"]);
        $image->output($params["format"]);  
        
         
    }
    
    
    

    function getParams() {
        $uri = $_SERVER["REQUEST_URI"];

        $baseurl = 'image/';
        $srcPrefix=$this->_srcPrefix;
        

        
        //$cachePrefix='../uploads/_cache/images/';



        $cachePrefix='../image/';
        
        $uri=(substr($uri,strlen($baseurl)+1));
		$pos=strpos($uri,"?");
        if($pos)
            $uri=(substr($uri,0,$pos));
        
        
        $params["cache"]=!isset($_GET["nocache"]);
        
        
        $params["cache_url"]=urldecode($cachePrefix.$uri);
		if(file_exists($params["cache_url"])) $time=filemtime($params["cache_url"]);
        
        if($time && (time()-$time < 600 ) ){
            $params["cached"]=true;
        }
        else
            $params["cached"]=false;
        
//        $params["cached"]=false;
        
        $type=(substr($uri,0,strpos($uri,'/')));
        $uri=(substr($uri,strlen($type)+1));
        $params["type"]=$type;
        
        $size=explode("x",(substr($uri,0,strpos($uri,'/'))));
        $params["w"]=$size[0];
        $params["h"]=$size[1];
        
        
        $image=(substr($uri,strpos($uri,'/')));
        $params["src"]=$srcPrefix.urldecode(preg_replace('/^\//','',$image)); 
        $x=explode(".",$params["src"]);
        $params["format"]=strtolower($x[count($x)-1]);
        
        $params["mime"]="image/".($params["format"]=="jpg" ? "jpeg" : $params["format"]);
        
        
        return $params;
       
    }

    
    function hex2rgb($hex) {
        $hex = str_replace("#", "", $hex);

        if(strlen($hex) == 3) {
           $r = hexdec(substr($hex,0,1).substr($hex,0,1));
           $g = hexdec(substr($hex,1,1).substr($hex,1,1));
           $b = hexdec(substr($hex,2,1).substr($hex,2,1));
        } else {
           $r = hexdec(substr($hex,0,2));
           $g = hexdec(substr($hex,2,2));
           $b = hexdec(substr($hex,4,2));
        }
        $rgb = array($r, $g, $b);
        //return implode(",", $rgb); // returns the rgb values separated by commas
        return $rgb; // returns an array with the rgb values
     }
    
     
     function _prepareCacheDir($file){
         $x=dirname($file);
		 $old = umask(0);
		 @mkdir($x,0777,true);
		 umask($old);
         
     }
     
     function _setParam(){}
     
     
     
}

new Image();
